$(document).ready(function(){
	var rightLength = 1115 - $(".slider__item").length * 16 - 434 - 8;
	$("#slider__border-right").css("width", rightLength);
	$("#slider").owlCarousel({
		loop: false,
		items: 1,
		nav: true,
		navText: ["", ""]
	});



	var blockMiniStone = $("#slider_stone_mini");
	var stonesImgs = $(".slider__item-stone");

	var stonePage = 0;
	var stonePageBlock;
	var stonePageBlockNum = 0;
	stonesImgs.each(function(i, item){
		if(i == 0){
			$("#slider_stone-name").html($(item).attr("alt"));
			blockMiniStone.append("<div class='slider__mini-page' id='stonePageBlockNum_"+stonePageBlockNum+"'></div>")
			stonePageBlock = $("#stonePageBlockNum_0");
		}
		stonePage++;
		if(stonePage == 17){
			stonePageBlockNum++;
			blockMiniStone.append("<div class='slider__mini-page' id='stonePageBlockNum_"+stonePageBlockNum+"'></div>")
			stonePageBlock = $("#stonePageBlockNum_"+stonePageBlockNum);
			stonePage = 0;
		}
		stonePageBlock.append("<div class='slider__mini-item' data-alt='"+ $(item).attr('alt') +"' data-num="+ i +" style='background-image: url("+ $(item).attr('src') +")'></div>");
	});
	$(".slider__mini-item").eq(0).addClass("slider__mini-item_active");

	if(stonePageBlockNum>=1){
		var sliderStonePage = $("#slider_stone_mini");
		sliderStonePage.owlCarousel({
			loop: false,
			items: 1,
			dots: true,
			margin: 20,
			nav: true,
			navText: ["", ""]
		});
	}

	var sliderStone = $("#slider_stone");
	sliderStone.owlCarousel({
		loop: false,
		items: 1,
		// dots: false,
		nav: false,
		navText: ["", ""]
	});
	$(".slider__mini-item").on("click", function(){
		$(".slider__mini-item").removeClass("slider__mini-item_active");
		$(this).addClass("slider__mini-item_active");
		$("#slider_stone-name").html($(this).data("alt"));
		sliderStone.trigger('to.owl.carousel', $(this).data("num"));
	})
	sliderStone.on('changed.owl.carousel', function(event) {
		// console.log(event.page.index);
	    $(".slider__mini-item").removeClass("slider__mini-item_active");
		$(".slider__mini-item").eq(event.page.index).addClass("slider__mini-item_active");
	})

	// if($(".stone__img").length>=7){

	// 	$("#stone__imgs").owlCarousel({
	// 		loop: true,
	// 		items: 7,
	// 		dots: false,
	// 		nav: true,
	// 		navText: ["", ""]
	// 	});
	// 	$("#stone__imgs").width(1198);
	// }
	
	$("#header__lang").click(function(){
		$("#header__lang-wrap").toggleClass("header__lang_active");
		return false;
	});
	$(window).scroll(function(e){
		var scrollTop = $(window).scrollTop();
		if(scrollTop>100){
			$("#header").addClass("header_min");
		}else{
			$("#header").removeClass("header_min");
		}
	});
	$(".team__video").hover(function(){
		$(".team__text").removeClass("team__text_show");
		$("#team__text_"+$(this).data("id")).addClass("team__text_show");
		$(this).get(0).play();
	});
	$(".header__feedback").click(function(){
		$("#popup__form").fadeIn();
		$("#popup__overlay").fadeIn();
		return false;
	});
	$("#popup__close").click(function(){
		$("#popup__form").fadeOut();
		$("#popup__overlay").fadeOut();
		return false;
	});

	var form = document.getElementById("write__content_form").getElementsByClassName("write__form")[0];
	var checkSubmit = function(e){
		e.preventDefault();
		var checkFailed = false;
		var json = {};
		var inputs = form.getElementsByTagName("input");
		var length = inputs.length;
		while(length--){
			if(inputs[length].value === ""){
				checkFailed = true;
				inputs[length].classList.add("write__input_err");
			}else{
				inputs[length].classList.remove("write__input_err");
				json[inputs[length].name] = inputs[length].value;
			}
		}
		var textarea = form.getElementsByTagName("textarea")[0];
		if(textarea.value === ""){
			checkFailed = true;
			textarea.classList.add("write__input_err");
		}else {
			textarea.classList.remove("write__input_err");
			json[textarea.name] = textarea.value;
		}
		if(!checkFailed){
			if(document.getElementById("g-recaptcha-response").value.length){
				json["captha"] = document.getElementById("g-recaptcha-response").value;
				$("#write__error-captha").hide();
				sendData(json);
			}else{
				$("#write__error-captha").show();
			}
		}
	};
	function sendData(data) {
		var XHR = new XMLHttpRequest();
		var urlEncodedData = "";
		var urlEncodedDataPairs = [];
		var name;

		for(name in data) {
			urlEncodedDataPairs.push(encodeURIComponent(name) + '=' + encodeURIComponent(data[name]));
		}

		urlEncodedData = urlEncodedDataPairs.join('&').replace(/%20/g, '+');

		XHR.onreadystatechange = function(){
			if(XHR.readyState == 4 && XHR.status == 200){
				document.getElementById("write__content_form").classList.remove("write__content_show");
				document.getElementById("write__content_done").classList.add("write__content_show");
			}
		};

		XHR.open('GET', form.action + "?"+ urlEncodedData);

		XHR.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

		XHR.send();
	}
	form.addEventListener("submit", checkSubmit, false);




	$(".team__member").hover(function(){
		$(".team__member").addClass("team__member_blur");
		$(this).find(".team__video")[0].play();
		$(this).removeClass("team__member_blur");
	}, function(){
		console.log(this)
		$(this).find(".team__video")[0].pause();
		$(this).find(".team__video")[0].currentTime = 0;
		$(".team__member").removeClass("team__member_blur");
	});


});