var gulp = require('gulp');
var connect = require('gulp-connect');
var stylus = require('gulp-stylus');
var concat = require('gulp-concat');
var uglify = require('gulp-uglifyjs');
var cssmin = require('gulp-cssmin');


gulp.task('watch', function (){
  gulp.watch(['./*.html'], ['reload']);
  gulp.watch(['./blocks/**/*.css','./blocks/**/*.styl'], ['style']);
  gulp.watch(['./blocks/**/*.js'], ['scripts']);
});

gulp.task('watchmain', function (){
  gulp.watch(['./index.html'], ['reload']);
  gulp.watch(['./blocks/common/*.css','./blocks/index.styl'], ['maincss']);
});

gulp.task('connect', function(){
  connect.server({
    port: 5001,
    livereload: true
  });
});

gulp.task('reload', function() {
  return gulp.src('./*.html')
    .pipe(connect.reload());
});

gulp.task('scripts', function() {
  return gulp.src(['./blocks/common/common.js','./blocks/**/!(common)*.js'])
    .pipe(concat('script.min.js'))
    .pipe(gulp.dest('./publish/'))
    .pipe(connect.reload());
});


gulp.task('style', function() {
  return gulp.src(['./blocks/common/reset.css','./blocks/common/!(reset)*.css','./blocks/**/!(index)*.styl'])
    .pipe(stylus({compress: true, 'include css': true}))
    .pipe(concat('style.min.css'))
    .pipe(gulp.dest('./publish/'))
    .pipe(connect.reload());
});

gulp.task('maincss', function() {
  return gulp.src([
      './blocks/common/reset.css',
      './blocks/common/btn.css',
      './blocks/write/write.styl',
      './blocks/index.styl'])
    .pipe(stylus({compress: true, 'include css': true}))
    .pipe(concat('style.main.css'))
    .pipe(gulp.dest('./publish/'))
    .pipe(connect.reload());
});


gulp.task('minjs',function(){
  return gulp.src(['./blocks/common/common.js','./blocks/**/!(common)*.js'])
    .pipe(concat('script.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./publish/'));
});
gulp.task('mincss',function(){
  return gulp.src(['./blocks/common/reset.css','./blocks/common/!(reset)*.css','./blocks/**/!(index)*.styl'])
    .pipe(stylus({compress: true, 'include css': true}))
    .pipe(concat('style.min.css'))
    .pipe(cssmin())
    .pipe(gulp.dest('./publish/'));
});

gulp.task('default', ['scripts', 'style']);
gulp.task('main', ['maincss']);


gulp.task('live', ['connect', 'watch']);
gulp.task('livemain', ['connect', 'watchmain']);

gulp.task('publish', ['minjs', 'mincss',]);