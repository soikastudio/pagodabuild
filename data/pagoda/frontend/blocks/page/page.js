// page.js


var Page = {};

if ("onwheel" in document) {
	Page.scroll = "wheel";
} else if ("onmousewheel" in document) {
	Page.scroll = "mousewheel";
} else {
	Page.scroll = "MozMousePixelScroll";
}

Page.lang = {
	init: function(){
		ge("header__lang").onclick = function(){
			ge("header__lang-wrap").classList.toggle("header__lang_active");
			return false;
		};
	}
};

Page.dot = {
	sum: 0,
	current: 0,
	init: function(length){
		this.sum = length;
		var htmlBtn = "";
		for(var i=0; i < length; i++){
			htmlBtn += '<button class="page__dot" data-num="'+i+'"></button>';
		}
		html("page__dot-wrap", htmlBtn);
		var dots = gc("page__dot");
		dots[0].classList.add("page__dot_active");
		for(var i=0; i<length; i++){
			dots[i].addEventListener("click", Page.dot.click, false);
		}
	},
	click: function(){
		gc("page__dot")[Page.dot.current].classList.remove("page__dot_active");
		Page.dot.current = parseInt(this.dataset.num);
		gc("page__dot")[Page.dot.current].classList.add("page__dot_active");
		Page[Page.type].click();
	}
};

Page.content = {
	arrDots: [],
	init: function(){
		this.height = ge("page").clientHeight;

		var titleHeight = ge("page__title").clientHeight;
		ge("page__scroll").style.height = window.innerHeight - titleHeight - 168 + 40 + "px";
		ge("page__scroll").style.top = titleHeight + 123 + "px";
		this.heightContent = ge("page__content-in").clientHeight;
		this.scrollMax = this.heightContent - this.height/2;

		if(this.heightContent>this.height/2){
			var length = (this.heightContent*2/this.height).toFixed();
			if(length > 8){
				length = 8;
			}
			Page.dot.init(length);

			for(var i=0;i<Page.dot.sum;i++){
				var step = this.heightContent*(i/Page.dot.sum);
				this.arrDots.push(step);
			}
			document.addEventListener(Page.scroll, Page.content.scroll, false);
		}

	},
	click: function(){
		var newScrollTop = this.heightContent*(Page.dot.current/Page.dot.sum);
		ge("page__content-wrap").scrollTop = newScrollTop;
	},
	scrollTop: 0,
	scrollMax: 0,
	scrollStop: false,
	scroll: function(event){

		event = event || window.event;
		var delta = event.deltaY || event.detail || event.wheelDelta;

		Page.content.scrollTop += delta;

		if(Page.content.scrollTop < 6){
			Page.content.scrollTop = 0;
			ge("page").classList.remove("page_min");
		}else{
			ge("page").classList.add("page_min");
		}
		if(Page.content.scrollTop > Page.content.scrollMax){
			Page.content.scrollTop = Page.content.scrollMax;
		}

		var first = 0;
		while(Page.content.arrDots[first]<Page.content.scrollTop+100){
			first++;
		}
		first--;
		gc("page__dot")[Page.dot.current].classList.remove("page__dot_active");
		Page.dot.current = first;
		gc("page__dot")[Page.dot.current].classList.add("page__dot_active");
		
		// ge("page__content-wrap").scrollTop = Page.content.scrollTop;
	},
};
// Page.main = {
// 	init: function(){
// 		document.addEventListener(Page.scroll, Page.main.scroll, false);
// 		var length = gc("main__project-block").length;
// 		Page.dot.init(length);
// 	},
// 	stop: false,
// 	scroll: function(event){
// 		if(Page.main.stop){
// 			return false;
// 		}
// 		event = event || window.event;
// 		var delta = event.deltaY || event.detail || event.wheelDelta;
// 		var shift = 0;
// 		if(delta<0){
// 			shift = -1;
// 		}else{
// 			shift = 1;
// 		}
// 		Page.main.change(shift);
// 	},
// 	old: 0,
// 	click: function(){
// 		Page.main.stop = true;
// 		var blocks = gc("main__project-block");
// 		blocks[Page.main.old].classList.remove("main__project-block_active");
// 		blocks[Page.main.old].classList.add("main__project-block_del");

// 		gc("page__dot")[Page.dot.current].classList.add("page__dot_active");
// 		setTimeout(function(){
// 			blocks[Page.main.old].classList.remove("main__project-block_del");
// 			blocks[Page.dot.current].classList.add("main__project-block_active");
// 			Page.main.stop = false;
// 		}, 400);

// 		Page.main.old = Page.dot.current;
// 	},
// 	change: function(shift){
// 		Page.main.stop = true;
// 		var old = Page.dot.current;
// 		var blocks = gc("main__project-block");
// 		blocks[Page.dot.current].classList.remove("main__project-block_active");
// 		blocks[Page.dot.current].classList.add("main__project-block_del");
// 		gc("page__dot")[Page.dot.current].classList.remove("page__dot_active");
// 		Page.dot.current += shift;
// 		if(Page.dot.current==Page.dot.sum){
// 			Page.dot.current = 0;
// 		}else if(Page.dot.current<0){
// 			Page.dot.current = Page.dot.sum-1;
// 		}
// 		gc("page__dot")[Page.dot.current].classList.add("page__dot_active");

// 		setTimeout(function(){
// 			blocks[old].classList.remove("main__project-block_del");
// 			blocks[Page.dot.current].classList.add("main__project-block_active");
// 			Page.main.stop = false;
// 		}, 400);
// 	}
// };
Page.catalog = {
	curType: "granite",
	granite: gc("catalog__block_granite"),
	marble: gc("catalog__block_marble"),
	all: gc("catalog__block_all"),
	init: function(){
		var _this = Page.catalog;
		document.addEventListener(Page.scroll, Page.catalog.scroll, false);
		if(_this.granite.length && !_this.marble.length){
			ge("catalog__type_marble").style.display = "none";
			ge("catalog__aside").classList.add("page__aside-border_solo");
		}
		_this[_this.curType][0].classList.add("catalog__block_active");
		ge("page__background").classList.add("page__background_gray");
		ge("page__background").style.backgroundImage = "url(" +_this[_this.curType][_this.current].dataset.img + ")";

		ge("catalog__type_marble").addEventListener("click", function(){
			ge("catalog__type_granite").classList.remove("catalog__aside-title_active");
			ge("catalog__type_marble").classList.add("catalog__aside-title_active");
			hardHide("catalog__aside_granite");
			hardShow("catalog__aside_marble");
			Page.catalog.selectType("marble");
		});
		ge("catalog__type_granite").addEventListener("click", function(){
			ge("catalog__type_marble").classList.remove("catalog__aside-title_active");
			ge("catalog__type_granite").classList.add("catalog__aside-title_active");
			hardShow("catalog__aside_granite");
			hardHide("catalog__aside_marble");
			Page.catalog.selectType("granite");
		});

		function setAsideLink(elem, type, id){
			var regex = /<br\s*[\/]?>/gi;
			var title = elem.getElementsByClassName("catalog__title")[0].innerHTML.replace(regex, " ");
			var el = document.createElement('div');
			el.classList.add("catalog__aside-item");
			el.dataset.num = id;
			htmlEl(el, title);
			el.addEventListener("click", Page.catalog.click, false);
			ge("catalog__aside_"+type).appendChild(el);
		}
		for(var i=0;i<_this.granite.length;i++){
			setAsideLink(_this.granite[i], "granite", i);
		}
		ge("catalog__aside_granite").getElementsByClassName("catalog__aside-item")[0].classList.add("catalog__aside-item_active");
		for(var i=0;i<_this.marble.length;i++){
			setAsideLink(_this.marble[i], "marble", i);
		}
	},
	sum: 0,
	current: 0,
	stop: false,
	click: function(){
		var shift = parseInt(this.dataset.num);
		Page.catalog.change(shift, true);
	},
	selectType: function(type){
		Page.catalog.change(0, true, type);
	},
	scroll: function(event){
		if(Page.catalog.stop){
			return false;
		}
		event = event || window.event;
		var delta = event.deltaY || event.detail || event.wheelDelta;
		var shift = 0;
		if(delta<0){
			shift = -1;
		}else{
			shift = 1;
		}
		Page.catalog.change(shift, false);
	},
	change: function(shift, hand, newType){
		var blocks = this[this.curType];
		
		this.stop = true;
		var old = blocks[this.current];
		ge("catalog__aside_"+this.curType).getElementsByClassName("catalog__aside-item")[this.current].classList.remove("catalog__aside-item_active");
		this[this.curType][this.current].classList.remove("catalog__block_active");
		this[this.curType][this.current].classList.add("catalog__block_del");
		
		if(hand){
			this.current = shift;
		}else{
			this.current += shift;
			if(this.current==this[this.curType].length){
				this.current = 0;
			}else if(this.current<0){
				this.current = this[this.curType].length-1;
			}
		}

		if(newType != undefined && !!newType){
			blocks = this[newType];
			this.current = 0;
			this.curType = newType;
		}

		ge("page__background").style.backgroundImage = "url(" +blocks[this.current].dataset.img + ")";
		ge("catalog__aside_"+this.curType).getElementsByClassName("catalog__aside-item")[this.current].classList.add("catalog__aside-item_active");

		setTimeout(function(){
			old.classList.remove("catalog__block_del");
			blocks[Page.catalog.current].classList.add("catalog__block_active");
			Page.catalog.stop = false;
		}, 400);
	}
};

Page.write = {
	open: false,
	presEsc: function(e){
		e = e || window.event;
		var isEscape = false;
		if("key" in e) {
			isEscape = e.key == "Escape";
		}else{
			isEscape = e.keyCode == 27;
		}
		if(isEscape) {
			Page.write.hide();
		}
	},
	show: function(){
		Page.write.open = true;
		ge("page").classList.add("page_hide");
		ge("page__write").classList.add("page__write_show");
		document.addEventListener("keydown", Page.write.presEsc, false);
	},
	hide: function(){
		Page.write.open = false;
		ge("page").classList.remove("page_hide");
		ge("page__write").classList.remove("page__write_show");
		document.removeEventListener("keydown", Page.write.presEsc, false);
	}
};


Page.catalogIn = {
	arrDots: [],
	init: function(){
		Page.catalogIn.height = window.innerHeight;

		var titleHeight = ge("page__title").clientHeight;
		var pageScrollHeight = window.innerHeight - titleHeight - 168 + 40;
		Page.catalogIn.pageScrollHeight = pageScrollHeight;

		ge("page__scroll").style.height = pageScrollHeight + "px";
		ge("page__scroll").style.top = titleHeight + 168 + "px";

		if(!!ge("catalog__fullimg")){
			ge("catalog__fullimg").style.width = ge("page__scroll").clientWidth - 15 + "px";
			ge("catalog__fullimg").style.height = (pageScrollHeight - 160) + "px";
		}
		ge("catalog__scroll").onclick = function(e){
			var target = e.target;
			while (target != this) {
				if (target.tagName == 'DIV') {
					Page.catalogIn.click(target);
					return;
				}
				target = target.parentNode;
			}
		}

		
		this.heightContent = ge("page__content-in").clientHeight +165;
		ge("page__content-hidden-in").style.height = this.heightContent + 200 + "px";

		this.scrollMax = this.heightContent - pageScrollHeight/2;

		if(this.heightContent>pageScrollHeight){
			
			var titles = ge("page__content-wrap").getElementsByTagName("h2");
			for(var i=0;i<titles.length;i++){
				var element = document.createElement("div");
				htmlEl(element, titles[i].innerHTML);
				element.dataset.top = titles[i].offsetTop;
				element.dataset.id = i;
				element.classList.add("catalog__scroll-item");
				ge("catalog__scroll").appendChild(element);
				this.arrDots.push(titles[i].offsetTop);
			}
			ge("page__content-wrap").addEventListener(Page.scroll, Page.catalogIn.scroll, false);
		}

	},
	click: function(el){
		gc("catalog__scroll-item")[Page.catalogIn.current].classList.remove("catalog__scroll-item_active");
		Page.catalogIn.current = el.dataset.id;
		gc("catalog__scroll-item")[Page.catalogIn.current].classList.add("catalog__scroll-item_active");
		Page.catalogIn.scrollTop = el.dataset.top-5;

		ge("page__content-wrap").scrollTop = Page.catalogIn.scrollTop;
	},
	current: 0,
	scrollTop: 0,
	scrollMax: 0,
	scrollFlag: false,
	scroll: function(event){

		Page.catalogIn.scrollTop = ge("page__content-wrap").scrollTop;
		
		if(Page.catalogIn.scrollTop < 1){
			ge("page").classList.remove("page_min");
		}else{
			ge("page").classList.add("page_min");
		}
		var first = 1;
		while(Page.catalogIn.arrDots[first]<Page.catalogIn.scrollTop+100){
			first++;
		}
		first--;
		gc("catalog__scroll-item")[Page.catalogIn.current].classList.remove("catalog__scroll-item_active");
		Page.catalogIn.current = first;
		gc("catalog__scroll-item")[Page.catalogIn.current].classList.add("catalog__scroll-item_active");
		if(Page.catalogIn.scrollTop > (Page.catalogIn.pageScrollHeight - 160)){
			ge("catalog__scroll").classList.add("catalog__scroll_show");
		}else{
			ge("catalog__scroll").classList.remove("catalog__scroll_show");
		}
	},
};

window.onload = (function(){
  return function (){
	// console.log("load page");

	ge("page__write-btn").addEventListener("click", Page.write.show, false);
	for(var i=0;i<2;i++){
		gc("page__write-back")[i].addEventListener("click", Page.write.hide, false);
	}

	if(!!ge("calc")){
		Calc.init();
	}

	Page.lang.init();
	
	if(!!ge("page__main")){
		Page.type = "main";
		Page.main.init();
	}else if(!!ge("page__catalog")){
		Page.type = "catalog";
		Page.catalog.init();
	}else if(!!ge("catalog__scroll")){
		Page.type = "catalogIn";
		Page.catalogIn.init();
	}else if(!!ge("page__scroll")){
		Page.type = "content";
		Page.content.init();
	}else{
		console.error('Не хватает "id=page__main" или "id=page__scroll"');
	}
  }
})();