// gallery.js

// (function(){

	var Gallery = {
		elem: ge("gallery"),
		items: gc("slider__item"),
		images: [],
		alt: [],
		current: 0,
		inited: false,

		init: function(){
			for(var i=0;i<this.items.length;i++){
				this.items[i].dataset.id = i;
				this.images.push(this.items[i].dataset.img);
				this.alt.push(this.items[i].dataset.alt);
				this.items[i].addEventListener("click", this.open, false);
			}

			this.elem.onclick = function(e){
				var target = e.target;
				if(target.hasAttribute('data-action')){
					Gallery[target.dataset.action]();
				}	
			};
		},
		pressKey: function(e){
			console.log(e.which);
			if(e.which==27){
				Gallery.close();
			}
			if(e.which==39){
				Gallery.next();
			}
			if(e.which==37){
				Gallery.prev();
			}
		},
		open: function(e){
			document.addEventListener('keydown', Gallery.pressKey, false);
			var id = this.dataset.id;
			Gallery.current = id;
			Gallery.elem.style.display = "block";
			Gallery.elem.clientHeight;
			Gallery.elem.classList.add("gallery__black_active");
			if(!Gallery.inited){
				Gallery.createElem();
				Gallery.inited = true;
			}
			for(var i=0;i<Gallery.images.length;i++){
				ge("gallery__images").appendChild(Gallery.images[i]);
			}
			Gallery.images[Gallery.current].classList.add("gallery__item_active");
			Gallery.images[Gallery.current].style.transform = "translateX(-50%) translateY(-50%)";
			Gallery.showAlt();

		},
		createElem: function(){
			for(var i=0;i<this.images.length;i++){
				var src = this.images[i];
				this.images[i] = new Image();
				this.images[i].src = src;
				this.images[i].dataset.id = i;
				this.images[i].classList.add("gallery__item");
				// img.addEventListener("load", Gallery.checkLoad, true);
			}
		},
		// checkLoad: function(){
			
		// },
		close: function(){
			document.removeEventListener('keydown', Gallery.pressKey, false);
			Gallery.elem.classList.remove("gallery__black_active");
			Gallery.images[Gallery.current].classList.remove("gallery__item_active");
			setTimeout(function(){
				Gallery.elem.style.display = "none";
				ge("gallery__images").innerHTML = "";
				ge("gallery__alt").innerHTML = "";
				ge("gallery__alt-wrap").classList.add("gallery__alt_hide");
			},500);
		},
		next: function(){
			this.oldCurrent = this.current;
			this.current++;
			if(this.current > this.images.length-1){
				this.current = 0;
			}
			this.changeCurrent(false);
		},
		prev: function(){
			this.oldCurrent = this.current;
			this.current--;
			if(this.current < 0){
				this.current = this.images.length-1;
			}
			this.changeCurrent(true);
		},
		changeCurrent: function(prev){
			this.showAlt();

			this.images[this.oldCurrent].style.transition = "opacity .5s, transform 1s";
			this.images[this.oldCurrent].classList.remove("gallery__item_active");

			this.images[this.current].style.transition = "opacity 0s, transform 0s";
			
			if(prev){
				this.images[this.current].style.transform = "translateX(-100%) translateY(-50%)";
				this.images[this.oldCurrent].style.transform = "translateX(0%) translateY(-50%)";
			}else{
				this.images[this.current].style.transform = "translateX(0%) translateY(-50%)";
				this.images[this.oldCurrent].style.transform = "translateX(-100%) translateY(-50%)";
			}
			
			this.images[this.current].clientHeight;
			this.images[this.current].style.transition = "opacity .5s, transform 1s";
			this.images[this.current].classList.add("gallery__item_active");
			this.images[this.current].style.transform = "translateX(-50%) translateY(-50%)";

			setTimeout(function(){
				Gallery.images[Gallery.oldCurrent].style.transition = "opacity 0s, transform 0s";
				Gallery.images[Gallery.current].style.transition = "opacity 0s, transform 0s";
			},500);
		},
		showAlt: function(){
			ge("gallery__alt-wrap").classList.add("gallery__alt_hide");
			if(Gallery.alt[Gallery.current].length){
				setTimeout(function(){
					ge("gallery__alt").innerHTML = Gallery.alt[Gallery.current];
					ge("gallery__alt-wrap").classList.remove("gallery__alt_hide");
				},400);
			}
		},
	};
	if(!!ge("gallery")){
		Gallery.init();
	}

// })();