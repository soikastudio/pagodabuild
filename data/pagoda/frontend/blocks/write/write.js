// write.js

(function(){
	var textEl = ge("write__text");
	var text_hidEl = ge("write__text_hid");

	var change = function(){
		var tmpText = this.value.replace(/[<>]/g, '_').split("\n");
		var string = "";
		for(var i=0;i<tmpText.length;i++){
			string = string + "<div>" + tmpText[i].replace(/\s\s/g, " &nbsp;") + "&nbsp;</div>" + "\n";
		}
		htmlEl(text_hidEl, string);
		textEl.style.height = text_hidEl.clientHeight + 20 + "px";
	};
	textEl.addEventListener("keyup", change, false);


	var form = ge("page__write").getElementsByClassName("write__form")[0];
	var checkSubmit = function(e){
		e.preventDefault();
		// console.log(this);
		var checkFailed = false;
		var json = {};
		// var stringSend = "";
		var inputs = form.getElementsByTagName("input");
		var length = inputs.length;
		while(length--){
			if(inputs[length].value === ""){
				checkFailed = true;
				inputs[length].classList.add("write__input_err");
			}else{
				inputs[length].classList.remove("write__input_err");
				json[inputs[length].name] = inputs[length].value;
				// stringSend += encodeURIComponent(inputs[length].name) + "=" + encodeURIComponent(inputs[length].value) + "&";
			}
		}
		var textarea = form.getElementsByTagName("textarea")[0];
		if(textarea.value === ""){
			checkFailed = true;
			textarea.classList.add("write__input_err");
		}else {
			textarea.classList.remove("write__input_err");
			json[textarea.name] = textarea.value;
			// stringSend += encodeURIComponent(textarea.name) + "=" + encodeURIComponent(textarea.value);
		}
		if(ge("g-recaptcha-response").value.length){
			json["captha"] = ge("g-recaptcha-response").value;
		}
		if(!checkFailed){
		
			if(ge("g-recaptcha-response").value.length){
				json["captha"] = ge("g-recaptcha-response").value;
				hardHide("write__error-captha");
				sendData(json);
			}else{
				hardShow("write__error-captha");
			}
		}
	};

	function sendData(data) {
		var XHR = new XMLHttpRequest();
		var urlEncodedData = "";
		var urlEncodedDataPairs = [];
		var name;

		for(name in data) {
			urlEncodedDataPairs.push(encodeURIComponent(name) + '=' + encodeURIComponent(data[name]));
		}

		urlEncodedData = urlEncodedDataPairs.join('&').replace(/%20/g, '+');

		XHR.onreadystatechange = function(){
			if(XHR.readyState == 4 && XHR.status == 200){
				ge("write__content_form").classList.remove("write__content_show");
				ge("write__content_done").classList.add("write__content_show");
			}
		};

		XHR.open('GET', form.action + "?"+ urlEncodedData);

		XHR.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

		XHR.send();
	}

	form.addEventListener("submit", checkSubmit, false);
})();
