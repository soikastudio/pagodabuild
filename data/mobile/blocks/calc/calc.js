// calc.js

// (function(){})();
var Calc = {
	data: false,
	selected: {
		port: false,
		city: false,
		material: false,
		processing: false
	},
	submit: false,
	init: function(){
		if(!!ge("calc")){
			Calc.sendXHR();
			Calc.click();
			Calc.checkType({"port": true, "city": false, "material": false, "processing": false});
		}
	},
	click: function(){
		ge("calc").onclick = function(e){
			var target = e.target;
			if(target.hasAttribute('data-block')){
				var block = target.dataset.block;
				switch(block){
					case "main":
						ge("calc").style.transform = "translateX(0px)";
						break;
					case "select":
						var type = target.dataset.type;
						if(Calc.selected[type]){
							htmlEl(ge("calc__list-name"), target.dataset.default);
							Calc.goSelect(type);
							ge("calc").style.transform = "translateX(-320px)";
						}
						break;
					case "final":
						if(Calc.submit){
							ge("calc").style.transform = "translateX(-640px)";
							Calc.final();
						}
						break;
					default:
						break;
				}
			}
			if(target.hasAttribute('data-type') && target.dataset.block == "main"){
				Calc.choice(target.dataset.type, target.dataset.id);
			}
		}
	},
	final: function(){
		var finalObj = Calc.data.prices.filter(function(obj){
			for(var type in Calc.selected){
				if(Calc.selected[type] != obj[type]){
					return false;
				}
			}
			return true;
		});
		for(var item in Calc.selected){
			htmlEl(ge("calc__final-" + item), Calc.data[item][Calc.selected[item]]);
		}
		htmlEl(ge("calc__final-wagon"), finalObj[0]["wagon"] + " USD");
		htmlEl(ge("calc__final-container"), finalObj[0]["container"] + " USD");
	},
	checkType: function(obj){
		this.submit = false;
		ge("calc__submit").classList.add("calc__submit_dis");
		for(var type in obj){
			Calc.selected[type] = obj[type];
			htmlEl(ge("calc__type_"+type), ge("calc__type_"+type).dataset.default);
			if(obj[type]){
				ge("calc__type_"+type).classList.remove("calc__btn_dis");
			}else{
				ge("calc__type_"+type).classList.add("calc__btn_dis");
			}
		}
	},
	choice: function(type, id){
		Calc.selected[type] = id;
		htmlEl(ge("calc__type_"+type), Calc.data[type][id]);
		switch(type){
			case "processing":
				ge("calc__submit").classList.remove("calc__submit_dis");
				Calc.submit = true;
				break;
			case "port":
				Calc.checkType({"city": true, "material": false, "processing": false});
				break;
			case "city":
				Calc.checkType({"material": true, "processing": false});
				break;
			case "material":
				Calc.checkType({"processing": true});
				break;
		}
	},
	goSelect: function(type){
		var list = ge("calc__list");
		htmlEl(list, "");

		var items = Calc.getSelectItems(type);
		if(items.length){
			for (var i=0;i<items.length;i++){
				var li = Calc.createLi(type, items[i]);
				list.appendChild(li);
			}
		}else{
			console.log("false")
		}

	},
	getSelectItems: function(type){
		var items = [];
		switch(type) {
			case "port":
				for(var item in Calc.data.prices){
					var obj = Calc.data.prices[item];
					if(items.indexOf(obj.port) == -1){
						items.push(obj.port);
					}
				}
				break;
			case "city":
				for(var item in Calc.data.prices){
					var obj = Calc.data.prices[item];
					if(items.indexOf(obj.city) == -1){
						if(obj.port == Calc.selected.port){
							items.push(obj.city);
						}
					}
				}
				break;
			case "material":
				for(var item in Calc.data.prices){
					var obj = Calc.data.prices[item];
					if(items.indexOf(obj.material) == -1){
						if(obj.port == Calc.selected.port){
							if(obj.city == Calc.selected.city){
								items.push(obj.material);
							}
						}
					}
				}
				break;
			case "processing":
				for(var item in Calc.data.prices){
					var obj = Calc.data.prices[item];
					if(items.indexOf(obj.processing) == -1){
						if(obj.port == Calc.selected.port){
							if(obj.city == Calc.selected.city){
								if(obj.material == Calc.selected.material){
									items.push(obj.processing);
								}
							}
						}
					}
				}
				break;
			default:
				break;
		}
		return items;
	},
	createLi: function(type, id){
		var el = document.createElement('li');
		el.dataset.id = id;
		el.dataset.type = type;
		el.dataset.block = "main";
		el.classList.add("calc__list-item");
		htmlEl(el, Calc.data[type][id]);
		return el;
	},
	loaded: function(){
		// console.log(Calc.data)
	},
	sendXHR: function(){
		var xhr = new XMLHttpRequest();
		xhr.open('GET', '/calc_data/calc.php', true);
		xhr.send();
		xhr.onreadystatechange = function(){
			if(xhr.readyState != 4) return;
			if(xhr.status != 200){
				console.log(xhr.status + ': ' + xhr.statusText);
			}else{
				Calc.data = JSON.parse(xhr.responseText);
				Calc.loaded();
			}
		}
	}
};

document.addEventListener("DOMContentLoaded", Calc.init);