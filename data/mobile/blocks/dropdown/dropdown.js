// dropdown.js

(function(){
	var drops = gc("dropdown");
	if(drops.length){
		for(var i=0;i<drops.length;i++){
			new Drop(drops[i]);
		}
	}

	function Drop(el){
		this.el = el;
		this.open = false;
		this.btn = this.el.getElementsByClassName("dropdown__btn")[0];
		this.li = this.el.getElementsByClassName("dropdown__list")[0].getElementsByTagName("li");
		
		var self = this;
		var clickLi = function(e){
			htmlEl(self.btn, this.innerHTML);
		};
		if(!this.el.classList.contains("dropdown_noselect")){
			for(var i=0;i<this.li.length;i++){
				this.li[i].addEventListener("click", clickLi, false);
			}
		};
		var toggle = function(){
			if(self.open){
				self.el.classList.remove("dropdown_open");
			}else{
				self.el.classList.add("dropdown_open");
			}
			self.open=!self.open;
		}
		this.el.addEventListener("click", toggle, false);
	}
})();