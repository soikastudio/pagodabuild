// var lol = Swipe(elem, {
  // startSlide: 4,
  // auto: 3000,
  // continuous: true,
  // disableScroll: true,
  // stopPropagation: true,
  // callback: function(index, element) {},
  // transitionEnd: function(index, element) {}


(function(){
	var sliders = gc("slider");
	var swipeArr = [];

	var generateDots = function(dots, length){
		var dot = "";
		for(var i=0; i<length; i++){
			dot += '<div class="slider__dot"></div>';
		}
		dot += '<div class="slider__dot-line"></div>'
		htmlEl(dots, dot);
		var dotEl = dots.getElementsByClassName("slider__dot");
		var line = dots.getElementsByClassName("slider__dot-line")[0];
		line.style.width = 260-(17*length) + "px";
		return dotEl;
	}
	var calbackSwipe = function(index, elem){
		var swipeId = elem.parentNode.parentNode.dataset.swipeid;
		var swipe = swipeArr[swipeId];
		var lengthTmp = swipe.length;
		var wrap = elem.parentNode.parentNode;
		if(wrap.dataset.type === "stone"){
			wrap.getElementsByClassName("main__slider-cur")[0].innerHTML = index+1;
		}
		while (lengthTmp--){
			swipe.dot[lengthTmp].classList.remove("slider__dot_active");
		}
		swipe.dot[elem.dataset.index].classList.add("slider__dot_active");

	};

	if(sliders.length){
		for(var i=0; i<sliders.length; i++){
			sliders[i].dataset.swipeid = i;
			console.log(sliders[i]);
			var lengthItems = sliders[i].getElementsByClassName("slider__item").length;
			var dots = sliders[i].getElementsByClassName("slider__dots")[0];
			var swipe = Swipe(sliders[i], {
				callback: calbackSwipe
			});
			if(sliders[i].dataset.type==="stone"){
				sliders[i].getElementsByClassName("main__slider-sum")[0].innerHTML = lengthItems;
			}
			swipe["dot"] = generateDots(dots, lengthItems);
			swipe["length"] = swipe.dot.length;
			swipe.dot[0].classList.add("slider__dot_active");
			swipeArr.push(swipe);
		}
	}
})();