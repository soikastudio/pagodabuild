// write.js

(function(){
	var textEl = ge("write__text");
	var text_hidEl = ge("write__text_hid");

	var change = function(){
		var tmpText = this.value.replace(/[<>]/g, '_').split("\n");
		var string = "";
		for(var i=0;i<tmpText.length;i++){
			string = string + "<div>" + tmpText[i].replace(/\s\s/g, " &nbsp;") + "&nbsp;</div>" + "\n";
		}
		htmlEl(text_hidEl, string);
		textEl.style.height = text_hidEl.clientHeight + 20 + "px";
	};
	textEl.addEventListener("keyup", change, false);


	var form = ge("popup__form").getElementsByClassName("write__form")[0];
	var checkSubmit = function(e){
		e.preventDefault();
		// console.log(this);
		var checkFailed = false;
		var json = {};
		var inputs = form.getElementsByTagName("input");
		var length = inputs.length;
		while(length--){
			if(inputs[length].value === ""){
				checkFailed = true;
				inputs[length].classList.add("write__input_err");
			}else{
				inputs[length].classList.remove("write__input_err");
				json[inputs[length].name] = inputs[length].value;
			}
		}
		var textarea = form.getElementsByTagName("textarea")[0];
		if(textarea.value === ""){
			checkFailed = true;
			textarea.classList.add("write__input_err");
		}else {
			textarea.classList.remove("write__input_err");
			json[textarea.name] = textarea.value;
		}
		if(!checkFailed){

			var xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function(){
				if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
					ge("write__content_form").classList.remove("write__content_show");
					ge("write__content_done").classList.add("write__content_show");
				}
			};
			xmlhttp.open(form.method, form.action, true);
			xmlhttp.setRequestHeader('Content-Type', 'application/json');
			xmlhttp.send(json);
			// xmlhttp.send(JSON.stringify(json));
		}
	};
	form.addEventListener("submit", checkSubmit, false);
})();