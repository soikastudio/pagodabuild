// popup.js

(function(){
	var Popup = {
		current: false,
		open: function(){
			var popupName = this.dataset.popup;
			Popup.current = popupName;
			ge("page__body").classList.add("page_popup");
			ge("popup__"+popupName).classList.add("popup_open");
		},
		close: function(){
			ge("popup__"+Popup.current).classList.remove("popup_open");
			ge("page__body").classList.remove("page_popup");
			Popup.current = false;
		}
	}
	function popupInit(){
		var links = gc("popup__link");
		var length = links.length;
		var clickEventType = ((document.ontouchstart!==null)?'click':'touchstart');
		
		while(length--){
			links[length].addEventListener(clickEventType, Popup.open, false);
			gc("popup__close")[length].addEventListener(clickEventType, Popup.close, false);
		}
	}

	document.addEventListener("DOMContentLoaded", popupInit, false);
})();