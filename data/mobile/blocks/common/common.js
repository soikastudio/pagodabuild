"use strict";

var ge = function (elName) {return document.getElementById(elName)},
	gc = function (elName) {return document.getElementsByClassName(elName)},
	hardHide = function (elName) {ge(elName).style.display = "none"},
	hardShow = function (elName) {ge(elName).style.display = "block"},
	html = function(elName,val) {ge(elName).innerHTML = val},
	htmlEl = function(el,val) {el.innerHTML = val};