(function(){
	function langShow(){
		ge("popup__menu").classList.toggle("page__menu-lang-list_active");
	}
	function langInit(){
		ge("page__menu-lang").addEventListener("click", langShow, false);
	}
	document.addEventListener("DOMContentLoaded", langInit, false);
})();