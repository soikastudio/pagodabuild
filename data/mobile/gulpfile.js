var gulp = require('gulp');
var connect = require('gulp-connect');
var stylus = require('gulp-stylus');
var concat = require('gulp-concat');
var uglify = require('gulp-uglifyjs');
var cssmin = require('gulp-cssmin');
var wrap = require('gulp-wrap');
var prettify = require('gulp-html-prettify');

gulp.task('watch', function (){
	gulp.watch(['./template/main.html', "./template/**/*.html"], ['layout','reload']);
	gulp.watch(['./blocks/**/*.css','./blocks/**/*.styl'], ['style']);
	gulp.watch(['./blocks/**/*.js'], ['scripts']);
});

gulp.task('connect', function(){
	connect.server({
		root: "./static",
		port: 5001,
		livereload: true
	});
});


gulp.task('prettify', function() {
	gulp.src('./static/*.html')
		.pipe(prettify({indent_char: '	', indent_size: 1}))
		.pipe(gulp.dest('./static'))
});

gulp.task('layout', function () {
	return gulp.src(['./template/pages/*.html'])
		.pipe(wrap({src: './template/main.html'}))
		.pipe(gulp.dest('./static'));
});

gulp.task('reload', function() {
	return gulp.src('./static/*.html')
		.pipe(connect.reload());
});

gulp.task('scripts', function() {
	return gulp.src(['./blocks/common/common.js','./blocks/**/!(common)*.js'])
		.pipe(concat('script.min.js'))
		.pipe(gulp.dest('./static/'))
		.pipe(connect.reload());
});

gulp.task('style', function() {
	return gulp.src(['./blocks/common/reset.css','./blocks/common/!(reset)*.css','./blocks/**/*.styl'])
		.pipe(stylus({compress: true, 'include css': true}))
		.pipe(concat('style.min.css'))
		.pipe(gulp.dest('./static/'))
		.pipe(connect.reload());
});


gulp.task('minjs',function(){
	return gulp.src(['./blocks/common/common.js','./blocks/**/!(common)*.js'])
		.pipe(concat('script.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('./static/'));
});
gulp.task('mincss',function(){
	return gulp.src(['./blocks/common/reset.css','./blocks/common/!(reset)*.css','./blocks/**/*.styl'])
		.pipe(stylus({compress: true, 'include css': true}))
		.pipe(concat('style.min.css'))
		.pipe(cssmin())
		.pipe(gulp.dest('./static/'));
});

gulp.task('default', ['scripts', 'style']);
gulp.task('live', ['connect', 'watch']);
gulp.task('publish', ['minjs', 'mincss', 'layout', 'prettify']);