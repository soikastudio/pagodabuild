/**
 * History/Remote - jQuery plugin for enabling history support and bookmarking
 * @requires jQuery v1.0.3
 *
 * http://stilbuero.de/jquery/history/
 *
 * Copyright (c) 2006 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 * Version: 0.2.3
 */
(function($) {
    $.ajaxHistory = new function() {
        var c = 'historyReset';
        var k = location.hash;
        var d = null;
        var g;
        this.update = function() {};
        var h = function() {
            $('.remote-output').empty()
        };
        $(document).bind(c, h);
        if ($.browser.msie) {
            var e, initialized = false;
            $(function() {
                e = $('<iframe style="display: none;"></iframe>').appendTo(document.body).get(0);
                var a = e.contentWindow.document;
                a.open();
                a.close();
                if (k && k != '#') {
                    a.location.hash = k.replace('#', '')
                }
            });
            this.update = function(a) {
                k = a;
                var b = e.contentWindow.document;
                b.open();
                b.close();
                b.location.hash = a.replace('#', '')
            };
            g = function() {
                var a = e.contentWindow.document;
                var b = a.location.hash;
                if (b != k) {
                    k = b;
                    if (b && b != '#') {
                        $('a[href="' + b + '"]').click();
                        location.hash = b
                    } else if (initialized) {
                        location.hash = '';
                        $(document).trigger(c)
                    }
                }
                initialized = true
            }
        } else if ($.browser.mozilla || $.browser.opera) {
            this.update = function(a) {
                k = a
            };
            g = function() {
                if (location.hash) {
                    if (k != location.hash) {
                        k = location.hash;
                        $('a[href="' + k + '"]').click()
                    }
                } else if (k) {
                    k = '';
                    $(document).trigger(c)
                }
            }
        } else if ($.browser.safari) {
            var f, _forwardStack, _addHistory;
            $(function() {
                f = [];
                f.length = history.length;
                _forwardStack = []
            });
            var j = false,
                initialized = false;
            _addHistory = function(a) {
                f.push(a);
                _forwardStack.length = 0;
                j = false
            };
            this.update = function(a) {
                k = a;
                _addHistory(k)
            };
            g = function() {
                var b = history.length - f.length;
                if (b) {
                    j = false;
                    if (b < 0) {
                        for (var i = 0; i < Math.abs(b); i++) _forwardStack.unshift(f.pop())
                    } else {
                        for (var i = 0; i < b; i++) f.push(_forwardStack.shift())
                    }
                    var a = f[f.length - 1];
                    $('a[href="' + a + '"]').click();
                    k = location.hash
                } else if (f[f.length - 1] == undefined && !j) {
                    if (document.URL.indexOf('#') >= 0) {
                        $('a[href="' + '#' + document.URL.split('#')[1] + '"]').click()
                    } else if (initialized) {
                        $(document).trigger(c)
                    }
                    j = true
                }
                initialized = true
            }
        }
        this.initialize = function(a) {
            if (typeof a == 'function') {
                $(document).unbind(c, h).bind(c, a)
            }
            if (location.hash && typeof _addHistory == 'undefined') {
                $('a[href="' + location.hash + '"]').trigger('click')
            }
            if (g && d == null) {
                d = setInterval(g, 200)
            }
        }
    };
    $.fn.remote = function(g, f, c) {
        c = c || function() {};
        if (typeof f == 'function') {
            c = f
        }
        f = $.extend({
            hashPrefix: 'remote-'
        }, f || {});
        var d = $(g).size() && $(g) || $('<div></div>').appendTo('body');
        d.addClass('remote-output');
        return this.each(function(i) {
            var a = this.href;
            var b = '#' + (this.title && this.title.replace(/\s/g, '_') || f.hashPrefix + (i + 1));
            this.href = b;
            $(this).click(function(e) {
                if (!d['locked']) {
                    if (e.clientX) {
                        $.ajaxHistory.update(b)
                    }
                    d.load(a, function() {
                        d['locked'] = null;
                        c()
                    })
                }
            })
        })
    };
    $.fn.history = function(a) {
        return this.click(function(e) {
            if (e.clientX) {
                $.ajaxHistory.update(this.hash)
            }
            typeof a == 'function' && a()
        })
    }
})(jQuery);