<?

checkOldLink();


if (isset($_GET["categoryID"]) and count($_GET) == 1) {
	$categoryID = $_GET["categoryID"];
	$linkcat = m_CatRedir((int)$categoryID);
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: ".$lang.$linkcat);
		exit();
}

if (!isset($_GET["prod_alias"]) and isset($_GET["productID"]) and count($_GET) == 1 and count($_POST) == 0) {
	$productID = (int)$_GET["productID"];
	$q = db_query("select alias FROM ".PRODUCTS_TABLE." WHERE productID=".(int)$productID);
	if ($r = db_fetch_row($q)) {
		$_GET["prod_alias"] = $r[0];
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: ".$lang."/".$r[0].".html");
		exit();
	}	
}

if (!isset($_GET['news_alias']) and isset($_GET['fullnews'])) {
	$q = db_query("select alias FROM ".NEWS_TABLE." WHERE NID='".(int)$_GET['fullnews']."'");
	if ($r = db_fetch_row($q)) {
	
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: ".$lang."/blog/".$r[0].".html");
		exit();
	} 
}

if (!isset($_GET['project_alias']) and isset($_GET['fullproject'])) {
	$q = db_query("select alias FROM pgd_project WHERE NID='".(int)$_GET['fullproject']."'");
	if ($r = db_fetch_row($q)) {
	
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: ".$lang."/project/".$r[0].".html");
		exit();
	} 
}

if (!isset($_GET['diary_alias']) and isset($_GET['fulldiary'])) {
	$q = db_query("select alias FROM ".DB_PRFX."diary WHERE NID='".(int)$_GET['fulldiary']."'");
	if ($r = db_fetch_row($q)) {
	
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: ".$lang."/diary/".$r[0].".html");
		exit();
	} 
}


if (!isset($_GET['company_alias']) and isset($_GET['fullcompany'])) {
	$q = db_query("select alias FROM ".DB_PRFX."company WHERE NID='".(int)$_GET['fullcompany']."'");
	if ($r = db_fetch_row($q)) {
	
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: ".$lang."/company/".$r[0].".html");
		exit();
	} 
}
if (!isset($_GET['aux_alias']) and isset($_GET['show_aux_page'])) {
	$q = db_query("select alias FROM ".AUX_PAGES_TABLE." WHERE aux_page_ID='".(int)$_GET['show_aux_page']."'");
	if ($r = db_fetch_row($q)) {
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: ".$lang."/page/".$r[0].".html");
		exit();
	}
}

?>
