<?php

  include("../core/config/init.php");
  include("../core/config/connect.inc.php");
  include("../core/includes/database/mysql.php");
  
  db_connect(DB_HOST, DB_USER, DB_PASS) or die(ERROR_DB_INIT);
  db_select_db(DB_NAME) or die(db_error());
  
  function _getData($table_name) {
    $data = array();
    $sqlData = "select id, name FROM " . $table_name;
    $qp = db_query($sqlData);  
    $data_count = 1;
    while ($row = db_fetch_row($qp)) { 
      //echo '<pre>'; print_r($row); echo '</pre>';
      
      $data[$data_count] = $row['name'];
      $data_count++;
    }
    
    return $data;
  }
  
  function _getPricesData() {
    $data = array();
    $sqlData = "select * FROM pgd_pc_prices";
    $qp = db_query($sqlData);  
    $data_count = 1;
    while ($row = db_fetch_row($qp)) { 
      //echo '<pre>'; print_r($row); echo '</pre>';
      
      //$data[$data_count] = $row['name'];
      $data[] = array(
        'port' => $row['china'],
        'material' => $row['material'],
        'processing' => $row['finishing'],
        'city' => $row['russia'],
        'wagon' => $row['wagon'],
        'container' => $row['container'],
      );
      //$data_count++;
    }
    
    return $data;
  }
/*
  $sqlPorts = "select id, name FROM pgd_pc_china";
  $qp = db_query($sqlPorts);  
  $ports_count = 1;
  while ($row = db_fetch_row($qp)) { 
    //echo '<pre>'; print_r($row); echo '</pre>';
    
    $ports[$ports_count] = $row['name'];
    $ports_count++;
  }
*/
  
  $ports = _getData('pgd_pc_china');
  $cities = _getData('pgd_pc_russia');
  $materials = _getData('pgd_pc_materials');
  $processing = _getData('pgd_pc_finishing');
  $prices = _getPricesData();


$data = array(
 
  'port' => $ports,
  'city' => $cities,
  'material' => $materials,
  'processing' => $processing,
  'prices' => $prices,
  

/*  
  $ports = array(
		'1' => 'FOB Xiamen',
		'2' => 'FOB Qingdao',
		'3' => 'FOB Tianjin',
		'4' => 'CFA Jining',
	),
  
	
	$cities = array(
		'1' => 'Красноярск',
		'2' => 'Новосибирск',
		'3' => 'Екатеринбург',
		'4' => 'Москва',
		'5' => 'Санкт-Петербург',
	),
	
	$materials = array(
		'1' => 'Гранит',
		'2' => 'Мрамор',
	),

	
	$processing = array(
		'1' => 'Полировка',
		'2' => 'Шлифовка',
		'2' => 'Термо-обработка',
	),
	
	
	$prices = array(
	  array(
			'port' => '1',
			'material' => '1',
			'processing' => '3',
			'city' => '4',
			'wagon' => '17400',
			'container' => '7800',
		), 
		
		array(
			'port' => '1',
			'material' => '1',
			'processing' => '1',
			'city' => '4',
			'wagon' => '0',
			'container' => '7800',
		),

		array(
			'port' => '1',
			'material' => '2',
			'processing' => '1',
			'city' => '4',
			'wagon' => '0',
			'container' => '11500',
		),

		array(
			'port' => '1',
			'material' => '1',
			'processing' => '1',
			'city' => '1',
			'wagon' => '500',
			'container' => '800',
		), 
	
	),
*/	
	
);
echo json_encode($data );