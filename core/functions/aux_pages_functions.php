<?php
#####################################
# ShopCMS: Скрипт интернет-магазина
# Copyright (c) by ADGroup
# http://shopcms.ru
#####################################

function auxpgGetAllPageAttributes()
{
        $q = db_query("select aux_page_ID, aux_page_name, aux_page_text_type, alias from ".AUX_PAGES_TABLE);
        $data = array();
        while( $row = db_fetch_row( $q ) ) $data[] = $row;
        return $data;
}

function auxpgGetAuxPage( $aux_page_ID )
{
        $q = db_query("select * from ".AUX_PAGES_TABLE." where aux_page_ID=".(int)$aux_page_ID);
        if  ( $row=db_fetch_row($q) )
        {
                if ( $row["aux_page_text_type"] !=1 ) $row["aux_page_text"] = ToText( $row["aux_page_text"] );
                $row["aux_page_title"] = $row["title"];
                $row["en_aux_page_title"] = $row["en_title"];
                $row["ch_aux_page_title"] = $row["ch_title"];
        }
        return $row;
}

function auxpgUpdateAuxPage(    $aux_page_ID, $aux_page_name,
                                $aux_page_text, $aux_page_text_type,
                                $meta_keywords, $meta_description, $aux_page_title, $old_url,
								
								$en_aux_page_name,
                                $en_aux_page_text, $en_aux_page_text_type,
                                $en_meta_keywords, $en_meta_description, $en_aux_page_title, $en_old_url, $en_use, 
								
								$ch_aux_page_name,
                                $ch_aux_page_text, $ch_aux_page_text_type,
                                $ch_meta_keywords, $ch_meta_description, $ch_aux_page_title, $ch_old_url, $ch_use  )
{
        db_query("update ".AUX_PAGES_TABLE.
                 " set     aux_page_name='".xToText($aux_page_name)."', ".
                 "         aux_page_text='".xEscSQL($aux_page_text)."', ".
                 "         aux_page_text_type=".(int)$aux_page_text_type.", ".
                 "         meta_keywords='".xToText($meta_keywords)."', ".
                 "         meta_description='".xToText($meta_description)."', ".
                 "         title='".xToText($aux_page_title)."', ".
                 "         old_url='".xToText($old_url)."',
				 
				en_aux_page_name='".xToText($en_aux_page_name)."', ".
                 "         en_aux_page_text='".xEscSQL($en_aux_page_text)."', ".
                 "         en_aux_page_text_type=".(int)$en_aux_page_text_type.", ".
                 "         en_meta_keywords='".xToText($en_meta_keywords)."', ".
                 "         en_meta_description='".xToText($en_meta_description)."', ".
                 "         en_title='".xToText($en_aux_page_title)."', ".
                 "         en_old_url='".xToText($en_old_url)."', ".
                 "         en_use=".(int)$en_use.",
				 
				ch_aux_page_name='".xToText($ch_aux_page_name)."', ".
                 "         ch_aux_page_text='".$ch_aux_page_text."', ".
                 "         ch_aux_page_text_type=".(int)$ch_aux_page_text_type.", ".
                 "         ch_meta_keywords='".$ch_meta_keywords."', ".
                 "         ch_meta_description='".xToText($ch_meta_description)."', ".
                 "         ch_title='".xToText($ch_aux_page_title)."', ".
                 "         ch_old_url='".xToText($ch_old_url)."', ".
                 "         ch_use=".(int)$ch_use."
				 
				 
				 ".
                 " where aux_page_ID=".(int)$aux_page_ID);
		aliasGenerateAliasAux($aux_page_ID, $_POST['alias'], $aux_page_name, "upd");
}

function auxpgAddAuxPage(       $aux_page_name,
                                $aux_page_text, $aux_page_text_type,
                                $meta_keywords, $meta_description, $aux_page_title, $old_url,
								
								$en_aux_page_name,
                                $en_aux_page_text, $en_aux_page_text_type,
                                $en_meta_keywords, $en_meta_description, $en_aux_page_title, $en_old_url, $en_use, 
								
								$ch_aux_page_name,
                                $ch_aux_page_text, $ch_aux_page_text_type,
                                $ch_meta_keywords, $ch_meta_description, $ch_aux_page_title, $ch_old_url, $ch_use)
{
        db_query( "insert into ".AUX_PAGES_TABLE.
                " ( aux_page_name, aux_page_text, aux_page_text_type, meta_keywords, meta_description, title, old_url,
				en_aux_page_name, en_aux_page_text, en_aux_page_text_type, en_meta_keywords, en_meta_description, en_title, en_old_url, en_use,
				ch_aux_page_name, ch_aux_page_text, ch_aux_page_text_type, ch_meta_keywords, ch_meta_description, ch_title, ch_old_url, ch_use
				)  ".
                " values( '".xToText($aux_page_name)."', '".xEscSQL($aux_page_text)."', ".(int)$aux_page_text_type.", ".
                " '".xToText($meta_keywords)."', '".xToText($meta_description)."', '".xToText($aux_page_title)."', '".xToText($old_url)."',
				'".xToText($en_aux_page_name)."', '".xEscSQL($en_aux_page_text)."', ".(int)$en_aux_page_text_type.", "." '".xToText($en_meta_keywords)."', '".xToText($en_meta_description)."', '".xToText($en_aux_page_title)."', '".xToText($en_old_url)."', ".(int)$en_use.",
				'".xToText($ch_aux_page_name)."', '".$ch_aux_page_text."', ".(int)$ch_aux_page_text_type.", "." '".$ch_meta_keywords."', '".xToText($ch_meta_description)."', '".xToText($ch_aux_page_title)."', '".xToText($ch_old_url)."', ".(int)$ch_use."
				) " );
		$id = db_insert_id();
        aliasGenerateAliasAux($id, $_POST['alias'], $aux_page_name, "add");
		
		checkNewPageGalleries('aux', $id);
}

function auxpgDeleteAuxPage( $aux_page_ID )
{
        db_query("delete from ".AUX_PAGES_TABLE." where aux_page_ID=".(int)$aux_page_ID);
}
?>