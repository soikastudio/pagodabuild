<?php
#####################################
# ShopCMS: Скрипт интернет-магазина
# Copyright (c) by ADGroup
# http://shopcms.ru
#####################################


function newsMainNews()
{
	global $langPref;
	$q = db_query( "select * from ".DB_PRFX."news_table WHERE main = 1 order by sort ASC");
	$data = array();

	$i=0;
	while( $r=db_fetch_row($q) )
	{
		if(trim($r[$langPref.'title']) != "") {

			$r['title'] = $r[$langPref.'title'];
			$r["add_date"]=dtConvertToStandartForm($r["add_date"]);
			$tmText = preg_replace("/##gallery_[0-9]+##/",'',$r[$langPref."textToPrePublication"]);
			//$r["textToPrePublication"] = $tmText;
			//$r["textToPrePublication"] = $tmText;

			$str = strip_tags( $tmText);
			$r["textToPrePublication"] = substr($str, 0, strpos($str, ' ', 260)).'...'; ;

			//$r["image"] = $r['project_image'];
			$r['pnum'] = $i;
			$data[] = $r;
			$i++;
			if($i == CONF_NEWS_COUNT_IN_CUSTOMER_PART) {
				break;
			}
		}
	}
	return $data;
}

function sendMC($lang, $mailcontent) {
	require_once ('core/classes/MCAPI.class.php');
    $api = new MCAPI('cd941eb7edba2d7bd6b9f69ed56516dd-us8');
	$type = 'regular';
	$opts['title'] = 'Рассылка новостей';
	$opts['from_email'] = 'stone@pagodabuild.com'; 
	$opts['from_name'] = 'Pagoda-Build Co., LTD';

	switch($lang) {
		case 'ru':
			$opts['list_id'] = '7f93f3aa3d';
			$opts['template_id'] = '161149';
			$opts['subject'] = 'На сайте Pagodabuild.com опубликован новый пост';
			break;
		case 'en':
			$opts['list_id'] = '00642ecb62';
			$opts['template_id'] = '161153';
			$opts['subject'] = 'The site published a new post Pagodabuild.com';
			break;
		case 'ch':
			$opts['list_id'] = '011d769939';
			$opts['template_id'] = '161157';
			$opts['subject'] = '该网站发布的一个新的职位Pagodabuild.com';
			break;
	}
	
	$mailcontent = str_replace('src="/','src="http://pagodabuild.com/',$mailcontent);
	$mailcontent = str_replace('src=\'/','src=\'http://pagodabuild.com/',$mailcontent);
	
	$content = array('html_MAILCONTENT'=>$mailcontent);

	$retval = $api->campaignCreate($type, $opts, $content);

	if ($api->errorCode){
	//	return "Error! Msg=".$api->errorMessage."\n";
	} else {
		
		$retval2 = $api->campaignSendNow($retval);

		if ($api->errorCode){
	//		return "Error! Msg=".$api->errorMessage."\n";
		} else {
	//		return "Расылка завершена!";
		}
	}

}

function mailChimpSend($type,$NID) {
	switch($type) {
		case 'news_table':
			$sbLink = '/blog/';
			break;
		case 'diary':
			$sbLink = '/diary/';
			break;
		case 'company':
			$sbLink = '/company/';
			break;
	}
	global $smarty;
	$q = db_query( "select  `title`, `en_title`, `ch_title`, `textToPrePublication`, `en_textToPrePublication`, `ch_textToPrePublication`, `alias` from ".DB_PRFX . $type ." WHERE NID=".$NID);
	$sendNew = array();
	if( $row=db_fetch_row($q) ) {
		if(trim($row['title']) != '') {
			$sendNew['title'] = $row['title'];
			$sendNew['content'] = $row['textToPrePublication'];
			$sendNew['link'] = 'http://pagodabuild.com'.$sbLink.$row['alias'].'.html';
			$smarty->assign( "sendNew", $sendNew );
			$mailcontent = $smarty->fetch('admin/mailchimp.tpl.html');
			sendMC('ru', $mailcontent);
		}
		if(trim($row['en_title']) != '') {
			$sendNew['title'] = $row['en_title'];
			$sendNew['content'] = $row['en_textToPrePublication'];
			$sendNew['link'] = 'http://pagodabuild.com/en'.$sbLink.$row['alias'].'.html';
			$smarty->assign( "sendNew", $sendNew );
			$mailcontent = $smarty->fetch('admin/mailchimp.tpl.html');
			sendMC('en', $mailcontent);
		}
		if(trim($row['ch_title']) != '') {
			$sendNew['title'] = $row['ch_title'];
			$sendNew['content'] = $row['ch_textToPrePublication'];
			$sendNew['link'] = 'http://pagodabuild.com/ch'.$sbLink.$row['alias'].'.html';
			$smarty->assign( "sendNew", $sendNew );
			$mailcontent = $smarty->fetch('admin/mailchimp.tpl.html');
			sendMC('ch', $mailcontent);
		}
	
	}
	
}
function moveNewsTo ($id, $from, $to) {
	$q = db_query( "select * from ".DB_PRFX . $from ." WHERE NID=".$id);
	
	if( $r=db_fetch_row($q) ) {
		if(!isset($r['add_stamp']) || $r['add_stamp'] = '') {
			$stamp = microtime();
			$stamp = explode(" ", $stamp);
			$r['add_stamp'] = $stamp[1];
		}
		db_query("INSERT INTO ".DB_PRFX . $to." SET `add_date`='".$r['add_date']."' ,`title`='".xToText(trim($r['title']))."' ,`en_title`='".xToText(trim($r['en_title']))."' ,`ch_title`='".xToText(trim($r['ch_title']))."' ,`textToPrePublication`='".xEscSQL($r['textToPrePublication'])."',`en_textToPrePublication`='".xEscSQL($r['en_textToPrePublication'])."',`ch_textToPrePublication`='".xEscSQL($r['ch_textToPrePublication'])."' ,`textToPublication`='".xEscSQL($r['textToPublication'])."' ,`en_textToPublication`='".xEscSQL($r['en_textToPublication'])."' ,`ch_textToPublication`='".xEscSQL($r['ch_textToPublication'])."' ,`textToMail`='".xEscSQL($r['textToMail'])."' ,`add_stamp`='".$r['add_stamp']."' ,`alias`='".$r['alias']."' ,`old_url`='".xEscSQL($r['old_url'])."' ,`en_old_url`='".xEscSQL($r['en_old_url'])."' ,`ch_old_url`='".xEscSQL($r['ch_old_url'])."'");
		
		
		$idNew = db_insert_id(DB_PRFX . $to);
		moveGallery($from, $to, $id, $idNew);
		if($id && (int)$idNew > 0) {
			 db_query("DELETE FROM ".DB_PRFX . $from." WHERE NID=".$id);
		}
		switch ($to) {
			case 'news_table':
				$link = "/admin.php?dpt=modules&sub=news&edit=".$idNew ;
				break;
			case 'diary':
				$link = "/admin.php?dpt=modules&sub=diary&edit=".$idNew ;
				break;
			case 'company':
				$link = "/admin.php?dpt=modules&sub=company&edit=".$idNew ;
				break;
		}
		
		return $link;
		
	}
}

function newsGetNewsToCustomerMob() {
	global $langPref;
	$q = db_query( "select * from ".NEWS_TABLE." order by add_date desc, sort asc LIMIT 0, 28");
	$q1 = db_query( "select * from ".NEWS_TABLE." order by add_date desc, sort asc LIMIT 0, 28
	   UNION (select * from pgd_diary order by add_date desc, sort asc LIMIT 0, 28)
	");
	$data = array();

	$i=0;
	while( $r=db_fetch_row($q) )
	{
		if(trim($r[$langPref.'title']) != "") {

			$r['title'] = $r[$langPref.'title'];
			$r["add_date"]=dtConvertToStandartForm($r["add_date"]);
			$tmText = preg_replace("/##gallery_[0-9]+##/",'',$r[$langPref."textToPrePublication"]);
			$r["textToPrePublication"] = $tmText;

			$data[] = $r;
			$i++;
			/*if($i == CONF_NEWS_COUNT_IN_CUSTOMER_PART) {
               break;
            }*/
		}
	}
	return $data;
}


function newsGetNewsItemToBlog()
{
	global $langPref;
	$q = db_query( "select * from ".NEWS_TABLE." WHERE main = 1 order by add_date desc, sort asc LIMIT 0,1 ");
	$data = array();

	$i=0;
	while( $r=db_fetch_row($q) )
	{
		if(trim($r[$langPref.'title']) != "") {

			$r['title'] = $r[$langPref.'title'];
			$r["add_date"]=dtConvertToStandartForm($r["add_date"]);
			$tmText = preg_replace("/##gallery_[0-9]+##/",'',$r[$langPref."textToPrePublication"]);
			$r["textToPrePublication"] = $tmText;

			$data[] = $r;
			$i++;
			/*if($i == CONF_NEWS_COUNT_IN_CUSTOMER_PART) {
               break;
            }*/
		}
	}
	return $data;
}

function newsGetNewsToCustomer()
{
	global $langPref;
	$q = db_query( "select * from ".NEWS_TABLE." order by add_date desc, sort asc");

	$q_diary = db_query( "select * from pgd_diary order by add_date desc, sort asc LIMIT 0, 28");

	$data = array();

	$i=0;
	while( $r=db_fetch_row($q) )
	{
		if(trim($r[$langPref.'title']) != "") {

			$r['title'] = $r[$langPref.'title'];
			$r["add_date"]=dtConvertToStandartForm($r["add_date"]);
			$tmText = preg_replace("/##gallery_[0-9]+##/",'',$r[$langPref."textToPrePublication"]);
			$str = strip_tags( $tmText);
			$r["textToPrePublication"] = substr($str, 0, strpos($str, ' ', 260)).'...'; ;

			$data[] = $r;
			$i++;
			/*if($i == CONF_NEWS_COUNT_IN_CUSTOMER_PART) {
               break;
            }*/
		}
	}
/*
	while( $r=db_fetch_row($q_diary) )
	{
		if(trim($r[$langPref.'title']) != "") {

			$r['title'] = $r[$langPref.'title'];
			$r["add_date"]=dtConvertToStandartForm($r["add_date"]);
			$tmText = preg_replace("/##gallery_[0-9]+##/",'',$r[$langPref."textToPrePublication"]);
			$str = strip_tags( $tmText);
			$r["textToPrePublication"] = substr($str, 0, strpos($str, ' ', 260)).'...'; ;

			$data[] = $r;
			$i++;

		}
	}
*/

	return $data;
}

function newsGetPreNewsToCustomer()
{
	global $langPref;
        $q = db_query( "select * from ".NEWS_TABLE." order by add_date DESC LIMIT 0,".CONF_NEWS_COUNT_IN_NEWS_PAGE);
        $data = array();

        while( $r=db_fetch_row($q) )
        {		
							if(trim($r[$langPref.'title']) == "" && trim($r[$langPref."textToPrePublication"]) == "") {
								$tmPref = "";
							} else {
								$tmPref = $langPref;
							}
				$r['title'] = $r[$tmPref.'title'];
               $r["add_date"]=dtConvertToStandartForm($r["add_date"]);
			   $r["textToPrePublication"] = preg_replace("/##gallery_[0-9]+##/",'',$r[$tmPref."textToPrePublication"]);
               $data[] = $r;
        }
        return $data;
}


function newsGetFullNewsToCustomer($newsid)
{
	global $langPref, $langN;
        $q = db_query( "select add_date, title, en_title, ch_title, textToPrePublication, en_textToPrePublication, ch_textToPrePublication, textToPublication, en_textToPublication, ch_textToPublication, alias from ".NEWS_TABLE." where NID=".(int)$newsid);
        if  ( $r = db_fetch_row($q) )
        {		
			if(trim($r[$langPref.'title']) != "") {
				$r["title"] = $r[$langPref."title"];
			} else {
				if($langN !='') {
					Redirect("/".$langN."/blog/");
				} else {
					Redirect("/blog/");
				}
			}
			
			 $r["textToPrePublication"] = checkGalleriesTags('blog',$newsid,$r[$langPref."textToPrePublication"]);
			   $r["textToPublication"] = checkGalleriesTags('blog',$newsid,$r[$langPref."textToPublication"]);
			   //$r["textToPublication"] = $r[$langPref."textToPublication"];
			   
        $r["add_date"]=dtConvertToStandartForm($r["add_date"]);
        $r["NID"] = (int)$newsid;
			   
			  
		}
        return $r;
}

function newsGetNewsToEdit($newsid)
{
        $q = db_query( "select * from ".NEWS_TABLE." where NID=".(int)$newsid);
        $r=db_fetch_row($q);
        $r["add_date"]=dtConvertToStandartForm($r["add_date"]);
        return $r;
}

function newsGetAllNews( $callBackParam, &$count_row, $navigatorParams = null )
{
        if ( $navigatorParams != null )
        {
                $offset                        = $navigatorParams["offset"];
                $CountRowOnPage        = $navigatorParams["CountRowOnPage"];
        }
        else
        {
                $offset = 0;
                $CountRowOnPage = 0;
        }

        $q = db_query( "select NID, add_date, title, alias, sort, main, main_sort from ".NEWS_TABLE." order by sort ASC, add_date DESC " );

        $i = 0;
        $data = array();
        while( $r=db_fetch_row($q) )
        {
                if ( ($i >= $offset && $i < $offset + $CountRowOnPage) ||
                                $navigatorParams == null  )
                {
                   $r["add_date"]=dtConvertToStandartForm($r["add_date"]);
                   $data[] = $r;
                }
                $i++;
        }
        $count_row = $i;
        return $data;
}

function newsAddNews( $add_date, $title, $en_title, $ch_title, $textToPrePublication, $en_textToPrePublication, $ch_textToPrePublication, $textToPublication, $en_textToPublication, $ch_textToPublication, $textToMail, $old_url, $en_old_url, $ch_old_url, $news_file )
{
        $stamp = microtime();
        $stamp = explode(" ", $stamp);
        $stamp = $stamp[1];
        db_query( "insert into ".NEWS_TABLE." ( add_date, title, en_title, ch_title, textToPrePublication, en_textToPrePublication, ch_textToPrePublication, textToPublication, en_textToPublication, ch_textToPublication, textToMail, old_url, en_old_url, ch_old_url, add_stamp, news_file ) ".
                  " values( '".xEscSQL(dtDateConvert($add_date))."', '".xToText(trim($title))."', '".xToText(trim($en_title))."', '".xToText(trim($ch_title))."', '".xEscSQL($textToPrePublication)."', '".xEscSQL($en_textToPrePublication)."', '".xEscSQL($ch_textToPrePublication)."', '".xEscSQL($textToPublication)."',  '".xEscSQL($en_textToPublication)."',  '".xEscSQL($ch_textToPublication)."', '".xEscSQL($textToMail)."', '".xEscSQL($old_url)."', '".xEscSQL($en_old_url)."', '".xEscSQL($ch_old_url)."', ".$stamp.",  '".$news_file."' ) ");
				  
		$id = db_insert_id();
        aliasGenerateAliasNews($id, $_POST['alias'], $title, "add");
		
		checkNewPageGalleries('blog', $id);
		
        return $id;
}

function newsUpdateNews( $add_date, $title, $en_title, $ch_title, $textToPrePublication, $en_textToPrePublication, $ch_textToPrePublication, $textToPublication, $en_textToPublication, $ch_textToPublication, $textToMail, $id_news, $old_url, $en_old_url, $ch_old_url, $news_file  )
{

	if ($news_file  && !is_numeric($news_file ) ) {

		db_query("update ".NEWS_TABLE.
			" set     add_date='".xEscSQL(dtDateConvert($add_date))."', ".
			"         title='".xToText($title)."', ".
			"         en_title='".xToText($en_title)."', ".
			"         ch_title='".xToText($ch_title)."', ".
			"         textToPrePublication='".xEscSQL($textToPrePublication)."', ".
			"         en_textToPrePublication='".xEscSQL($en_textToPrePublication)."', ".
			"         ch_textToPrePublication='".xEscSQL($ch_textToPrePublication)."', ".
			"         textToPublication='".xEscSQL($textToPublication)."', ".
			"         en_textToPublication='".xEscSQL($en_textToPublication)."', ".
			"         ch_textToPublication='".xEscSQL($ch_textToPublication)."', ".
			"         textToMail='".xEscSQL($textToMail)."', ".
			"         old_url='".xEscSQL($old_url)."' , ".
			"         en_old_url='".xEscSQL($en_old_url)."' , ".
			"         ch_old_url='".xEscSQL($ch_old_url)."' , ".

			"         news_file='".xEscSQL($news_file)."' ".
			" where NID = ".(int)$id_news);
	}
	else {
		db_query("update ".NEWS_TABLE.
			" set     add_date='".xEscSQL(dtDateConvert($add_date))."', ".
			"         title='".xToText($title)."', ".
			"         en_title='".xToText($en_title)."', ".
			"         ch_title='".xToText($ch_title)."', ".
			"         textToPrePublication='".xEscSQL($textToPrePublication)."', ".
			"         en_textToPrePublication='".xEscSQL($en_textToPrePublication)."', ".
			"         ch_textToPrePublication='".xEscSQL($ch_textToPrePublication)."', ".
			"         textToPublication='".xEscSQL($textToPublication)."', ".
			"         en_textToPublication='".xEscSQL($en_textToPublication)."', ".
			"         ch_textToPublication='".xEscSQL($ch_textToPublication)."', ".
			"         textToMail='".xEscSQL($textToMail)."', ".
			"         old_url='".xEscSQL($old_url)."' , ".
			"         en_old_url='".xEscSQL($en_old_url)."' , ".
			"         ch_old_url='".xEscSQL($ch_old_url)."'  ".


			" where NID = ".(int)$id_news);

	}
	            /*
                db_query("update ".NEWS_TABLE.
                 " set     add_date='".xEscSQL(dtDateConvert($add_date))."', ".
                 "         title='".xToText($title)."', ".
                 "         en_title='".xToText($en_title)."', ".
                 "         ch_title='".xToText($ch_title)."', ".
                 "         textToPrePublication='".xEscSQL($textToPrePublication)."', ".
                 "         en_textToPrePublication='".xEscSQL($en_textToPrePublication)."', ".
                 "         ch_textToPrePublication='".xEscSQL($ch_textToPrePublication)."', ".
                 "         textToPublication='".xEscSQL($textToPublication)."', ".
                 "         en_textToPublication='".xEscSQL($en_textToPublication)."', ".
                 "         ch_textToPublication='".xEscSQL($ch_textToPublication)."', ".
                 "         textToMail='".xEscSQL($textToMail)."', ".
                 "         old_url='".xEscSQL($old_url)."' , ".
                 "         en_old_url='".xEscSQL($en_old_url)."' , ".
                 "         ch_old_url='".xEscSQL($ch_old_url)."' ".
                 " where NID = ".(int)$id_news);
				 */
				 aliasGenerateAliasNews($id_news, $_POST['alias'], $title, "upd");
}

function newsDeleteNews( $newsid )
{
        db_query( "delete from ".NEWS_TABLE." where NID=".(int)$newsid );
}

function newsSendNews($newsid)
{
        $q = db_query( "select add_date, title, textToMail from ".NEWS_TABLE." where NID=".(int)$newsid );
        $news = db_fetch_row( $q );
        $news["add_date"]=dtConvertToStandartForm($news["add_date"]);
        $q = db_query( "select Email from ".MAILING_LIST_TABLE );
        while( $subscriber = db_fetch_row($q) ) xMailTxtHTMLDATA($subscriber["Email"], EMAIL_NEWS_OF." - ".CONF_SHOP_NAME, $news["title"]."<br><br>".$news["textToMail"]);
}


/*
 * Last news item to main
 */
function newsGetNewsItemToMain()
{
	global $langPref, $langN;
	$q = db_query( "select NID, add_date, title, en_title, ch_title, textToPrePublication, en_textToPrePublication, ch_textToPrePublication, textToPublication, en_textToPublication, ch_textToPublication, alias, news_file from ".NEWS_TABLE." ORDER BY NID DESC LIMIT 0,1");
	if  ( $r = db_fetch_row($q) )
	{

		//$r["textToPrePublication"] = checkGalleriesTags('blog',$q['NID'],$r[$langPref."textToPrePublication"]);
		$r["title"] = $r[$langPref . "title"];
		$r["textToPrePublication"] = $r[$langPref."textToPrePublication"];
		//$r["textToPublication"] = checkGalleriesTags('blog',$newsid,$r[$langPref."textToPublication"]);
		$r["textToPublication"] = $r[$langPref."textToPublication"];
		//$r["textToPublication"] = $r[$langPref."textToPublication"];

		$r["add_date"]=dtConvertToStandartForm($r["add_date"]);
		//$r["NID"] = (int)$newsid;
		if ($langN) {
			$r["alias"] = '/' . $langN . '/blog/'. $r["alias"] . '.html';
		}
		else {
			$r["alias"] =  '/blog/'. $r["alias"] . '.html';
		}


	}
	if ($r["title"] || !empty($r["title"]) ) {
		return $r;
	}
	return false;
}

?>