<?

function getExtension($str)
	{
			 $i = strrpos($str,".");
			 if (!$i) { return ""; }
			 $l = strlen($str) - $i;
			 $ext = substr($str,$i+1,$l);
			 return $ext;
	}

	function createphoto ($input,$output,$mime) {
		 $w = 1024;  // мы получим  пропорциональное изображение шириной 800px
		 $q = 95;  // качество jpeg по умолчанию
		 
		 
		$f=$input; 

		if($mime == "image/jpeg") {
		 $src = imagecreatefromjpeg($f);
		 $jp=0;
		} else if ($mime == "image/gif") {
		 $src = imagecreatefromgif($f);
		 $jp=2;
		} else if ($mime == "image/png") {
		 $src = imagecreatefrompng($f);
		 $jp=1;
		 };
		// функция imagecreatefromjpeg создает изображение JPEG из файла
		 // т.е. создаём исходное изображение на основе исходного файла и определяем его размеры
		 
		$w_src = imagesx($src);
		 $h_src= imagesy($src);
		// получение ширины и высоты изображения в пикселях
		 
		$ratio = $w_src/$w;
		 $w_dest = round($w_src/$ratio);
		 $h_dest = round($h_src/$ratio);
		// получение координат для построения нового изображения необходимой нам ширины
		 
		$dest = imagecreatetruecolor($w_dest,$h_dest);
		// функция  imagecreatetruecolor пустое полноцветное изображение размерами x_size и y_size.
		 // Созданное изображение имеет черный фон.
		 if ($jp==1) {
			imagealphablending($dest, false);
			imagesavealpha($dest, true);
			
		 } elseif ($jp==2) {
			$transparent_source_index=imagecolortransparent($src);
		 
			//Проверяем наличие прозрачности
			if($transparent_source_index!==-1){
				$transparent_color=imagecolorsforindex($src, $transparent_source_index);
			 
				//Добавляем цвет в палитру нового изображения, и устанавливаем его как прозрачный
				$transparent_destination_index=imagecolorallocate($dest, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
				imagecolortransparent($dest, $transparent_destination_index);
			 
				//На всякий случай заливаем фон этим цветом
				imagefill($dest, 0, 0, $transparent_destination_index);
			}
		 }
		 
		imagecopyresampled($dest, $src, 0, 0, 0, 0, $w_dest, $h_dest, $w_src, $h_src);
		// imagecopyresized($dest, $src, 0, 0, 0, 0, $w_dest, $h_dest, $w_src, $h_src);
		// Функция imagecopyresized копирует прямоугольные области с одного изображения на другое
		 
		// вывод картинки и очистка памяти
		if($mime == "image/jpeg") {
		 imagejpeg($dest,$output,$q);
		} else if ($mime == "image/gif") {
		 imagegif($dest,$output);
		} else if ($mime == "image/png") {
		 imagepng($dest,$output);
		}
		 imagedestroy($dest);
		 imagedestroy($src);
		 
	}

function createthumb ($input,$output,$mime) {
   $w = 200;  // мы получим  пропорциональное изображение шириной 200px
   $q = 95;  // качество jpeg по умолчанию
   
  $f=$input;
  if($mime == "image/jpeg") {
   $src = imagecreatefromjpeg($f);
   $jp=0;
  } else if ($mime == "image/gif") {
   $src = imagecreatefromgif($f);
   $jp=2;
  } else if ($mime == "image/png") { 
   $src = imagecreatefrompng($f);
   $jp=1;
   }
  // функция imagecreatefromjpeg создает изображение JPEG из файла
   // т.е. создаём исходное изображение на основе исходного файла и определяем его размеры
   
  $w_src = imagesx($src);
   $h_src= imagesy($src);
  // получение ширины и высоты изображения в пикселях
   
  $ratio = $w_src/$w;
   $w_dest = round($w_src/$ratio);
   $h_dest = round($h_src/$ratio);
  // получение координат для построения нового изображения необходимой нам ширины
   
  $dest = imagecreatetruecolor($w_dest,$h_dest);
  // функция  imagecreatetruecolor пустое полноцветное изображение размерами x_size и y_size.
   // Созданное изображение имеет черный фон.
   if ($jp==1) {
    imagealphablending($dest, false);
    imagesavealpha($dest, true);
    
   } elseif ($jp==2) {
    $transparent_source_index=imagecolortransparent($src);
   
    //Проверяем наличие прозрачности
    if($transparent_source_index!==-1){
      $transparent_color=imagecolorsforindex($src, $transparent_source_index);
     
      //Добавляем цвет в палитру нового изображения, и устанавливаем его как прозрачный
      $transparent_destination_index=imagecolorallocate($dest, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
      imagecolortransparent($dest, $transparent_destination_index);
     
      //На всякий случай заливаем фон этим цветом
      imagefill($dest, 0, 0, $transparent_destination_index);
    }
   }
   
  imagecopyresampled($dest, $src, 0, 0, 0, 0, $w_dest, $h_dest, $w_src, $h_src);
  // imagecopyresized($dest, $src, 0, 0, 0, 0, $w_dest, $h_dest, $w_src, $h_src);
  // Функция imagecopyresized копирует прямоугольные области с одного изображения на другое
   
  // вывод картинки и очистка памяти
  if($mime == "image/jpeg") {
   imagejpeg($dest,$output,$q);
  } else if ($mime == "image/gif") {
   imagegif($dest,$output);
  } else if ($mime == "image/png") {
   imagepng($dest,$output);
  }
   imagedestroy($dest);
   imagedestroy($src);
   
}

function createMiddle ($input,$output,$mime) {
   $w = 259;  // мы получим  пропорциональное изображение шириной 200px
   $q = 95;  // качество jpeg по умолчанию
   
  $f=$input;
  if($mime == "image/jpeg") {
   $src = imagecreatefromjpeg($f);
   $jp=0;
  } else if ($mime == "image/gif") {
   $src = imagecreatefromgif($f);
   $jp=2;
  } else if ($mime == "image/png") { 
   $src = imagecreatefrompng($f);
   $jp=1;
   }
  // функция imagecreatefromjpeg создает изображение JPEG из файла
   // т.е. создаём исходное изображение на основе исходного файла и определяем его размеры
   
  $w_src = imagesx($src);
   $h_src= imagesy($src);
  // получение ширины и высоты изображения в пикселях
   
  $ratio = $w_src/$w;
   $w_dest = round($w_src/$ratio);
   $h_dest = round($h_src/$ratio);
  // получение координат для построения нового изображения необходимой нам ширины
   
  $dest = imagecreatetruecolor($w_dest,$h_dest);
  // функция  imagecreatetruecolor пустое полноцветное изображение размерами x_size и y_size.
   // Созданное изображение имеет черный фон.
   if ($jp==1) {
    imagealphablending($dest, false);
    imagesavealpha($dest, true);
    
   } elseif ($jp==2) {
    $transparent_source_index=imagecolortransparent($src);
   
    //Проверяем наличие прозрачности
    if($transparent_source_index!==-1){
      $transparent_color=imagecolorsforindex($src, $transparent_source_index);
     
      //Добавляем цвет в палитру нового изображения, и устанавливаем его как прозрачный
      $transparent_destination_index=imagecolorallocate($dest, $transparent_color['red'], $transparent_color['green'], $transparent_color['blue']);
      imagecolortransparent($dest, $transparent_destination_index);
     
      //На всякий случай заливаем фон этим цветом
      imagefill($dest, 0, 0, $transparent_destination_index);
    }
   }
   
  imagecopyresampled($dest, $src, 0, 0, 0, 0, $w_dest, $h_dest, $w_src, $h_src);
  // imagecopyresized($dest, $src, 0, 0, 0, 0, $w_dest, $h_dest, $w_src, $h_src);
  // Функция imagecopyresized копирует прямоугольные области с одного изображения на другое
   
  // вывод картинки и очистка памяти
  if($mime == "image/jpeg") {
   imagejpeg($dest,$output,$q);
  } else if ($mime == "image/gif") {
   imagegif($dest,$output);
  } else if ($mime == "image/png") {
   imagepng($dest,$output);
  }
   imagedestroy($dest);
   imagedestroy($src);
   
}
	

function getGalleries($page) {
	global $langPref;
	$q = db_query("SELECT * FROM ".DB_PRFX."gallery WHERE gPage='".$page."' ORDER BY gID DESC");
		
		$gallery = array();
		
		while  ( $row=db_fetch_row($q) ) {
			$tmgImages = explode(',',$row["gImages"]);
			$tmImTitles = explode('@:#',$row["gImTitles"]);
			$resImgs=array();
			foreach ($tmgImages as $key => $img) {
				$resImgs[] = array('image'=>$img, 'title'=>$tmImTitles[$key]);
				
			}
			
			$row["gImages"] = $resImgs;
			$row['gHead'] = $row[$langPref.'gHead'];
			$row['gText'] = $row[$langPref.'gText'];
			$gallery[] = $row;
		}
		
		return $gallery;
	
}

function getGalleriesAll($prodID) {
	global $langPref;
	$q = db_query("SELECT * FROM ".DB_PRFX."gallery WHERE gPage ='product_" . $prodID ."' ORDER BY gID DESC");
		
		$gallery = array();
		
		while  ( $row=db_fetch_row($q) ) {
			$tmgImages = explode(',',$row["gImages"]);
			$tmImTitles = explode('@:#',$row["gImTitles"]);
			$resImgs=array();
			foreach ($tmgImages as $key => $img) {
				$resImgs[] = array('image'=>$img, 'title'=>$tmImTitles[$key]);
				
			}
			
			$row["gImages"] = $resImgs;
			$row['gHead'] = $row[$langPref.'gHead'];
			$row['gText'] = $row[$langPref.'gText'];
			$gallery[] = $row;
		}
		
		return $gallery;
	
}

function removeGallery($id) {
	$q = db_query("SELECT gImages FROM ".DB_PRFX."gallery WHERE gID=".(int)$id);
	
	if($row=db_fetch_row($q)) {
		$gImages = explode(',',$row['gImages']);
	}
	
	foreach ($gImages as $img) {
		unlink(ROOT_DIR."/data/gallery/".$img);
		unlink(ROOT_DIR."/data/gallery/thmbs/".$img);
	}
	db_query("DELETE FROM ".DB_PRFX."gallery WHERE gID=".(int)$id);
}


function removeGalleryByGPage($type, $id) {
	$gPage = $type.'_'.$id;
	$q = db_query("SELECT gImages FROM ".DB_PRFX."gallery WHERE gPage='".$gPage."'");
	
	while($row=db_fetch_row($q)) {
		$gImages = explode(',',$row['gImages']);
		
		foreach ($gImages as $img) {
			unlink(ROOT_DIR."/data/gallery/".$img);
			unlink(ROOT_DIR."/data/gallery/thmbs/".$img);
		}
	}
	
	db_query("DELETE FROM ".DB_PRFX."gallery WHERE gPage='".$gPage."'");
}

function checkGalleriesTags ($type, $id, $text) {
	global $langPref, $smarty;
	$gPage = $type."_".$id;
	$q = db_query("SELECT * FROM ".DB_PRFX."gallery WHERE gPage='".$gPage."'");

	while  ( $row=db_fetch_row($q) ) {
		$gallery = array();
		$tmgImages = explode(',',$row["gImages"]);
		$tmImTitles = explode('@:#',$row["gImTitles"]);
		$resImgs=array();
		foreach ($tmgImages as $key => $img) {
			$resImgs[] = array('image'=>$img, 'title'=>$tmImTitles[$key]);

		}

		$row["gImages"] = $resImgs;

		$row['gHead'] = $row[$langPref.'gHead'];
		$row['gText'] = $row[$langPref.'gText'];
		$gallery[] = $row;

		$smarty->assign( "gallery", $gallery );
		$tagToGall = $smarty->fetch("gallery.tpl.html");
		$text = str_replace($row['gTag'],$tagToGall,$text);
	}
	$text = preg_replace("/##gallery_[0-9]+##/",'',$text);
	return $text;
}

function checkGalleriesTagsId ($type, $id, $text) {
	global $langPref, $smarty;
	//$gPage = $type."_".$id;
	$q = db_query("SELECT * FROM ".DB_PRFX."gallery WHERE gId='".$id."'");

	while  ( $row=db_fetch_row($q) ) {
		$gallery = array();
		$tmgImages = explode(',',$row["gImages"]);
		$tmImTitles = explode('@:#',$row["gImTitles"]);
		$resImgs=array();
		foreach ($tmgImages as $key => $img) {
			$resImgs[] = array('image'=>$img, 'title'=>$tmImTitles[$key]);

		}

		$row["gImages"] = $resImgs;

		$row['gHead'] = $row[$langPref.'gHead'];
		$row['gText'] = $row[$langPref.'gText'];
		$gallery[] = $row;

		$smarty->assign( "gallery", $gallery );
		$tagToGall = $smarty->fetch("gallery.tpl.html");
		$text = str_replace($row['gTag'],$tagToGall,$text);
	}
	$text = preg_replace("/##gallery_[0-9]+##/",'',$text);
	return $text;
}

function checkNewPageGalleries($type, $newId) {
			$gPage = $type."_".$newId;
			db_query("UPDATE ".DB_PRFX."gallery SET gPage='".$gPage."' WHERE gPage='".$type."'");
			
			
}

function moveGallery($from, $to, $id, $idNew) {
	if($to == 'news_table') {
		$gPageTo = 'blog_'. $idNew;
	} else {
		$gPageTo = $to.'_'. $idNew;
	}
	if($from == 'news_table') {
		$gPage = 'blog_'. $id;
	} else {
		$gPage = $from.'_'. $id;
	}
	$q = db_query("SELECT gID FROM ".DB_PRFX."gallery WHERE gPage='".$gPage."'");
	if($row=db_fetch_row($q)) {
		db_query("UPDATE ".DB_PRFX."gallery SET gPage='".$gPageTo."' WHERE gID=".$row['gID']);
	}
	
}

?>