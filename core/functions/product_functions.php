<?php
#####################################
# ShopCMS: Скрипт интернет-магазина
# Copyright (c) by ADGroup
# http://shopcms.ru
#####################################

function prdProductExists( $productID )
{
        $q = db_query( "select count(*) from ".PRODUCTS_TABLE." where productID=".(int)$productID );
        $row = db_fetch_row($q);
        return ($row[0]!=0);
}


function getDesktopMainProducts()
{
  global $langPref;

  $lang_map = array('en_' => 'en', 'ch_' => 'ch');
  $out = array();
  $return_out = array();

 // $q = db_query( "select * from ".PRODUCTS_TABLE." where categoryID = 6 ORDER BY productID DESC LIMIT 0, 7");
 
 
  //$q = db_query( "select * from ".PRODUCTS_TABLE." ORDER BY productID DESC LIMIT 0, 8");
  $q = db_query( "select * from ".PRODUCTS_TABLE." WHERE categoryID IN (7,8) AND enabled = 1 ORDER BY productID DESC");
  //SELECT * FROM `pgd_products` ORDER BY productID DESC LIMIT 0, 4
  $prod_counter = 0;
  $out_prod_counter = 0;
  while ($row = db_fetch_row($q)) {

    $imgs = db_query("select photoID, filename, thumbnail, enlarged from " . PRODUCT_PICTURES . " where productID=" . $row[0]);
    while ($img = db_fetch_row($imgs)) {
      $out[$prod_counter]['preview'] = $img['filename'];
      $out[$prod_counter]['thumbnail'] = $img['thumbnail'];
      $out[$prod_counter]['img'] = $img['enlarged'];
      
      //$out[$r] = 
    }
    
    $out[$prod_counter]['header'] = $row[$langPref.'name'];
    $out[$prod_counter]['title'] = $row[$langPref.'title'];
      if ($langPref) {
          $out[$prod_counter]['alias'] = $lang_map[$langPref].'/' . $row['alias'];
      }
      else {
          $out[$prod_counter]['alias'] = $row['alias'];
      }


    if ( $out[$prod_counter]['preview'] || $out[$prod_counter]['thumbnail'] || $out[$prod_counter]['img'] ) {
      $return_out[$out_prod_counter] = $out[$prod_counter];
      $out_prod_counter++;
    }
    
    $prod_counter++;
    
  }
  return $return_out;
}


function getMobileMainProducts()
{
  global $langPref;
  $out = array();
  $return_out = array();
  
  $q = db_query( "select * from ".PRODUCTS_TABLE." WHERE categoryID IN (7,8) AND enabled = 1 ORDER BY productID DESC");
  //SELECT * FROM `pgd_products` ORDER BY productID DESC LIMIT 0, 4
  $prod_counter = 0;
  $out_prod_counter = 0;
  while ($row = db_fetch_row($q)) {
    
    
    $imgs = db_query("select photoID, filename, thumbnail, enlarged from " . PRODUCT_PICTURES . " where productID=" . $row[0]);
    while ($img = db_fetch_row($imgs)) {
      $out[$prod_counter]['preview'] = $img['filename'];
      $out[$prod_counter]['thumbnail'] = $img['thumbnail'];
      $out[$prod_counter]['img'] = $img['enlarged'];
      
      //$out[$r] = 
    }
    
    $out[$prod_counter]['header'] = $row[$langPref.'header'];
    $out[$prod_counter]['title'] = $row[$langPref.'title'];
    $out[$prod_counter]['alias'] = $row['alias'];
    
    if ( $out[$prod_counter]['preview'] || $out[$prod_counter]['thumbnail'] || $out[$prod_counter]['img'] ) {
      $return_out[$out_prod_counter] = $out[$prod_counter];
      $out_prod_counter++;
    }
    
    $prod_counter++;
    
  }
  return $return_out;
}

function getMobileMainProductsEn()
{
  global $langPref;
  $out = array();
  $return_out = array();

  //$q = db_query( "select * from ".PRODUCTS_TABLE." ORDER BY productID DESC");
  
  $q = db_query( "select * from ".PRODUCTS_TABLE." WHERE categoryID IN (7,8) AND enabled = 1 ORDER BY productID DESC");
  //SELECT * FROM `pgd_products` ORDER BY productID DESC LIMIT 0, 4
  $prod_counter = 0;
  $out_prod_counter = 0;
  while ($row = db_fetch_row($q)) {

    $imgs = db_query("select photoID, filename, thumbnail, enlarged from " . PRODUCT_PICTURES . " where productID=" . $row[0]);
    while ($img = db_fetch_row($imgs)) {

      if ($row['en_title'] != '') {
        $out[$prod_counter]['preview'] = $img['filename'];
        $out[$prod_counter]['thumbnail'] = $img['thumbnail'];
        $out[$prod_counter]['img'] = $img['enlarged'];
        $out[$prod_counter]['header'] = $row[$langPref . 'header'];
        $out[$prod_counter]['title'] = $row[$langPref . 'title'];
        $out[$prod_counter]['alias'] = $row['alias'];
      }
    }

    if ( $out[$prod_counter]['preview'] || $out[$prod_counter]['thumbnail'] || $out[$prod_counter]['img'] ) {
      $return_out[$out_prod_counter] = $out[$prod_counter];
      $out_prod_counter++;
    }

    $prod_counter++;

  }

  $return = array();

  $return_count = 0;
  foreach ($return_out as $out) {
    //if ($return_count >=4)  break;
    $return[] = $out;
    $return_count++;
  }
  //return $return_out;
  return $return;
}

function getMobileMainProductsCh()
{
  global $langPref;
  $out = array();
  $return_out = array();

  $q = db_query( "select * from ".PRODUCTS_TABLE." WHERE categoryID IN (7,8) AND enabled = 1 ORDER BY productID DESC");
  
  //$q = db_query( "select * from ".PRODUCTS_TABLE." ORDER BY productID DESC LIMIT 0, 24");
  //SELECT * FROM `pgd_products` ORDER BY productID DESC LIMIT 0, 4
  $prod_counter = 0;
  $out_prod_counter = 0;
  while ($row = db_fetch_row($q)) {

    $imgs = db_query("select photoID, filename, thumbnail, enlarged from " . PRODUCT_PICTURES . " where productID=" . $row[0]);
    while ($img = db_fetch_row($imgs)) {

      if ($row['ch_title'] != '') {
        $out[$prod_counter]['preview'] = $img['filename'];
        $out[$prod_counter]['thumbnail'] = $img['thumbnail'];
        $out[$prod_counter]['img'] = $img['enlarged'];
        $out[$prod_counter]['header'] = $row[$langPref . 'header'];
        $out[$prod_counter]['title'] = $row[$langPref . 'title'];
        $out[$prod_counter]['alias'] = $row['alias'];
      }
    }

    if ( $out[$prod_counter]['preview'] || $out[$prod_counter]['thumbnail'] || $out[$prod_counter]['img'] ) {
      $return_out[$out_prod_counter] = $out[$prod_counter];
      $out_prod_counter++;
    }

    $prod_counter++;

  }

  $return = array();

  $return_count = 0;
  foreach ($return_out as $out) {
    //if ($return_count >=4)  break;
    $return[] = $out;
    $return_count++;
  }
  //return $return_out;
  return $return;
}



function getMobileMenuProducts($cat_id)
{
  global $langPref;
  $out = array();
  $return_out = array();
  
  $q = db_query( "select * from ".PRODUCTS_TABLE." WHERE categoryID IN (" . $cat_id . ") AND enabled = 1  
  ORDER BY productID DESC");
  //SELECT * FROM `pgd_products` ORDER BY productID DESC LIMIT 0, 4
  $prod_counter = 0;
  $out_prod_counter = 0;
  while ($row = db_fetch_row($q)) {
    
    //echo '<pre>'; print_r($row); echo '</pre>'; die();
  
    $out[$prod_counter]['header'] = $row[$langPref.'header'];
    $out[$prod_counter]['title'] = $row[$langPref.'title'];
    $out[$prod_counter]['name'] = $row[2];
    $out[$prod_counter]['alias'] = $row['alias'];
    
   
    $return_out[$out_prod_counter] = $out[$prod_counter];
    $out_prod_counter++;
    
    
    $prod_counter++;
    
  }
  return $return_out;
}

function dkeywords($keywords)
{
$keywords = strip_tags($keywords);
$keywords = trim($keywords);
$keywords = ereg_replace('("|\?|!|:|\.|\(|\)|;|\\\\)+', ' ', $keywords);
$repArr = array('алтухов', 'большой', 'бы', 'быть', 
'в', 'весь', 'вот', 'все', 'всей', 'вы', 'говорить',
 'год', 'да', 'для', 'до', 'еще', 'же', 'знать', 'и',
 'из', 'к', 'как', 'который', 'мочь', 'мы', 'на', 'наш',
 'не', 'него', 'нее', 'нет', 'них', 'но', 'о', 'один',
 'она', 'они', 'оно', 'оный', 'от', 'ото', 'по', 'с',
 'свой', 'себя', 'сказать', 'та', 'такой', 'только', 
 'тот', 'ты', 'у', 'что', 'это', 'этот', 'я', 'почему-то', 
 'тут', 'так', 'или', 'уже', 'эти', 'почему', 'после',
 'когда', 'зачем', 'туда', 'было', 'вообщем', 'того',
 'сделал', 'для', 'чтобы', 'ведь', 'можно', 'если', 'чего', 
 'потому', 'что', 'нет', 'наш', 'ваш', 'вашего', 'этой',
 'будет', 'них', 'теперь', 'делать', 'через', 'лучше',
 'сразу', 'нажимаешь', 'которая', 'каторая', 'новый',
 'свой',  "\x22", "\x60", "\t", '\n', '\r', "\n",
 "\r", '\\', ",", ".", "/", "¬", "#", ";", ":", "@", 
 "~", "[", "]", "{", "}", "=", "-", "+", ")", "(", "*",
 "^", "%", "$", "<", ">", "?", "!", '"');
$keywords = str_replace('', ' ', $keywords);
$keywords = ereg_replace('( |'.CHR(10).'|'.CHR(13).')+', ',', $keywords);
$keywords = substr($keywords,0,1600);
$keywords = array_unique (explode (",", $keywords));
for ($a=0,$b=7; $a < sizeof($keywords) && $b < 700; $a++) {
if ( ($c=strlen(@$keywords[$a])) > 2) {
@$key_words="".$key_words.", ".$keywords[$a].""; $b+=$c+2; }
}
$key_words=strtolower($key_words);
$key_words=substr($key_words, 2);
return $key_words;
} 
function getcontentprod($productID)
{
         $out = array();
         $cnt = 0;
         $q = db_query("select Owner from ".RELATED_CONTENT_TABLE." where productID=".(int)$productID);
         while ($row = db_fetch_row($q))
         {
                $outpre = $row["Owner"];
                $qh = db_query("select aux_page_name, alias from ".AUX_PAGES_TABLE." where aux_page_ID=".(int)$outpre);
                $rowh = db_fetch_row($qh);
                $out[$cnt][0] = $outpre;
                $out[$cnt][1] = $rowh["aux_page_name"];
				$out[$cnt][2] = $rowh["alias"];
                $cnt++;
         }
         return $out;
}

// *****************************************************************************
// Purpose        gets product
// Inputs   $productID - product ID
// Remarks
// Returns        array of fieled value
//                        "name"                                - product name
//                        "product_code"                - product code
//                        "description"                - description
//                        "brief_description"        - short description
//                        "customers_rating"        - product rating
//                        "in_stock"                        - in stock (this parametr is persist if CONF_CHECKSTOCK == 1 )
//                        "option_values"                - array of
//                                        "optionID"                - option ID
//                                        "name"                        - name
//                                        "value"        - option value
//                                        "option_type" - option type
//                        "ProductIsProgram"                - 1 if product is program, 0 otherwise
//                        "eproduct_filename"                - program filename
//                        "eproduct_available_days"        - program is available days to download
//                        "eproduct_download_times"        - attempt count download file
//                        "weight"                        - product weigth
//                        "meta_description"                - meta tag description
//                        "meta_keywords"                        - meta tag keywords
//                        "free_shipping"                        - 1 product has free shipping,
//                                                        0 - otherwise
//                        "min_order_amount"                - minimum order amount
//                        "classID"                        - tax class ID

function GetProduct( $productID)
{
        $q = db_query('select * FROM '.PRODUCTS_TABLE.' WHERE productID='.(int)$productID);

        if ( $product=db_fetch_row($q ) ){

                $product["ProductIsProgram"] =         (trim($product["eproduct_filename"]) != "");
                $sql = 'select pot.optionID,pot.name,povt.option_value,povt.option_value as value,povt.option_type FROM '.PRODUCT_OPTIONS_VALUES_TABLE.' as povt
                        LEFT JOIN '.PRODUCT_OPTIONS_TABLE.' as pot ON pot.optionID=povt.optionID
                        WHERE productID='.(int)$productID.'
                ';
                $Result = db_query($sql);
                $product['option_values'] = array();

                while ($_Row = db_fetch_row($Result)) {

                        $product['option_values'][] = $_Row;
                }

                $product['date_added']=format_datetime( $product['date_added'] );
                $product['date_modified']=format_datetime( $product['date_modified'] );
                return $product;
        }
        return false;
}

function GetProductP( $productID)
{
        $q = db_query('select * FROM '.PRODUCTS_TABLE.' WHERE productID='.(int)$productID);

        if ( $product=db_fetch_row($q ) ){

                $product["ProductIsProgram"] =         (trim($product["eproduct_filename"]) != "");
                $sql = 'select pot.optionID,pot.name,povt.option_value,povt.option_value as value,povt.option_type FROM '.PRODUCT_OPTIONS_VALUES_TABLE.' as povt
                        LEFT JOIN '.PRODUCT_OPTIONS_TABLE.' as pot ON pot.optionID=povt.optionID
                        WHERE productID='.(int)$productID.'
                ';
                $Result = db_query($sql);
                $product['option_values'] = array();

                while ($_Row = db_fetch_row($Result)) {

                        $product['option_values'][] = $_Row;
                }

                $product['date_added']=format_datetime( $product['date_added'] );
                $product['date_modified']=format_datetime( $product['date_modified'] );
                return $product;
        }
        return false;
}



// *****************************************************************************
// Purpose        updates product
// Inputs   $productID - product ID
//                                $categoryID                        - category ID ( see CATEGORIES_TABLE )
//                                $name                                - name of product
//                                $Price                                - price of product
//                                $description                - product description
//                                $in_stock                        - stock counter
//                                $customers_rating        - rating
//                                $brief_description  - short product description
//                                $list_price                        - old price
//                                $product_code                - product code
//                                $sort_order                        - sort order
//                                $ProductIsProgram                - 1 if product is program, 0 otherwise
//                                $eproduct_filename                - program filename
//                                $eproduct_available_days        - program is available days to download
//                                $eproduct_download_times        - attempt count download file
//                                $weight                        - product weigth
//                                $meta_description        - meta tag description
//                                $meta_keywords                - meta tag keywords
//                                $free_shipping                - 1 product has free shipping,
//                                                        0 - otherwise
//                                $min_order_amount        - minimum order amount
//                                $classID                - tax class ID
// Remarks
// Returns
function UpdateProduct( $productID,
                                $categoryID, $name, $en_name, $ch_name, $Price, $description, $en_description, $ch_description,
                                $in_stock,
                                $brief_description, $en_brief_description, $ch_brief_description, $list_price,
                                $product_code, $sort_order,
                                $ProductIsProgram, $eproduct_filename,
                                $eproduct_available_days, $eproduct_download_times,
                                $weight, $meta_description, $en_meta_description, $ch_meta_description, $meta_keywords, $en_meta_keywords, $ch_meta_keywords,
                                $free_shipping, $min_order_amount, $shipping_freight,
                                $classID, $title, $en_title, $ch_title, $old_url, $en_old_url, $ch_old_url, $header='', $en_header='', $ch_header='', $updateGCV = 1  )
{

			$catn = db_query('select name FROM  '.CATEGORIES_TABLE.' WHERE categoryID='.$categoryID);
					if($_Row = db_fetch_row($catn)) {
							$catname = $_Row['name'];
					} else {
						$catname = '';
					}
				  if(trim($header) == '') {
						$header = $catname.' '.$name;
					}
				  if(trim($title) == '') {
						$title = $catname.' '.$name." от PagodaBuild - поставки гранита и мрамора из Китая";
					}
		
				  if(trim($meta_description) == '') {
					$meta_description = "Предлагаем к продаже ". mb_strtolower($catname,'utf-8')." ".$name ." в слэбах и модульной плите. Поставки гранита и мрамора из Китая от камнеобрабатывающего завода PagodaBuild";
				  }
				  if(trim($meta_keywords) == '') {
						$meta_keywords = dkeywords($header.' '.$brief_description.' '.$description.' '.$meta_description);
				}



        if ( $min_order_amount == 0 )$min_order_amount = 1;

        if ( !$ProductIsProgram ) $eproduct_filename = "";

        if ( !$free_shipping ) $free_shipping = 0;
        else $free_shipping = 1;

        $q = db_query("select eproduct_filename from ".PRODUCTS_TABLE." where productID=".(int)$productID);
        $old_file_name = db_fetch_row( $q );
        $old_file_name = $old_file_name[0];

        if ( $classID == null ) $classID = "NULL";

        if ( $eproduct_filename != "" && $ProductIsProgram)
        {
                if ( trim($_FILES[$eproduct_filename]["name"]) != ""  )
                {
                        if ( trim($old_file_name) != "" && file_exists("core/files/".$old_file_name) )
                                unlink("core/files/$old_file_name");

                        if ( $_FILES[$eproduct_filename]["size"]!=0 )
                                        $r = move_uploaded_file($_FILES[$eproduct_filename]["tmp_name"],
                                                "core/files/".$_FILES[$eproduct_filename]["name"]);
                        $eproduct_filename = trim($_FILES[$eproduct_filename]["name"]);
                        SetRightsToUploadedFile( "core/files/".$eproduct_filename );
                }
                else
                        $eproduct_filename = $old_file_name;
        }
        elseif ($old_file_name != "") unlink("core/files/".$old_file_name);
		
        $s = "UPDATE ".PRODUCTS_TABLE." SET ".
                                "categoryID=".(int)$categoryID.", ".
                                "name='".xToText(trim($name))."', ".
                                "en_name='".xToText(trim($en_name))."', ".
                                "ch_name='".xToText(trim($ch_name))."', ".
                                "header='".xToText(trim($header))."', ".
                                "en_header='".xToText(trim($en_header))."', ".
                                "ch_header='".xToText(trim($ch_header))."', ".
                                "Price=".(double)$Price.", ".
                                "description='".xEscSQL($description)."', ".
                                "en_description='".xEscSQL($en_description)."', ".
                                "ch_description='".xEscSQL($ch_description)."', ".
                                "in_stock=".(int)$in_stock.", ".
                                "customers_rating=".(float)$customers_rating.", ".
                                "brief_description='".xEscSQL($brief_description)."', ".
                                "en_brief_description='".xEscSQL($en_brief_description)."', ".
                                "ch_brief_description='".xEscSQL($ch_brief_description)."', ".
                                "list_price=".(double)$list_price.", ".
                                "product_code='".xToText(trim($product_code))."', ".
                                "sort_order=".(int)$sort_order.", ".
                                "date_modified='".xEscSQL(get_current_time())."', ".
                                "eproduct_filename='".xEscSQL($eproduct_filename)."', ".
                                "eproduct_available_days=".(int)$eproduct_available_days.", ".
                                "eproduct_download_times=".(int)$eproduct_download_times.",  ".
                                "weight=".(float)$weight.", meta_description='".xToText(trim($meta_description))."', ".
                                "weight=".(float)$weight.", en_meta_description='".xToText(trim($en_meta_description))."', ".
                                "weight=".(float)$weight.", ch_meta_description='".xToText(trim($ch_meta_description))."', ".
                                "meta_keywords='".xToText(trim($meta_keywords))."', en_meta_keywords='".xToText(trim($en_meta_keywords))."', ch_meta_keywords='".xToText(trim($ch_meta_keywords))."', free_shipping=".(int)$free_shipping.", ".
                                "min_order_amount = ".(int)$min_order_amount.", ".
                                "shipping_freight = ".(double)$shipping_freight.", ".
                                "title = '".xToText(trim($title))."', ".
                                "en_title = '".xToText(trim($en_title))."', ".
                                "ch_title = '".xToText(trim($ch_title))."', ".
                                "old_url = '".xToText(trim($old_url))."', ".
                                "en_old_url = '".xToText(trim($en_old_url))."', ".
                                "ch_old_url = '".xToText(trim($ch_old_url))."' ";

        if ($classID != null) $s .= ", classID = ".(int)$classID;

        $s .= " where productID=".(int)$productID;
        db_query($s);
		
		aliasGenerateAliasProd($productID, $_POST['alias'], $name, "upd");

        db_query("delete from ".CATEGORIY_PRODUCT_TABLE." where productID = ".(int)$productID." and categoryID = ".(int)$categoryID);

        if ($updateGCV == 1 && CONF_UPDATE_GCV == '1') //update goods count values for categories in case of regular file editing. do not update during import from excel
                update_psCount(1);
}




// *****************************************************************************
// Purpose        sets product file
// Inputs
// Remarks
// Returns
function SetProductFile( $productID, $eproduct_filename )
{
        db_query( "update ".PRODUCTS_TABLE." set eproduct_filename='".xEscSQL($eproduct_filename)."' ".
                        " where productID=".(int)$productID );

}



// *****************************************************************************
// Purpose        adds product
// Inputs
//                                $categoryID                        - category ID ( see CATEGORIES_TABLE )
//                                $name                                - name of product
//                                $Price                                - price of product
//                                $description                - product description
//                                $in_stock                        - stock counter
//                                $brief_description  - short product description
//                                $list_price                        - old price
//                                $product_code                - product code
//                                $sort_order                        - sort order
//                                $ProductIsProgram                - 1 if product is program,
//                                                                        0 otherwise
//                                $eproduct_filename                - program filename ( it is index of $_FILE variable )
//                                $eproduct_available_days        - program is available days
//                                                                        to download
//                                $eproduct_download_times        - attempt count download file
//                                $weight                        - product weigth
//                                $meta_description        - meta tag description
//                                $meta_keywords                - meta tag keywords
//                                $free_shipping                - 1 product has free shipping,
//                                                        0 - otherwise
//                                $min_order_amount        - minimum order amount
//                                $classID                - tax class ID
// Remarks
// Returns
function AddProduct(
                                $categoryID, $name, $en_name, $ch_name, $Price, $description, $en_description, $ch_description,
                                $in_stock,
                                $brief_description, $en_brief_description, $ch_brief_description, $list_price,
                                $product_code, $sort_order,
                                $ProductIsProgram, $eproduct_filename,
                                $eproduct_available_days, $eproduct_download_times,
                                $weight, $meta_description, $en_meta_description, $ch_meta_description, $meta_keywords, $en_meta_keywords, $ch_meta_keywords,
                                $free_shipping, $min_order_amount, $shipping_freight,
                                $classID, $title, $en_title, $ch_title, $old_url, $en_old_url, $ch_old_url, $header='', $en_header='', $ch_header='', $updateGCV = 1 )
{


			$catn = db_query('select name FROM  '.CATEGORIES_TABLE.' WHERE categoryID='.$categoryID);
					if($_Row = db_fetch_row($catn)) {
							$catname = $_Row['name'];
					} else {
						$catname = '';
					}
				  if(trim($header) == '') {
						$header = $catname.' '.$name;
					}
				  if(trim($title) == '') {
						$title = $catname.' '.$name." от PagodaBuild - поставки гранита и мрамора из Китая";
					}
		
				  if(trim($meta_description) == '') {
					$meta_description = "Предлагаем к продаже ". mb_strtolower($catname,'utf-8')." ".$name ." в слэбах и модульной плите. Поставки гранита и мрамора из Китая от камнеобрабатывающего завода PagodaBuild";
				  }
				  if(trim($meta_keywords) == '') {
						$meta_keywords = dkeywords($header.' '.$brief_description.' '.$description.' '.$meta_description);
				}


        // special symbol prepare
        if ( $free_shipping )
                $free_shipping = 1;
        else
                $free_shipping = 0;

        if ( $classID == null ) $classID = "NULL";

        if ( $min_order_amount == 0 ) $min_order_amount = 1;

        if ( !$ProductIsProgram ) $eproduct_filename = "";
		
		
		
		

        if ( $eproduct_filename != "" )
        {
                if ( trim($_FILES[$eproduct_filename]["name"]) != ""  )
                {
                        if ( $_FILES[$eproduct_filename]["size"]!=0 )
                                        $r = move_uploaded_file($_FILES[$eproduct_filename]["tmp_name"],
                                                "core/files/".$_FILES[$eproduct_filename]["name"]);
                        $eproduct_filename = trim($_FILES[$eproduct_filename]["name"]);
                        SetRightsToUploadedFile( "core/files/".$eproduct_filename );
                }
        }

        if ( trim($name) == "" ) $name = "?";
        db_query("INSERT INTO ".PRODUCTS_TABLE.
                " ( categoryID, name, en_name, ch_name, header, en_header, ch_header, description, en_description, ch_description,".
                "        customers_rating, Price, in_stock, ".
                "        customer_votes, items_sold, enabled, ".
                "        brief_description, en_brief_description, ch_brief_description, list_price, ".
                "        product_code, sort_order, date_added, ".
                "         eproduct_filename, eproduct_available_days, ".
                "         eproduct_download_times, ".
                "        weight, meta_description, en_meta_description, ch_meta_description, meta_keywords, en_meta_keywords, ch_meta_keywords, ".
                "        free_shipping, min_order_amount, shipping_freight, classID, title, en_title, ch_title, old_url, en_old_url, ch_old_url ".
                " ) ".
                " VALUES (".
                                (int)$categoryID.",'".
                                xToText(trim($name))."','".
                                xToText(trim($en_name))."','".
                                xToText(trim($ch_name))."','".
                                xToText(trim($header))."','".
                                xToText(trim($en_header))."','".
                                xToText(trim($ch_header))."','".
                                xEscSQL($description)."', '".
                                xEscSQL($en_description)."', '".
                                xEscSQL($ch_description)."', ".
                                "0, '".
                                (double)$Price."', ".
                                (int)$in_stock.", ".
                                " 0, 0, 1, '".
                                xEscSQL($brief_description)."', '".
                                xEscSQL($en_brief_description)."', '".
                                xEscSQL($ch_brief_description)."', '".
                                (double)$list_price."', '".
                                xToText(trim($product_code))."', ".
                                (int)$sort_order.", '".
                                xEscSQL(get_current_time())."',  '".
                                xEscSQL($eproduct_filename)."', ".
                                (int)$eproduct_available_days.", ".
                                (int)$eproduct_download_times.",  ".
                                (float)$weight.", ".
                                "'".xToText(trim($meta_description))."', ".
                                "'".xToText(trim($en_meta_description))."', ".
                                "'".xToText(trim($ch_meta_description))."', ".
                                "'".xToText(trim($meta_keywords))."', ".
                                "'".xToText(trim($en_meta_keywords))."', ".
                                "'".xToText(trim($ch_meta_keywords))."', ".
                                (int)$free_shipping.", ".
                                (int)$min_order_amount.", ".
                                (double)$shipping_freight.", ".
                                (int)$classID.", '".
                                xToText(trim($title))."', '".
                                xToText(trim($en_title))."', '".
                                xToText(trim($ch_title))."', '".
                                xToText(trim($old_url))."', '".
                                xToText(trim($en_old_url))."', '".
                                xToText(trim($ch_old_url))."' ".
                        ");" );
        $insert_id = db_insert_id();
		aliasGenerateAliasProd($insert_id, $_POST['alias'], $name, "add");
        if ( $updateGCV == 1 && CONF_UPDATE_GCV == '1') update_psCount(1);
        return $insert_id;
}


// *****************************************************************************
// Purpose        deletes product
// Inputs   $productID - product ID
// Remarks
// Returns        true if success, else false otherwise
function DeleteProduct($productID, $updateGCV = 1)
{
        $whereClause = " where productID=".(int)$productID;

        $q = db_query( "select itemID from ".SHOPPING_CART_ITEMS_TABLE." ".$whereClause );
        while( $row=db_fetch_row($q) )
                db_query( "delete from ".SHOPPING_CARTS_TABLE." where itemID=".(int)$row["itemID"] );

        // delete all items for this product
        db_query("update ".SHOPPING_CART_ITEMS_TABLE.
                " set productID=NULL ".$whereClause);

        // delete all product option values
        db_query("delete from ".PRODUCTS_OPTIONS_SET_TABLE.$whereClause);
        db_query("delete from ".PRODUCT_OPTIONS_VALUES_TABLE.$whereClause);

        // delete pictures
        DeleteThreePictures2($productID);

        // delete additional categories records
        db_query("delete from ".CATEGORIY_PRODUCT_TABLE.$whereClause);

        // delete discussions
        db_query("delete from ".DISCUSSIONS_TABLE.$whereClause);

        // delete special offer
        db_query("delete from ".SPECIAL_OFFERS_TABLE.$whereClause);

        // delete related items
        db_query("delete from ".RELATED_PRODUCTS_TABLE.$whereClause );
        db_query("delete from ".RELATED_PRODUCTS_TABLE." where Owner=".(int)$productID);

        // delete product
        db_query("delete from ".PRODUCTS_TABLE.$whereClause);


        if ( $updateGCV == 1 && CONF_UPDATE_GCV == '1') update_psCount(1);

        return true;
}


// *****************************************************************************
// Purpose        deletes all products of category
// Inputs   $categoryID - category ID
// Remarks
// Returns        true if success, else false otherwise
function DeleteAllProductsOfThisCategory($categoryID)
{
        $q=db_query("select productID from ".PRODUCTS_TABLE.
                        " where categoryID=".(int)$categoryID);
        $res=true;
        while( $r=db_fetch_row( $q ) )
        {
                if ( !DeleteProduct( $r["productID"], 0 ) )
                        $res = false;
        }

        if ( CONF_UPDATE_GCV == '1') update_psCount(1);

        return $res;
}


// *****************************************************************************
// Purpose        gets extra parametrs
// Inputs   $productID - product ID
// Remarks
// Returns        array of value extraparametrs
//                                each item of this array has next struture
//                                        first type "option_type" = 0
//                                                "name"                                        - parametr name
//                                                "option_value"                        - value
//                                                "option_type"                        - 0
//                                        second type "option_type" = 1
//                                                "name"                                        - parametr name
//                                                "option_show_times"                - how times does show in client side this
//                                                                                                parametr to select
//                                                "variantID_default"                - variant ID by default
//                                                "values_to_select"                - array of variant value to select
//                                                        each item of "values_to_select" array has next structure
//                                                                "variantID"                        - variant ID
//                                                                "price_surplus"                - to added to price
//                                                                "option_value"                - value
function GetExtraParametrs( $productID ){
 global $langPref;
        if(!is_array($productID)){

                $ProductIDs = array($productID);
                $IsProducts = false;
        }elseif(count($productID)) {

                $ProductIDs = &$productID;
                $IsProducts = true;
        }else {

                return array();
        }

        $ProductsExtras = array();
        $sql = 'select povt.productID,pot.optionID,pot.name,pot.en_name,pot.ch_name, povt.option_value, povt.en_option_value, povt.ch_option_value, povt.option_type,povt.option_show_times, povt.variantID, povt.optionID
                FROM ?#PRODUCT_OPTIONS_VALUES_TABLE as povt LEFT JOIN  ?#PRODUCT_OPTIONS_TABLE as pot ON pot.optionID=povt.optionID
                WHERE povt.productID IN (?@) ORDER BY pot.sort_order, pot.name
        ';
        $Result = db_phquery($sql, $ProductIDs);

        while ($_Row = db_fetch_assoc($Result)) {
                $_Row;
                $b=null;
				
				$_Row['name'] = $_Row[$langPref.'name'];
				
		if((int)$_Row['optionID'] > 0 && trim($_Row['name']) != '') {
                if (($_Row['option_type']==0 || $_Row['option_type']==NULL) && strlen( trim($_Row['option_value']))>0){
					if(trim($_Row[$langPref.'option_value']) != '' && trim($_Row['name']) != '') {
                        $ProductsExtras[$_Row['productID']][] = array(
                                'option_type' => $_Row['option_type'],
                                'name' => $_Row['name'],
                                'option_value' => $_Row[$langPref.'option_value'],
                                'link' => strtolower($_Row['en_option_value'])
                        );
					}
                }
/**
* @features "Extra options values"
* @state begin
*/
                else if ( $_Row['option_type']==1 ){

                        //fetch all option values variants
                        $sql = 'select povvt.option_value, povvt.en_option_value, povvt.ch_option_value, povvt.variantID, post.price_surplus
                                FROM '.PRODUCTS_OPTIONS_SET_TABLE.' as post
                                LEFT JOIN '.PRODUCTS_OPTIONS_VALUES_VARIANTS_TABLE.' as povvt
                                ON povvt.variantID=post.variantID
                                WHERE povvt.optionID='.$_Row['optionID'].' AND post.productID='.$_Row['productID'].' AND povvt.optionID='.$_Row['optionID'].'
                                ORDER BY povvt.sort_order, povvt.option_value
                        ';
                        $q2=db_query($sql);
                        $_Row['values_to_select']=array();
                        $i=0;
						$addToP = 1;
                        while( $_Rowue = db_fetch_assoc($q2)  ){
							
                                $_Row['values_to_select'][$i]=array();
                                $_Row['values_to_select'][$i]['option_value'] = $_Rowue[$langPref.'option_value'];
                                $_Row['values_to_select'][$i]['variantID'] = $_Rowue['variantID'];
                                $_Row['values_to_select'][$i]['link'] = strtolower($_Rowue['en_option_value']);
                                // if ( $_Rowue['price_surplus'] > 0 )$_Row['values_to_select'][$i]['option_value'] .= ' (+ '.show_price($_Rowue['price_surplus']).')';
                                // elseif($_Rowue['price_surplus'] < 0 )$_Row['values_to_select'][$i]['option_value'] .= ' (- '.show_price(-$_Rowue['price_surplus']).')';

                                $_Row['values_to_select'][$i]['option_valueWithOutPrice'] = $_Rowue['option_value'];
                                $_Row['values_to_select'][$i]['price_surplus'] = show_priceWithOutUnit($_Rowue['price_surplus']);
                                $_Row['values_to_select'][$i]['variantID']=$_Rowue['variantID'];
                                $i++;
							
                        }
                        $_Row['values_to_select_count'] = count($_Row['values_to_select']);
                        $ProductsExtras[$_Row['productID']][] = $_Row;
                }
                /**
* @features "Extra options values"
* @state end
*/
        }
        }
        if(!$IsProducts){

                if(!count($ProductsExtras))return array();
                else {
                        return $ProductsExtras[$productID];
                }
        }
        return $ProductsExtras;
}


function _setPictures( & $product )
{
        if ( isset($product['default_picture'])&&!is_null($product['default_picture'])&&isset($product['productID']) )
        {
                $pictire=db_query("select filename, thumbnail, enlarged from ".
                                        PRODUCT_PICTURES." where photoID=".(int)$product["default_picture"] );
                $pictire_row=db_fetch_row($pictire);
                $product['picture'] =   file_exists('data/small/'.$pictire_row['filename'])?$pictire_row['filename']:0;
                $product['thumbnail']=  file_exists('data/medium/'.$pictire_row['thumbnail'])?$pictire_row['thumbnail']:0;
                $product['big_picture']=file_exists('data/big/'.$pictire_row['enlarged'])?$pictire_row['enlarged']:0;
        }
}


function GetProductInSubCategories( $callBackParam, &$count_row, $navigatorParams = null )
{

        if ( $navigatorParams != null )
        {
                $offset                        = $navigatorParams["offset"];
                $CountRowOnPage        = $navigatorParams["CountRowOnPage"];
        }
        else
        {
                $offset = 0;
                $CountRowOnPage = 0;
        }

        $categoryID        = $callBackParam["categoryID"];
        $subCategoryIDArray = catGetSubCategories( $categoryID );
        $cond = "";
        foreach( $subCategoryIDArray as $subCategoryID )
        {
                if ( $cond != "" )
                        $cond .= " OR categoryID=".(int)$subCategoryID;
                else
                        $cond .= " categoryID=".(int)$subCategoryID." ";
        }
        $whereClause = "";
        if ( $cond != "" )
                $whereClause = " where ".$cond;

        $result = array();
        if ( $whereClause == "" )
        {
                $count_row = 0;
                return $result;
        }

        $q=db_query("select categoryID, name, brief_description, ".
                         " customers_rating, Price, in_stock, ".
                        " customer_votes, list_price, ".
                        " productID, default_picture, sort_order from ".PRODUCTS_TABLE.
                        " ".$whereClause." order by ".CONF_DEFAULT_SORT_ORDER);
        $i=0;
        while( $row=db_fetch_row($q) )
        {
                if ( ($i >= $offset && $i < $offset + $CountRowOnPage) ||
                                                 $navigatorParams == null  )
                {
                        $row["PriceWithUnit"]                = show_price($row["Price"]);
                        $row["list_priceWithUnit"]         = show_price($row["list_price"]);
                        // you save (value)
                        $row["SavePrice"]                = show_price($row["list_price"]-$row["Price"]);

                        // you save (%)
                        if ($row["list_price"])
                                $row["SavePricePercent"] = ceil(((($row["list_price"]-$row["Price"])/$row["list_price"])*100));

                        _setPictures( $row );

                        $row["product_extra"]=GetExtraParametrs($row["productID"]);
                        $row["PriceWithOutUnit"]= show_priceWithOutUnit( $row["Price"] );
                        $result[] = $row;
                }
                $i++;
        }
        $count_row = $i;
        return $result;
}


// *****************************************************************************
// Purpose        gets all products by categoryID
// Inputs             $callBackParam item
//                        "categoryID"
//                        "fullFlag"
// Remarks
// Returns
function prdGetProductByCategory( $callBackParam, &$count_row, $navigatorParams = null )
{

        if ( $navigatorParams != null )
        {
                $offset                        = $navigatorParams["offset"];
                $CountRowOnPage        = $navigatorParams["CountRowOnPage"];
        }
        else
        {
                $offset = 0;
                $CountRowOnPage = 0;
        }

        $result = array();

        $categoryID        = $callBackParam["categoryID"];
        $fullFlag        = $callBackParam["fullFlag"];
        if ( $fullFlag )
        {
                $conditions = array( " categoryID=".(int)$categoryID." " );
                $q = db_query("select productID from ".
                                CATEGORIY_PRODUCT_TABLE." where  categoryID=".(int)$categoryID);
                while( $products = db_fetch_row( $q ) )
                        $conditions[] = " productID=".(int)$products[0];

                $data = array();
                foreach( $conditions as $cond )
                {
                        $q=db_query("select categoryID, name, brief_description, ".
                                 " customers_rating, Price, in_stock, ".
                                " customer_votes, list_price, ".
                                " productID, default_picture, sort_order, items_sold, enabled, product_code from ".PRODUCTS_TABLE.
                                " where ".$cond." order by ".CONF_DEFAULT_SORT_ORDER);
                        while( $row = db_fetch_row($q) )
                        {
                                $row["PriceWithUnit"]                = show_price($row["Price"]);
                                $row["list_priceWithUnit"]         = show_price($row["list_price"]);
                                // you save (value)
                                $row["SavePrice"]                = show_price($row["list_price"]-$row["Price"]);

                                // you save (%)
                                if ($row["list_price"])
                                        $row["SavePricePercent"] = ceil(((($row["list_price"]-$row["Price"])/$row["list_price"])*100));
                                _setPictures( $row );
                                $row["product_extra"]=GetExtraParametrs($row["productID"]);
                                $row["product_extra_count"]=count($row["product_extra"]);
                                $row["PriceWithOutUnit"]= show_priceWithOutUnit( $row["Price"] );
                                $data[] = $row;
                        }
                }

                function _compare( $row1, $row2 )
                {
                         if ( (int)$row1["sort_order"] == (int)$row2["sort_order"] )
                                return 0;
                         return ((int)$row1["sort_order"] < (int)$row2["sort_order"]) ? -1 : 1;
                }

                usort($data, "_compare");

                $result = array();
                $i = 0;
                $ccdata = count($data);
                for ($s=0; $s<$ccdata; $s++)
                {
                        if ( ($i >= $offset && $i < $offset + $CountRowOnPage) ||
                                        $navigatorParams == null )
                                $result[] = $data[$s];
                        $i++;
                }
                $count_row = $i;
                return $result;
        }
        else
        {
                $q=db_query("select categoryID, name, brief_description, ".
                                " customers_rating, Price, in_stock, ".
                                " customer_votes, list_price, ".
                                " productID, default_picture, sort_order, items_sold, enabled, product_code from ".PRODUCTS_TABLE.
                                " where categoryID=".(int)$categoryID." order by ".CONF_DEFAULT_SORT_ORDER);
                $i=0;
                while( $row=db_fetch_row($q) )
                {
                        if ( ($i >= $offset && $i < $offset + $CountRowOnPage) ||
                                $navigatorParams == null  )
                                $result[] = $row;
                        $i++;
                }
                $count_row = $i;
                return $result;
        }
}




function _getConditionWithCategoryConjWithSubCategories( $condition, $categoryID ) //fetch products from current category and all its subcategories
{
        $new_condition = "";
        $tempcond = "";

        $categoryID_Array = catGetSubCategories( $categoryID );
        $categoryID_Array[] = (int)$categoryID;
	
        foreach( $categoryID_Array as $catID )
        {
                if ( $new_condition != "" )
                        $new_condition .= " OR ";

                $new_condition .= _getConditionWithCategoryConj($tempcond, $catID);

        }
        if ( $condition == "" ) return $new_condition;
        else return $condition." AND (".$new_condition.")";
}


function _getConditionWithCategoryConj( $condition, $categoryID ) //fetch products from current category
{
        $category_condition = "";
        $q = db_query("select productID from ".
                                CATEGORIY_PRODUCT_TABLE." where categoryID=".(int)$categoryID);
        $icounter = 0;
        while( $product = db_fetch_row( $q ) )
        {
                if ( $icounter==0 )
                $category_condition .= " productID IN (";
                if ( $icounter>0 )
                $category_condition .= ",";
                $category_condition .= (int)$product[0];
                $icounter++;
        }
        if ( $icounter>0 ) $category_condition .= ")";

        if ( $condition == "" )
        {
                if ( $category_condition == "" )
                        return "categoryID=".(int)$categoryID;
                else
                        return "(".$category_condition." OR categoryID=".(int)$categoryID.")";
        }
        else
        {
                if ( $category_condition == "" )
                        return $condition." AND categoryID=".(int)$categoryID;
                else
                        return "(".$condition." AND (".$category_condition." OR categoryID=".(int)$categoryID."))";
        }      
}


// *****************************************************************************
// Purpose
// Inputs
//                                $productID - product ID
//                                $template  - array of item
//                                        "optionID"        - option ID
//                                        "value"                - value or variant ID
// Remarks
// Returns        returns true if product matches to extra parametr template
//                        false otherwise
function _testExtraParametrsTemplate( $productID, &$template ){

        // get category ID
        $categoryID = $template["categoryID"];

        foreach( $template as $key => $item ){

                if( !isset($item["optionID"]) ) continue;

                if((string)$key == "categoryID" ) continue;

                // get value to search
                if ( $item['set_arbitrarily'] == 1 ){

                        $valueFromForm = $item["value"];
                }else{

                        if ( (int)$item["value"] == 0 ) continue;

                        if(!isset($template[$key]['__option_value_from_db'])){

                                $SQL = 'select option_value FROM ?#PRODUCTS_OPTIONS_VALUES_VARIANTS_TABLE WHERE variantID=?
                                ';
                                $option_value = db_fetch_assoc(db_phquery($SQL, (int)$item['value']));
                                $template[$key]['__option_value_from_db'] = $option_value['option_value'];
                        }
                        $valueFromForm = $template[$key]['__option_value_from_db'];
                }

                // get option value
                $SQL = 'select option_value, option_type FROM ?#PRODUCT_OPTIONS_VALUES_TABLE WHERE optionID=? AND productID=?
                ';
                $q = db_phquery($SQL,(int)$item['optionID'],(int)$productID);

                if(!($row=db_fetch_row($q))){

                        if ( trim($valueFromForm) == '' ) continue;
                        else return false;
                }

                $option_value = $row['option_value'];
                $option_type        = $row['option_type'];
                $valueFromDataBase = array();

                if ( $option_type == 0 ){

                        $valueFromDataBase[] = $option_value;
                }else{

                        $SQL = 'select povv.option_value FROM ?#PRODUCTS_OPTIONS_SET_TABLE as pos
                                LEFT JOIN ?#PRODUCTS_OPTIONS_VALUES_VARIANTS_TABLE as povv ON pos.variantID=povv.variantID WHERE pos.optionID=? AND pos.productID=?
                        ';
                        $Result = db_phquery($SQL,(int)$item["optionID"], (int)$productID);
                        while ($Row = db_fetch_assoc($Result)){

                                $valueFromDataBase[] = $Row['option_value'];
                        }
                }

                if ( trim($valueFromForm) != '' ){

                        $existFlag = false;
                        $vcount = count($valueFromDataBase);
                        for ($v=0; $v<$vcount; $v++) {
                                if(strstr(strtolower((string)trim($valueFromDataBase[$v])),strtolower((string)trim($valueFromForm)))){
                                        $existFlag = true;
                                        break;
                                }
                        }
                        if ( !$existFlag ) return false;
                }
        }
        return true;
}




function _deletePercentSymbol( &$str )
{
        $str = str_replace( "%", "", $str );
        return $str;
}


function prdSearchProductByTemplateAdmin($callBackParam, &$count_row, $navigatorParams = null )
{
        // navigator params
        if ( $navigatorParams != null )
        {
                $offset                        = xEscSQL($navigatorParams["offset"]);
                $CountRowOnPage        = xEscSQL($navigatorParams["CountRowOnPage"]);
        }
        else
        {
                $offset = 0;
                $CountRowOnPage = 0;
        }

        if ( isset($callBackParam["extraParametrsTemplate"]) ){

                $replicantExtraParametersTpl = $callBackParam["extraParametrsTemplate"];
        }
        // special symbol prepare
        if ( isset($callBackParam["search_simple"]) )
        {
/*                for( $i=0; $i<count($callBackParam["search_simple"]); $i++ )
                {
                        $callBackParam["search_simple"][$i] = $callBackParam["search_simple"][$i];
                }*/
                _deletePercentSymbol( $callBackParam["search_simple"] );
        }
        if ( isset($callBackParam["name"]) )
        {
                for( $i=0; $i<count($callBackParam["name"]); $i++ )
                        $callBackParam["name"][$i] = xToText(trim($callBackParam["name"][$i]) );
                _deletePercentSymbol( $callBackParam["name"][$i] );
        }
        if ( isset($callBackParam["product_code"]) )
        {
                for( $i=0; $i<count($callBackParam["product_code"]); $i++ )
                {
                        $callBackParam["product_code"][$i] = xToText(trim($callBackParam["product_code"][$i] ));
                }
                _deletePercentSymbol( $callBackParam["product_code"] );
        }

        if ( isset($callBackParam["extraParametrsTemplate"]) )
        {
                foreach( $callBackParam["extraParametrsTemplate"] as $key => $value )
                {
                        if ( is_int($key) )
                        {
                                $callBackParam["extraParametrsTemplate"][$key] = xEscSQL(trim($callBackParam["extraParametrsTemplate"][$key]) );
                                _deletePercentSymbol( $callBackParam["extraParametrsTemplate"][$key] );
                        }
                }
        }


        $where_clause = "";

        if ( isset($callBackParam["search_simple"]) )
        {
                if (!count($callBackParam["search_simple"])) //empty array
                {
                        $where_clause = " where 0";
                }
                else //search array is not empty
                {
                        $sscount = count($callBackParam["search_simple"]);
                        for ($n=0; $n<$sscount; $n++)
                        {
                                if ( $where_clause != "" ) $where_clause .= " AND ";
                                $where_clause .= " ( LOWER(name) LIKE '%".xToText(trim(strtolower($callBackParam["search_simple"][$n])))."%' OR ".
                                                 "   LOWER(description) LIKE '%".xEscSQL(trim(strtolower($callBackParam["search_simple"][$n])))."%' OR ".
                                                 "   LOWER(product_code) LIKE '%".xEscSQL(trim(strtolower($callBackParam["search_simple"][$n])))."%' OR ".
                                                 "   LOWER(brief_description) LIKE '%".xEscSQL(trim(strtolower($callBackParam["search_simple"][$n])))."%' ) ";
                        }

                        if ( $where_clause != "" )
                        {
                                $where_clause = " where categoryID>1 and enabled=1 and ".$where_clause;
                        }
                        else
                        {
                                $where_clause = " where categoryID>1 and enabled=1";
                        }
						

                }

        }
        else
        {

                // "enabled" parameter
                if ( isset($callBackParam["enabled"]) )
                {
                        if ( $where_clause != "" )
                                $where_clause .= " AND ";
                        $where_clause.=" enabled=".(int)$callBackParam["enabled"];
                }

                // take into "name" parameter
                if ( isset($callBackParam["name"]) )
                {
                        foreach( $callBackParam["name"] as $name )
                                if (strlen($name)>0)
                                {
                                        if ( $where_clause != "" )
                                                $where_clause .= " AND ";
                                         $where_clause .= " LOWER(name) LIKE '%".xToText(trim(strtolower($name)))."%' ";
                                }
                }

                // take into "product_code" parameter
                if ( isset($callBackParam["product_code"]) )
                {
                        foreach( $callBackParam["product_code"] as $product_code )
                        {
                                if ( $where_clause != "" )
                                        $where_clause .= " AND ";
                                $where_clause .= " LOWER(product_code) LIKE '%".xToText(trim(strtolower($product_code)))."%' ";
                        }
                }

                // take into "price" parameter
                if ( isset($callBackParam["price"]) )
                {
                        $price = $callBackParam["price"];

                        if ( trim($price["from"]) != "" && $price["from"] != null )
                        {
                                if ( $where_clause != "" )
                                        $where_clause .= " AND ";
                                $from        = ConvertPriceToUniversalUnit( $price["from"] );
                                $where_clause .= " Price>=".(double)$from." ";
                        }
                        if ( trim($price["to"]) != "" && $price["to"] != null )
                        {
                                if ( $where_clause != "" )
                                        $where_clause .= " AND ";
                                $to                = ConvertPriceToUniversalUnit( $price["to"] );
                                $where_clause .= " Price<=".(double)$to." ";
                        }
                }
		
       
        // categoryID
                if ( isset($callBackParam["categoryID"]) )
                {
                        $searchInSubcategories = false;
                        if ( isset($callBackParam["searchInSubcategories"]) )
                        {
                                if ( $callBackParam["searchInSubcategories"] )
                                        $searchInSubcategories = true;
                                else
                                        $searchInSubcategories = false;
                        }

                        if ( $searchInSubcategories )
                        {
                                $where_clause = _getConditionWithCategoryConjWithSubCategories( $where_clause,
                                                                                        $callBackParam["categoryID"] );
                        }
                        else
                        {
                                $where_clause = _getConditionWithCategoryConj( $where_clause,
                                                                                        $callBackParam["categoryID"] );
                        }
                }

                if ( $where_clause != "" )
                        $where_clause = "where ".$where_clause;

        }
        

		
        $order_by_clause = "order by ".CONF_DEFAULT_SORT_ORDER;

        if ( isset($callBackParam["sort"]) )
        {
                if (        $callBackParam["sort"] == "categoryID"                        ||
                                $callBackParam["sort"] == "name"                                ||
                                $callBackParam["sort"] == "brief_description"        ||
                                $callBackParam["sort"] == "in_stock"                        ||
                                $callBackParam["sort"] == "Price"                                ||
                                $callBackParam["sort"] == "customer_votes"                ||
                                $callBackParam["sort"] == "customers_rating"        ||
                                $callBackParam["sort"] == "list_price"                        ||
                                $callBackParam["sort"] == "sort_order"                        ||
                                $callBackParam["sort"] == "items_sold"                        ||
                                $callBackParam["sort"] == "product_code"                ||
                                $callBackParam["sort"] == "shipping_freight"        ||
                                $callBackParam["sort"] == "viewed_times" )
                {
                        $order_by_clause = " order by ".xEscSQL($callBackParam["sort"])." ASC ";
                        if (  isset($callBackParam["direction"]) )
                                if (  $callBackParam["direction"] == "DESC" )
                                        $order_by_clause = " order by ".xEscSQL($callBackParam["sort"])." DESC ";
                }
        }

        $sqlQueryCount = "select count(*) from ".PRODUCTS_TABLE." ".$where_clause;
        $q = db_query( $sqlQueryCount );
        $products_count = db_fetch_row($q);
        $products_count = $products_count[0];
        $limit_clause= (isset($callBackParam["extraParametrsTemplate"]) || !$CountRowOnPage)?"":" LIMIT ".$offset.",".$CountRowOnPage;
        $sqlQuery = "select categoryID, name, brief_description, ".
                                 " customers_rating, Price, in_stock, ".
                                " customer_votes, list_price, ".
                                " productID, default_picture, sort_order, items_sold, enabled, ".
                                " product_code, description, shipping_freight, viewed_times, min_order_amount from ".PRODUCTS_TABLE." ".
                                $where_clause." ".$order_by_clause.$limit_clause;

        $q = db_query( $sqlQuery );
        $result = array();
        $i = 0;

        if ($offset >= 0 && $offset <= $products_count )
        {
                while( $row = db_fetch_row($q) )
                {

                        if ( isset($callBackParam["extraParametrsTemplate"]) ){

                                // take into "extra" parametrs
                                $testResult = _testExtraParametrsTemplate( $row["productID"], $replicantExtraParametersTpl );
                                if ( !$testResult ) continue;
                        }

                        if ( (($i >= $offset || !isset($callBackParam["extraParametrsTemplate"])) && $i < $offset + $CountRowOnPage) ||
                                        $navigatorParams == null  )
                        {
                                $row["PriceWithUnit"]     = show_price($row["Price"]);
                                $row["list_priceWithUnit"]= show_price($row["list_price"]);
                                // you save (value)
                                $row["SavePrice"]         = show_price($row["list_price"]-$row["Price"]);

                                // you save (%)
                                if ($row["list_price"]) $row["SavePricePercent"] = ceil(((($row["list_price"]-$row["Price"])/$row["list_price"])*100));
                                _setPictures( $row );
                                $row["product_extra"]     = GetExtraParametrs( $row["productID"] );
                                $row["product_extra_count"]  = count($row["product_extra"]);
                                $row["PriceWithOutUnit"]  = show_priceWithOutUnit( $row["Price"] );
                                if ( ((double)$row["shipping_freight"]) > 0 ) $row["shipping_freightUC"] = show_price( $row["shipping_freight"] );
                                $row["name"]              = $row["name"];
                                $row["description"]       = $row["description"];
                                $row["brief_description"] = $row["brief_description"];
                                $row["product_code"]      = $row["product_code"];
                                $row["viewed_times"]      = $row["viewed_times"];
                                $row["items_sold"]        = $row["items_sold"];
                                $result[] = $row;
                        }
                        $i++;
                }
        }
        $count_row = isset($callBackParam["extraParametrsTemplate"])?$i:$products_count;
        return $result;
}

// *****************************************************************************
// Purpose        gets all products by categoryID
// Inputs             $callBackParam item
//                                        "search_simple"                                - string search simple
//                                        "sort"                                        - column name to sort
//                                        "direction"                                - sort direction DESC - by descending,
//                                                                                                by ascending otherwise
//                                        "searchInSubcategories" - if true function searches
//                                                product in subcategories, otherwise it does not
//                                        "searchInEnabledSubcategories"        - this parametr is actual when
//                                                                                        "searchInSubcategories" parametr is specified
//                                                                                        if true this function take in mind enabled categories only
//                                        "categoryID"        - is not set or category ID to be searched
//                                        "name"                        - array of name template
//                                        "product_code"                - array of product code template
//                                        "price"                        -
//                                                                array of item
//                                                                        "from"        - down price range
//                                                                        "to"        - up price range
//                                        "enabled"                - value of column "enabled"
//                                                                        in database
//                                        "extraParametrsTemplate"
// Remarks
// Returns
function prdSearchProductByTemplate($callBackParam, &$count_row, $navigatorParams = null )
{
	global $langPref;
        // navigator params
        if ( $navigatorParams != null )
        {
                $offset                        = xEscSQL($navigatorParams["offset"]);
                $CountRowOnPage        = xEscSQL($navigatorParams["CountRowOnPage"]);
        }
        else
        {
                $offset = 0;
                $CountRowOnPage = 0;
        }

        if ( isset($callBackParam["extraParametrsTemplate"]) ){

                $replicantExtraParametersTpl = $callBackParam["extraParametrsTemplate"];
        }
        // special symbol prepare
        if ( isset($callBackParam["search_simple"]) )
        {
/*                for( $i=0; $i<count($callBackParam["search_simple"]); $i++ )
                {
                        $callBackParam["search_simple"][$i] = $callBackParam["search_simple"][$i];
                }*/
                _deletePercentSymbol( $callBackParam["search_simple"] );
        }
        if ( isset($callBackParam["name"]) )
        {
                for( $i=0; $i<count($callBackParam["name"]); $i++ )
                        $callBackParam["name"][$i] = xToText(trim($callBackParam["name"][$i]) );
                _deletePercentSymbol( $callBackParam["name"][$i] );
        }
        if ( isset($callBackParam["product_code"]) )
        {
                for( $i=0; $i<count($callBackParam["product_code"]); $i++ )
                {
                        $callBackParam["product_code"][$i] = xToText(trim($callBackParam["product_code"][$i] ));
                }
                _deletePercentSymbol( $callBackParam["product_code"] );
        }

        if ( isset($callBackParam["extraParametrsTemplate"]) )
        {
                foreach( $callBackParam["extraParametrsTemplate"] as $key => $value )
                {
                        if ( is_int($key) )
                        {
                                $callBackParam["extraParametrsTemplate"][$key] = xEscSQL(trim($callBackParam["extraParametrsTemplate"][$key]) );
                                _deletePercentSymbol( $callBackParam["extraParametrsTemplate"][$key] );
                        }
                }
        }


        $where_clause = "";

        if ( isset($callBackParam["search_simple"]) )
        {
                if (!count($callBackParam["search_simple"])) //empty array
                {
                        $where_clause = " where 0";
                }
                else //search array is not empty
                {
                        $sscount = count($callBackParam["search_simple"]);
                        for ($n=0; $n<$sscount; $n++)
                        {
                                if ( $where_clause != "" ) $where_clause .= " AND ";
                                $where_clause .= " ( LOWER(name) LIKE '%".xToText(trim(strtolower($callBackParam["search_simple"][$n])))."%' OR ".
                                                 "   LOWER(description) LIKE '%".xEscSQL(trim(strtolower($callBackParam["search_simple"][$n])))."%' OR ".
                                                 "   LOWER(product_code) LIKE '%".xEscSQL(trim(strtolower($callBackParam["search_simple"][$n])))."%' OR ".
                                                 "   LOWER(brief_description) LIKE '%".xEscSQL(trim(strtolower($callBackParam["search_simple"][$n])))."%' ) ";
                        }

                        if ( $where_clause != "" )
                        {
                                $where_clause = " where categoryID>1 and enabled=1 and ".$where_clause;
                        }
                        else
                        {
                                $where_clause = " where categoryID>1 and enabled=1";
                        }
						
		if(CONF_CHECKSTOCK && CONF_SHOW_NULL_STOCK){
            if ( $where_clause != "" )
                $where_clause .= " AND in_stock>0 ";
            else
                $where_clause = "where in_stock>0 ";		
        }
                }

        }
        else
        {

                // "enabled" parameter
                if ( isset($callBackParam["enabled"]) )
                {
                        if ( $where_clause != "" )
                                $where_clause .= " AND ";
                        $where_clause.=" enabled=".(int)$callBackParam["enabled"];
                }

                // take into "name" parameter
                if ( isset($callBackParam["name"]) )
                {
                        foreach( $callBackParam["name"] as $name )
                                if (strlen($name)>0)
                                {
                                        if ( $where_clause != "" )
                                                $where_clause .= " AND ";
                                         $where_clause .= " LOWER(name) LIKE '%".xToText(trim(strtolower($name)))."%' ";
                                }
                }

                // take into "product_code" parameter
                if ( isset($callBackParam["product_code"]) )
                {
                        foreach( $callBackParam["product_code"] as $product_code )
                        {
                                if ( $where_clause != "" )
                                        $where_clause .= " AND ";
                                $where_clause .= " LOWER(product_code) LIKE '%".xToText(trim(strtolower($product_code)))."%' ";
                        }
                }

                // take into "price" parameter
                if ( isset($callBackParam["price"]) )
                {
                        $price = $callBackParam["price"];

                        if ( trim($price["from"]) != "" && $price["from"] != null )
                        {
                                if ( $where_clause != "" )
                                        $where_clause .= " AND ";
                                $from        = ConvertPriceToUniversalUnit( $price["from"] );
                                $where_clause .= " Price>=".(double)$from." ";
                        }
                        if ( trim($price["to"]) != "" && $price["to"] != null )
                        {
                                if ( $where_clause != "" )
                                        $where_clause .= " AND ";
                                $to                = ConvertPriceToUniversalUnit( $price["to"] );
                                $where_clause .= " Price<=".(double)$to." ";
                        }
                }
		
		if(CONF_CHECKSTOCK && CONF_SHOW_NULL_STOCK){
            if ( $where_clause != "" )
                $where_clause .= " AND in_stock>0 ";
            else
                $where_clause = "where in_stock>0 ";		
        }
        
        // categoryID
                if ( isset($callBackParam["categoryID"]) )
                {
                        $searchInSubcategories = false;
                        if ( isset($callBackParam["searchInSubcategories"]) )
                        {
                                if ( $callBackParam["searchInSubcategories"] )
                                        $searchInSubcategories = true;
                                else
                                        $searchInSubcategories = false;
                        }

                        if ( $searchInSubcategories )
                        {
                                $where_clause = _getConditionWithCategoryConjWithSubCategories( $where_clause,
                                                                                        $callBackParam["categoryID"] );
                        }
                        else
                        {
                                $where_clause = _getConditionWithCategoryConj( $where_clause,
                                                                                        $callBackParam["categoryID"] );
                        }
                }

                if ( $where_clause != "" )
                        $where_clause = "where ".$where_clause;

        }
        

		
        $order_by_clause = "order by ".CONF_DEFAULT_SORT_ORDER."";

        if ( isset($callBackParam["sort"]) )
        {
                if (        $callBackParam["sort"] == "categoryID"                        ||
                                $callBackParam["sort"] == "name"                                ||
                                $callBackParam["sort"] == "brief_description"        ||
                                $callBackParam["sort"] == "in_stock"                        ||
                                $callBackParam["sort"] == "Price"                                ||
                                $callBackParam["sort"] == "customer_votes"                ||
                                $callBackParam["sort"] == "customers_rating"        ||
                                $callBackParam["sort"] == "list_price"                        ||
                                $callBackParam["sort"] == "sort_order"                        ||
                                $callBackParam["sort"] == "items_sold"                        ||
                                $callBackParam["sort"] == "product_code"                ||
                                $callBackParam["sort"] == "shipping_freight"        ||
                                $callBackParam["sort"] == "viewed_times" )
                {
                        $order_by_clause = " order by ".xEscSQL($callBackParam["sort"])." ASC ";
                        if (  isset($callBackParam["direction"]) )
                                if (  $callBackParam["direction"] == "DESC" )
                                        $order_by_clause = " order by ".xEscSQL($callBackParam["sort"])." DESC ";
                }
        }

        $sqlQueryCount = "select count(*) from ".PRODUCTS_TABLE." ".$where_clause;
        $q = db_query( $sqlQueryCount );
        $products_count = db_fetch_row($q);
        $products_count = $products_count[0];
        $limit_clause= (isset($callBackParam["extraParametrsTemplate"]) || !$CountRowOnPage)?"":" LIMIT ".$offset.",".$CountRowOnPage;
        $sqlQuery = "select categoryID, name, en_name, ch_name, brief_description, en_brief_description, ch_brief_description, ".
                                 " customers_rating, Price, in_stock, ".
                                " customer_votes, list_price, ".
                                " productID, default_picture, sort_order, items_sold, enabled, ".
                                " product_code, description, en_description, ch_description, shipping_freight, viewed_times, min_order_amount, alias from ".PRODUCTS_TABLE." ".
                                $where_clause." ".$order_by_clause.$limit_clause;

        $q = db_query( $sqlQuery );
        $result = array();
        $i = 0;

        if ($offset >= 0 && $offset <= $products_count )
        {
                while( $row = db_fetch_row($q) )
                {

                        if ( isset($callBackParam["extraParametrsTemplate"]) ){

                                // take into "extra" parametrs
                                $testResult = _testExtraParametrsTemplate( $row["productID"], $replicantExtraParametersTpl );
                                if ( !$testResult ) continue;
                        }

                        if ( (($i >= $offset || !isset($callBackParam["extraParametrsTemplate"])) && $i < $offset + $CountRowOnPage) ||
                                        $navigatorParams == null  )
                        {
                                $row["PriceWithUnit"]     = show_price($row["Price"]);
                                $row["list_priceWithUnit"]= show_price($row["list_price"]);
                                // you save (value)
                                $row["SavePrice"]         = show_price($row["list_price"]-$row["Price"]);

								//get url
								$row['url'] = m_Prod($row);
								
                                // you save (%)
                                if ($row["list_price"]) $row["SavePricePercent"] = ceil(((($row["list_price"]-$row["Price"])/$row["list_price"])*100));
                                _setPictures( $row );
                                $row["product_extra"]     = GetExtraParametrs( $row["productID"] );
                                $row["product_extra_count"]  = count($row["product_extra"]);
                                $row["PriceWithOutUnit"]  = show_priceWithOutUnit( $row["Price"] );
                                if ( ((double)$row["shipping_freight"]) > 0 ) $row["shipping_freightUC"] = show_price( $row["shipping_freight"] );
								
								
					if(trim($row[$langPref.'name']) != '') {
						$row['name'] = $row[$langPref.'name'];
					} else {
						$row["name"]  = $row["name"];
					}
					
					
						$row["brief_description"]  = $row["brief_description"];
					
						$row['description'] = $row[$langPref.'description'];
							/*
                                $row["name"]              = $row["name"];
                                $row["description"]       = $row["description"];
                                $row["brief_description"] = $row["brief_description"];
							*/	
								
                                $row["product_code"]      = $row["product_code"];
                                $row["viewed_times"]      = $row["viewed_times"];
                                $row["items_sold"]        = $row["items_sold"];
                                $result[] = $row;
                        }
                        $i++;
                }
        }
        $count_row = isset($callBackParam["extraParametrsTemplate"])?$i:$products_count;
        return $result;
}


function prdGetMetaKeywordTag( $productID )
{
	global $langPref;
        $q = db_query("select meta_description, en_meta_description, ch_meta_description from ".PRODUCTS_TABLE." where productID=".(int)$productID);
        if ( $row=db_fetch_row($q) ) {
			if(trim($row[$langPref.'meta_description']) != '') {
				$row['meta_description'] = $row[$langPref.'meta_description'];
			} 
                return  $row["meta_description"];
        } else
                return "";
}

function prdGetMetaTags( $productID ) //gets META keywords and description - an HTML code to insert into <head> section
{
	global $langPref;
        $q = db_query( "select meta_description, en_meta_description, ch_meta_description, meta_keywords, en_meta_keywords, ch_meta_keywords from ".
                PRODUCTS_TABLE." where productID=".(int)$productID );
        $row = db_fetch_row($q);
		
			
		if(trim($row[$langPref.'meta_description']) != '') {
			$row['meta_description'] = $row[$langPref.'meta_description'];
		} 
		if(trim($row[$langPref.'meta_keywords']) != '') {
			$row['meta_keywords'] = $row[$langPref.'meta_keywords'];
		} 
		
        $meta_description  = $row["meta_description"];
        $meta_keywords     = $row["meta_keywords"];

        $res = "";

        if  ( $meta_description != "" )
                $res .= "<meta name=\"Description\" content=\"".$meta_description."\">\n";
        if  ( $meta_keywords != "" )
                $res .= "<meta name=\"KeyWords\" content=\"".$meta_keywords."\" >\n";

        return $res;
}

?>