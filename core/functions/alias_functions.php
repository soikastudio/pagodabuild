<?
function m_CatRedir ($catID, $url='') {
	
	if ($catID != 1 ) {
		$data = db_query("SELECT  alias, parent FROM ".CATEGORIES_TABLE." WHERE categoryID=".$catID);
		if($row = db_fetch_assoc( $data )) {
			$url = $url.'/'.$row['alias'];
			$url = m_CatRedir ((int)$row['parent'], $url);
		}
	}
	 return $url;
}
function checkOldLink() {
	
	$tstOld = $_SERVER['REQUEST_URI'];
	$tstOldLen = xEscSQL(strlen($tstOld));
	

	if(isset($_GET['utm_source'])) {
		$tstOld = str_replace('?utm_source='.$_GET['utm_source']."&",'',$tstOld);
		$tstOld = str_replace('?utm_source='.$_GET['utm_source'],'',$tstOld);
		$tstOld = str_replace('utm_source='.$_GET['utm_source']."&",'',$tstOld);
		$tstOld = str_replace('utm_source='.$_GET['utm_source'],'',$tstOld);
	}
	if(isset($_GET['utm_medium'])) {
		$tstOld = str_replace('?utm_medium='.$_GET['utm_medium']."&",'',$tstOld);
		$tstOld = str_replace('?utm_medium='.$_GET['utm_medium'],'',$tstOld);
		$tstOld = str_replace('utm_medium='.$_GET['utm_medium']."&",'',$tstOld);
		$tstOld = str_replace('utm_medium='.$_GET['utm_medium'],'',$tstOld);
	}
	if(isset($_GET['utm_term'])) {
		$tstOld = str_replace('?utm_term='.$_GET['utm_term']."&",'',$tstOld);
		$tstOld = str_replace('?utm_term='.$_GET['utm_term'],'',$tstOld);
		$tstOld = str_replace('utm_term='.$_GET['utm_term']."&",'',$tstOld);
		$tstOld = str_replace('utm_term='.$_GET['utm_term'],'',$tstOld);
	}
	if(isset($_GET['utm_campaign'])) {
		$tstOld = str_replace('?utm_campaign='.$_GET['utm_campaign']."&",'',$tstOld);
		$tstOld = str_replace('?utm_campaign='.$_GET['utm_campaign'],'',$tstOld);
		$tstOld = str_replace('utm_campaign='.$_GET['utm_campaign']."&",'',$tstOld);
		$tstOld = str_replace('utm_campaign='.$_GET['utm_campaign'],'',$tstOld);
	}
	
	
	
	if($tstOldLen > 1) {
	
		if (substr($tstOld, -1) == '/') {
			$tstOld = xEscSQL(substr($tstOld,0,-1));
		}
		// Проверяем новости
		$data = db_query("SELECT  NID FROM ".NEWS_TABLE." WHERE old_url LIKE '$tstOld' LIMIT 0,1");
		if($row = db_fetch_assoc( $data )) {
		
		
			$q = db_query("select alias FROM ".NEWS_TABLE." WHERE NID='".(int)$row['NID']."'");
			if ($r = db_fetch_row($q)) {
				header("HTTP/1.1 301 Moved Permanently");
				header("Location: /blog/".$r[0].".html");
				exit();
			} 
		} 
		$data = db_query("SELECT  NID FROM ".NEWS_TABLE." WHERE en_old_url LIKE '$tstOld' LIMIT 0,1");
		if($row = db_fetch_assoc( $data )) {
		
		
			$q = db_query("select alias FROM ".NEWS_TABLE." WHERE NID='".(int)$row['NID']."'");
			if ($r = db_fetch_row($q)) {
				header("HTTP/1.1 301 Moved Permanently");
				header("Location: /en/blog/".$r[0].".html");
				exit();
			} 
		} 
		$data = db_query("SELECT  NID FROM ".NEWS_TABLE." WHERE ch_old_url LIKE '$tstOld' LIMIT 0,1");
		if($row = db_fetch_assoc( $data )) {
		
		
			$q = db_query("select alias FROM ".NEWS_TABLE." WHERE NID='".(int)$row['NID']."'");
			if ($r = db_fetch_row($q)) {
				header("HTTP/1.1 301 Moved Permanently");
				header("Location: /ch/blog/".$r[0].".html");
				exit();
			} 
		} 
		// Проверяем дневник
		$data = db_query("SELECT  NID FROM ".DB_PRFX."diary WHERE old_url LIKE '$tstOld' LIMIT 0,1");
		if($row = db_fetch_assoc( $data )) {
		
		
			$q = db_query("select alias FROM ".DB_PRFX."diary WHERE NID='".(int)$row['NID']."'");
			if ($r = db_fetch_row($q)) {
				header("HTTP/1.1 301 Moved Permanently");
				header("Location: /diary/".$r[0].".html");
				exit();
			} 
		} 
		$data = db_query("SELECT  NID FROM ".DB_PRFX."diary WHERE en_old_url LIKE '$tstOld' LIMIT 0,1");
		if($row = db_fetch_assoc( $data )) {
		
		
			$q = db_query("select alias FROM ".DB_PRFX."diary WHERE NID='".(int)$row['NID']."'");
			if ($r = db_fetch_row($q)) {
				header("HTTP/1.1 301 Moved Permanently");
				header("Location: /en/diary/".$r[0].".html");
				exit();
			} 
		} 
		$data = db_query("SELECT  NID FROM ".DB_PRFX."diary WHERE ch_old_url LIKE '$tstOld' LIMIT 0,1");
		if($row = db_fetch_assoc( $data )) {
		
		
			$q = db_query("select alias FROM ".DB_PRFX."diary WHERE NID='".(int)$row['NID']."'");
			if ($r = db_fetch_row($q)) {
				header("HTTP/1.1 301 Moved Permanently");
				header("Location: /ch/diary/".$r[0].".html");
				exit();
			} 
		} 
		// Проверяем новости компании
		$data = db_query("SELECT  NID FROM ".DB_PRFX."company WHERE old_url LIKE '$tstOld' LIMIT 0,1");
		if($row = db_fetch_assoc( $data )) {
		
		
			$q = db_query("select alias FROM ".DB_PRFX."company WHERE NID='".(int)$row['NID']."'");
			if ($r = db_fetch_row($q)) {
				header("HTTP/1.1 301 Moved Permanently");
				header("Location: /company/".$r[0].".html");
				exit();
			} 
		} 
		$data = db_query("SELECT  NID FROM ".DB_PRFX."company WHERE en_old_url LIKE '$tstOld' LIMIT 0,1");
		if($row = db_fetch_assoc( $data )) {
		
		
			$q = db_query("select alias FROM ".DB_PRFX."company WHERE NID='".(int)$row['NID']."'");
			if ($r = db_fetch_row($q)) {
				header("HTTP/1.1 301 Moved Permanently");
				header("Location: /en/company/".$r[0].".html");
				exit();
			} 
		} 
		$data = db_query("SELECT  NID FROM ".DB_PRFX."company WHERE ch_old_url LIKE '$tstOld' LIMIT 0,1");
		if($row = db_fetch_assoc( $data )) {
		
		
			$q = db_query("select alias FROM ".DB_PRFX."company WHERE NID='".(int)$row['NID']."'");
			if ($r = db_fetch_row($q)) {
				header("HTTP/1.1 301 Moved Permanently");
				header("Location: /ch/company/".$r[0].".html");
				exit();
			} 
		} 
		// Проверяем Статические страницы
		$data = db_query("SELECT  aux_page_ID FROM ".AUX_PAGES_TABLE." WHERE old_url LIKE '$tstOld' LIMIT 0,1");
		if($row = db_fetch_assoc( $data )) {
			$q = db_query("select alias FROM ".AUX_PAGES_TABLE." WHERE aux_page_ID='".(int)$row['aux_page_ID']."'");
			if ($r = db_fetch_row($q)) {
				header("HTTP/1.1 301 Moved Permanently");
				header("Location: /page/".$r[0].".html");
				exit();
			}
		} 
		$data = db_query("SELECT  aux_page_ID FROM ".AUX_PAGES_TABLE." WHERE en_old_url LIKE '$tstOld' LIMIT 0,1");
		if($row = db_fetch_assoc( $data )) {
			$q = db_query("select alias FROM ".AUX_PAGES_TABLE." WHERE aux_page_ID='".(int)$row['aux_page_ID']."'");
			if ($r = db_fetch_row($q)) {
				header("HTTP/1.1 301 Moved Permanently");
				header("Location: /en/page/".$r[0].".html");
				exit();
			}
		} 
		$data = db_query("SELECT  aux_page_ID FROM ".AUX_PAGES_TABLE." WHERE ch_old_url LIKE '$tstOld' LIMIT 0,1");
		if($row = db_fetch_assoc( $data )) {
			$q = db_query("select alias FROM ".AUX_PAGES_TABLE." WHERE aux_page_ID='".(int)$row['aux_page_ID']."'");
			if ($r = db_fetch_row($q)) {
				header("HTTP/1.1 301 Moved Permanently");
				header("Location: /ch/page/".$r[0].".html");
				exit();
			}
		} 
		// Проверяем Категории
		$data = db_query("SELECT  categoryID FROM ".CATEGORIES_TABLE." WHERE old_url LIKE '$tstOld' LIMIT 0,1");
		if($row = db_fetch_assoc( $data )) {
				$linkcat = m_CatRedir((int)$row['categoryID']);
				header("HTTP/1.1 301 Moved Permanently");
				header("Location: ".$linkcat);
				exit();
		} 
		$data = db_query("SELECT  categoryID FROM ".CATEGORIES_TABLE." WHERE en_old_url LIKE '$tstOld' LIMIT 0,1");
		if($row = db_fetch_assoc( $data )) {
				$linkcat = m_CatRedir((int)$row['categoryID']);
				header("HTTP/1.1 301 Moved Permanently");
				header("Location: /en".$linkcat);
				exit();
		} 
		$data = db_query("SELECT  categoryID FROM ".CATEGORIES_TABLE." WHERE ch_old_url LIKE '$tstOld' LIMIT 0,1");
		if($row = db_fetch_assoc( $data )) {
				$linkcat = m_CatRedir((int)$row['categoryID']);
				header("HTTP/1.1 301 Moved Permanently");
				header("Location: /ch".$linkcat);
				exit();
		} 
		// Проверяем Товары
		$data = db_query("SELECT  productID FROM ".PRODUCTS_TABLE." WHERE old_url LIKE '$tstOld' LIMIT 0,1");
		if($row = db_fetch_assoc( $data )) {
			$q = db_query("select alias FROM ".PRODUCTS_TABLE." WHERE productID=".(int)$row['productID']);
			if ($r = db_fetch_row($q)) {
				$_GET["prod_alias"] = $r[0];
				header("HTTP/1.1 301 Moved Permanently");
				header("Location: /".$r[0].".html");
				exit();
			}
		}
		$data = db_query("SELECT  productID FROM ".PRODUCTS_TABLE." WHERE en_old_url LIKE '$tstOld' LIMIT 0,1");
		if($row = db_fetch_assoc( $data )) {
			$q = db_query("select alias FROM ".PRODUCTS_TABLE." WHERE productID=".(int)$row['productID']);
			if ($r = db_fetch_row($q)) {
				$_GET["prod_alias"] = $r[0];
				header("HTTP/1.1 301 Moved Permanently");
				header("Location: /en/".$r[0].".html");
				exit();
			}
		}
		$data = db_query("SELECT  productID FROM ".PRODUCTS_TABLE." WHERE ch_old_url LIKE '$tstOld' LIMIT 0,1");
		if($row = db_fetch_assoc( $data )) {
			$q = db_query("select alias FROM ".PRODUCTS_TABLE." WHERE productID=".(int)$row['productID']);
			if ($r = db_fetch_row($q)) {
				$_GET["prod_alias"] = $r[0];
				header("HTTP/1.1 301 Moved Permanently");
				header("Location: /ch/".$r[0].".html");
				exit();
			}
		} 		
	}
}


function translit($str)
{
    $transtable = array();
    $transtable = array(
        'А' => 'A',
        'Б' => 'B',
        'В' => 'V',
        'Г' => 'G',
        'Д' => 'D',
        'Е' => 'E',
        'Ё' => 'Yo',
        'Ж' => 'Zh',
        'З' => 'Z',
        'И' => 'I',
        'Й' => 'Y',
        'К' => 'K',
        'Л' => 'L',
        'М' => 'M',
        'Н' => 'N',
        'О' => 'O',
        'П' => 'P',
        'Р' => 'R',
        'С' => 'S',
        'Т' => 'T',
        'У' => 'U',
        'Ф' => 'F',
        'Х' => 'H',
        'Ц' => 'Ts',
        'Ч' => 'Ch',
        'Ш' => 'Sh',
        'Щ' => 'Shch',
        'Ъ' => '',
        'Ы' => 'I',
        'Ь' => '',
        'Э' => 'E',
        'Ю' => 'Yu',
        'Я' => 'Ya',
        'а' => 'a',
        'б' => 'b',
        'в' => 'v',
        'г' => 'g',
        'д' => 'd',
        'е' => 'e',
        'ё' => 'yo',
        'ж' => 'zh',
        'з' => 'z',
        'и' => 'i',
        'й' => 'y',
        'к' => 'k',
        'л' => 'l',
        'м' => 'm',
        'н' => 'n',
        'о' => 'o',
        'п' => 'p',
        'р' => 'r',
        'с' => 's',
        'т' => 't',
        'у' => 'u',
        'ф' => 'f',
        'х' => 'h',
        'ц' => 'ts',
        'ч' => 'ch',
        'ш' => 'sh',
        'щ' => 'shch',
        'ъ' => '',
        'ы' => 'i',
        'ь' => '',
        'э' => 'e',
        'ю' => 'yu',
        'я' => 'ya',
        ' ' => '-');
        
    $str = strtr($str, $transtable);
    return $str;
}

function aliasClearAlias($alias, $name="")
{
	$name = str_replace(" ","-", trim($name));
	$name = strtolower(translit($name));
	$name = preg_replace('#[^a-z0-9_-]+#i', '', $name);
	$alias = str_replace(" ","-", trim($alias));
	$alias = strtolower(translit($alias));
	$alias = preg_replace('#[^a-z0-9_-]+#i', '', $alias);
	if ($alias=="")
		$alias = $name;

	return $alias;
}

//Дополнительные страницы
function auxCheckAlias($alias, $id, $type="add")
{
	if ($type=="upd")
		$q = db_query("SELECT COUNT(*) FROM ".AUX_PAGES_TABLE." WHERE alias='".xEscSQL($alias)."' AND aux_page_ID<>".(int)$id);
	else
		$q = db_query("SELECT COUNT(*) FROM ".AUX_PAGES_TABLE." WHERE alias='".xEscSQL($alias)."'");

	$r = db_fetch_row($q);
	$res = $r[0];
	if ($res>0)
		return $id."-".$alias;
	return $alias;
}

function aliasGenerateAliasAux($id, $alias, $name, $type)
{
    $alias = aliasClearAlias($alias, $name);
    $alias = auxCheckAlias($alias, $id, $type);
    db_query("UPDATE ".AUX_PAGES_TABLE." SET alias='".xEscSQL($alias)."' WHERE aux_page_ID=".(int)$id);
}

function auxGetDataByAlias($alias)
{
		$alias = preg_replace('#[^a-z0-9_-]+#i', '', $alias);
		$q = db_query("SELECT aux_page_ID FROM ".AUX_PAGES_TABLE." WHERE alias='".xEscSQL($alias)."'");
		if ($r = db_fetch_row($q))
		{
            $_GET['show_aux_page'] = (int)$r[0];
			return;
		}
		else
		{
			header("HTTP/1.0 404 Not Found");
			header("HTTP/1.1 404 Not Found");
			header("Status: 404 Not Found");
			die(ERROR_404_HTML);
			exit;
		}
}

//Новсоти
function newsCheckAlias($alias, $id, $type="add")
{
	if ($type=="upd")
		$q = db_query("SELECT COUNT(*) FROM ".NEWS_TABLE." WHERE alias='".xEscSQL($alias)."' AND NID<>".(int)$id);
	else
		$q = db_query("SELECT COUNT(*) FROM ".NEWS_TABLE." WHERE alias='".xEscSQL($alias)."'");

	$r = db_fetch_row($q);
	$res = $r[0];
	if ($res>0)
		return $id."-".$alias;
	return $alias;
}

//Дневник
function diaryCheckAlias($alias, $id, $type="add")
{
	if ($type=="upd")
		$q = db_query("SELECT COUNT(*) FROM ".DB_PRFX."diary WHERE alias='".xEscSQL($alias)."' AND NID<>".(int)$id);
	else
		$q = db_query("SELECT COUNT(*) FROM ".DB_PRFX."diary WHERE alias='".xEscSQL($alias)."'");

	$r = db_fetch_row($q);
	$res = $r[0];
	if ($res>0)
		return $id."-".$alias;
	return $alias;
}

//Проект
function projectCheckAlias($alias, $id, $type="add")
{
	if ($type=="upd")
		$q = db_query("SELECT COUNT(*) FROM ".DB_PRFX."project WHERE alias='".xEscSQL($alias)."' AND NID<>".(int)$id);
	else
		$q = db_query("SELECT COUNT(*) FROM ".DB_PRFX."project WHERE alias='".xEscSQL($alias)."'");

	$r = db_fetch_row($q);
	$res = $r[0];
	if ($res>0)
		return $id."-".$alias;
	return $alias;
}
function companyCheckAlias($alias, $id, $type="add")
{
	if ($type=="upd")
		$q = db_query("SELECT COUNT(*) FROM ".DB_PRFX."company WHERE alias='".xEscSQL($alias)."' AND NID<>".(int)$id);
	else
		$q = db_query("SELECT COUNT(*) FROM ".DB_PRFX."company WHERE alias='".xEscSQL($alias)."'");

	$r = db_fetch_row($q);
	$res = $r[0];
	if ($res>0)
		return $id."-".$alias;
	return $alias;
}

function aliasGenerateAliasNews($id, $alias, $name, $type)
{
    $alias = aliasClearAlias($alias, $name);
    $alias = newsCheckAlias($alias, $id, $type);
    db_query("UPDATE ".NEWS_TABLE." SET alias='".xEscSQL($alias)."' WHERE NID=".(int)$id);
}
function aliasGenerateAliasDiary($id, $alias, $name, $type)
{
    $alias = aliasClearAlias($alias, $name);
    $alias = diaryCheckAlias($alias, $id, $type);
    db_query("UPDATE ".DB_PRFX."diary SET alias='".xEscSQL($alias)."' WHERE NID=".(int)$id);
}

function aliasGenerateAliasproject($id, $alias, $name, $type)
{
    $alias = aliasClearAlias($alias, $name);
    $alias = projectCheckAlias($alias, $id, $type);
    db_query("UPDATE ".DB_PRFX."project SET alias='".xEscSQL($alias)."' WHERE NID=".(int)$id);
}
function aliasGenerateAliascompany($id, $alias, $name, $type)
{
    $alias = aliasClearAlias($alias, $name);
    $alias = companyCheckAlias($alias, $id, $type);
    db_query("UPDATE ".DB_PRFX."company SET alias='".xEscSQL($alias)."' WHERE NID=".(int)$id);
}


function newsGetDataByAlias($alias)
{
		$alias = preg_replace('#[^a-z0-9_-]+#i', '', $alias);
		$q = db_query("SELECT NID FROM ".NEWS_TABLE." WHERE alias='".xEscSQL($alias)."'");
		if ($r = db_fetch_row($q))
		{
            $_GET['fullnews'] = (int)$r[0];
			return;
		}
		else
		{
			header("HTTP/1.0 404 Not Found");
			header("HTTP/1.1 404 Not Found");
			header("Status: 404 Not Found");
			die(ERROR_404_HTML);
			exit;
		}
}

function projectGetDataByAlias($alias)
{
		$alias = preg_replace('#[^a-z0-9_-]+#i', '', $alias);
		$q = db_query("SELECT NID FROM pgd_project WHERE alias='".xEscSQL($alias)."'");
		if ($r = db_fetch_row($q))
		{
            $_GET['fullproject'] = (int)$r[0];
			return;
		}
		else
		{
			header("HTTP/1.0 404 Not Found");
			header("HTTP/1.1 404 Not Found");
			header("Status: 404 Not Found");
			die(ERROR_404_HTML);
			exit;
		}
}
function diaryGetDataByAlias($alias)
{
		$alias = preg_replace('#[^a-z0-9_-]+#i', '', $alias);
		$q = db_query("SELECT NID FROM ".DB_PRFX."diary WHERE alias='".xEscSQL($alias)."'");
		if ($r = db_fetch_row($q))
		{
            $_GET['fulldiary'] = (int)$r[0];
			return;
		}
		else
		{
			header("HTTP/1.0 404 Not Found");
			header("HTTP/1.1 404 Not Found");
			header("Status: 404 Not Found");
			die(ERROR_404_HTML);
			exit;
		}
}
function companyGetDataByAlias($alias)
{
		$alias = preg_replace('#[^a-z0-9_-]+#i', '', $alias);
		$q = db_query("SELECT NID FROM ".DB_PRFX."company WHERE alias='".xEscSQL($alias)."'");
		if ($r = db_fetch_row($q))
		{
            $_GET['fullcompany'] = (int)$r[0];
			return;
		}
		else
		{
			header("HTTP/1.0 404 Not Found");
			header("HTTP/1.1 404 Not Found");
			header("Status: 404 Not Found");
			die(ERROR_404_HTML);
			exit;
		}
}

//Товары
function prodCheckAlias($alias, $id, $type="add")
{
	if ($type=="upd")
		$q = db_query("SELECT COUNT(*) FROM ".PRODUCTS_TABLE." WHERE alias='".xEscSQL($alias)."' AND productID<>".(int)$id);
	else
		$q = db_query("SELECT COUNT(*) FROM ".PRODUCTS_TABLE." WHERE alias='".xEscSQL($alias)."'");

	$r = db_fetch_row($q);
	$res = $r[0];
	if ($res>0)
		return $id."-".$alias;
	return $alias;
}

function aliasGenerateAliasProd($id, $alias, $name, $type)
{
    $alias = aliasClearAlias($alias, $name);
    $alias = prodCheckAlias($alias, $id, $type);
    db_query("UPDATE ".PRODUCTS_TABLE." SET alias='".xEscSQL($alias)."' WHERE productID=".(int)$id);
}

function prodGetDataByAlias($alias)
{
		$alias = preg_replace('#[^a-z0-9_/-]+#i', '', $alias);
        $get = explode('/', trim($alias, '/'));
        $c = sizeof($get)-1;
		$alias = preg_replace('#[^a-z0-9_-]+#i', '', $get[$c]);
		$q = db_query("SELECT productID FROM ".PRODUCTS_TABLE." WHERE alias='".xEscSQL($alias)."'");
		if ($r = db_fetch_row($q))
		{
            $_GET['productID'] = $r['productID'];
			return;
		}
		else
		{
			header("HTTP/1.0 404 Not Found");
			header("HTTP/1.1 404 Not Found");
			header("Status: 404 Not Found");
			die(ERROR_4041_HTML);
			exit;
		}
}

//Категории
function catCheckAlias($alias, $id, $type="add")
{
	if ($type=="upd")
		$q = db_query("SELECT COUNT(*) FROM ".CATEGORIES_TABLE." WHERE alias='".xEscSQL($alias)."' AND categoryID<>".(int)$id);
	else
		$q = db_query("SELECT COUNT(*) FROM ".CATEGORIES_TABLE." WHERE alias='".xEscSQL($alias)."'");

	$r = db_fetch_row($q);
	$res = $r[0];
	if ($res>0)
		return "";
	return $alias;
}

function aliasGenerateAliasCat($id, $alias, $name, $type)
{
    $alias = aliasClearAlias($alias, $name);
    $alias = catCheckAlias($alias, $id, $type);
    db_query("UPDATE ".CATEGORIES_TABLE." SET alias='".xEscSQL($alias)."' WHERE categoryID=".(int)$id);
}

function catGetDataByAlias($alias)
{
	global $lang;
		if (($_GET['offset'] || $_GET['show_all']) && substr($_GET['cat_alias'], -1)!="/")
		{
			$_GET['cat_alias'] .= "/";
			$alias = $_GET['cat_alias'];
		}
		if (substr($alias, -1)!="/")
		{
			header("HTTP/1.1 301 Moved Permanently");
			header("Location: ".$lang."/".$alias."/");
			exit();
		}
		$alias = preg_replace('#[^a-z0-9_/-]+#i', '', $alias);
        $get = explode('/', trim($alias, '/'));
        $c = sizeof($get)-1;
		$alias = preg_replace('#[^a-z0-9_-]+#i', '', $get[$c]);
		$_GET['colorCatAlias'] = $_GET['cat_alias'].'/';
		$_GET['colorCatAlias'] = str_replace('//','/',$_GET['colorCatAlias']);
		
		$qC = db_query('select variantID FROM '.PRODUCTS_OPTIONS_VALUES_VARIANTS_TABLE.' WHERE optionID=5 AND LOWER(en_option_value)="'.strtolower($alias).'"');
		if($varId=db_fetch_row($qC)) {
			$_GET['param_5'] = $varId['variantID'];
			$_GET['paramC_5'] = strtolower($alias);
			$_GET['colorCatAlias'] = str_replace('/'.$alias,'',$_GET['cat_alias']).'/';
			$_GET['colorCatAlias'] = str_replace('//','/',$_GET['colorCatAlias']);
			
			$alias = preg_replace('#[^a-z0-9_-]+#i', '', $get[$c-1]);
		}
	
		
		$q = db_query("SELECT categoryID FROM ".CATEGORIES_TABLE." WHERE alias='".xEscSQL($alias)."'");
		if ($r = db_fetch_row($q))
		{
            $_GET['categoryID'] = (int)$r['categoryID'];
			return;
		}
		else
		{
			
			header("HTTP/1.0 404 Not Found");
			header("HTTP/1.1 404 Not Found");
			header("Status: 404 Not Found");
			die(ERROR_404_HTML);
			exit;
		
		}
}

//Генерируем ссылки
function m_Aux($alias)
{
	global $langRev;
	$url = $langRev."page/".$alias.".html";
	return $url;
}

function m_AuxID($id)
{
	global $langRev;
	$q = db_query("SELECT alias FROM ".AUX_PAGES_TABLE." WHERE aux_page_ID='".(int)$id."'");
	$r = db_fetch_row($q);
	if ($r[0] != "") {
		$url = $langRev."page/".$r[0].".html";
	} else {
		$url = $langRev."page_".$id.".html";
	}
	return $url;
}

function m_News($alias)
{
	global $langRev;
	$url = $langRev."blog/".$alias.".html";
	return $url;
}
function m_Diary($alias)
{
	global $langRev;
	$url = $langRev."diary/".$alias.".html";
	return $url;
}
function m_company($alias)
{
	global $langRev;
	$url = $langRev."company/".$alias.".html";
	return $url;
}

function m_Cat($id, $url="")
{    
  global $fc, $lang, $langRev, $langN;
  $r = array();
  if ($id>1)
  {
	$r = $fc[$id];
    if ($r)
    {
		if ($r['alias'] != "") {
			if ($url=="") $url = "/".$r['alias'];
			else $url = $r['alias']."/".trim($url, "/");
			if ($r['parent']>1)
				$url = m_Cat($r['parent'], $url);
			
			$chpu=1;
		} else {
			$chpu=0;
			$url = "category_".$id.".html";
		}
    }
  }
	if ($chpu == 1) {
		return trim($langN.$url, "/")."/";
	} else {
		return $lang.$url;
	}
}

function m_Prod($r)
{
	global $langRev;
    $url = "";
    if ($r)
    {
		if ($r['alias'] != "")
		{
			$url = $langRev.$r['alias'].".html";
		} else {
			$url = $langRev."product_".$r['productID'].".html";
		}
	}
    return $url;
}

function aliasGetDataByAlias()
{
	if ($_GET['news_alias']) {
		newsGetDataByAlias($_GET['news_alias']);
		return true;
	} elseif ($_GET['diary_alias']) {
		diaryGetDataByAlias($_GET['diary_alias']);
		return true;
  } elseif ($_GET['project_alias']) {
		projectGetDataByAlias($_GET['project_alias']);
		return true;
	} elseif ($_GET['company_alias']) {
		companyGetDataByAlias($_GET['company_alias']);
		return true;
	} elseif ($_GET['aux_alias']) {
		auxGetDataByAlias($_GET['aux_alias']);
		return true;
	} elseif ($_GET['cat_alias']) {
		catGetDataByAlias($_GET['cat_alias']);
		return true;
	} elseif ($_GET['prod_alias']) {
		prodGetDataByAlias($_GET['prod_alias']);
		return true;
	} else return false;
}
?>