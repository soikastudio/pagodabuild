<?php
#####################################
# ShopCMS: Скрипт интернет-магазина
# Copyright (c) by ADGroup
# http://shopcms.ru
#####################################

function projectMainMobileProjects() {
  
    //echo 'dada'; die();
  
    global $langPref;
      //echo ''; die();
     
        
    $qpr = db_query("select * FROM pgd_project ORDER BY NID DESC LIMIT 0, 4" );
    $main_projects = array();
    $row_count = 1;
    while ($row = db_fetch_row($qpr)) {
        //$class = $row_count % 2 == 0 ? 'right' : 'left';
        //$row['position'] = $class;
        $row['title'] = $row[$langPref.'title'];
        $row['textToPrePublication'] = $row[$langPref.'textToPrePublication'];
        $main_projects[] = $row;
        
        $row_count++;
      //}
    }
        
    return $main_projects;

}

function projectMainProjects()
{
	global $langPref;
    $q = db_query( "select * from ".DB_PRFX."project WHERE main = 1 order by sort ASC");
    $data = array();

    $i=0;
    while( $r=db_fetch_row($q) )
    {
      if(trim($r[$langPref.'title']) != "") {

      $r['title'] = $r[$langPref.'title'];
               $r["add_date"]=dtConvertToStandartForm($r["add_date"]);
         $tmText = preg_replace("/##gallery_[0-9]+##/",'',$r[$langPref."textToPrePublication"]);
         $r["textToPrePublication"] = $tmText;
         $r["textToPrePublication"] = $tmText;
         //$r["image"] = $r['project_image'];
               $r['pnum'] = $i;
               $data[] = $r;
        $i++;
       if($i == CONF_NEWS_COUNT_IN_CUSTOMER_PART) {
        break;
       }
      }
    }
    return $data;
}

function projectGetNewsToCustomer()
{
	global $langPref;
        $q = db_query( "select * from ".DB_PRFX."project order by sort ASC");
        $data = array();

        $i=0;
        while( $r=db_fetch_row($q) )
        { 
			if(trim($r[$langPref.'title']) != "") {
				
			$r['title'] = $r[$langPref.'title'];
               $r["add_date"]=dtConvertToStandartForm($r["add_date"]);
			   $tmText = preg_replace("/##gallery_[0-9]+##/",'',$r[$langPref."textToPrePublication"]);
			   $r["textToPrePublication"] = $tmText;
			   
               $data[] = $r;
			  $i++;
			 if($i == CONF_NEWS_COUNT_IN_CUSTOMER_PART) {
				break;
			 }
			}
        }
        return $data;
}

function projectGetPreNewsToCustomer()
{
	global $langPref;
        $q = db_query( "select * from ".DB_PRFX."project order by add_date DESC LIMIT 0,".CONF_NEWS_COUNT_IN_NEWS_PAGE);
        $data = array();

        while( $r=db_fetch_row($q) )
        {
							if(trim($r[$langPref.'title']) == "" && trim($r[$langPref."textToPrePublication"]) == "") {
								$tmPref = "";
							} else {
								$tmPref = $langPref;
							}
				$r['title'] = $r[$tmPref.'title'];
               $r["add_date"]=dtConvertToStandartForm($r["add_date"]);
			   $r["textToPrePublication"] = preg_replace("/##gallery_[0-9]+##/",'',$r[$tmPref."textToPrePublication"]);
               $data[] = $r;
        }
        return $data;
}


function projectGetFullNewsToCustomer($newsid)
{
	global $langPref, $langN;
        $q = db_query( "select * from ".DB_PRFX."project where NID=".(int)$newsid);
        if  ( $r = db_fetch_row($q) )
        {
			
        $r["add_date"]=dtConvertToStandartForm($r["add_date"]);
        $r["NID"] = (int)$newsid;
		
			if(trim($r[$langPref.'title']) != "") {
				$r["title"] = $r[$langPref."title"];
			} else {
				if($langN !='') {
					Redirect("/".$langN."/project/");
				} else {
					Redirect("/project/");
				}
			}
			   $r["textToPrePublication"] = checkGalleriesTags('project',$newsid,$r[$langPref."textToPrePublication"]);
			   $r["textToPublication"] = checkGalleriesTags('project',$newsid,$r[$langPref."textToPublication"]);
		}
        return $r;
}

function projectGetNewsToEdit($newsid)
{
        $q = db_query( "select * from ".DB_PRFX."project where NID=".(int)$newsid);
        $r=db_fetch_row($q);
        $r["add_date"]=dtConvertToStandartForm($r["add_date"]);
        return $r;
}

function projectGetAllNews( $callBackParam, &$count_row, $navigatorParams = null )
{
        if ( $navigatorParams != null )
        {
                $offset                        = $navigatorParams["offset"];
                $CountRowOnPage        = $navigatorParams["CountRowOnPage"];
        }
        else
        {
                $offset = 0;
                $CountRowOnPage = 0;
        }

        $q = db_query( "select NID, add_date, title, alias, sort, main, main_sort from ".DB_PRFX."project order by sort ASC, add_date DESC" );

        $i = 0;
        $data = array();
        while( $r=db_fetch_row($q) )
        {
                if ( ($i >= $offset && $i < $offset + $CountRowOnPage) ||
                                $navigatorParams == null  )
                {
                   $r["add_date"]=dtConvertToStandartForm($r["add_date"]);
                   $data[] = $r;
                }
                $i++;
        }
        $count_row = $i;
        return $data;
}

function projectAddNews( $add_date, $title, $en_title, $ch_title, $textToPrePublication, $en_textToPrePublication, $ch_textToPrePublication, $textToPublication, $en_textToPublication, $ch_textToPublication, $textToMail, $old_url, $en_old_url, $ch_old_url, $to_main, $project_file )
{
        $stamp = microtime();
        $stamp = explode(" ", $stamp);
        $stamp = $stamp[1];
        db_query( "insert into ".DB_PRFX."project ( add_date, title, en_title, ch_title, textToPrePublication, en_textToPrePublication, ch_textToPrePublication, textToPublication, en_textToPublication, ch_textToPublication, textToMail, old_url, en_old_url, ch_old_url, add_stamp, main, project_file ) ".
                  " values( '".xEscSQL(dtDateConvert($add_date))."', '".xToText(trim($title))."', '".xToText(trim($en_title))."', '".xToText(trim($ch_title))."', '".xEscSQL($textToPrePublication)."', '".xEscSQL($en_textToPrePublication)."', '".xEscSQL($ch_textToPrePublication)."', '".xEscSQL($textToPublication)."',  '".xEscSQL($en_textToPublication)."',  '".xEscSQL($ch_textToPublication)."', '".xEscSQL($textToMail)."', '".xEscSQL($old_url)."', '".xEscSQL($en_old_url)."', '".xEscSQL($ch_old_url)."', ".$stamp.", ".$to_main.", '".$project_file."' ) ");
				  
		$id = db_insert_id();
        aliasGenerateAliasproject($id, $_POST['alias'], $title, "add");
		
		checkNewPageGalleries('project', $id);
        return $id;
}

function projectUpdateNews( $add_date, $title, $en_title, $ch_title, $textToPrePublication, $en_textToPrePublication, $ch_textToPrePublication, $textToPublication, $en_textToPublication, $ch_textToPublication, $textToMail, $id_news, $old_url, $en_old_url, $ch_old_url, $to_main, $project_file  )
{
                
                if ($project_file && !is_numeric($project_file) ) {

                db_query("update ".DB_PRFX.
                    "project set     add_date='".xEscSQL(dtDateConvert($add_date))."', ".
                 "         title='".xToText($title)."', ".
                 "         en_title='".xToText($en_title)."', ".
                 "         ch_title='".xToText($ch_title)."', ".
                 "         textToPrePublication='".xEscSQL($textToPrePublication)."', ".
                 "         en_textToPrePublication='".xEscSQL($en_textToPrePublication)."', ".
                 "         ch_textToPrePublication='".xEscSQL($ch_textToPrePublication)."', ".
                 "         textToPublication='".xEscSQL($textToPublication)."', ".
                 "         en_textToPublication='".xEscSQL($en_textToPublication)."', ".
                 "         ch_textToPublication='".xEscSQL($ch_textToPublication)."', ".
                 "         textToMail='".xEscSQL($textToMail)."', ".
                 "         old_url='".xEscSQL($old_url)."' , ".
                 "         en_old_url='".xEscSQL($en_old_url)."' , ".
                 "         ch_old_url='".xEscSQL($ch_old_url)."' , ".                
                 "         main='".$to_main."' , ".
                 "         project_file='".xEscSQL($project_file)."' ".
                 " where NID = ".(int)$id_news);
                }
                else {
                  db_query("update ".DB_PRFX.
                    "project set     add_date='".xEscSQL(dtDateConvert($add_date))."', ".
                 "         title='".xToText($title)."', ".
                 "         en_title='".xToText($en_title)."', ".
                 "         ch_title='".xToText($ch_title)."', ".
                 "         textToPrePublication='".xEscSQL($textToPrePublication)."', ".
                 "         en_textToPrePublication='".xEscSQL($en_textToPrePublication)."', ".
                 "         ch_textToPrePublication='".xEscSQL($ch_textToPrePublication)."', ".
                 "         textToPublication='".xEscSQL($textToPublication)."', ".
                 "         en_textToPublication='".xEscSQL($en_textToPublication)."', ".
                 "         ch_textToPublication='".xEscSQL($ch_textToPublication)."', ".
                 "         textToMail='".xEscSQL($textToMail)."', ".
                 "         old_url='".xEscSQL($old_url)."' , ".
                 "         en_old_url='".xEscSQL($en_old_url)."' , ".
                 "         ch_old_url='".xEscSQL($ch_old_url)."' , ".                
                 "         main='".$to_main."'".
                
                 " where NID = ".(int)$id_news);
                
                }
				 
				 aliasGenerateAliasproject($id_news, $_POST['alias'], $title, "upd");
}

function projectDeleteNews( $newsid )
{
        db_query( "delete from ".DB_PRFX."project where NID=".(int)$newsid );
}

function projectSendNews($newsid)
{
        $q = db_query( "select add_date, title, textToMail from ".DB_PRFX."project where NID=".(int)$newsid );
        $news = db_fetch_row( $q );
        $news["add_date"]=dtConvertToStandartForm($news["add_date"]);
        $q = db_query( "select Email from ".MAILING_LIST_TABLE );
        while( $subscriber = db_fetch_row($q) ) xMailTxtHTMLDATA($subscriber["Email"], EMAIL_NEWS_OF." - ".CONF_SHOP_NAME, $news["title"]."<br><br>".$news["textToMail"]);
}

?>