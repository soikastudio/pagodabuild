<div class="team__info">
			<div id="team__text_1" class="team__text team__text_show">
				{if $current_lang == 'en'}
					Maksym Triasunov<br>
					<b>Managing Director, Co-Founder</b><br>

					Believes in specialization and aims to be best in anything what company engages. Determined to deliver best value to our customers through our company unique focus strategy. <br>Master degree from best economics university in Ukraine and EMBA degree from Tsinghua SEM <br>and INSEAD (currently ranked world #1 by Financial Times magazine) make Maksym best educated stone CEO in China.				
				{elseif $current_lang == 'ch'}
					总经理，公司创始人<br>

专注专业化管理并力求公司一切运营达到最好效果。致力通过公司特有的重点策略为客户<br>

提供最大的价值。乌克兰重点大学的经济学硕士学位和清华大学经济管理学院及欧洲工商<br>

管理学院EMBA学位（欧洲工商管理学院是目前在金融时报杂志排名世界第一），这些使<br>

得 Maksym成为中国受教育程度最高的石材执行总裁。<br>
				{else}
					Максим Трясунов<br>

<b>Ген.директор, со-основатель</b><br>

Нацелен на совершенство всех аспектов деятельности компании. Уверен, что благодаря уникальной стратегии специализации FOCUS, <br>клиенты компании смогут получить продукцию и сервис максимально высокого качества по наиболее конкурентным ценам.  <br>Наличие димплома магистра лучшего экономического университета Украины, а также степень EМВА бизнес школ Tsinghua SEM и INSEAD <br>(№1 в мире по версии журнала Financial Times) делает Максима наиболее образованным управленцем в сфере камня в Китае.
				{/if}
			</div>

			<div id="team__text_2" class="team__text">
				Kelvin Guo<br>
				{if $current_lang == 'en'}
				<b>Director of Import and Asia-Pacific Sales, Co-Founder</b><br>

13 years in stone business and prior experience in many other spheres in China and abroad make Kelvin most experienced man in the company. <br>Proficiency in Tai-Chi helps him to stay calm and always find best solution in most challenging situations.
				{elseif $current_lang == 'ch'}
				进口及亚太地区销售经理，公司创始人<br>

有着13年的石材业务经验及在国内外其他领域的丰富经验使得Kelvin成为公司最具经验的<br>

员工。精通太极使得他能够始终保持冷静并且能在最具挑战性的情况下找到最佳解决方案<br>

。
				{else}
				<b>Директор по импорту и продажам в Азиатско-Тихоокеанском регионе, со-основатель</b><br>

Многолетний жизненный и профессиональный опыт в разных областях деятельности, а также 13-летний стаж  <br>в каменной индустрии делает Кельвина наиболее опытным человеков в компании. Регулярная практика Тайцзицюань позволяют ему <br>даже в самых сложных ситуациях всегда сохранять спокойствие и находить наилучшие решения.
				{/if}
			</div>
			<div id="team__text_3" class="team__text">
				{if $current_lang == 'en'}
				Andrii Izhevskyi<br>
					<b>Director of Marketing and Sales</b><br>

Before joining company in 2011 Andrei has got his master degree in engineering, worked in a big international corporation and traveled around the world.<br> His creativity, passion for art, spiritual pursuits, honesty and sense of humor set foundation of unique Pagoda-Build more-then-just-work spirit,<br> helps us to go beyond conventional thinking, develop non-standard solutions for company and for our customers.
				{elseif $current_lang == 'ch'}
				市场及销售经理<br>

具有工程硕士学位并于2011年加入公司，此前工作于一间大型国际公司并且环游世界。他<br>

的创造力，对艺术的热情，对精神的追求，诚实和幽默感是公司特有精神的基础，并且帮<br>

助我们超越传统的设计理念，为公司和客户提供了较灵活的解决方案。<br>
				{else}
				Андрей Ижевский<br>

<b>Директор продаж и маркетинга</b><br>

В 2011 году стал частью команды Pagoda-Build. До этого получил степень магистра в инженерии, <br>работал в крупной международной компании и путешествовал по миру. Сочетание креативности, интереса к искусству и духновным практикам, <br>честность и чувство юмора Андрея во многом формируют основу уникальной атмосферы<br> Pagoda-Build, а также позволяют находить нестандартные решения для клиентов и компании в целом.
				{/if}
			</div>

			<div id="team__text_4" class="team__text">
				Yanjun Liu<br>
				{if $current_lang == 'en'}
				<b>Director of Production and Procurement</b><br>

				Yanjun has got his Bachelor of Philosophy degree from Xiamen University – one of the leading Universities in China. <br>But works in stone industry ever since his graduation in 2006.<br> Being a big admirer of Confucius Teaching and a man of high morale principles, he sets a high standard for all of us and for our relations with clients and suppliers. 
				{elseif $current_lang == 'ch'}
				生产及采购经理<br>

具有厦门大学哲学学士学位，厦门大学是中国重点大学之一。从2006年毕业后就开始从事<br>

石材行业。对孔子教学的仰慕和对高尚品德的追求使得他对我们公司员工，客户和供应商<br>

设置较高的标准。<br>
				{else}
				<b>Директор производства и закупок</b><br>

Обладатель степени бакалавра по философии в Сямэньском Университете - одном из ведущих ВУЗов Китая. <br> С 2006 года работает в каменной отрасли. Большой поклонник учения Конфуция. Комбинация высоких моральных принципов, незаурядного аналитического ума <br>и профессионального опыта задает высокие стандарты работы с клиентами и поставщиками.
				{/if}
			</div>
		</div>
	</section>