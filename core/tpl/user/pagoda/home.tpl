<header id="header" class="header">
		<div class="header__content">
			<!--<a class="header__learn" href="#">Обучение</a>-->
			<a class="header__logo" href="{$mainUrl}"><img src="/data/pagoda/frontend/img/Logo.svg" width="141"></a>
			<a class="header__logo_min" href="{$mainUrl}"><img src="/data/pagoda/frontend/img/logo_min.png"></a>
      
			<nav class="header__nav">
         {section name=m loop=$menu_points}
				<a class="header__nav-item header__nav-item_active" href="{$menu_points[m].link}">{$menu_points[m].anchor}</a>
        {/section}
			</nav>
      

			{if $current_lang == 'en'}
				<a class="btn header__feedback" href="#">Contact Us</a>
			{elseif $current_lang == 'ch'}
				<div class="popup__title"></div>
				<a class="btn header__feedback" href="#">联系我们</a>
			{else}
				<a class="btn header__feedback" href="#">Связаться с нами</a>
			{/if}

			<div id="header__lang-wrap" class="header__lang">
				<div class="header__lang-in">
					<a id="header__lang" class="header__lang-link header__lang-link_current" href="{$langPath}">{$current_lang}</a>
					{foreach from=$lang_list item=lang}
						{if $lang->iso2 != $current_lang}
							<a class="header__lang-link" href="{if $lang->iso2 != 'ru'}/{$lang->iso2}{/if}{$langPath}">{$lang->iso2}</a>
						{/if}
					{/foreach}
				</div>
			</div>
		</div>
	</header>

	<div class="top">
		<video class="top__video" autoplay loop muted preload="auto" width="1920" src="/data/pagoda/frontend/video/SMALL.mp4"></video>
		<div class="top__bg"></div>
		<div class="top__title">
			{if $current_lang == 'en'}
				Manufacturing of stone <br>products. <span class="top__title_min">Supplies of marble <br>and granite from China</span>
			{elseif $current_lang == 'ch'}
				石材制品厂商<br><span class="top__title_min">供应产自中国的花岗岩和大理石</span>
			{else}
				Производство продукции <br>из камня. <span class="top__title_min">Поставка гранита <br>и мрамора из Китая</span>
			{/if}
		</div>
			
		<div class="top__mouse"></div>
	</div>

	<section class="advantage">
		<div class="content">
			<div class="advantage__text">

				{if $current_lang == 'en'}
					Xiamen Pagoda-Build Co., LTD is an international stone trading, consulting and manufacturing company. We started business in 2006 and our head quarter is in Xiamen, China. We distribute over 30 granites and marbles of Chinese, Ukrainian, Iranian and other countries origins. We have deep knowledge of each of our focus stones and deliver highest value to our customers through cost and quality optimization. We supply broader range of materials according to project specification to our regular customers. We believe in specialization, communication and long-term partnership.
				{elseif $current_lang == 'ch'}
        
					厦门百国达建材进出口有限公司现已发展成一家国际贸易，咨询和制造公司。公司于2006年在厦门创建。公司直销30多种花岗岩和大理石和一系列来自于乌克兰、伊朗、中国的独家石材。我们对于石头的深入了解使得我们能为客户提供高质量和极富竞争力价格的产品，并且能够为客户不同的工程提供大量的材料选择。我们专注建立专业和长期的合作伙伴关系。
        {else}
					Хiamen Pagoda-Build CO., LTD - международная торгово-производственная каменная компания. 
					Мы начали бизнес в 2006 г. Главный офис компании находится в г. Сямынь, Китай. Мы поставляем более 30 гранитов и мраморов китайских, украинских, иранских и месторождений других стран. Мы досконально разбираемся в основных камнях, с которым работаем и максимально удовлетворяем потребности Клиентов, оптимизируя качество и стоимость товара. Более широкий спектр материалов согласно спецификации мы поставляем для наших постоянных клиентов. Мы верим в профессионализм, коммуникацию и долгосрочное сотрудничество.
				{/if}

			</div>
		
			<div class="advantage__cols">
				
				{if $current_lang == 'en'}
					<h2>Our advantages</h2>
				{elseif $current_lang == 'ch'}
					<h2>公司优势</h2>
				{else}
					<h2>Наши преимущества</h2>
				{/if}

				<div class="advantage__row">
					<div class="advantage__col">
						<img src="/data/pagoda/img/ico/main/1.svg">
						{if $current_lang == 'en'}
							<div class="advantage__col-name">Exclusive<br> Materials</div>
						{elseif $current_lang == 'ch'}
							<div class="advantage__col-name">独家石材</div>
						{else}
							<div class="advantage__col-name">Эксклюзивные<br> материалы</div>
						{/if}
					</div>
					<div class="advantage__col">
						<img src="/data/pagoda/img/ico/main/2.svg" width="60">
						{if $current_lang == 'en'}
							<div class="advantage__col-name">Experienced<br> International Team</div>
						{elseif $current_lang == 'ch'}
							<div class="advantage__col-name">极富经验的国际团队</div>
						{else}
							<div class="advantage__col-name">Русскоязычные<br>менеджеры</div>
						{/if}
					</div>
					<div class="advantage__col">
						<img src="/data/pagoda/img/ico/main/3.svg">
						{if $current_lang == 'en'}
							<div class="advantage__col-name">Advanced Manufacturing<br> Facilities</div>
						{elseif $current_lang == 'ch'}
							<div class="advantage__col-name">先进的制造设备</div>
						{else}
							<div class="advantage__col-name">Высокотехнологичное<br> производство</div>
						{/if}
					</div>
					<div class="advantage__col">
						<img src="/data/pagoda/img/ico/main/4.svg">
						{if $current_lang == 'en'}
							<div class="advantage__col-name">Stone Consulting</div>
						{elseif $current_lang == 'ch'}
							<div class="advantage__col-name">石材咨询</div>
						{else}
							<div class="advantage__col-name">Консалтинг по камню</div>
						{/if}
					</div>
					<div class="advantage__col">
						<img src="/data/pagoda/img/ico/main/5.svg" width="60">
						{if $current_lang == 'en'}
							<div class="advantage__col-name">Efficient <br>Quality Control</div>
						{elseif $current_lang == 'ch'}
							<div class="advantage__col-name">高效的质量控制</div>
						{else}
							<div class="advantage__col-name">Эффективный<br> контроль качества</div>
						{/if}
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="content">
		
		{if $current_lang == 'en'}
			<h2>our Stones</h2>
		{elseif $current_lang == 'ch'}
			<h2>我们的石头</h2>
		{else}
			<h2>Наши камни</h2>
		{/if}
    
    {*
    {if $current_lang == 'en'}
			<a href="/catalog" class="stone__all">all stones</a>
		{elseif $current_lang == 'ch'}
			<a href="/catalog" class="stone__all">我们的石材</a>
		{else}
			<a href="/catalog" class="stone__all">посмотреть все камни</a>
		{/if}
    *}

		
    
    <div class="slider_stone">
			<div class="slider_stone-name" id="slider_stone-name"></div>
			<div id="slider_stone" class="slider slider__block">
      
      {*
      {section name=p loop=$projectMainProds}
        <div class="slider__item item">
          <a href="/{$projectMainProds[p].alias}.html">
            <img class="slider__item-stone" src="/data/big/{$projectMainProds[p].img}" alt="{$projectMainProds[p].header}">
          </a>
        </div>
      {/section}
      *}
      
    {if $current_lang == 'en'}
      {assign var=lang_pref value="/en"}
    {elseif $current_lang == 'ru'}
      {assign var=lang_pref value=""}
    {elseif $current_lang == 'ch'}
      {assign var=lang_pref value="/ch"}
    {/if}
      
        {if $current_lang != 'ch'}
          <div class="slider__item item">
            <a href="{$lang_pref}/138-volga-blue.html">
              <img class="slider__item-stone" src="/data/pagoda/img/volga_blue.png" alt="Volga Blue">
            </a>
          </div>
        {/if}
        
        {if $current_lang == 'ru'}
          <div class="slider__item item">
          <a href="{$lang_pref}/143-baltic-brown.html">
              <img class="slider__item-stone" src="/data/pagoda/img/baltic_brown.png" alt="Baltic Brown">
            </a> 
          </div>
        {/if}
        
        {if $current_lang != 'ch'}
          <div class="slider__item item">
            <a href="{$lang_pref}/112-emperador-light.html">
              <img class="slider__item-stone" src="/data/pagoda/img/emperador_light.png" alt="Emperator Light">
            </a>
          </div> 
        {/if}
        
        {if $current_lang != 'ch'}
          <div class="slider__item item">
            <a href="{$lang_pref}/g383.html">
              <img class="slider__item-stone" src="/data/pagoda/img/g383.png" alt="G383">
            </a>
          </div>
        {/if}
        
        {if $current_lang != 'ch'}
          <div class="slider__item item">
            <a href="{$lang_pref}/g603-jj.html">
              <img class="slider__item-stone" src="/data/pagoda/img/g603jj.png" alt="G603-jj">
            </a>
          </div> 
        {/if}
        
        {if $current_lang != 'ch'}
          <div class="slider__item item">
            <a href="{$lang_pref}/g654-padang-dark.html">
              <img class="slider__item-stone" src="/data/pagoda/img/g654.png" alt="G654 (PADANG DARK)">
            </a>
          </div>
        {/if}
        
        {if $current_lang != 'ch'}
          <div class="slider__item item">
            <a href="{$lang_pref}/134-g684.html">
              <img class="slider__item-stone" src="/data/pagoda/img/g684.png" alt="G684">
            </a>
          </div> 
        {/if}          
        
        {if $current_lang != 'ch'}
          <div class="slider__item item">
            <a href="{$lang_pref}/giallo-ornamentalle.html">
              <img class="slider__item-stone" src="/data/pagoda/img/giallo.png" alt="GIALLO ORNAMENTALLE">
            </a>
          </div> 
        {/if}
        <div class="slider__item item">
       
          {if $current_lang == 'ch'}
            {assign var=altv value="哈雷米黄"}
          {else}
            {assign var=altv value="GOHARE BEIGE"}
          {/if}
          <a href="{$lang_pref}/gohare.html">
            <img class="slider__item-stone" src="/data/pagoda/img/gohare.png" alt="{$altv}">
          </a>
        </div>
        
        <div class="slider__item item">
        
          {if $current_lang == 'ch'}
            {assign var=altv value="天山冰玉"}
          {else}
            {assign var=altv value="ice jade"}
          {/if}
          <a href="{$lang_pref}/ice-jade.html">
            <img class="slider__item-stone" src="/data/pagoda/img/ice_jade.png" alt="{$altv}">
          </a>
        </div>
        
        {if $current_lang != 'ch'}
          <div class="slider__item item">
            <a href="{$lang_pref}/mapple-red.html">
              <img class="slider__item-stone" src="/data/pagoda/img/mapple_red.png" alt="MAPPLE RED">
            </a>
          </div>
        {/if}        
        
        <div class="slider__item item">
        
          {if $current_lang == 'ch'}
            {assign var=altv value="灰太狼"}
          {else}
            {assign var=altv value="NOBLE GREY"}
          {/if}
          <a href="{$lang_pref}/noble-grey.html">
            <img class="slider__item-stone" src="/data/pagoda/img/noble_grey.png" alt="{$altv}">
          </a>
        </div> 

        <div class="slider__item item">
        
          {if $current_lang == 'ch'}
            {assign var=altv value="莎士比亚灰"}
          {else}
            {assign var=altv value="pietra grey"}
          {/if}
          
          <a href="{$lang_pref}/pietra-grey.html">
            <img class="slider__item-stone" src="/data/pagoda/img/pietra_grey.png" alt="{$altv}">
          </a>
        </div>  
        
        <div class="slider__item item">
          <a href="{$lang_pref}/rainbow-jade.html">
          
          {if $current_lang == 'ch'}
            {assign var=altv value="彩虹玉"}
          {else}
            {assign var=altv value="rainbow jade"}
          {/if}
            <img class="slider__item-stone" src="/data/pagoda/img/rainbow_jade.png" alt="{$altv}">
          </a>
        </div> 
        {if $current_lang != 'ch'}
          <div class="slider__item item">
            <a href="{$lang_pref}/shandong-beige.html">
              <img class="slider__item-stone" src="/data/pagoda/img/shandong_beige.png" alt="shandong beige">
            </a>
          </div>
        {/if}        

        {if $current_lang != 'ch'}
          <div class="slider__item item">
            <a href="{$lang_pref}/shandong-rust.html">
              <img class="slider__item-stone" src="/data/pagoda/img/shandong_rust.png" alt="shandong rust">
            </a>
          </div>
        {/if} 
        
        <div class="slider__item item">
          <a href="{$lang_pref}/volga-cristal.html">
          {if $current_lang == 'ch'}
            {assign var=altv value="幻影蓝（黑晶矿）"}
          {else}
            {assign var=altv value="volga cristal"}
          {/if}
            
            <img class="slider__item-stone" src="/data/pagoda/img/volga_cristal.png" alt="{$altv}">
          </a>
        </div>
        
             
			</div>

			<div id="slider_stone_mini" class="slider__mini slider"></div>
      
      
      
    {if $current_lang == 'en'}
      <a class="slider__mini-all" href="/en/catalog">all stones</a>
		{elseif $current_lang == 'ch'}
      <a class="slider__mini-all" href="/ch/catalog">我们的石材</a>
		{else}
			<a class="slider__mini-all" href="/catalog">посмотреть все камни</a>
		{/if}
      
		</div>
    
    
	</div>




	<section class="team">

		<div class="team__wrapper">
			<svg width="0" height="0" version="1.1" xmlns="http://www.w3.org/2000/svg">
				<filter id="blur">
					<feGaussianBlur stdDeviation="3"></feGaussianBlur>
				</filter>
			</svg>



			<div class="team__member">
				<div class="team__video-frame">
					<!-- 3 -->
					<video class="team__video">
						<source src="/data/pagoda/frontend/video/Andrii.mp4" type="video/mp4">
					</video>
				</div>
				<div class="team__desc">
					{if $current_lang == 'en'}
						<div class="team__title">Andrii Izhevskyi</div>
						<div class="team__position">Director of Marketing and Sales</div>
						<div class="team__text">Before joining company in 2011 Andrei has got his master degree in engineering, worked in a big international corporation and traveled around the world. His creativity, passion for art, spiritual pursuits, honesty and sense of humor set foundation of unique Pagoda-Build more-then-just-work spirit, helps us to go beyond conventional thinking, develop non-standard solutions for company and for our customers.</div>

					{elseif $current_lang == 'ch'}
						<div class="team__title">Andrii Izhevskyi</div>
						<div class="team__position">市场及销售经理</div>
						<div class="team__text">
            工程硕士学位，2011年加入公司，此前工作于一家大型国际公司并且环游世界。他的创造力，对艺术的热情，对精神的追求，诚实和幽默感是公司特有精神特质，并且帮助我们超越传统的设计理念，为公司和客户提供了较灵活的解决方案。
            </div>
					{else}
						<div class="team__title">Андрей Ижевский</div>
						<div class="team__position">Директор продаж и маркетинга</div>
						<div class="team__text">В 2011 году стал частью команды Pagoda-Build. До этого получил степень магистра в инженерии, работал в крупной международной компании и путешествовал по миру. Сочетание креативности, интереса к искусству и духновным практикам, честность и чувство юмора Андрея во многом формируют основу уникальной атмосферы Pagoda-Build, а также позволяют находить нестандартные решения для клиентов и компании в целом.</div>
					{/if}
				</div>
			</div>

			<div class="team__member">
				<div class="team__video-frame">
					<video class="team__video">
						<!-- 2 -->
						<source src="/data/pagoda/frontend/video/Kelvin.mp4" type="video/mp4">
					</video>
				</div>
				<div class="team__desc">
					{if $current_lang == 'en'}
						<div class="team__title">Kelvin Guo</div>
						<div class="team__position">Director of Import and Asia-Pacific Sales, Co-Founder</div>
						<div class="team__text">13 years in stone business and prior experience in many other spheres in China and abroad make Kelvin most experienced man in the company. Proficiency in Tai-Chi helps him to stay calm and always find best solution in most challenging situations.</div>
					{elseif $current_lang == 'ch'}
						<div class="team__title">郭开筑</div>
						<div class="team__position">进口及亚太地区销售经理，公司创始人</div>
						<div class="team__text">
							有着13年的石材业务经验及在国内外其他领域的丰富经验使得Kelvin成为公司最具经验的员工。精通太极拳使得他能够始终保持冷静并且能在最具挑战性的情况下找到最佳解决方案。
						</div>
					{else}
						<div class="team__title">Kelvin Guo</div>
						<div class="team__position">Директор по импорту и продажам в Азиатско-Тихоокеанском регионе, со-основатель</div>
						<div class="team__text">Многолетний жизненный и профессиональный опыт в разных областях деятельности, а также 13-летний стаж  <br>в каменной индустрии делает Кельвина наиболее опытным человеков в компании. Регулярная практика Тайцзицюань позволяют ему <br>даже в самых сложных ситуациях всегда сохранять спокойствие и находить наилучшие решения.</div>
					{/if}
				</div>
			</div>
			<div class="team__member js-team-item">
				<div class="team__video-frame">
					<video class="team__video">
						<!-- 1 -->
						<source src="/data/pagoda/frontend/video/Maksym.mp4" type="video/mp4">
					</video>
				</div>
				<div class="team__desc team__desc_right">
					{if $current_lang == 'en'}
						<div class="team__title">Maksym Triasunov</div>
						<div class="team__position">Managing Director, Co-Founder</div>
						<div class="team__text">Believes in specialization and aims to be best in anything what company engages. Determined to deliver best value to our customers through our company unique focus strategy. Master degree from best economics university in Ukraine and EMBA degree from Tsinghua SEM and INSEAD (currently ranked world #1 by Financial Times magazine) make Maksym best educated stone CEO in China.</div>
					{elseif $current_lang == 'ch'}
						<div class="team__title">Maksym Triasunov</div>
						<div class="team__position">总经理，公司创始人</div>
						<div class="team__text">
							专注专业化管理并力求公司一切运营达到最好效果。致力通过公司特有的重点策略为客户提供最大的价值。乌克兰重点大学的经济学硕士学位和清华大学经济管理学院及欧洲工商管理学院EMBA学位(目前在金融时报杂志排名世界第一），这些使得 Maksym成为中国受教育程度最高的石材执行总裁。
						</div>
					{else}
						<div class="team__title">Максим Трясунов</div>
						<div class="team__position">Ген.директор, со-основатель</div>
						<div class="team__text">Нацелен на совершенство всех аспектов деятельности компании. Уверен, что благодаря уникальной стратегии специализации FOCUS, клиенты компании смогут получить продукцию и сервис максимально высокого качества по наиболее конкурентным ценам. Наличие димплома магистра лучшего экономического университета Украины, а также степень EМВА бизнес школ Tsinghua SEM и INSEAD (№1 в мире по версии журнала Financial Times) делает Максима наиболее образованным управленцем в сфере камня в Китае.</div>
					{/if}
				</div>
			</div>
			<div class="team__member">
				<div class="team__video-frame">
					<video class="team__video">
						<!-- 4 -->
						<source src="/data/pagoda/frontend/video/Xiao Liu.mp4" type="video/mp4">
					</video>
				</div>
				<div class="team__desc team__desc_right">
					{if $current_lang == 'en'}
						<div class="team__title">Yanjun Liu</div>
						<div class="team__position">Director of Production and Procurement</div>
						<div class="team__text">Yanjun has got his Bachelor of Philosophy degree from Xiamen University – one of the leading Universities in China. But works in stone industry ever since his graduation in 2006. Being a big admirer of Confucius Teaching and a man of high morale principles, he sets a high standard for all of us and for our relations with clients and suppliers. </div>
					{elseif $current_lang == 'ch'}
						<div class="team__title">刘彦俊</div>
						<div class="team__position">生产及采购经理</div>
						<div class="team__text">
							毕业于厦门大学哲学系。从2006年毕业后就开始从事石材行业。对孔夫子和高尚品德的推崇使得他为我们公司员工，以及公司与客户，供应商关系都建立了较高的标准。
						</div>
					{else}
						<div class="team__title">Yanjun Liu</div>
						<div class="team__position">Директор производства и закупок</div>
						<div class="team__text">Обладатель степени бакалавра по философии в Сямэньском Университете - одном из ведущих ВУЗов Китая. <br> С 2006 года работает в каменной отрасли. Большой поклонник учения Конфуция. Комбинация высоких моральных принципов, незаурядного аналитического ума <br>и профессионального опыта задает высокие стандарты работы с клиентами и поставщиками.</div>
					{/if}
				</div>
			</div>

		</div>

	</section>
  
  <div class="team__info" style="text-align: center;">
			{if $current_lang == 'en' }
				According to our understanding, the successful order is based on clear quality standards <br>and communication between client and all other participants of the production chain.<br> That's why we pay utmost attention to an efficient communication at each stage of order execution.
			{elseif $current_lang == 'ch'}
      
     根据我们的经验，成功有效的订单是基于高质量标准以及与客户和整个供应链之间的良好沟通。
这就是我们注重订单每个环节有效沟通的原因。

      {else}
					Успешно выполненный заказ, в нашем понимании, основан<br>  на четко оговоренных стандартах качества и тесной коммуникации между клиентом <br> и всей производственной цепочкой, начиная с самого первого звонка или письма.<br> Именно поэтому мы уделяем особо пристальное внимание<br>  эффективной коммуникации на каждом этапе выполнения заказа.

      {/if}

		</div>


<!-- 	<section class="team">
		

		<div class="team__video-wrap">
			<div class="team__block">
				<video class="team__video" data-id="3" src="/data/pagoda/frontend/video/Andrii.mp4"></video>
			</div>
			<div class="team__block">
				<video class="team__video" data-id="2" src="/data/pagoda/frontend/video/Kelvin.mp4"></video>
			</div>
			<div class="team__block">
				<video class="team__video" data-id="1" src="/data/pagoda/frontend/video/Maksym.mp4"></video>
			</div>
			
			<div class="team__block">
				<video class="team__video" data-id="4" src="/data/pagoda/frontend/video/Xiao Liu.mp4"></video>
			</div>
		</div>
		<div class="team__info">
			{if $current_lang == 'en' }
				According to our understanding, the successful order is based on clear quality standards <br>and communication between client and all other participants of the production chain.<br> That's why we pay utmost attention to an efficient communication at each stage of order execution.
			{elseif $current_lang == 'ch'}
				据我们所了解，成功有效的订单是基于高质量标准和客户与其整个参与的生产链之间的良好沟通。这就是我们注重订单每个环节有效沟通的原因。
			{else}
				Успешно выполненный заказ, в нашем понимании, основан на четко оговоренных стандартах качества<br> и тесной коммуникации между клиентом и всей производственной цепочкой, начиная с самого первого звонка или письма.<br> Именно поэтому мы уделяем особо пристальное внимание эффективной коммуникации на каждом этапе выполнения заказа.
			{/if}
		</div>
	</section> -->

	<section class="numbers">
		
		{if $current_lang == 'en'}
			<h2>Company by numbers:</h2>
		{elseif $current_lang == 'ch'}
			<h2>公司成员</h2>
		{else}
			<h2>Компания в цифрах</h2>
		{/if}
		<div class="numbers__col-wrap">
			<div class="numbers__col numbers__col_1">
				<div class="numbers__title">$48M</div>
				
				{if $current_lang == 'en' }
					Accumulated Sales
				{elseif $current_lang == 'ch'}
					累计销售
				{else}
					Аккумулированные <br>продажи
				{/if}
			</div>
			<div class="numbers__col numbers__col_2">
				<div class="numbers__title">30+</div>
				
				{if $current_lang == 'en'}
					Focus Materials <br>Distributed
				{elseif $current_lang == 'ch'}
					材料分布
				{else}
					Основных <br>материалов
				{/if}
			</div>
			<div class="numbers__col numbers__col_3" style="margin-top: -9px;">
				<div class="numbers__title">2000000+ m<sup>2</sup></div>
				
				{if $current_lang == 'en'}
					Stone Tiles Manufactured
				{elseif $current_lang == 'ch'}
					石砖制造
				{else}
					Произведенной продукции
				{/if}
			</div>
			<div class="numbers__col numbers__col_4">
				<div class="numbers__title">2000+</div>
				
				{if $current_lang == 'en' }
					Stone Orders Executed
				{elseif $current_lang == 'ch'}
					订单执行
				{else}
					Выполненных заказов
				{/if}
			</div>
			<div class="numbers__col numbers__col_5">
				<div class="numbers__title">2006</div>
				
				{if $current_lang == 'en' }
					We Started
				{elseif $current_lang == 'ch'}
					建于2006年
				{else}
					Год основания
				{/if}
			</div>
		</div>
	</section>

	<div class="content slider__wrap">
		
		<div id="slider" class="slider">
    
    {section name=u loop=$newsMainNews}
			<div class="slider__item item">

				<div class="slider__text">
					{if $current_lang == 'en' || $current_lang == 'ch' }
						Our projects
					{else}
						Наши проекты
					{/if}
					{if $current_lang == 'en' || $current_lang == 'ch' }
					<div><a class="slider__title" href="/{$current_lang}/blog/{$newsMainNews[u].alias}.html">{$newsMainNews[u].title}</a></div>
					{else}
						<div><a class="slider__title" href="/blog/{$newsMainNews[u].alias}.html">{$newsMainNews[u].title}</a></div>
					{/if}
					<div>{$newsMainNews[u].textToPrePublication}</div>
				</div>
				<img class="slider__img" src="/data/images/news/{$newsMainNews[u].news_file}">
			</div>
    {/section}  
    
      
		</div>
		<div class="slider__border">
			<div class="slider__border-left"></div>
			<div id="slider__border-right" class="slider__border-right"></div>
		</div>
	</div>
	<section class="last">
		<div class="content">
			<div class="last__half-wrap">

				{if $companyNewsItem}
				<div class="last__half">
					<div class="last__item last__item_news">
						<a href="{$companyNewsItem.alias}"><img class="last__item-img" src="/data/images/company/middle/{$companyNewsItem.news_file}"></a>
						<div class="last__item-sub">
							{if $current_lang == 'en' || $current_lang == 'ch' }
								Company news
							{else}
								Новости компании
							{/if}
						</div>
						<a href="{$companyNewsItem.alias}" class="last__item-title">{$companyNewsItem.title}</a>
						<a class="last__all" href="/company">
							{if $current_lang == 'en' || $current_lang == 'ch' }
								All news
							{else}
								Все новости
							{/if}
						</a>
					</div>
				</div>
				{/if}

				{if $blogNewsItem}
				<div class="last__half">
					<div class="last__item last__item_blog">
						<a href="{$blogNewsItem.alias}"><img class="last__item-img" src="/data/images/news/middle/{$blogNewsItem.news_file}"></a>
						<div class="last__item-sub">
							{if $current_lang == 'en' || $current_lang == 'ch' }
								Blog
							{else}
								Блог о камне
							{/if}
						</div>
						<a href="{$blogNewsItem.alias}" class="last__item-title">{$blogNewsItem.title}</a>
						<a class="last__all" href="/blog">
							{if $current_lang == 'en' || $current_lang == 'ch' }
								all articles
							{else}
								Все статьи
							{/if}
						</a>
					</div>
				</div>
				{/if}

			</div>
		</div>
	</section>

	<footer class="footer">
		<div class="last__half-wrap">
			<div class="last__half">
				<div class="social">
        {if $current_lang == 'en' || $current_lang == 'ru' }
					<a href="https://www.youtube.com/user/XiamenPagodaBuild" target="_blank" class="social__item">Y</a>

					<a href="http://www.facebook.com/pages/Xiamen-Pagoda-Build-Co-LTD/214424148615132" target="_blank" class="social__item">f</a>
				{/if}
					<!-- <a href="https://new.vk.com/pagodabuild" class="social__item" target="_blank">B</a>
					<a href="https://twitter.com/izhevskyi" class="social__item" target="_blank">t</a> -->
				</div>
			</div>
			<div class="last__half">
				<div class="footer__phone">+86 (592) 519 24 54<br>
					<span class="footer__phone-min">
						{if $current_lang == 'en' || $current_lang == 'ch' }
							9 am - 6 pm, Chinese time
						{else}
							с 9.00 до 18.00 по китайскому времени
						{/if}
					</span>
				</div>
			</div>
		</div>
	</footer>

<div id="popup__overlay" class="popup__overlay"></div>
	<div id="popup__form" class="popup popup__form">
		<div class="page__write">
			<a href="#" id="popup__close" class="popup__close write__x">×</a>
			<div id="write__content_done" class="write__content">
      {if $current_lang == 'ru'}
				<div class="popup__title">Спасибо</div>
          <p>Ваше письмо получено и будет рассмотрено в ближайшее время.</p>
        </div>
      {else}
       <div class="popup__title">Thank you!</div>
        <p>Your letter is received and soon will be considered.</p>
      </div>
      {/if}
      
			<div id="write__content_form" class="write__content write__content_show">
				{if $current_lang == 'en'}
				<div class="popup__title">Contact Us</div>
					<p>Fill in the form and within a few minutes <br/>of our experts will contact you</p>
				{elseif $current_lang == 'ch'}
				<div class="popup__title">聯繫我們</div>
					<p>填寫表格，我們的專家在幾分鐘之內將與您聯繫</p>
				{else}
					<div class="popup__title">Cвязаться с нами</div>
					<p>Заполните форму и через несколько минут <br>наши специалисты свяжуться с Вами</p>
				{/if}
        		<form method="get" id="write_form" class="write__form" action="/mail/mail.php">
					<input type="text" name="name" class="write__input" placeholder="{$ph_name}" required oninvalid="this.setCustomValidity('{$name_error}')" oninput="setCustomValidity('')" >
					<div class="write__line">
						<input type="text" name="email" class="write__input" placeholder="email" required oninvalid="this.setCustomValidity('{$email_error}')" oninput="setCustomValidity('')" >
						<input type="text" name="phone" class="write__input" placeholder="{$ph_contact}" required oninvalid="this.setCustomValidity('{$contact_error}')" oninput="setCustomValidity('')" >
					</div>
					<textarea id="write__text" name="text" placeholder="{$ph_message}" class="write__input write__textarea" required oninvalid="this.setCustomValidity('{$message_error}')" oninput="setCustomValidity('')"></textarea>
					<div id="write__text_hid" class="write__textarea_hid"></div>
         
         
           			<div class="g-recaptcha" data-sitekey="6LcmwCkTAAAAAPyy8ntAEALsv5K1oCSZSheugCsQ"></div>
           			<div id="write__error-captha" class="write__errortext">Check captha</div>
          
					<button type="submit" class="write__submit">{$btn_submit}</button>
				</form>
			</div>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
	<script src="/data/pagoda/frontend/publish/owl.carousel.min.js"></script>
	<script src="/data/pagoda/frontend/publish/index.js"></script>
  
  