{if $smarty.server.REQUEST_URI|@strstr:"blog/?"}

  {assign var="header" value="БЛОГ О Камне"}
  
{/if}

{if $smarty.server.REQUEST_URI|@strstr:"en/blog/?"}

  {assign var="header" value="BLOG"}
  
{/if}

<div id="page__title" class="page__title">
  <h1>{$header}</h1>
     {if $lang neq 'ru'} 
       {assign var=langg value=/$lang }
       {assign var=na_title value='Company News' }
     {elseif $lang eq 'ru'}     
       {assign var=langg value=''}
       {assign var=na_title value='Новости' }
     {/if} 
  
{if $smarty.server.REQUEST_URI|@strstr:"blog/?" && !$smarty.server.REQUEST_URI|@strstr:"en/blog/?"}

   {assign var=na_title value='Новости' }

  <a href="/company/" class="page__title-tab page__title-tab_2">{$na_title}</a>
  <a href="/blog/" class="page__title-tab page__title-tab_active">{$header}</a>
  
{/if}

{if $smarty.server.REQUEST_URI|@strstr:"en/blog/?"}

  {assign var=na_title value='Company News' }

  <a href="/en/company/" class="page__title-tab page__title-tab_2">{$na_title}</a>
  <a href="en/blog/" class="page__title-tab page__title-tab_active">{$header}</a>
  
{/if}  

 {if !$smarty.server.REQUEST_URI|@strstr:"blog/?"} 
    <a href="{$langg}/company/" class="page__title-tab page__title-tab_2">{$na_title}</a>
    <a href="{$langg}/blog/" class="page__title-tab page__title-tab_active">{$header}</a>
 {/if} 
</div>

<div class="page__content" id="page__{$page_id}">
  <div class="page__content-hidden" id="page__content-hidden">
    <div id="page__content-hidden-in"></div>
  </div>
  <div class="page__content-wrap" id="page__content-wrap" style="transform: translateY(0px);">
    <div id="page__content-in" class="page__content-in">

      {foreach from=$smarty.get key=key item=val}
        <!--<p>{$key}:{$val}</p> --> 
      {/foreach}

      <!--<p>{$langPath|regex_replace:"/\/*/":""}</p> -->

      {assign var=current_year value=null}

      {foreach from=$news_array item=val}
        {if $current_year == null}
          {assign var=current_year value=$val.add_date|date_format:"%Y"}
        {elseif $current_year != $val.add_date|date_format:"%Y"}
          {assign var=current_year value=$val.add_date|date_format:"%Y"}
          <div class="blog__year">
            <div class="blog__year-num">{$current_year}</div>
          </div>
        {/if}
        <div class="blog">
          <a href="/blog/{$val.alias}.html" class="blog__image" style="background-image: url(/data/images/news/middle/{$val.news_file});"></a>
          <div class="blog__desc">
            <div class="blog__desc-date">{$val.add_date}</div>
            
            {if $current_lang == 'en' || $current_lang == 'ch' }
              <a class="blog__desc-title" href="/{$current_lang}/blog/{$val.alias}.html">{$val.title}</a>
            {else}
              <a class="blog__desc-title" href="/blog/{$val.alias}.html">{$val.title}</a>
            {/if}
            

            <div class="blog__desc-text">{*$val.textToPrePublication*}</div>

          </div>
        </div>
      {/foreach}

      {assign var=current_year value=null}


    </div>
  </div>
</div>
