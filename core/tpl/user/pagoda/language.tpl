<div id="header__lang-wrap" class="header__lang">
  <div class="header__lang-in">
    <a id="header__lang" class="header__lang-link header__lang-link_current" href="{$langPath}">{$current_lang}</a>
    {foreach from=$lang_list item=lang}
      {if $lang->iso2 != $current_lang}
        <a class="header__lang-link" href="{if $lang->iso2 != 'ru'}/{$lang->iso2}{/if}{$langPath}">{$lang->iso2}</a>
      {/if}
    {/foreach}
  </div>
</div>