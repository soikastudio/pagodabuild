<!--<div class="ppp">{$main_projects|@debug_print_var}</div> -->
<div class="projects-in-article"> 
  {section name=u loop=$main_projects}

  {if $main_projects[u].pnum == 0}
  <div class="main__project-block main__project-block_active">

  {else}
   <div class="main__project-block {$main_projects[u].pnum}">
   {/if}
    <div class="main__project">
      <div class="main__project-image">
        <img class="main__project-img" src="/data/images/projects/middle/{$main_projects[u].project_file}">
      </div>
      <div class="main__project-text">
        <div class="main__project-title">Проекты</div>
        <div class="main__project-big">{$main_projects[u].title}</div>
      </div>
    </div>
     <div class="main__project-desc">
      <div class="main__project-num">56 000 м<sup>2</sup></div>
      в месяц производительная<br>мощность
    </div>
    <div class="main__project-ico">
      <img class="main__project-icon" src="/data/{$smarty.const.TPL}/img/ico/Icon_main_2.svg">
      <div class="main__project-big">регулярные<br>поставки</div>
    </div>
  </div>
  {/section}
</div> 