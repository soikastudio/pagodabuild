<!--$news_item_to_blog -->

{section name=u loop=$news_item_to_blog}
<div class="blog__aside">
  <div class="blog__image">
    <a href="{$news_item_to_blog[u].alias}.html"><img src="/data/images/news/{$news_item_to_blog[u].news_file}"></a>
  </div>
  <div class="blog__desc">
    <div class="blog__desc-date">{$news_item_to_blog[u].add_date}</div>
    <a class="blog__desc-title" href="{$news_item_to_blog[u].alias}.html">{$news_item_to_blog[u].title}</a>
  </div>
</div>
{/section}