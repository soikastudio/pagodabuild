<div class="page__aside-border">
  <div class="page__aside-border-title">Калькулятор</div>
  <div class="calc__wrap">
    <div class="calc" id="calc">
      <div id="calc__main" class="calc__main">
        <div data-block="select" data-type="port" data-default="Порт отгрузки" id="calc__type_port" class="calc__btn">Порт отгрузки</div>
        <div data-block="select" data-type="city" data-default="Город назначения" id="calc__type_city" class="calc__btn calc__btn_dis">Город назначения</div>
        <div data-block="select" data-type="material" data-default="Материал" id="calc__type_material" class="calc__btn calc__btn_dis">Материал</div>
        <div data-block="select" data-type="processing" data-default="Обработка" id="calc__type_processing" class="calc__btn calc__btn_dis">Обработка</div>
        <button data-block="final" id="calc__submit" class="calc__submit calc__submit_dis">Рассчитать стоимость</button>
      </div>
      <div id="calc__select" class="calc__select">
        <div data-block="main" id="calc__list-name" class="calc__btn"></div>
        <ul id="calc__list" class="calc__list"></ul>
      </div>
      <div id="calc__final" class="calc__final">
        <div class="calc__final-row">
          <div class="calc__final-key">Порт отгрузки:</div><div id="calc__final-port" class="calc__final-val">1</div>
          <div class="calc__final-key">Город назначения:</div><div id="calc__final-city" class="calc__final-val">1</div>
          <div class="calc__final-key">Материал:</div><div id="calc__final-material" class="calc__final-val">1</div>
          <div class="calc__final-key">Обработка:</div><div id="calc__final-processing" class="calc__final-val">1</div>
          <div class="calc__final-big">
            <div class="calc__final-key">Цена за вагон:</div><div id="calc__final-wagon" class="calc__final-val">1</div>
            <div class="calc__final-key">Цена за контейнер:</div><div id="calc__final-container" class="calc__final-val">1</div>
          </div>
        </div>
        <button data-block="main" class="calc__submit">Пересчитать стоимость</button>
      </div>
    </div>
  </div>
</div>