<h1 class="main__title">
  {if $current_lang == 'en'}
    Manufacture of stone products. <br/>
    <span class="main__title-min">Supplies of marble and granite from China</span>
  {elseif $current_lang == 'ch'}
    製造的石材產品。 <br/>
    <span class="main__title-min">來自中國的大理石和花崗岩供應</span>
  {else}
    Производство изделий из&nbsp;камня. <br/>
    <span class="main__title-min">Поставки мрамора и&nbsp;гранита из&nbsp;китая</span>
  {/if}
</h1>

<div class="main__content">
  <div class="main__content-item main__content-item_1">
    <div class="main__content-img" style="background-image: url(/data/big/slider.jpg)">
      <span class="main__content-title">Bianco carrara</span>
    </div>
  </div>
  <div class="main__content-item main__content-item_2">
    <div class="main__content-img" style="background-image: url(/data/big/main.jpg)">
      <span class="main__content-title">Bianco carrara</span>
    </div>
  </div>
  <div class="main__content-item main__content-item_3">
    <div class="main__content-img" style="background-image: url(/data/big/image-blog.jpg)">
      <span class="main__content-title">Bianco carrara</span>
    </div>
  </div>
  <div class="main__content-item main__content-item_4">
    <div class="main__content-img" style="background-image: url(/data/big/catalog3.jpg)">
      <span class="main__content-title">Bianco carrara</span>
    </div>
  </div>
  <div class="main__content-item main__content-item_5">
    <div class="main__content-img" style="background-image: url(/data/big/catalog.jpg)">
      <span class="main__content-title">Bianco carrara</span>
    </div>
  </div>
  <div class="main__content-item main__content-item_6">
    <div class="main__content-img" style="background-image: url(/data/big/catalog2.jpg)">
      <span class="main__content-title">Bianco carrara</span>
    </div>
  </div>
  <div class="main__content-item main__content-item_7">
    <div class="main__content-img" style="background-image: url(/data/big/like.jpg)">
      <span class="main__content-title">Bianco carrara</span>
    </div>
  </div>
  <div class="main__content-item main__content-item_all"><a class="main__content-link" href="/{$smarty.get.lang}/catalog">{$btn_all_stones}</a></div>
</div>