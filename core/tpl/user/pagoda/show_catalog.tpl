<div id="page__title" class="page__title cat">
  <h1>{$smarty}</h1>
</div>

<div class="page__content" id="page__{$page_id}">
  <div class="page__content-wrap">
    <svg width="100%" height="400px" viewBox="0 0 602 400" preserveAspectRatio="none" class="catalog__svg">
      <clipPath id="catalog-mask">
        <rect x="0" y="0" width="146" height="146" fill="black" />
        <rect x="151" y="0" width="146" height="400" fill="black" />
        <rect x="302" y="0" width="146" height="400" fill="black" />
        <rect x="453" y="0" width="146" height="400" fill="black" />
      </clipPath>
    </svg>

  {if $current_lang == 'ch'}
    {section name=u loop=$products_to_show}
    
      {if $products_to_show[u].brief_description != ''}
        <div class="catalog__block catalog__block_granite"  data-img="/data/big/{$products_to_show[u].big_picture}">
                  
          <h2 class="catalog__title">{$products_to_show[u].name}</h2>
          {$products_to_show[u].brief_description} 
          <div>
            <a class="catalog__mainlink" href="/{$products_to_show[u].url}">
              <svg class="catalog__image" width="100%" viewBox="0 0 602 300" preserveAspectRatio="none">
                <image xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/data/big/{$products_to_show[u].big_picture}" clip-path="url('#catalog-mask')" x="0" y="-50%" width="100%" height="200%"></image>
                <text x="0" y="170">{$btn_more} ></text>
              </svg>
            </a>
          </div>
        </div>
      {/if}
    {/section}
    
    
  {else}
    
    {* гранит products_to_show *}
    
    {section name=u loop=$products_to_show_granite}
    
      {if $products_to_show_granite[u].brief_description != ''}
          <div class="catalog__block catalog__block_granite" data-type="granite" data-img="/data/big/{$products_to_show_granite[u].big_picture}">       
            <h2 class="catalog__title">{$products_to_show_granite[u].name}</h2>      
            {$products_to_show_granite[u].brief_description}      
            <div>
              <a class="catalog__mainlink" href="/{$products_to_show_granite[u].url}">
                <svg class="catalog__image" width="100%" viewBox="0 0 602 300" preserveAspectRatio="none">
                  <image xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/data/big/{$products_to_show_granite[u].big_picture}" clip-path="url('#catalog-mask')" x="0" y="-80%" width="100%" height="250%"></image>
                  <text x="0" y="170">{$btn_more} ></text>
                </svg>
              </a>
            </div>
          </div>
      {/if} 
      
    {/section}
   
    

    {* мрамор *}
   
    {section name=u loop=$products_to_show_marble}
    
    {if $products_to_show_marble[u].brief_description != ''}
      <div class="catalog__block catalog__block_marble" data-type="marble" data-img="/data/big/{$products_to_show_marble[u].big_picture}">
        <h2 class="catalog__title">{$products_to_show_marble[u].name}</h2>
      
          {$products_to_show_marble[u].brief_description}

        <div>
          <a class="catalog__mainlink" href="/{$products_to_show_marble[u].url}">
            <svg class="catalog__image" width="100%" viewBox="0 0 602 300" preserveAspectRatio="none">
              <image xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/data/big/{$products_to_show_marble[u].big_picture}" clip-path="url('#catalog-mask')" x="0" y="-80%" width="100%" height="250%"></image>
              <text x="0" y="170">{$btn_more} ></text>
            </svg>
          </a>
        </div>
      </div>
    {/if}
    {/section}
    
    
  {/if}
    

  </div>
</div>