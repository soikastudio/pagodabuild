<div id="page__title" class="page__title">
 <h1>{$header}</h1>
     {if $lang neq 'ru'} 
       {assign var=langg value=/$lang }
       {assign var=na_title value='Blog' }     
     {elseif $lang eq 'ru'}     
       {assign var=langg value=''}
       {assign var=na_title value='Блог' }
     {/if} 
  <a href="{$langg}/project/" class="page__title-tab proj">{$header}</a>
  <a href="{$langg}/blog/" class="page__title-tab page__title-tab_active">{$na_title}</a>
</div>

<div class="page__content" id="page__{$page_id}">
  <div class="page__content-hidden" id="page__content-hidden">
    <div id="page__content-hidden-in"></div>
  </div>
  <div class="page__content-wrap" id="page__content-wrap" style="transform: translateY(0px);">
    <div id="page__content-in" class="page__content-in">

      {foreach from=$smarty.get key=key item=val}
        <!--<p>{$key}:{$val}</p> --> 
      {/foreach}

      <!--<p>{$langPath|regex_replace:"/\/*/":""}</p> -->

      {assign var=current_year value=null}

      {foreach from=$project_array item=val}
        {if $current_year == null}
          {assign var=current_year value=$val.add_date|date_format:"%Y"}
        {elseif $current_year != $val.add_date|date_format:"%Y"}
          {assign var=current_year value=$val.add_date|date_format:"%Y"}
          <div class="blog__year">
            <div class="blog__year-num">{$current_year}</div>
          </div>
        {/if}
        <div class="blog">
          <div class="blog__image">
            <img src="/data/images/projects/middle/{$val.project_file}" alt="">
          </div>
          <div class="blog__desc">
            <div class="blog__desc-date">{$val.add_date}</div>
            <a class="blog__desc-title" href="/project/{$val.alias}.html">{$val.title}</a>

            <div class="blog__desc-text">{$val.textToPrePublication}</div>

          </div>
        </div>
      {/foreach}

      {assign var=current_year value=null}

      <h1>Новости</h1>

      {foreach from=$company_array item=val}
        {if $current_year == null}
          {assign var=current_year value=$val.add_date|date_format:"%Y"}
        {elseif $current_year != $val.add_date|date_format:"%Y"}
          {assign var=current_year value=$val.add_date|date_format:"%Y"}
          <div class="blog__year">
            <div class="blog__year-num">{$current_year}</div>
          </div>
        {/if}
        <div class="blog">
          <div class="blog__image"></div>
          <div class="blog__desc">
            <div class="blog__desc-date">{$val.add_date}</div>
            <a class="blog__desc-title" href="/news/{$val.alias}">{$val.title}</a>

            <div class="blog__desc-text">{$val.textToPrePublication}</div>
          </div>
        </div>
      {/foreach}

      {literal}
        <script type="text/javascript">
          $(function () {
            var $blogs = $('.blog');

            $blogs.each(function (i) {
              var $t = $(this),
                  $lead = $t.find('.blog__desc-text'),
                  $img = $lead.find('img'),
                  $prev = $t.find('.blog__image');

              if ($img.length) {
                $prev.html('').append($img[0]);
                console.log($prev);
              }
              $lead.hide();
            });
          });
        </script>
      {/literal}
    </div>
  </div>
</div>
