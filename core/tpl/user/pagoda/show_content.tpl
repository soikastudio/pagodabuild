{
if $page_info != NULL}
  {if $page_info.name != ''}
    {assign var=header value=$page_info.name}
    {assign var=header_lnk value="/catalog"}
    {assign var=header_btn value=$btn_all_stones}
  {else}
    {if $current_lang == 'en' && $page_info.en_title != ''}
      {assign var=header value=$page_info.en_title}
    {elseif $current_lang == 'ch' && $page_info.ch_title != ''}
      {assign var=header value=$page_info.ch_title}
    {elseif $page_info.title != ''}
      {assign var=header value=$page_info.title}
    {/if}
  {/if}
  {if $page_info.name != ''}
    {assign var=body value="<div id=\"catalog__fullimg\" class=\"catalog__fullimg\" style=\"width: 1410px; height: 280px; background-image: url(/data/big/`$page_info.big_picture`);\"></div>"}


    {assign var=body value=`$body``$page_info.description`}
    {*<!--
    {if $product_related_number > 0}
      {assign var=body value="`$body`<h2>Похожие камни</h2><div class=\"catalog__like\">"}
      {section name=i loop=$product_related}
        {assign var=body value="`$body`<a href=\"/`$product_related[i].url`\" class=\"catalog__like-item\"><img class=\"catalog__like-img\" src=\"/data/small/`$product_related[i].preview`\"><span class=\"catalog__like-name\">`$product_related[i].name`</span></a>"}
      {/section}
      {assign var=body value="`$body`</div>"}
    {/if}
     -->
     *}
   {* 
   <!-- <img src=\"/data/`$smarty.const.TPL`/../images/2016_06_20/project.jpg\"> <img src=\"/data/images/projects/`$product_related_projects[u].project_file`\"> -->
    <!--{assign var=body value="`$body`<h2>Реализованые Проекты</h2><div class=\"catalog__project catalog__project_left\"><div class=\"catalog__project-img\"><img src=\"/data/`$smarty.const.TPL`/../images/2016_06_20/project.jpg\"></div><div class=\"catalog__project-text\"><h3>Лучший мрамор для интерьера частного особняка</h3><p>Мы знаем, как украсить интерьер — камень для этого у нас всегда под рукой. Мраморные и гранитные столешницы, лестницы и ступени, полы и подоконники, скульптура из лучших сортов мрамора включая знаменитый каррарский — все это изготовят</p></div></div><div class=\"catalog__project catalog__project_right\"><div class=\"catalog__project-img\"><img src=\"/data/`$smarty.const.TPL`/../images/2016_06_20/project.jpg\"></div><div class=\"catalog__project-text\"><h3>Лучший мрамор для интерьера частного особняка</h3><p>Мы знаем, как украсить интерьер — камень для этого у нас всегда под рукой. Мраморные и гранитные столешницы, лестницы и ступени, полы и подоконники, скульптура из лучших сортов мрамора включая знаменитый каррарский — все это изготовят</p></div></div><div class=\"catalog__project catalog__project_left\"><div class=\"catalog__project-img\"><img src=\"/data/`$smarty.const.TPL`/../images/2016_06_20/project.jpg\"></div><div class=\"catalog__project-text\"><h3>Лучший мрамор для интерьера частного особняка</h3><p>Мы знаем, как украсить интерьер — камень для этого у нас всегда под рукой. Мраморные и гранитные столешницы, лестницы и ступени, полы и подоконники, скульптура из лучших сортов мрамора включая знаменитый каррарский — все это изготовят</p></div></div>"}-->
   *}
   
    {if $article_related_number > 0}
      {if $current_lang == 'en'}
        {assign var=body value="`$body`<h2>Articles about `$header`</h2>"}      
      {elseif $current_lang == 'ch'}
        {assign var=body value="`$body`<h2>用品 `$header`</h2>"}
      {elseif $current_lang == 'ru'}
        {assign var=body value="`$body`<h2>Статьи о `$header`</h2>"}
      {/if}
      {section name=u loop=$product_related_articles}
      
      {assign var=body value="`$body`      
      <div class=\"catalog__project catalog__project_`$product_related_articles[u].position`\">
        <div class=\"catalog__project-img\">
         <a href=\"/`$product_related_articles[u].alias`.html\">
            <img src=\"/data/images/news/middle/`$product_related_articles[u].news_file`\">
          </a>
        </div>
        <div class=\"catalog__project-text\">
          <h3>
           <a href=\"/`$product_related_articles[u].alias`.html\">
            `$product_related_articles[u].title`
          </a></h3>
          <p>`$product_related_articles[u].textToPrePublication`</p>
        </div>
      </div>"}
      {/section}  
     
    {/if}
  
  {else}
    {if $current_lang == 'en' && $page_info.en_textToPublication != ''}
      {assign var=body value=$page_info.en_textToPublication}
    {elseif $current_lang == 'ch' && $page_info.ch_textToPublication != ''}
      {assign var=body value=$page_info.ch_textToPublication}
    {elseif $page_info.textToPublication != ''}
      {assign var=body value=$page_info.textToPublication}
    {/if}
  {/if}
{/if}

{if $smarty.get.show_aux_page == 1}
  {assign var=header_lnk value="/page/reviews.html"}
  {assign var=header_btn value=''}
{elseif $smarty.get.show_aux_page == 15}
  {assign var=header_lnk value="/page/about.html"}
  {assign var=header_btn value=$btn_about}
{/if}

{* Разобраться с корректным подключением кнопки редактирования *}
{*assign var=edit_btn value="<a href='/`$smarty.const.ADMIN_FILE`?dpt=conf&amp;sub=blocks_edit&amp;edit=`$smarty.get.show_aux_page`' title='`$smarty.const.EDIT_BUTTON`' style='margin-left:30px;'>+</a>"*}


{if $header != NULL}

  {if $isadmin == 'yes'}
    {assign var=header value=`$header``$edit_btn`}
  {/if}
  
   {if $smarty.server.REQUEST_URI|@strstr:"blog" && $page_info.add_date}
      <div id="page__title" class="page__title blog_article">
       <div class="blog-data">
       {if $current_lang == 'en'}         
        <span class="article_add_date">{$page_info.add_date}</span><a href="/en/blog" class="page__title-tab blog_item">All articles</a>
       {else}
        <span class="article_add_date">{$page_info.add_date}</span><a href="/blog" class="page__title-tab blog_item">Все статьи</a>
       {/if}
       
      </div>
               
       {else} 
      <div id="page__title" class="page__title">
      {/if}
  
  
    <h1>{$header}</h1>
    {if $header_lnk != NULL && $header_btn != NULL}
      <a href="{if $current_lang != 'ru'}/{$current_lang}{/if}{$header_lnk}" class="page__title-tab">{$header_btn}</a>
    {/if}
  </div>
{/if}

{if $body != NULL}
  
    {if $product_info != NULL}
      <div class="page__content product-page" id="page__{$page_id}">
    {else}
      <div class="page__content" id="page__{$page_id}">
    {/if}

    <div class="page__content-hidden" id="page__content-hidden">
      <div id="page__content-hidden-in"></div>
    </div>
    <div class="page__content-wrap" id="page__content-wrap">
      <div id="page__content-in" class="page__content-in">
        {*foreach from=$page_info key=key item=val}
          <p>{$key}:{$val}</p>
        {/foreach*}
        {$body}
      </div>
    </div>
  </div>
{elseif $blocks != NULL}
  {section name=i loop=$blocks}
    {if $blocks[i].show_head eq 1}
      <div id="page__title" class="page__title">
        <h1>{$blocks[i].title}</h1>
      </div>
    {/if}
    <div class="page__content" id="page__{$page_id}">
      {if $blocks[i].html eq 1}
        {include file="blocks/`$blocks[i].url`" blocknum=$smarty.section.i.index}
      {else}
        {$blocks[i].content}
      {/if}
    </div>
  {/section}
{/if}