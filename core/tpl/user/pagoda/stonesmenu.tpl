<div class="catalog__aside page__aside-border" id="catalog__aside">

{if $catalog_param == 'marble'}
  <span class="catalog__aside-title" id="catalog__type_granite">
{else}
  <span class="catalog__aside-title catalog__aside-title_active" id="catalog__type_granite">
{/if}

  {if $current_lang =='ru'}
  Гранит
  {/if}
  {if $current_lang =='en'}
  Granite
  {/if} 
  {if $current_lang =='ch'}
  我们的石头
  {/if}
  </span>
  
{if $catalog_param == 'marble'}
  <span class="catalog__aside-title catalog__aside-title_r catalog__aside-title_active" id="catalog__type_marble"> 
{else}
  <span class="catalog__aside-title catalog__aside-title_r" id="catalog__type_marble"> 
{/if}
  
   
   {if $current_lang =='ru'}
  МРАМОР
  {/if}
  {if $current_lang =='en'}
  Marble 
  {/if} 
  {if $current_lang =='ch'}
  大理石
  {/if}
  </span>
  
{if $catalog_param == 'marble'} 
   <div class="catalog__aside-list" id="catalog__aside_granite"></div>
  <div class="catalog__aside-list catalog__aside-list_active" id="catalog__aside_marble"></div>
{else} 
  <div class="catalog__aside-list catalog__aside-list_active" id="catalog__aside_granite"></div>
  <div class="catalog__aside-list" id="catalog__aside_marble"></div>
{/if}
  
</div>
{if $current_lang =='ru'}
  <a href="https://www.youtube.com/user/XiamenPagodaBuild" class="catalog__youtube">Подписывайтесь<br>на наш канал</a>
{/if}

{if $current_lang =='en'}
  <a href="https://www.youtube.com/user/XiamenPagodaBuild" class="catalog__youtube">Sign up for <br>our channel </a>
{/if}