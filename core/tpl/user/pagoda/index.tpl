{* Задаем iso2 текущий язык *}
{if $smarty.get.lang}
  {assign var="current_lang" value=$smarty.get.lang}
{else}
  {assign var="current_lang" value="ru"}
{/if}

 

{* Задаем Id страницы *}
 
  

  
{if $smarty.server.REQUEST_URI == "/" || $smarty.server.REQUEST_URI == "/?" 
|| $smarty.server.REQUEST_URI == "/index.php" || $smarty.server.REQUEST_URI == "/`$current_lang`/"}
  {assign var="page_id" value="main"}
 
 
 
{elseif $langPath == '/catalog/' || $langPath|regex_replace:"/\\?.*/":"" == '/catalog/'}
  {assign var="page_id" value="catalog"}
{else}
  {assign var="page_id" value="scroll"}
{/if}

{* Задаем Id страницы в связи с ошибкой с $_GET *}
  {assign var="currentPage" value=$smarty.server.REQUEST_URI|@strtok:"?"}
  {if $currentPage == '/' || $currentPage == '/en/' || $currentPage == '/ch/'}
    {assign var="page_id" value="main"}
  {/if}

{* Задаем надписи на постоянных кнопках, в зависимости от языка *}
{if $current_lang == 'en'}
  {assign var="btn_more" value="More"}
  {assign var="btn_traning" value="Traning"}
  {assign var="btn_feedback" value="CONTACT US"}
  {assign var="btn_up" value="Upstairs"}
  {assign var="btn_submit" value="Send a message"}
  {assign var="btn_all_stones" value="All stones"}
  {assign var="btn_reviews" value="Reviews"}
  {assign var="btn_about" value="About"}

  {assign var="name_error" value="Fill in this field"}
  {assign var="email_error" value="Fill in this field"}
  {assign var="contact_error" value="Fill in this field"}
  {assign var="message_error" value="Fill in this field"}
  {assign var="ph_name" value="Your name"}
  {assign var="ph_contact" value="Your phone or e-mail"}
  {assign var="ph_message" value="Your message"}
{elseif $current_lang == 'ch'}
  {assign var="btn_more" value="更"}
  {assign var="btn_traning" value="訓練"}
  {assign var="btn_feedback" value="联系我们"}
  {assign var="btn_up" value="樓上"}
  {assign var="btn_submit" value="發送消息"}
  {assign var="btn_all_stones" value="所有的石頭"}
  {assign var="btn_reviews" value="評論"}
  {assign var="btn_about" value="关于我们公司"}

  {assign var="name_error" value="Fill in this field"}
  {assign var="email_error" value="Fill in this field"}
  {assign var="contact_error" value="Fill in this field"}
  {assign var="message_error" value="Fill in this field"}
  {assign var="ph_name" value="你的名字"}
  {assign var="ph_contact" value="您的電話或電子郵件"}
  {assign var="ph_message" value="您的留言信息"}
{else}
  {assign var="btn_more" value="Подробнее"}
  {assign var="btn_traning" value="Обучение"}
  {assign var="btn_feedback" value="Связаться с нами"}
  {assign var="btn_up" value="Наверх"}
  {assign var="btn_submit" value="Отправить сообщение"}
  {assign var="btn_all_stones" value="Все камни"}
  {assign var="btn_reviews" value="Отзывы"}
  {assign var="btn_about" value="О нас"}
  
  
  {assign var="name_error" value="Заполните это поле"}
  {assign var="email_error" value="Заполните это поле"}
  {assign var="contact_error" value="Заполните это поле"}
  {assign var="message_error" value="Заполните это поле"}
  {assign var="ph_name" value="Ваше имя"}
  {assign var="ph_contact" value="Ваш телефон"}
  {assign var="ph_message" value="Ваше сообщение"}
{/if}

{* Убираем из пустой страницы всякие ненужные пробелы *}
{assign var="page_body" value=$page_body|regex_replace:"/^(&nbsp;)$/":""}

<!DOCTYPE html>
<html lang="{$current_lang}" xmlns="http://www.w3.org/1999/html">
<head>
  <meta charset="{$smarty.const.DEFAULT_CHARSET}">
  <title>{$page_title}</title>

  {if $page_meta_tags eq ''}
    <meta name="description" content="{$smarty.const.CONF_HOMEPAGE_META_DESCRIPTION}">
    <meta name="keywords" content="{$smarty.const.CONF_HOMEPAGE_META_KEYWORDS}">
  {else}
    {$page_meta_tags}
  {/if}

  <link rel="icon" href="/data/{$smarty.const.TPL}/favicon.ico" type="image/x-icon">



  {if $page_id == "main"}
    <link rel="stylesheet" href="/data/pagoda/frontend/publish/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="/data/pagoda/frontend/publish/style.main.css" type="text/css">
    <link rel="stylesheet" href="/data/pagoda/frontend/publish/style.main-prog.css" type="text/css">
  {else}
    <link rel="stylesheet" href="/data/{$smarty.const.TPL}/frontend/publish/style.min.css" type="text/css">
    <link rel="stylesheet" href="/data/{$smarty.const.TPL}/style-prog.min.css" type="text/css">
  {/if}
    <link rel="stylesheet" href="/data/{$smarty.const.TPL}/font/font.css" type="text/css">

  <script src="http://code.jquery.com/jquery-1.9.0.js"></script>
  <!--<script src="/data/pagoda/js/jquery.ebcaptcha.js"></script>
  <script src="/data/pagoda/js/captcha.js"></script> -->
  
  {if $current_lang =='ru'}
    <script src='https://www.google.com/recaptcha/api.js?hl=ru'></script>
  {else}
    <script src='https://www.google.com/recaptcha/api.js?hl=en'></script>
  {/if}
  

  
<!-- Google Tag Manager -->

{literal}
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5M8WZK5');
</script>
{/literal}
<!-- End Google Tag Manager -->


</head>
<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5M8WZK5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

{*$page_id*}
{if $page_id == "main" }
  {include file="home.tpl"}
  {else}
  <div class="page__background" id="page__background"></div>

  <div class="page" id="page">
    
    <header class="header">

      {* Если главная, выводим логотип картинкой, иначе оформляем ссылку на главную (с учетом языка) *}
      {if $page_id == "main"}
        <span class="header__logo">
        <img src="/data/{$smarty.const.TPL}/img/logo.png">
      </span>
      {else}
        <a class="header__logo" href="{"/`$smarty.get.lang`"}">
          <img src="/data/{$smarty.const.TPL}/img/logo.png">
        </a>
        <a class="header__logo_min" href="{"/`$smarty.get.lang`"}"><img src="/data/{$smarty.const.TPL}/img/logo_min.png"></a>
      {/if}

      {* Подключаем главное меню *}
      {include file="mainmenu.tpl"}

      {* Подключаем языковую панель *}
      {include file="language.tpl"}

    </header>

    {* Подключаем левую колонку *}
    {include file="leftaside.tpl"}

    {if $page_id == 'main'}
      {* Подключаем верхний блок *}
      {* {include file="show_content.tpl" blocks=$top_blocks}*}
     {elseif $page_body != ''}
       {* Выводим основной контент, если он есть *}
      {include file="show_content.tpl" header=$page_title body=$page_body}
    {elseif $product_info != NULL}
      {include file="show_content.tpl" page_info=$product_info}
    {elseif $news_full_array != NULL}
      {include file="show_content.tpl" page_info=$news_full_array}
    {elseif $company_full_array != NULL}
      {include file="show_content.tpl" page_info=$company_full_array}
    {*{elseif $langPath|regex_replace:"/\/*/":"" == 'blog'} *}
    {elseif $langPath|regex_replace:"/\\?.*/":"" == '/blog/'} 
      {include file="show_blog.tpl" header=$page_title lang=$current_lang}
    {*{elseif $langPath|regex_replace:"/\/*/":"" == 'company'}*}
    {elseif $langPath|regex_replace:"/\\?.*/":"" == '/company/'}
      {include file="show_company.tpl" header=$page_title lang=$current_lang}
    {elseif $langPath|regex_replace:"/\/*/":"" == 'project'}
      {include file="show_project.tpl" header=$page_title lang=$current_lang}
        {* {elseif $langPath|regex_replace:"/\/*/":"" == 'catalog'} *}
        
         {elseif $langPath|regex_replace:"/\\?.*/":"" == '/catalog/'} 
      {include file="show_catalog.tpl"}
    {/if}
 
    {* Подключаем нижний блок *}
    {*include file="blocks.tpl" binfo=$bottom_blocks*}
      
      {if $smarty.server.REQUEST_URI|@strstr:"contacts.html"}
        <aside class="page__aside contacts">
     
      {else} 
    <aside class="page__aside">
     {/if}
      {* Подключаем правую колонку *}
      {* Из-за ошибок $_GET используем такую схему *}
      
      {if $langPath|regex_replace:"/\\?.*/":"" != '/catalog/' && $langPath|regex_replace:"/\\?.*/":"" != '/blog/' && $langPath|regex_replace:"/\\?.*/":"" != '/company/'}
        {include file="blocks.tpl" binfo=$right_blocks}
      
      {elseif $langPath|regex_replace:"/\\?.*/":"" == '/catalog/'}
         
         {if $smarty.server.REQUEST_URI|@strstr:"marble"}
            {assign var="catalog_param" value="marble"}
         {else} 
           {assign var="catalog_param" value="granite"}
         {/if}
         
        {include file="stonesmenu.tpl"}
      {/if}
      
    </aside>

    <footer class="page__footer">
      <a id="page__write-btn" class="btn page__footer-btn" href="#">{$btn_feedback}</a>
      <span class="page__footer-phone">
        +86 (592) 519 24 54
        <span class="page__footer-phone-min">
          {if $current_lang == 'en' || $current_lang == 'ch' }
            9 am - 6 pm, Chinese time
          {else}
            с 9.00 до 18.00, Chinese time
          {/if}
        </span>
      </span>
    </footer>
  </div>

  <div id="page__write" class="page__write-wrap">
    <div class="page__write">
      <a href="#" class="page__write-back write__back">{$btn_up}</a>
      <a href="#" class="page__write-back write__x">×</a>
     
      {if $current_lang == 'ru'}
        <div id="write__content_done" class="write__content">
          <h1>Спасибо</h1>
          <p>Ваше письмо получено и будет рассмотрено в ближайшее время.</p>
        </div>
      {else}
        <div id="write__content_done" class="write__content">
          <h1>Thank you!</h1>
          <p>Your letter is received and soon will be considered.</p>
        </div>
      {/if}

      <div id="write__content_form" class="write__content write__content_show">

        {if $current_lang == 'en'}
          <h1>Contact Us</h1>
          <p>Fill in the form and within a few minutes <br/>of our experts will contact you</p>
        {elseif $current_lang == 'ch'}
          <h1>聯繫我們</h1>
          <p>填寫表格，我們的專家在幾分鐘之內將與您聯繫</p>
        {else}
          <h1>Cвязаться с нами</h1>
          <p>Заполните форму и через несколько минут <br>наши специалисты свяжуться с Вами</p>
        {/if}

        <form method="get" action="/mail/mail.php" id="write_form" class="form write__form">

          <input type="text" name="name" class="write__input" placeholder="{$ph_name}" required oninvalid="this.setCustomValidity('{$name_error}')" oninput="setCustomValidity('')" >
          <div class="write__line">
            <input type="text" name="email" class="write__input" placeholder="email" required oninvalid="this.setCustomValidity('{$email_error}')" oninput="setCustomValidity('')" >
            <input type="text" name="phone" class="write__input" placeholder="{$ph_contact}" required oninvalid="this.setCustomValidity('{$contact_error}')" oninput="setCustomValidity('')" >
          </div>
          
          <textarea name="text" id="write__text" placeholder="{$ph_message}" class="write__input write__textarea" required oninvalid="this.setCustomValidity('{$message_error}')" oninput="setCustomValidity('')" ></textarea>

          <div id="write__text_hid" class="write__textarea_hid"></div>
          
          <div class="write__line">
            <div class="g-recaptcha" data-sitekey="6LcmwCkTAAAAAPyy8ntAEALsv5K1oCSZSheugCsQ"></div>
            <div id="write__error-captha" class="write__errortext">Check captha</div>
          </div>
          
          <button type="submit" class="write__submit">{$btn_submit}</button>
        </form>
      </div>
    </div>
  </div>

  <div id="gallery" class="gallery__black">
    <div id="gallery__images" class="gallery__images"></div>
    <div class="gallery__controls">
      <div class="gallery__close" data-action="close">×</div>
      <button class="gallery__left" data-action="prev"></button>
      <button class="gallery__right" data-action="next"></button>
    </div>
    <div id="gallery__alt-wrap" class="gallery__alt gallery__alt_hide">
      <div id="gallery__alt" class="gallery__alt-text"></div>
    </div>
  </div>

<script src="/data/{$smarty.const.TPL}/frontend/publish/cat_type.js"></script>
<script src="/data/{$smarty.const.TPL}/frontend/publish/script.min.js"></script>


{/if}

<!--
{if $page_id == "main"}
<div id="page__write" class="page__write-wrap">
  <div class="page__write">
    <a href="#" class="page__write-back write__back">{$btn_up}</a>
    <a href="#" class="page__write-back write__x">×</a>

    <div class="write__content">

      {if $current_lang == 'en'}
        <h1>Contact Us</h1>
        <p>Fill in the form and within a few minutes <br/>of our experts will contact you</p>
      {elseif $current_lang == 'ch'}
        <h1>聯繫我們</h1>
        <p>填寫表格，我們的專家在幾分鐘之內將與您聯繫</p>
      {else}
        <h1>Cвязаться с нами</h1>
        <p>Заполните форму и через несколько минут <br>наши специалисты свяжуться с Вами</p>
      {/if}

      <form class="write__form">
        <input type="text" class="write__input" placeholder="{$ph_name}">
        <input type="text" class="write__input" placeholder="{$ph_contact}">
        <textarea id="write__text" placeholder="{$ph_message}" class="write__input write__textarea"></textarea>

        <div id="write__text_hid" class="write__textarea_hid"></div>
        <input type="submit" class="write__submit" value="{$btn_submit}">
      </form>
    </div>
  </div>
</div>
{/if}
-->
</body>
</html>