

{if $page_info != NULL}
  {if $page_info.name != ''}
    {assign var=header value=$page_info.name}
    {assign var=header_lnk value="/catalog"}
    {assign var=header_btn value=$btn_all_stones}
  {else}
    {if $current_lang == 'en' && $page_info.en_title != ''}
      {assign var=header value=$page_info.en_title}
    {elseif $current_lang == 'ch' && $page_info.ch_title != ''}
      {assign var=header value=$page_info.ch_title}
    {elseif $page_info.title != ''}
      {assign var=header value=$page_info.title}

    {/if}
  {/if}

{if $page_info.catalog_title != ''}
  {assign var=header value=$page_info.catalog_title}
{/if}
  {if $page_info.name != ''}
    {assign var=body value="<div id=\"catalog__fullimg\" class=\"catalog__fullimg\" style=\"width: 1410px; height: 280px; background-image: url(/data/big/`$page_info.big_picture`);\"></div><h2>`$page_info.name` </h2>"}
    {assign var=body value=`$body``$page_info.description`}

    {*
    <!--
    {if $product_related_number > 0}
      {assign var=body value="`$body`<h2>Похожие камни</h2><div class=\"catalog__like\">"}
      {section name=i loop=$product_related}
        {assign var=body value="`$body`<a href=\"/`$product_related[i].url`\" class=\"catalog__like-item\"><img class=\"catalog__like-img\" src=\"/data/small/`$product_related[i].preview`\"><span class=\"catalog__like-name\">`$product_related[i].name`</span></a>"}
      {/section}
      {assign var=body value="`$body`</div>"}
    {/if}
    -->
    *}

    {if $article_related_number > 0}
    
    {if $current_lang == 'en'}
      {assign var=art_preff value="Articles about"}
    {elseif $current_lang == 'ru'}
      {assign var=art_preff value="Статьи о"}
    {elseif $current_lang == 'ch'}
      {assign var=art_preff value="用品"}
    {/if}
    
    {assign var=body value="`$body`<h2>`$art_preff` `$header`</h2>"}
       {assign var=body value="`$body`<div class='slider catalog__slider'><div class='slider__wrap'>"}
     
        {section name=u loop=$product_related_articles}
        {assign var=body value="`$body`
        
        <div class=\"slider__item\">
          
            <div class=\"catalog__project-img\">
              <img src=\"/data/images/news/middle/`$product_related_articles[u].news_file`\">
            </div>
            <div class=\"catalog__project-text\">
              <h3>`$product_related_articles[u].title`</h3>
            </div>
          
        </div>"}
        {/section}
        {assign var=body value="`$body`</div><div class=\"slider__dots\"></div></div>"}
        
      
    {/if}

  {else}
    {if $current_lang == 'en' && $page_info.en_textToPublication != ''}
      {assign var=body value=$page_info.en_textToPublication}
    {elseif $current_lang == 'ch' && $page_info.ch_textToPublication != ''}
      {assign var=body value=$page_info.ch_textToPublication}
    {elseif $page_info.textToPublication != ''}
      {assign var=body value=$page_info.textToPublication}
    {/if}
  {/if}
{/if}

{if $smarty.get.show_aux_page == 1}
  {assign var=header_lnk value="/page/reviews.html"}
  {assign var=header_btn value=$btn_reviews}
{elseif $smarty.get.show_aux_page == 15}
  {assign var=header_lnk value="/page/about.html"}
  {assign var=header_btn value=$btn_about}
{/if}

{* Разобраться с корректным подключением кнопки редактирования *}
{*assign var=edit_btn value="<a href='/`$smarty.const.ADMIN_FILE`?dpt=conf&amp;sub=blocks_edit&amp;edit=`$smarty.get.show_aux_page`' title='`$smarty.const.EDIT_BUTTON`' style='margin-left:30px;'>+</a>"*}

{if $header != NULL}

  {if $isadmin == 'yes'}
    {assign var=header value=`$header``$edit_btn`}
  {/if}

{/if}

{if $body != NULL and !$smarty.server.REQUEST_URI|strstr:"contacts"}

  <div class="page__block page__content">
    <h1>{$header}</h1>

  {if $smarty.server.REQUEST_URI|strstr:"company" || $smarty.server.REQUEST_URI|strstr:"news" || $smarty.server.REQUEST_URI|strstr:"blog"} 
    {if $page_info.NID != 91}
      <a class="page__btn" href="/blog"><span class="page__btn-trgl"></span>Все статьи</a>
   {/if}
  {elseif !$smarty.server.REQUEST_URI|strstr:"logistics" && !$smarty.server.REQUEST_URI|strstr:"about"}
    {if $categoryID == 7}
      {include file="dropdown.tpl" menu_mobile_products = $menu_mobile_products_marble dropdown__btn_text = 'Nouble grey'}
    {else}
      {include file="dropdown.tpl" menu_mobile_products = $menu_mobile_products_granite dropdown__btn_text = 'Mapple Red'}
    {/if}
  {/if}


        {*foreach from=$page_info key=key item=val}
          <p>{$key}:{$val}</p>
        {*foreach*}
        
     
        {$body}
        

  </div>
  
  {if $smarty.server.REQUEST_URI|strstr:"logistics"}
    {include file="calc.tpl"}
  {/if}
  
{elseif $blocks != NULL}
  {section name=i loop=$blocks}
    {if $blocks[i].show_head eq 1}
      <div id="page__title" class="page__title">
        <h1>{$blocks[i].title}</h1>
      </div>
    {/if}
    <div class="page__content" id="page__{$page_id}">
      {if $blocks[i].html eq 1}
        {include file="blocks/`$blocks[i].url`" blocknum=$smarty.section.i.index}
      {else}
        {$blocks[i].content}
      {/if}
    </div>
  {/section}
{/if}


{if $smarty.server.REQUEST_URI =='/page/contacts.html' or  $smarty.server.REQUEST_URI =='/en/page/contacts.html'
or  $smarty.server.REQUEST_URI =='/ch/page/contacts.html' or $smarty.server.REQUEST_URI|@strstr:"contacts.html" }

  {if $current_lang eq 'ru'}
    {include file="show_contacts.tpl"}
  {elseif $current_lang eq 'en'}
    {include file="show_contacts_en.tpl"}
  {elseif $current_lang eq 'ch'}
    {include file="show_contacts_ch.tpl"}
  {/if}

{/if}