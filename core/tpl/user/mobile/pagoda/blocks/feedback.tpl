<div class="feedback">
  <div class="feedback__image">
    <div class="feedback__image-img" style="background-image: url('/data/{$smarty.const.TPL}/../images/2016_06_20/people.jpg');"></div>
  </div>
  <div class="feedback__desc">
    <div class="feedback__name">Елена Безбородова</div>
    <div>Гендиректор "Навигатор-Байкал"<br>Россия г. Иркутск</div>
  </div>
  <div class="feedback__text">
    Работаем с Пагода-Билд с 2010г. в ритуальной сфере. Совместно обработали десятки проектов и сделали сотни памятников. Общей сложностью около 50 вагонов камня. Камень, поставляемый Пагодой из Китая - высокого качества. Толковые менеджеры, вникающие в суть каждого заказа, что очень ценно! Надежный партнер в Китае!
  </div>
</div>