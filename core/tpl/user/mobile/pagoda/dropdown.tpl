<div class="dropdown">
		<button class="dropdown__btn">
    {$dropdown__btn_text}
		</button>
		<ul class="dropdown__list">
			{section name=u loop=$menu_mobile_products}
				<li><a href="/{$menu_mobile_products[u].alias}.html">{$menu_mobile_products[u].name}</a></li>
			{/section}
		</ul>
	</div>