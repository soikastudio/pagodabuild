<div class="page__block page__content">
	<h1>{$header}</h1>
	{if $lang neq 'ru'}
		{assign var=langg value=/$lang }
		{assign var=na_title value='Blog' }
	{elseif $lang eq 'ru'}
		{assign var=langg value=''}
		{assign var=na_title value='Блог о камне' }
	{/if}

	<div class="blog__tabs">
		<a href="{$langg}/blog/" class="blog__tab ">
			{$na_title}
		</a>
		<a href="{$langg}/company/" class="blog__tab blog__tab_active">

			{$header}
		</a>
	</div>
  
  {assign var=current_year value=null}
  {foreach from=$company_array item=val}
    {if $current_year == null}
      {assign var=current_year value=$val.add_date|date_format:"%Y"}
    {elseif $current_year != $val.add_date|date_format:"%Y"}
      {assign var=current_year value=$val.add_date|date_format:"%Y"}
			<div class="blog__hr">{$current_year}</div>
    {/if}
	<div class="blog">
		<a href="/blog/{$val.alias}.html">
			<img src="/data/images/company/middle/{$val.news_file}" class="blog__img">
		</a>
		<div class="blog__date">{$val.add_date}</div>
		
    
     {if $current_lang == 'en' || $current_lang == 'ch' }
        <a class="blog__title" href="/{$current_lang}/company/{$val.alias}.html">{$val.title}</a>
      {else}
        <a class="blog__title" href="/company/{$val.alias}.html">{$val.title}</a>
      {/if}
    
	</div>
  {/foreach}
	{assign var=current_year value=null}
</div>



