<div class="page__block page__content">
  <h1>
    {if $current_lang eq 'ru'}
		Производство<br>изделий из камня.<br>
		<span class="h1_min">Поставки мрамора<br>и гранита из китая</span>
    
    {elseif $current_lang eq 'en'}
      MANUFACTURING OF <br>STONE PRODUCTS<br>
		<span class="h1_min">SUPPLIES OF MARBLE<br>AND GRANITE FROM CHINA</span>

    {elseif $current_lang eq 'ch'}
      石材制品厂商<br>
		<span class="h1_min">供应产自中国的花岗岩和大理石</span>
    {/if}
	</h1>
  
  <h1>
		
	</h1>
  
</div>

<!-- Products blocks -->
<div class="page__block">

{*
	<div class="slider main__slider main__slider_no-line">
  
  
      <div class="slider__wrap">
        {section name=i loop=$mobileMainProds}
          <div class="slider__item main__slider-item">
            <div class="main__slider-img" style="background-image: url(/data/small/{$mobileMainProds[i].img})"></div>
            <span class="main__slider-title"><a style="color: #000; text-decoration: none;" href="/{$mobileMainProds[i].alias}.html">{$mobileMainProds[i].title}</a></span>
          </div>
        {/section}
              
      </div>
		<div class="slider__dots"><div class="slider__dot slider__dot_active"></div><div class="slider__dot"></div><div class="slider__dot-line" style="width: 226px;"></div></div>
		<div class="main__slider-border"></div>
    {if $current_lang eq 'ru'}
      <a class="main__slider-border-dop" href="/catalog">все камни</a>
    {elseif $current_lang eq 'en'}
      <a class="main__slider-border-dop" href="/en/catalog">all stones</a>
    {elseif $current_lang eq 'ch'}
      <a class="main__slider-border-dop" href="/ch/catalog">我们的石材</a>
    {/if}
	</div>
*}  
  
 

 
  <div class="slider main__slider main__slider_no-line main__slider_stone" data-type="stone">
  
    {if $current_lang == 'en'}
      {assign var=lang_pref value="/en"}
    {elseif $current_lang == 'ru'}
      {assign var=lang_pref value=""}
    {elseif $current_lang == 'ch'}
      {assign var=lang_pref value="/ch"}
    {/if}
  
		<div class="slider__wrap">
    {*{section name=i loop=$mobileMainProds} *}
    
      {if $current_lang != 'ch'}        
        <div class="slider__item main__slider-item">
          <span class="main__slider-title"><a style="color: #000; font-size:14px; text-decoration: none;" href="{$lang_pref}/138-volga-blue.html">Volga Blue</a></span>
          <div class="main__slider-img" style="background-image: url(/data/pagoda/img/volga_blue.png); background-size: contain; background-repeat: no-repeat;"></div>
        </div>                   
      {/if}
         
      <div class="slider__item main__slider-item">
        <span class="main__slider-title">{*<a style="color: #000; font-size:14px; text-decoration: none;" href="{$lang_pref}/138-volga-blue.html">*}Baltic Brown{*</a>*}</span>
        <div class="main__slider-img" style="background-image: url(/data/pagoda/img/baltic_brown.png); background-size: contain; background-repeat: no-repeat;"></div>
      </div> 

      {if $current_lang != 'ch'}        
        <div class="slider__item main__slider-item">
          <span class="main__slider-title"><a style="color: #000; font-size:14px; text-decoration: none;" href="{$lang_pref}/112-emperador-light.html">Emperator Light</a></span>
          <div class="main__slider-img" style="background-image: url(/data/pagoda/img/emperador_light.png); background-size: contain; background-repeat: no-repeat;"></div>
        </div>                   
      {/if}  

      {if $current_lang != 'ch'}        
        <div class="slider__item main__slider-item">
          <span class="main__slider-title"><a style="color: #000; font-size:14px; text-decoration: none;" href="{$lang_pref}/g383.html">g383</a></span>
          <div class="main__slider-img" style="background-image: url(/data/pagoda/img/g383.png); background-size: contain; background-repeat: no-repeat;"></div>
        </div>                   
      {/if} 

      {if $current_lang != 'ch'}        
        <div class="slider__item main__slider-item">
          <span class="main__slider-title"><a style="color: #000; font-size:14px; text-decoration: none;" href="{$lang_pref}/g603-jj.html">g603-jj</a></span>
          <div class="main__slider-img" style="background-image: url(/data/pagoda/img/g603jj.png); background-size: contain; background-repeat: no-repeat;"></div>
        </div>                   
      {/if}  
      
      {if $current_lang != 'ch'}        
        <div class="slider__item main__slider-item">
          <span class="main__slider-title"><a style="color: #000; font-size:14px; text-decoration: none;" href="{$lang_pref}/g654-padang-dark.html">G654 (PADANG DARK)</a></span>
          <div class="main__slider-img" style="background-image: url(/data/pagoda/img/g654.png); background-size: contain; background-repeat: no-repeat;"></div>
        </div>                   
      {/if} 

      {if $current_lang != 'ch'}        
        <div class="slider__item main__slider-item">
          <span class="main__slider-title"><a style="color: #000; font-size:14px; text-decoration: none;" href="{$lang_pref}/134-g684.html">g684</a></span>
          <div class="main__slider-img" style="background-image: url(/data/pagoda/img/g684.png); background-size: contain; background-repeat: no-repeat;"></div>
        </div>                   
      {/if}  
      
      {if $current_lang != 'ch'}        
        <div class="slider__item main__slider-item">
          <span class="main__slider-title"><a style="color: #000; font-size:14px; text-decoration: none;" href="{$lang_pref}/giallo-ornamentalle.html">GIALLO ORNAMENTALLE</a></span>
          <div class="main__slider-img" style="background-image: url(/data/pagoda/img/giallo.png); background-size: contain; background-repeat: no-repeat;"></div>
        </div>                   
      {/if} 
      
      
      <div class="slider__item main__slider-item">
        {if $current_lang == 'ch'}
          {assign var=p_title value="哈雷米黄"}
        {else}
          {assign var=p_title value="GOHARE BEIGE"}
        {/if}     
        <span class="main__slider-title"><a style="color: #000; font-size:14px; text-decoration: none;" href="{$lang_pref}/gohare.html">{$p_title}</a></span>
        <div class="main__slider-img" style="background-image: url(/data/pagoda/img/gohare.png); background-size: contain; background-repeat: no-repeat;"></div>
      </div>   
      
      <div class="slider__item main__slider-item">
        {if $current_lang == 'ch'}
          {assign var=p_title value="天山冰玉"}
        {else}
          {assign var=p_title value="ice jade"}
        {/if}     
        <span class="main__slider-title"><a style="color: #000; font-size:14px; text-decoration: none;" href="{$lang_pref}/ice-jade.html">{$p_title}</a></span>
        <div class="main__slider-img" style="background-image: url(/data/pagoda/img/ice_jade.png); background-size: contain; background-repeat: no-repeat;"></div>
      </div> 

      {if $current_lang != 'ch'}        
        <div class="slider__item main__slider-item">
          <span class="main__slider-title"><a style="color: #000; font-size:14px; text-decoration: none;" href="{$lang_pref}/mapple-red.html">MAPPLE RED</a></span>
          <div class="main__slider-img" style="background-image: url(/data/pagoda/img/mapple_red.png); background-size: contain; background-repeat: no-repeat;"></div>
        </div>                   
      {/if} 

      <div class="slider__item main__slider-item">
        {if $current_lang == 'ch'}
          {assign var=p_title value="灰太狼"}
        {else}
          {assign var=p_title value="NOBLE GREY"}
        {/if}     
        <span class="main__slider-title"><a style="color: #000; font-size:14px; text-decoration: none;" href="{$lang_pref}/noble-grey.html">{$p_title}</a></span>
        <div class="main__slider-img" style="background-image: url(/data/pagoda/img/noble_grey.png); background-size: contain; background-repeat: no-repeat;"></div>
      </div>

      <div class="slider__item main__slider-item">
        {if $current_lang == 'ch'}
          {assign var=p_title value="莎士比亚灰"}
        {else}
          {assign var=p_title value="pietra grey"}
        {/if}     
        <span class="main__slider-title"><a style="color: #000; font-size:14px; text-decoration: none;" href="{$lang_pref}/pietra-grey.html">{$p_title}</a></span>
        <div class="main__slider-img" style="background-image: url(/data/pagoda/img/pietra_grey.png); background-size: contain; background-repeat: no-repeat;"></div>
      </div> 

      <div class="slider__item main__slider-item">
        {if $current_lang == 'ch'}
          {assign var=p_title value="彩虹玉"}
        {else}
          {assign var=p_title value="rainbow jade"}
        {/if}     
        <span class="main__slider-title"><a style="color: #000; font-size:14px; text-decoration: none;" href="{$lang_pref}/rainbow-jade.html">{$p_title}</a></span>
        <div class="main__slider-img" style="background-image: url(/data/pagoda/img/rainbow_jade.png); background-size: contain; background-repeat: no-repeat;"></div>
      </div> 

      {if $current_lang != 'ch'}        
        <div class="slider__item main__slider-item">
          <span class="main__slider-title"><a style="color: #000; font-size:14px; text-decoration: none;" href="{$lang_pref}/shandong-beige.html">shandong beige</a></span>
          <div class="main__slider-img" style="background-image: url(/data/pagoda/img/shandong_beige.png); background-size: contain; background-repeat: no-repeat;"></div>
        </div>                   
      {/if}  

      {if $current_lang != 'ch'}        
        <div class="slider__item main__slider-item">
          <span class="main__slider-title"><a style="color: #000; font-size:14px; text-decoration: none;" href="{$lang_pref}/shandong-rust.html">shandong rust</a></span>
          <div class="main__slider-img" style="background-image: url(/data/pagoda/img/shandong_rust.png); background-size: contain; background-repeat: no-repeat;"></div>
        </div>                   
      {/if} 

      <div class="slider__item main__slider-item">
        {if $current_lang == 'ch'}
          {assign var=p_title value="幻影蓝（黑晶矿）"}
        {else}
          {assign var=p_title value="volga cristal"}
        {/if}     
        <span class="main__slider-title"><a style="color: #000; font-size:14px; text-decoration: none;" href="{$lang_pref}/volga-cristal.html">{$p_title}</a></span>
        <div class="main__slider-img" style="background-image: url(/data/pagoda/img/volga_cristal.png); background-size: contain; background-repeat: no-repeat;"></div>
      </div>       

            
    
    {* {/section}  *}
			
		</div>
    
    
		<div class="main__slider-nums">
			<span class="main__slider-cur">1</span> из
       {if $current_lang == 'ch'}
          {assign var=p_count value="6"}
        {else}
          {assign var=p_title value="17"}
        {/if}    
			<span class="main__slider-sum">{$p_count}</span>
		</div>
		<div class="slider__dots"></div>
		<div class="main__slider-border"></div>
    
	</div>
  
  
  
  
   {if $current_lang eq 'ru'}
      <a class="main__slider-border-dop" href="/catalog">все камни</a>
    {elseif $current_lang eq 'en'}
      <a class="main__slider-border-dop" href="/en/catalog">all stones</a>
    {elseif $current_lang eq 'ch'}
      <a class="main__slider-border-dop" href="/ch/catalog">我们的石材</a>
    {/if}
  
  
</div>

<!-- Power blocks -->
<div class="page__block">
	<div class="slider main__slider2">
		<div class="slider__wrap">
			<div class="slider__item">
				<div class="main__slider2-item">
					<div class="main__slider2-num">
						<div class="main__slider2-big">$48M</div>
            {if $current_lang eq 'ru'}
              Аккумулированные<br>продажи
            {elseif $current_lang eq 'en'}
              Accumulated<br>Sales
            {elseif $current_lang eq 'ch'}
              累计销售
            {/if}
					</div>
					<div class="main__slider2-ico">
						<img src="/data/{$smarty.const.MOBILE_TPL}/static/img/ico/Icon_main_1.svg">
					</div>
				</div>
			</div>
      
			<div class="slider__item">
				<div class="main__slider2-item">
					<div class="main__slider2-num">
						<div class="main__slider2-big">30+</div>
            
            {if $current_lang eq 'ru'}
              Основных<br>материалов
            {elseif $current_lang eq 'en'}
              Focus Materials<br>Distributed
            {elseif $current_lang eq 'ch'}
              材料分布
            {/if}
						
					</div>
					<div class="main__slider2-ico">
						<img src="/data/{$smarty.const.MOBILE_TPL}/static/img/ico/Icon_main_2.svg">
					</div>
				</div>
			</div>

      <div class="slider__item">
				<div class="main__slider2-item">
					<div class="main__slider2-num">
						<div class="main__slider2-big">2000000+ m<sup>2</sup></div>
            
            {if $current_lang eq 'ru'}
              Произведенной<br>продукции
            {elseif $current_lang eq 'en'}
              Stone Tiles<br>Manufactured
            {elseif $current_lang eq 'ch'}
              石砖制造
            {/if}
            
					</div>
					<div class="main__slider2-ico">
						<img src="/data/{$smarty.const.MOBILE_TPL}/static/img/ico/Icon_main_1.svg">
					</div>
				</div>
			</div>
      
       <div class="slider__item">
				<div class="main__slider2-item">
					<div class="main__slider2-num">
						<div class="main__slider2-big">2000+</div>
             {if $current_lang eq 'ru'}
              Выполненных<br>заказов
            {elseif $current_lang eq 'en'}
              Stone Orders<br>Executed
            {elseif $current_lang eq 'ch'}
              订单执行
            {/if}
						
					</div>
					<div class="main__slider2-ico">
						<img src="/data/{$smarty.const.MOBILE_TPL}/static/img/ico/Icon_main_2.svg">
					</div>
				</div>
			</div>

      <div class="slider__item">
				<div class="main__slider2-item">
					<div class="main__slider2-num">
						<div class="main__slider2-big">2006</div>
            {if $current_lang eq 'ru'}
              Год<br>основания
            {elseif $current_lang eq 'en'}
              We Started
            {elseif $current_lang eq 'ch'}
              建于2006年
            {/if}
						
					</div>
					<div class="main__slider2-ico">
						<img src="/data/{$smarty.const.MOBILE_TPL}/static/img/ico/Icon_main_1.svg">
					</div>
				</div>
			</div>
      
		</div>
		<div class="slider__dots"></div>
	</div>
</div>

<!-- News blocks -->
<div class="page__block">
	<div class="slider main__slider">
		<div class="slider__wrap">
    {section name=u loop=$newsMainNews}
			<div class="slider__item main__slider-item">
				<div class="main__slider-img" style="background-image: url(/data/images/news/middle/{$newsMainNews[u].news_file})"></div>
				<div class="main__slider-subtitle">Новости</div>
				<span class="main__slider-title">{$newsMainNews[u].title}</span> 
			</div>
    {/section}
			
		</div>
		<div class="slider__dots"></div>
		<div class="main__slider-border"></div>
	</div>
</div>