{section name=b loop=$binfo}

  {*assign var=preheader value='<a href="/`$smarty.const.ADMIN_FILE`?dpt=conf&amp;sub=blocks_edit&amp;edit=`$binfo[b].bid`" title="`$smarty.const.EDIT_BUTTON`" style="margin-left:30px;">+</a>'*}

  {if $isadmin == 'yes' && $binfo[b].title != ''}
    {assign var=postheader value=`$preheader``$binfo[b].title`}
  {else}
    {assign var=postheader value=$binfo[b].title}
  {/if}
  {* вывод заголовка блока *}
  {if $binfo[b].show_head eq 1}
    <span class="page__aside-border-title">{$postheader}</span>
  {/if}
  {* вывод содержания блока *}
  {if $binfo[b].html eq 1}
    {include file="blocks/`$binfo[b].url`" blocknum=$smarty.section.b.index}
  {else}
    {$binfo[b].content}
  {/if}
{/section}