<div class="page__block page__content">
	<h1>聯繫我們</h1>
	<p>任何问题请随时与我们的经理联系
  </p>

  </div>
<div class="slider">
	<div class="slider__wrap">
		<div class="slider__item">
			<div class="page__content">
				<div class="feedback__contact">
					<span class="feedback__contact-name">郭先生</span>
					<img class="feedback__contact-img" src="/data/images/Kelvin.png">
				</div>
				<p>
					手机:<br>
					<b>+86 (136) 009 24 442</b>
				</p><p>
					微信:<br>
					<b>kelvinguo001</b>
				</p>
			</div>
		</div>
		<div class="slider__item">
			<div class="page__content">
				<div class="feedback__contact">
					<span class="feedback__contact-name">张小姐</span>
					<img class="feedback__contact-img" src="/data/images/IMG_0693.png">
				</div>
				<p>
					手机:<br>
					<b>+86 (136) 009 24 442</b>
				</p><p>
					微信:<br>
					<b>Pagoda168</b>
				</p>
			</div>
		</div>
	</div>
	<div class="slider__dots"></div>
</div>


<div class="page__block page__content">
	<h2>驻中国办事处:</h2>
  
  <!--
	<p>
		<b>Office 1105, Be-Top Building,</b><br>
		Wuyuan Bay, 617 Sishui Rd,<br>
		Huli District, Xiamen 361016, PR China.
	</p><p>
		Tel (fr 9:00 till 13:00 МСК):<br>
		<b>+86 (592) 519 24 54</b>
	</p><p>
		E-mail:<br>
		<b>stone@pagodabuild.com</b>
	</p>
  -->
  
  <p><b>厦门市湖里区五缘湾泗水道<br>
  617号宝拓大厦1105</b></p>
  
  <b>
<p>电话:&nbsp;<b>+86 (592) 519 24 54</b></p>
<p>电子信箱：<br>
<b>stone@pagodabuild.com</b></p>
</b>

<!--
	<h2>Office in China:</h2>
	<p>
		<b>Hongjiang Stone Enterprise <br>Processing Zone Jujiang Area, Shijing town Nan‘an city 362343 Fujian Province, China.</b>
	</p>
-->  
</div>