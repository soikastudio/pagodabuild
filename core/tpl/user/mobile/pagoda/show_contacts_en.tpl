

<div class="page__block page__content">
	<h1>Contacts </h1>
	<p>Feel free to contact one of our managers from the main office in China for any request. Our replies are professional and fast.
  </p>

  </div>
<div class="slider">
	<div class="slider__wrap">
		<div class="slider__item">
			<div class="page__content">
				<div class="feedback__contact">
					<span class="feedback__contact-name">ANDRII<br>IZHEVSKYI</span>
					<img class="feedback__contact-img" src="/data/mobile/static/img/person1.png">
				</div>
				<p>
					Tel./What's up messanger:<br>
					<b>+86 (185) 592 22 834</b>
				</p><p>
					Skype:<br>
					<b>pgd.andrii</b>
				</p><p>
					E-mail:<br>
					<b>stone@pagodabuild.com</b>
				</p>
			</div>
		</div>
    
		<div class="slider__item">
			<div class="page__content">
				<div class="feedback__contact">
					<span class="feedback__contact-name">OLESYA <br>SHILENKO</span>
					<img class="feedback__contact-img" src="/data/images/olesya.png">
				</div>
				<p>
					Tel:<br>
					<b>+86 (159) 809 60 03</b>
				</p><p>
					Skype:<br>
					<b>olesyaxiamen</b>
				</p><p>
					E-mail:<br>
					<b>stone@pagodabuild.com</b>
				</p>
			</div>
		</div>
    
	</div>
	<div class="slider__dots"></div>
</div>


<div class="page__block page__content">
	<h2>Office in China:</h2>
	<p>
		<b>Office 1105, Be-Top Building,</b><br>
		Wuyuan Bay, 617 Sishui Rd,<br>
		Huli District, Xiamen 361016, PR China.
	</p><p>
		Tel (fr 9:00 till 13:00 МСК):<br>
		<b>+86 (592) 519 24 54</b>
	</p><p>
		E-mail:<br>
		<b>stone@pagodabuild.com</b>
	</p>

	<h2>Office in China:</h2>
	<p>
		<b>Hongjiang Stone Enterprise <br>Processing Zone Jujiang Area, Shijing town Nan‘an city 362343 Fujian Province, China.</b>
	</p>
</div>