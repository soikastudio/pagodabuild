<div class="page__block page__content">
  {if $smarty.server.REQUEST_URI == '/catalog/'}
  {assign var=header value='Наши камни'}
  {elseif $smarty.server.REQUEST_URI == '/en/catalog/'}
  {assign var=header value='Our stones'}
  {elseif $smarty.server.REQUEST_URI == '/ch/catalog/'}
  {assign var=header value='我们的石头'}
  {/if}
  <!-- <h1>{$header}</h1> -->
</div>
<!--div class="page__block page__content">
	<div class="dropdown">
		<button class="dropdown__btn">
			Nouble grey
		</button>
		<ul class="dropdown__list">
			{section name=u loop=$products_to_show}
				<li><a href="/{$products_to_show[u].url}">{$products_to_show[u].name}</a></li>
			{/section}
		</ul>
	</div>
</div-->


{if $smarty.server.REQUEST_URI == '/catalog/'}
  <h2 class="page__block page__content catalog__title">Гранит</h2>
  {elseif $smarty.server.REQUEST_URI == '/en/catalog/'}
  <h2 class="page__block page__content catalog__title">Granite</h2>
  {elseif $smarty.server.REQUEST_URI == '/ch/catalog/'}
  <h2 class="page__block page__content catalog__title">花岗岩</h2>
{/if}

<div class="slider catalog__slider">
	<div class="slider__wrap">
	{section name=u loop=$products_to_show_granite}
		<div class="slider__item">
			<a href="/{$products_to_show_granite[u].url}" class="slider__img" style="background-image: url(/data/big/{$products_to_show_granite[u].big_picture});"></a>
			<div class="page__content">
				<a href="/{$products_to_show_granite[u].url}" class="catalog__slider-title">{$products_to_show_granite[u].name}</a>
				<!-- <div class="catalog__slider-subtitle">{$products_to_show_granite[u].name}</div> -->
        		<!-- <a class="page__btn" href="/{$products_to_show_granite[u].url}"><span class=\"page__btn-trgl\"></span>Подробнее</a> -->
			</div>
		</div>
	{/section}
	</div>
	<div class="slider__dots"></div>
</div>



{if $smarty.server.REQUEST_URI == '/catalog/'}
  <h2 class="page__block page__content catalog__title">Мрамор</h2>
  {elseif $smarty.server.REQUEST_URI == '/en/catalog/'}
  <h2 class="page__block page__content catalog__title">Marble</h2>
  {elseif $smarty.server.REQUEST_URI == '/ch/catalog/'}
  <h2 class="page__block page__content catalog__title">大理石</h2>
{/if}

<div class="slider catalog__slider">
	<div class="slider__wrap">
	{section name=u loop=$products_to_show_marble}
		<div class="slider__item">
			<a href="/{$products_to_show_marble[u].url}" class="slider__img" style="background-image: url(/data/big/{$products_to_show_marble[u].big_picture});"></a>
			<div class="page__content">
				<a href="/{$products_to_show_marble[u].url}" class="catalog__slider-title">{$products_to_show_marble[u].name}</a>
				<!-- <div class="catalog__slider-subtitle">{$products_to_show_granite[u].name}</div> -->
				<!-- <a class="page__btn" href="/{$products_to_show_marble[u].url}"><span class=\"page__btn-trgl\"></span>Подробнее</a> -->
			</div>
		</div>
	{/section}
	</div>
	<div class="slider__dots"></div>
</div>