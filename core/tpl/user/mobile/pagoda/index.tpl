{* Задаем iso2 текущий язык *}
{if $smarty.get.lang}
  {assign var="current_lang" value=$smarty.get.lang}
{else}
  {assign var="current_lang" value="ru"}
{/if}

{* Задаем Id страницы *}
{if $smarty.server.REQUEST_URI == "/" || $smarty.server.REQUEST_URI == "/`$current_lang`/"}
  {assign var="page_id" value="main"}
{elseif $langPath == '/catalog/'}
  {assign var="page_id" value="catalog"}
{else}
  {assign var="page_id" value="scroll"}
{/if}

{* Задаем надписи на постоянных кнопках, в зависимости от языка *}
{if $current_lang == 'en'}
  {assign var="btn_more" value="More"}
  {assign var="btn_traning" value="Traning"}
  {assign var="btn_feedback" value="CONTACT US"}
  {assign var="btn_up" value="Upstairs"}
  {assign var="btn_submit" value="Send a message"}
  {assign var="btn_all_stones" value="All stones"}
  {assign var="btn_reviews" value="Reviews"}
  {assign var="btn_about" value="About"}
  {assign var="btn_send_feedback" value="Send a message"}
  {assign var="msg_send_feedback" value="Contact us"}
  {assign var="msg_send_feedback_desc" value="Fill in the form and within a few minutes of our experts will contact you"}

  {assign var="ph_name" value="Your name"}
  {assign var="ph_contact" value="Your phone or e-mail"}
  {assign var="ph_message" value="Your message"}
  
{elseif $current_lang == 'ch'}
  {assign var="btn_more" value="更"}
  {assign var="btn_traning" value="訓練"}
  {assign var="btn_feedback" value="聯繫我們"}
  {assign var="btn_up" value="樓上"}
  {assign var="btn_submit" value="發送消息"}
  {assign var="btn_all_stones" value="所有的石頭"}
  {assign var="btn_reviews" value="評論"}
  {assign var="btn_about" value="关于我们公司"}

  {assign var="ph_name" value="你的名字"}
  {assign var="ph_contact" value="您的電話或電子郵件"}
  {assign var="ph_message" value="您的留言信息"}
  {assign var="btn_send_feedback" value="發送消息"}
  {assign var="msg_send_feedback" value="聯繫我們"}
  {assign var="msg_send_feedback_desc" value="填寫表格，我們的專家在幾分鐘之內將與您聯繫"}
  
{else}
  {assign var="btn_more" value="Подробнее"}
  {assign var="btn_traning" value="Обучение"}
  {assign var="btn_feedback" value="Связаться с нами"}
  {assign var="btn_up" value="Наверх"}
  {assign var="btn_submit" value="Отправить сообщение"}
  {assign var="btn_all_stones" value="Все камни"}
  {assign var="btn_reviews" value="Отзывы"}
  {assign var="btn_about" value="О нас"}

  {assign var="ph_name" value="Ваше имя"}
  {assign var="ph_contact" value="Ваш телефон"}
  {assign var="ph_message" value="Ваше сообщение"}
  {assign var="btn_send_feedback" value="Отправить сообщение"}
  {assign var="msg_send_feedback" value="Связаться<br>с нами"}
  {assign var="msg_send_feedback_desc" value="Заполните форму и через несколько минут наши специалисты свяжуться с Вами"}
{/if}

{* Убираем из пустой страницы всякие ненужные пробелы *}
{assign var="page_body" value=$page_body|regex_replace:"/^(&nbsp;)$/":""}

<!DOCTYPE html>
<html lang="{$current_lang}" xmlns="http://www.w3.org/1999/html">
<head>
  <meta charset="{$smarty.const.DEFAULT_CHARSET}">
  <title>{$page_title}</title>

  <link rel="stylesheet" href="/data/{$smarty.const.MOBILE_TPL}/static/style.min.css" type="text/css">
  <link rel="stylesheet" href="/data/{$smarty.const.MOBILE_TPL}/static/style-prog.min.css" type="text/css">
  <link rel="stylesheet" href="/data/{$smarty.const.MOBILE_TPL}/static/font/font.css" type="text/css">

  {if $page_meta_tags eq ''}
    <meta name="description" content="{$smarty.const.CONF_HOMEPAGE_META_DESCRIPTION}">
    <meta name="keywords" content="{$smarty.const.CONF_HOMEPAGE_META_KEYWORDS}">
    
  {else}
    {$page_meta_tags}
  {/if}
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="icon" href="/data/{$smarty.const.TPL}/favicon.ico" type="image/x-icon">

  <script src="/data/{$smarty.const.TPL}/js/jquery-1.11.2.min.js"></script>
  {if $current_lang =='ru'}
    <script src='https://www.google.com/recaptcha/api.js?hl=ru'></script>
  {else}
    <script src='https://www.google.com/recaptcha/api.js?hl=en'></script>
  {/if}

   
<!-- Google Tag Manager -->

{literal}
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5M8WZK5');
</script>
{/literal}
<!-- End Google Tag Manager -->
   
</head>


<body class="page__body" id="page__body">
  <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5M8WZK5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <div class="page__bg"></div>
  
  <header id="header" class="header">
		<a href="{"/`$smarty.get.lang`"}" class="header__logo">
			<img class="header__logo-img" src="/data/{$smarty.const.MOBILE_TPL}/static/img/logo.png" />
		</a>
		<button class="header__menu-btn popup__link" data-popup="menu">
			<span class="header__menu-ico"></span>
		</button>
	</header>
  
  <div id="popup__menu" class="popup">
		<button class="popup__close">×</button>
        
		<div class="popup__content page__menu">
    
      {*russian*}      
      {if $smarty.get.lang == ''}
        <button id="page__menu-lang" class="page__menu-lang">language Russian <span class="page__menu-lang-corner"></span></button>
        <div id="page__menu-lang-list" class="page__menu-lang-list">       
          {if $smarty.server.REQUEST_URI == '/'}
            <a class="page__menu-lang-item" href="/en">language English</a>
            <a class="page__menu-lang-item" href="/ch">language Chinese</a>
          {else}                  
              {assign var=path value=$smarty.server.REQUEST_URI}                      
            <a class="page__menu-lang-item" href="/en{$path}">language English</a>
            <a class="page__menu-lang-item" href="/ch{$path}">language Chinese</a>
          {/if}
        </div>
      
      {*english*}     
      {elseif $smarty.get.lang == 'en'} 
        <button id="page__menu-lang" class="page__menu-lang">language English <span class="page__menu-lang-corner"></span></button>
        <div id="page__menu-lang-list" class="page__menu-lang-list">       
          {if $smarty.server.REQUEST_URI == '/en'}
            <a class="page__menu-lang-item" href="/">language Russian</a>
            <a class="page__menu-lang-item" href="/ch">language Chinese</a>
          {else}                   
              {assign var=path value=$smarty.server.REQUEST_URI|replace:'/en':''}
            <a class="page__menu-lang-item" href="{$path}">language Russian</a>
            <a class="page__menu-lang-item" href="/ch{$path}">language Chinese</a>
          {/if}
        </div> 

      {*Chinese*}       
      {elseif $smarty.get.lang == 'ch'}      
        <button id="page__menu-lang" class="page__menu-lang">language Chinese <span class="page__menu-lang-corner"></span></button>
        <div id="page__menu-lang-list" class="page__menu-lang-list">        
          {if $smarty.server.REQUEST_URI == '/ch'}
            <a class="page__menu-lang-item" href="/">language Russian</a>
            <a class="page__menu-lang-item" href="/ch">language English</a>
          {else}                  
              {assign var=path value=$smarty.server.REQUEST_URI|replace:'/ch':''}
            <a class="page__menu-lang-item" href="{$path}">language Russian</a>
            <a class="page__menu-lang-item" href="/en{$path}">language English</a>
          {/if}
        </div>           
      {/if}  
          
		  {* Подключаем главное меню *}
      {include file="mainmenu.tpl"}
                
		</div>
        
	</div>
  
  <div id="popup__form" class="popup">
		<button class="popup__close">×</button>
		<div id="write__content_done" class="write__content popup__content">
    {if $current_lang == 'ru'}
      <div class="like_h1">Спасибо</div>
        <p>Ваше письмо получено и будет рассмотрено в ближайшее время.</p>
      </div>
    {else} 
       <div class="like_h1">Thank you!</div>
        <p>Your letter is received and soon will be considered.</p>
      </div>
    {/if}
		<div id="write__content_form" class="write__content popup__content write__content_show">
			<div class="like_h1">{$msg_send_feedback}</div>
			<p>{$msg_send_feedback_desc}</p>
			<form class="form write__form">
				<input type="text" name="name" class="write__input" placeholder="{$ph_name}">
				<input type="text" name="phone" class="write__input" placeholder="{$ph_contact}">
				<div id="write__text_hid" class="write__textarea_hid"></div>
				<textarea name="text" id="write__text" class="write__input write__textarea" placeholder="{$ph_message}"></textarea>
        
        <div class="g-recaptcha" data-sitekey="6LcmwCkTAAAAAPyy8ntAEALsv5K1oCSZSheugCsQ"></div>
        
				<button class="write__submit" type="submit">{$btn_send_feedback}</button>
			</form>
		</div>
	</div>
  
  
 <!-- TO DO includes -->
  <div class="page">

    {* Подключаем левую колонку *}

    {if $page_id == 'main'}
      {* Подключаем верхний блок *}
      {include file="show_content_main.tpl"}
    {elseif $page_body != ''}
      {* Выводим основной контент, если он есть *}
      {include file="show_content.tpl" header=$page_title body=$page_body}
    {elseif $product_info != NULL}
      {include file="show_content.tpl" page_info=$product_info}
    {elseif $news_full_array != NULL}
      {include file="show_content.tpl" page_info=$news_full_array}
    {elseif $company_full_array != NULL}
      {include file="show_content.tpl" page_info=$company_full_array}
    {elseif $langPath|regex_replace:"/\/*/":"" == 'blog'}
      {include file="show_blog.tpl" header=$page_title lang=$current_lang}
    {elseif $langPath|regex_replace:"/\/*/":"" == 'company'}
      {include file="show_company.tpl" header=$page_title lang=$current_lang}
    {elseif $langPath|regex_replace:"/\/*/":"" == 'catalog'}
      {include file="show_catalog.tpl"}
    {/if}
    
    {* Подключаем нижний блок *}
    {*include file="blocks.tpl" binfo=$bottom_blocks*}
  </div>
   

  <footer class="footer">
   <!-- <a class="footer__learn" href="#">Обучение</a> -->

    <div class="footer__right">
      <div class="footer__tel">+86 (592) 519 24 54</div>
      {if $current_lang == 'en'}
        <button class="btn footer__feed popup__link" data-popup="form">Contact us</button>
      
      {elseif $current_lang == 'ch'}
        <button class="btn footer__feed popup__link" data-popup="form">联系方式</button>
      {else}
        <button class="btn footer__feed popup__link" data-popup="form">Связаться с нами</button>
      {/if}
      <div class="footer__social">
        <a class="footer__social-item" href="https://www.youtube.com/user/XiamenPagodaBuild" target="_blank">Y</a>
        <a class="footer__social-item" href="http://www.facebook.com/pages/Xiamen-Pagoda-Build-Co-LTD/214424148615132" target="_blank">f</a>

      </div>
    </div>
  </footer>

  <script src="/data/{$smarty.const.MOBILE_TPL}/static/lib/swipe.js"></script>

  <script src="/data/{$smarty.const.MOBILE_TPL}/static/script.min.js"></script>

</body>
</html>