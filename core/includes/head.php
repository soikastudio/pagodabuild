<?php
#####################################
# ShopCMS: Скрипт интернет-магазина
# Copyright (c) by ADGroup
# http://shopcms.ru
#####################################

        // <head> variables definition: title, meta

        // TITLE & META Keywords & META Description

        if ( isset($_GET["show_aux_page"]) ) // aux page => get title and META information from database
        {
                $page = auxpgGetAuxPage( $show_aux_page );
					
					if(trim($page[$langPref.'aux_page_title']) != '') {
						$page['aux_page_title'] = $page[$langPref.'aux_page_title'];
					} 
                        if ($page["aux_page_title"]) 
						$page_title = $page["aux_page_title"];
                        elseif ($page["aux_page_name"]) $page_title = $page["aux_page_name"];
                        else $page_title =  CONF_SHOP_NAME." - ".CONF_DEFAULT_TITLE;
                $meta_tags = "";
					
					if(trim($page[$langPref.'meta_description']) != '') {
						$page['meta_description'] = $page[$langPref.'meta_description'];
					} 
					if(trim($page[$langPref.'meta_keywords']) != '') {
						$page['meta_keywords'] = $page[$langPref.'meta_keywords'];
					} 
                if  ( $page["meta_description"] != "" )
                        $meta_tags .= "<meta name=\"description\" content=\"".$page["meta_description"]."\">\n";
                if  ( $page["meta_keywords"] != "" )
                        $meta_tags .= "<meta name=\"keywords\" content=\"".$page["meta_keywords"]."\">\n";

        }
        elseif (isset($_GET["fullnews"]))  //  fullnews => get title
        {
                $fullnews_array_head = newsGetFullNewsToCustomer($_GET["fullnews"]);
                        if ($fullnews_array_head["title"]) $page_title = $fullnews_array_head["title"];
                        else $page_title =  CONF_SHOP_NAME." - ".CONF_DEFAULT_TITLE;
                                $meta_tags = "";
                                if  ( CONF_HOMEPAGE_META_DESCRIPTION != "" )
                                        $meta_tags .= "<meta name=\"description\" content=\"".CONF_HOMEPAGE_META_DESCRIPTION."\">\n";
                                if  ( CONF_HOMEPAGE_META_KEYWORDS != "" )
                                        $meta_tags .= "<meta name=\"keywords\" content=\"".CONF_HOMEPAGE_META_KEYWORDS."\">\n";
        }

        elseif (isset($_GET["fullproject"]))  //  fullproject => get title
        {
          //echo 'fsdf'; die();
                $fullproject_array_head = projectGetFullNewsToCustomer($_GET["fullproject"]);
                
                //echo '<pre>'; print_r($fullproject_array_head); echo '</pre>'; die();
                
                        if ($fullproject_array_head["title"]) $page_title = $fullproject_array_head["title"];
                        else $page_title =  CONF_SHOP_NAME." - ".CONF_DEFAULT_TITLE;
                                $meta_tags = "";
                                if  ( CONF_HOMEPAGE_META_DESCRIPTION != "" )
                                        $meta_tags .= "<meta name=\"description\" content=\"".CONF_HOMEPAGE_META_DESCRIPTION."\">\n";
                                if  ( CONF_HOMEPAGE_META_KEYWORDS != "" )
                                        $meta_tags .= "<meta name=\"keywords\" content=\"".CONF_HOMEPAGE_META_KEYWORDS."\">\n";
        }
        elseif (isset($_GET["fulldiary"]))  //  fulldiary => get title
        {
                $fullnews_array_head = diaryGetFullNewsToCustomer($_GET["fulldiary"]);
                        if ($fullnews_array_head["title"]) $page_title = $fullnews_array_head["title"];
                        else $page_title =  CONF_SHOP_NAME." - ".CONF_DEFAULT_TITLE;
                                $meta_tags = "";
                                if  ( CONF_HOMEPAGE_META_DESCRIPTION != "" )
                                        $meta_tags .= "<meta name=\"description\" content=\"".CONF_HOMEPAGE_META_DESCRIPTION."\">\n";
                                if  ( CONF_HOMEPAGE_META_KEYWORDS != "" )
                                        $meta_tags .= "<meta name=\"keywords\" content=\"".CONF_HOMEPAGE_META_KEYWORDS."\">\n";
        }
		 elseif (isset($_GET["fullcompany"]))  //  fullcompany => get title
        {
                $fullnews_array_head = companyGetFullNewsToCustomer($_GET["fullcompany"]);
                        if ($fullnews_array_head["title"]) $page_title = $fullnews_array_head["title"];
                        else $page_title =  CONF_SHOP_NAME." - ".CONF_DEFAULT_TITLE;
                                $meta_tags = "";
                                if  ( CONF_HOMEPAGE_META_DESCRIPTION != "" )
                                        $meta_tags .= "<meta name=\"description\" content=\"".CONF_HOMEPAGE_META_DESCRIPTION."\">\n";
                                if  ( CONF_HOMEPAGE_META_KEYWORDS != "" )
                                        $meta_tags .= "<meta name=\"keywords\" content=\"".CONF_HOMEPAGE_META_KEYWORDS."\">\n";
        }
		//вставка статьи
        		
        else  //not an aux page, e.g. homepage, product/category page, registration form, checkout, etc.
        {
                if (isset($categoryID) && !isset($productID) && $categoryID>0) //category page
                {
                        $q = db_query("select name, en_name, ch_name, title, en_title, ch_title FROM ".CATEGORIES_TABLE." WHERE categoryID=".(int)$categoryID);
                        $r = db_fetch_row($q);
                        
					if(trim($r[$langPref.'title']) != '') {
						$r['title'] = $r[$langPref.'title'];
					} 
							
					if(trim($r[$langPref.'name']) != '') {
						$r['name'] = $r[$langPref.'name'];
					} 
								
                                if($r['title']) $page_title = $r['title'];
                                elseif($r['name']) $page_title = $r['name'];
                        else $page_title =  CONF_SHOP_NAME." - ".CONF_DEFAULT_TITLE;
                        $meta_tags = catGetMetaTagsArr($categoryID);
						
						
						if(isset($_GET['param_5']) && isset($_GET['paramC_5'])) {
							$colTitle = catGetColorParam($categoryID, 5, $_GET['param_5'], $langPref.'meta_title');
							if(trim($colTitle) != '' ) {
								$page_title = $colTitle;
							}
							
							$colDesM = catGetColorParam($categoryID, 5, $_GET['param_5'], $langPref.'meta_description');
							if(trim($colDesM) != '' ) {
								 $meta_tags['meta_description'] = $colDesM;
							}
							
							$colKey = catGetColorParam($categoryID, 5, $_GET['param_5'], $langPref.'meta_keywords');
							if(trim($colKey) != '' ) {
								$meta_tags['meta_keywords'] = $colKey;
							}
						}
						
						$meta_tags = "<meta name=\"Description\" content=\"".$meta_tags["meta_description"]."\">\n<meta name=\"KeyWords\" content=\"".$meta_tags["meta_keywords"]."\" >\n";
						
						
						
						

                }
                else if (isset($productID) && $productID>0) //product information page
                        {
                                $q = db_query("select name, en_name, ch_name, title, en_title, ch_title FROM ".PRODUCTS_TABLE." WHERE productID=".(int)$productID);
                                $r = db_fetch_row($q);
								
								
					if(trim($r[$langPref.'title']) != '') {
						$r['title'] = $r[$langPref.'title'];
					} 
							
					if(trim($r[$langPref.'name']) != '') {
						$r['name'] = $r[$langPref.'name'];
					} 
								
                                if($r['title']) $page_title = $r['title'];
                                elseif($r['name']) $page_title = $r['name'];
                                else $page_title =  CONF_SHOP_NAME." - ".CONF_DEFAULT_TITLE;
                                $meta_tags = prdGetMetaTags($productID);
                        }
                        else // other page
                        {
                                $page_title = CONF_SHOP_NAME." - ".CONF_DEFAULT_TITLE;
                                $meta_tags = "";
                                if  ( CONF_HOMEPAGE_META_DESCRIPTION != "" )
                                        $meta_tags .= "<meta name=\"description\" content=\"".CONF_HOMEPAGE_META_DESCRIPTION."\">\n";
                                if  ( CONF_HOMEPAGE_META_KEYWORDS != "" )
                                        $meta_tags .= "<meta name=\"keywords\" content=\"".CONF_HOMEPAGE_META_KEYWORDS."\">\n";
                        }
        }

        $variodesign = settingSELECT_USERTEMPLATE();
        $smarty->assign("variodesign",$variodesign );
      
      //echo $_SERVER['REQUEST_URI']; die() ;     
             
      if ( $_SERVER['REQUEST_URI'] == '/project/' ) {
        $page_title = 'Проекты';     
      }
      
      if ( $_SERVER['REQUEST_URI'] == '/en/project/' ) {
        $page_title = 'Projects';     
      } 
      
      if ( $_SERVER['REQUEST_URI'] == '/ch/project/' ) {
        $page_title = 'Projects';     
      }
      
      if ( $_SERVER['REQUEST_URI'] == '/company/' ) {
        $page_title = 'Новости';     
      }
      
      if ( $_SERVER['REQUEST_URI'] == '/en/company/' ) {
        $page_title = 'Company News';     
      } 
      
      if ( $_SERVER['REQUEST_URI'] == '/ch/company/' ) {
        $page_title = 'Company News';     
      }
      
      if ( $_SERVER['REQUEST_URI'] == '/blog/' ) {
        $page_title = 'Блог о камне'; 
      }

      if ( $_SERVER['REQUEST_URI'] == '/en/blog/' ) {
        $page_title = 'Blog'; 
      }

      if ( $_SERVER['REQUEST_URI'] == '/ch/blog/' ) {
        $page_title = 'Blog'; 
      }
        
        $smarty->assign("page_title", $page_title );
        $smarty->assign("page_meta_tags", $meta_tags );

?>