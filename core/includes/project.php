<?php
#####################################
# ShopCMS: Скрипт интернет-магазина
# Copyright (c) by ADGroup
# http://shopcms.ru
#####################################


$news_array = projectGetNewsToCustomer();
$smarty->assign("project_array", $news_array);

if (isset($_GET["project"])) {

  echo ''; die();
  function _getUrlToNavigateNews()
  {
    //    $url = "project/";
    $url = "/project/";
    return $url;
  }

  function projectSearchNewsByTemplate($callBackParam, &$count_row, $navigatorParams = null)
  {
//----------------->
    global $langPref;
    if ($navigatorParams != null) {
      $offset = xEscSQL($navigatorParams["offset"]);
      $CountRowOnPage = xEscSQL($navigatorParams["CountRowOnPage"]);
    } else {
      $offset = 0;
      $CountRowOnPage = 0;
    }

    $order_by_clause = " order by add_date DESC";

    $sqlQueryCount = "select count(*) from " . DB_PRFX . "project WHERE " . $langPref . "title !='' ";
    $q = db_query($sqlQueryCount);
    $project_count = db_fetch_row($q);
    $project_count = $project_count[0];
    $limit_clause = (!$CountRowOnPage) ? "" : " LIMIT " . $offset . "," . $CountRowOnPage;
//----------------->
    $sqlQuery = "select NID, add_date, title, en_title, ch_title, textToPrePublication, en_textToPrePublication, ch_textToPrePublication, alias from " . DB_PRFX . "project WHERE " . $langPref . "title !='' " .
      $order_by_clause . $limit_clause;

    $q = db_query($sqlQuery);
    $result = array();
    $i = 0;

    if ($offset >= 0 && $offset <= $project_count) {
      while ($row = db_fetch_row($q)) {
        if (($i < $offset + $CountRowOnPage) ||
          $navigatorParams == null
        ) {
//----------------->							
          if (trim($row[$langPref . 'title']) != "") {


            $row["title"] = $row[$langPref . 'title'];
            $row["add_date"] = dtConvertToStandartForm($row["add_date"]);
            $row["textToPrePublication"] = preg_replace("/##gallery_[0-9]+##/", '', $row[$langPref . "textToPrePublication"]);

            $result[] = $row;
          }
        }
        $i++;
      }
    }
    $count_row = $project_count;
    return $result;
  }

  $callBackParam = array();
  $pre_project_array = array();

  $count = 0;
  $navigatorHtml = GetNavigatorHtml(
    _getUrlToNavigateNews(), CONF_NEWS_COUNT_IN_NEWS_PAGE,
    'projectSearchNewsByTemplate', $callBackParam,
    $pre_project_array, $offset, $count);
  $navigatorHtml = strtr($navigatorHtml, array("&amp;offset=0" => ""));
  $navigatorHtml = strtr($navigatorHtml, array("&amp;offset=" => ""));
  $navigatorHtml = strtr($navigatorHtml, array("&amp;show_all=yes" => "all/"));
  $navigatorHtml = strtr($navigatorHtml, array("//" => "/"));

  $smarty->assign("project_navigator", $navigatorHtml);
  $smarty->assign("pre_project_array", $pre_project_array);
  $smarty->assign("pre_project_array_counter", count($pre_project_array));
  $smarty->assign("main_content_template", "show_project.tpl.html");
}

if (isset($_GET["fullproject"])) {
  
  //echo 'ddd'; die();

  $fullproject_array = projectGetFullNewsToCustomer($_GET["fullproject"]);

  //echo '<pre>'; print_r($fullproject_array); echo '</pre>'; die();

  if ($fullproject_array) {
    $smarty->assign("news_full_array", $fullproject_array);
    $smarty->assign("main_content_template", "show_full_project.tpl.html");
  } else {
    header("HTTP/1.0 404 Not Found");
    header("HTTP/1.1 404 Not Found");
    header("Status: 404 Not Found");
    die(ERROR_404_HTML);
  }

}
?>