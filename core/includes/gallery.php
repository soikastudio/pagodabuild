<?
if(isset($_POST['removeGallery'])) {

	removeGallery($_POST['removeGallery']);
	die();
}

if(isset($_POST['removeGImage']) && isset($_POST['removeGImageGal'])) {
  
  //echo $_POST['removeGImage'] . ' ' . $_POST['removeGImageGal']; die();
  
	$newImgs = array();
	
	$q = db_query("SELECT gImages FROM ".DB_PRFX."gallery WHERE gID=".(int)$_POST['removeGImageGal']);
	if($row=db_fetch_row($q)) {
		$gImages = explode(',',$row['gImages']);
	}
	
  $newImgs = array();
	foreach ($gImages as $img) {
		if($img != $_POST['removeGImage']) {
			$newImgs[] = $img;
		}
	}
	
	db_query("UPDATE ".DB_PRFX."gallery SET  gImages='".implode(',',$newImgs)."'  WHERE gID=".(int)$_POST['removeGImageGal']);
	
	unlink(ROOT_DIR."/data/gallery/".$_POST['removeGImage']);
	unlink(ROOT_DIR."/data/gallery/thmbs/".$_POST['removeGImage']);
	
	die();
}


if(isset($_POST['updGall'])) {
  
  //echo 'die'; die();
  
//	$q = db_query("SELECT gImages FROM ".DB_PRFX."gallery WHERE gID=".(int)$_POST['updGall']);
	
//	if($row=db_fetch_row($q)) {
//		$gImages = explode(',',$row['gImages']);
//	}

	$gImages = array();
	foreach($_POST['srtImages'] as $oldimg) {
		$gImages[] = $oldimg;
	}

	$gImTitles = array();
	foreach($_POST['imageTitle'] as $imgTit) {
		$gImTitles[] = $imgTit;
	}
		
		$valid_formats = array("jpg", "png", "gif", "bmp","jpeg");
		
		$uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/' . "data/gallery/"; //a directory inside
		$uploaddirTh = $_SERVER['DOCUMENT_ROOT'] . '/' . "data/gallery/thmbs/"; //a directory inside
    
    
		
		$resImgs = array();
		$tmImgs = array();
		if(isset($_FILES['photos'])) {
			foreach ($_FILES['photos']['name'] as $name => $value) {
				$filename = stripslashes($_FILES['photos']['name'][$name]);
				$size=filesize($_FILES['photos']['tmp_name'][$name]);
				//get the extension of the file in a lower case format
				  $ext = getExtension($filename);
				  $ext = strtolower($ext);
				
				if(in_array($ext,$valid_formats)) {
				   $image_name=time().translit($filename);
					$imageinfo = getimagesize($_FILES['photos']['tmp_name'][$name]);
				   $mime = $imageinfo['mime'];
					createphoto($_FILES['photos']['tmp_name'][$name],$uploaddir.$image_name,$mime);
					createthumb($_FILES['photos']['tmp_name'][$name],$uploaddirTh.$image_name,$mime);
					
					echo '<div class="pgOGImg" rel="'.(int)$_POST['updGall'].'">
							
							<div class="imW">
								<img src="/'.$uploaddirTh.$image_name.'" class="">
							</div>
							<a href="/'.$uploaddir.$image_name.'" class="highslide" onclick="return hs.expand(this, { slideshowGroup: '.(int)$_POST['updGall'].' })"></a>
							<a rel="'.$image_name.'" class="remIm"></a>
							<input type="hidden" name="srtImages[]" value="'.$image_name.'"/>
							<div class="gallchTitleBtt" rel="'.$image_name.'">
							</div>
							<div class="gallchTitle gallchTitle_'.$image_name.'">
									Подпись:<br><input type="text" name="imageTitle[]" value=""/>
								</div>
							</div>';  
				   $tmImgs[] = $image_name;

				} else { 
					echo '<div class="pgOGImg"><span class="">Неизвестное разширение!</span></div>';
				   
				}
			}
		}
		$tmTitles=array();
		if(count($tmImgs) > 0) {
			
			foreach($tmImgs as $img) {
				$resImgs[] = $img;
				$tmTitles[] = ' ';
			}
		}
		
		$gImTitles = array_merge($tmTitles,$gImTitles);
		
		foreach($gImages as $img) {
			$resImgs[] = $img;
		}
    
    //echo '<pre>'; print_r($resImgs); echo '</pre>';
		
		db_query("UPDATE ".DB_PRFX."gallery SET gHead='".xToText($_POST['gHead'])."', gText='".xToText($_POST['gText'])."', en_gHead='".xToText($_POST['en_gHead'])."', en_gText='".xToText($_POST['en_gText'])."', ch_gHead='".xToText($_POST['ch_gHead'])."', ch_gText='".xToText($_POST['ch_gText'])."', gImages='".implode(',',$resImgs)."', gImTitles='".implode('@:#',$gImTitles)."'  WHERE gID=".(int)$_POST['updGall']);
		
		
		
		die();
}

// ADD GALL
if(isset($_POST['addGall'])) {
	
	$valid_formats = array("jpg", "png", "gif", "bmp","jpeg");
	if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST") 
	{
		
		$uploaddir = "data/gallery/"; //a directory inside
		$uploaddirTh = "data/gallery/thmbs/"; //a directory inside
		$imgsGood = 0;
		 $resImgs ='';
		 $qImgs= array();
		 $gImTitles = array();
		foreach ($_FILES['photos']['name'] as $name => $value)
		{
			$filename = stripslashes($_FILES['photos']['name'][$name]);
			$size=filesize($_FILES['photos']['tmp_name'][$name]);
			//get the extension of the file in a lower case format
			  $ext = getExtension($filename);
			  $ext = strtolower($ext);
			
			 if(in_array($ext,$valid_formats))
			 {
			   $image_name=time().translit($filename);
			   
			   
				$imageinfo = getimagesize($_FILES['photos']['tmp_name'][$name]);
			   $mime = $imageinfo['mime'];
				createphoto($_FILES['photos']['tmp_name'][$name],$uploaddir.$image_name,$mime);
				createthumb($_FILES['photos']['tmp_name'][$name],$uploaddirTh.$image_name,$mime);
			   
			   $imgsGood = 1;
			   $resImgs .= '<div class="pgOGImg"  rel="##GID##">
						<div class="imW">
							<img src="/'.$uploaddirTh.$image_name.'" class="">
						</div>
						<a href="/'.$uploaddir.$image_name.'" class="highslide glShowBig" onclick="return hs.expand(this, { slideshowGroup: ##GID## })"></a>
						<a rel="'.$image_name.'" class="remIm"></a>
						<input type="hidden" name="srtImages[]" value="'.$image_name.'"/>
						<div class="gallchTitleBtt" rel="'.$image_name.'">
						</div>
						<div class="gallchTitle gallchTitle_'.$image_name.'">
								Подпись:<br><input type="text" name="imageTitle[]" value=""/>
							</div>
					</div>';  
			   $qImgs[] = $image_name;
			   $gImTitles[] = ' ';

			}
			  else
			 { 
				$resImgs .= '<div class="pgOGImg"><span class="">Неизвестное разширение!</span></div>';
			   
			 }
			   
		 }
		 
		if($imgsGood == 1) {
			if($_POST['pageID'] > 0) {			
				$gPage = $_POST['pageType']."_".$_POST['pageID'];
				 
				db_query("INSERT INTO ".DB_PRFX."gallery (`gHead`, `gText`, `en_gHead`, `en_gText`, `ch_gHead`, `ch_gText`, `gImages`, `gImTitles`, `gPage`) VALUES ('".xToText($_POST['gHead'])."', '".xToText($_POST['gText'])."','".xToText($_POST['en_gHead'])."', '".xToText($_POST['en_gText'])."','".xToText($_POST['ch_gHead'])."', '".xToText($_POST['ch_gText'])."','".implode(',',$qImgs)."','".implode('@:#',$gImTitles)."', '".$gPage."')");
				 $gID = db_insert_id( DB_PRFX."gallery" );
			} else {	
				db_query("INSERT INTO ".DB_PRFX."gallery (`gHead`, `gText`, `en_gHead`, `en_gText`, `ch_gHead`, `ch_gText`, `gImages`, `gImTitles`, `gPage`) VALUES ('".xToText($_POST['gHead'])."', '".xToText($_POST['gText'])."','".xToText($_POST['en_gHead'])."', '".xToText($_POST['en_gText'])."','".xToText($_POST['ch_gHead'])."', '".xToText($_POST['ch_gText'])."','".implode(',',$qImgs)."','".implode('@:#',$gImTitles)."','".$_POST['pageType']."')");
				 $gID = db_insert_id( DB_PRFX."gallery" );
				
				
				
				
			}
						 
				$resImgs = str_replace('##GID##', $gID, $resImgs);
				 
				 $gTag = "##gallery_".$gID."##";
				db_query("UPDATE ".DB_PRFX."gallery SET gTag='".$gTag."' WHERE gID=".$gID);
			
		$gHdRes = trim(xToText($_POST['gHead']));
		if ($gHdRes == '') {
			$gHdRes = '«Заголовок отсутствует»';
		}
		
			$resHtml = '<div class="pgOneGallBlock gBlock_'.$gID.'">
		<div class="pgOGBHader" rel="'.$gID.'">'.$gHdRes.'</div>
		<div class="pgOGBWr sh_'.$gID.'" style="display:block"><form action="" id="pgOGForm_'.$gID.'" name="pgOGForm_'.$gID.'" method="post" enctype="multipart/form-data">
			<div class="pgOGHeader">
				Заголовок
			</div>
			<div class="pgOGLine">
				<input type="text" name="gHead" value="'.xToText($_POST['gHead']).'">
			</div>
			<div class="pgOGHeader">
				Текст
			</div>
			<div class="pgOGLine">
				<textarea name="gText" >'.xToText($_POST['gText']).'</textarea>
			</div>
			<div class="pgOGHeader">
				Тег для втавки в текст статьи
			</div>
			<div class="pgOGLine">
				<div class="pgOGTag">'.$gTag.'</div>
			</div>
			<div class="pgOGHeader">
				Изображения
			</div>
			<div class="pgOGLine preview_'.$gID.'">
				'.$resImgs.'
		<div class="clear"></div>
			</div>
			<div class="pgOGHeader">
				Добавить изображения к галерее
			</div>
			<div class="pgOGLine">
<div id="imageloadstatus_'.$gID.'" ><img src="/data/admin/loader.gif" style="display:none" alt="Uploading...."/></div>
<div id="imageloadbutton_'.$gID.'">
<input type="file" name="photos[]" rel="'.$gID.'" class="photoimg" multiple="true" />
</div>
			<div class="pgOGLine">
<button type="button" class="saveGallBtt" rel="'.$gID.'" class="addGButt" style="cursor:pointer">Сохранить</button>
<a rel="'.$gID.'" class="remGall">Удалить галерею</a>
<div class="clear"></div>
			</div>
			</div>
<input type="hidden" name="updGall" value="'.$gID.'"/>
		</form>
		</div>
	</div>
		';
		 } else {
			$resHtml = 'Изображения не подходят';
		 }
		 echo $resHtml;
	}
	
	
	die();
}
?>