<? 

if(isset($_POST['mailChimp'])) {
  function clean_msg($key) {
    $key=str_replace("\r", "", $key);
    $key=str_replace("\n", "", $key);
    $find=array(
      "/bcc\:/i",
      "/Content\-Type\:/i",
      "/Mime\-Type\:/i",
      "/cc\:/i",
      "/to\:/i"
    );
    $key=preg_replace($find, "", $key);
    return $key;
  }
  $err = '';
  if(!preg_match("/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/", $_POST['mChMail']) || trim($_POST['mChMail'] == '')) {
    $err .= 'Укажите Ваш E-mail<br>';
  }
  if(trim(clean_msg(nl2br(strip_tags($_POST['mChName'])))) == '') {
    $err .= 'Укажите Ваше имя<br>';
  }
  
  if($err != '') {
    echo $err;
  } else {
    require_once ('core/classes/MCAPI.class.php');
    $api = new MCAPI('cd941eb7edba2d7bd6b9f69ed56516dd-us8');
    $mcName = trim(clean_msg(nl2br(strip_tags($_POST['mChName']))));
    
if(isset($_GET['lang'])) {
$langTMP = $_GET['lang'];
} else {
$langTMP = '';
}

    switch($langTMP) {
      case 'en':
        $langMC = 'en';
         $goodMess =  "<div class='mcMess goodMC'>Subscribed - look for the confirmation email!</div>";
		 $listID = '00642ecb62';
        break;
      case 'ch':
        $langMC = 'zh';
         $goodMess =  "<div class='mcMess goodMC'>您已订阅！在指定的电子邮件确认信发送！</div>";
		 $listID = '011d769939';
        break;
      case '':
        $langMC = 'ru';
         $goodMess =  "<div class='mcMess goodMC'>Вы успешно подписаны! На указанный e-mail отправлено подтверждение!</div>";
		 $listID = '7f93f3aa3d';
        break;
    }
    
    
    $merge_vars = array('FNAME'=>$mcName, 'MC_LANGUAGE'=>$langMC);
    
    $api->listSubscribe( $listID,$_POST['mChMail'], $merge_vars );

    if ($api->errorCode){
echo '<div class="mcMess errMC ">'.$api->errorMessage .'</div>';
    } else {
      echo $goodMess;
    }
  }
  die();
}


if(isset($_POST['pCounterTb'])) {
	$resSel = '';
	if(isset($_POST['getCitiesPort'])) {
		$q = db_query("SELECT DISTINCT russia FROM ".DB_PRFX."pc_prices WHERE china=".$_POST['getCitiesPort']);
		$ctsR = array();
		while($row=db_fetch_row($q)) {
			$ctsR[] = $row['russia'];
		}
		
		if(count($ctsR) > 0) {
			$resSel .= '<select name="pCity" onchange="chCity(this)" ><option value="">Выбрать</option>';
			
			$qR = db_query("SELECT * FROM ".DB_PRFX."pc_russia WHERE id IN (".implode(',',$ctsR).")");
			while($row=db_fetch_row($qR)) {
				$resSel .= '<option value="'.$row['id'].'">'.$row['name'].'</option>';
			}
			
			
			$resSel .= '</select>';
		} else {
			$resSel = '<select name="" id="" disabled="disabled"><option value="">Выбрать</option></select>';
		}
		
	} elseif (isset($_POST['getMatCity'])) {
		$q = db_query("SELECT DISTINCT material FROM ".DB_PRFX."pc_prices WHERE russia=".$_POST['getMatCity']." AND china=".$_POST['portVal']);
		$ctsR = array();
		while($row=db_fetch_row($q)) {
			$ctsR[] = $row['material'];
		}
		
		if(count($ctsR) > 0) {
			$resSel .= '<select name="pMat" onchange="chMat(this)" ><option value="">Выбрать</option>';
			
			$qR = db_query("SELECT * FROM ".DB_PRFX."pc_materials WHERE id IN (".implode(',',$ctsR).")");
			while($row=db_fetch_row($qR)) {
				$resSel .= '<option value="'.$row['id'].'">'.$row['name'].'</option>';
			}
			$resSel .= '</select>';
		} else {
			$resSel = '<select name="" id="" disabled="disabled"><option value="">Выбрать</option></select>';
		}
	} elseif (isset($_POST['getFinMat'])) {
		$q = db_query("SELECT DISTINCT finishing FROM ".DB_PRFX."pc_prices WHERE material=".$_POST['getFinMat']." AND russia=".$_POST['ctVal']." AND china=".$_POST['portVal']);
		$ctsR = array();
		while($row=db_fetch_row($q)) {
			$ctsR[] = $row['finishing'];
		}
		
		if(count($ctsR) > 0) {
			$resSel .= '<select name="pFin" onchange="chFin(this)" ><option value="">Выбрать</option>';
			
			$qR = db_query("SELECT * FROM ".DB_PRFX."pc_finishing WHERE id IN (".implode(',',$ctsR).")");
			while($row=db_fetch_row($qR)) {
				$resSel .= '<option value="'.$row['id'].'">'.$row['name'].'</option>';
			}
			$resSel .= '</select>';
		} else {
			$resSel = '<select name="" id="" disabled="disabled"><option value="">Выбрать</option></select>';
		}
	} elseif (isset($_POST['getPriceFin'])) {
		$q = db_query("SELECT wagon, container FROM ".DB_PRFX."pc_prices WHERE finishing=".$_POST['getPriceFin']." AND material=".$_POST['matVal']." AND russia=".$_POST['ctVal']." AND china=".$_POST['portVal']." ORDER BY id LIMIT 0,1");
		
		if($row=db_fetch_row($q)) {
			$resSel .= $row['wagon'].'@@'.$row['container'];
		} else {
			$resSel .= '0@@0';
		}
		
	}

	echo $resSel;
	die();
}



if (isset($_POST['senQuest'])) {


  function clean_msg($key) {
    $key=str_replace("\r", "", $key);
    $key=str_replace("\n", "", $key);
    $find=array(
      "/bcc\:/i",
      "/Content\-Type\:/i",
      "/Mime\-Type\:/i",
      "/cc\:/i",
      "/to\:/i"
    );
    $key=preg_replace($find, "", $key);
    return $key;
  }

  $name = clean_msg(nl2br(strip_tags($_POST['qName'])));
  $mailPhone = clean_msg(nl2br(strip_tags($_POST['kont'])));
  $qBody = clean_msg(nl2br(strip_tags($_POST['qMess'])));
  
  if(!preg_match("/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+$/", $mailPhone)) {
    $qMail = 'NoName@noMail.no';
    $qMessKont = 'Телефон: '.$mailPhone."\n";
  } else {
    $qMail = $mailPhone;
    $qMessKont = 'E-mail: '.$mailPhone."\n";
  }
  
  $message_text = 'Имя: '.$name."\n";
  $message_text .= $qMessKont;
  $message_text .= 'Сообщение: ';
  $message_text .= $qBody;
  
  $message_subject = 'Сообщение с сайта '.CONF_SHOP_URL;
  
  $chMail= explode(',',CONF_GENERAL_EMAIL);
  
  
  if (xMailTxtHTML($chMail[0], $message_subject, $message_text, $qMail, $name)){
        echo '1';
    } else {
        echo 'err';
  }
  die();
  
}
?>