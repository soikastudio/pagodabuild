<?php
#####################################
# ShopCMS: Скрипт интернет-магазина
# Copyright (c) by ADGroup
# http://shopcms.ru
#####################################

        if ( isset($show_aux_page) )
        {
                $aux_page = auxpgGetAuxPage( $show_aux_page );

                if ( $aux_page )
                {
				
				if(trim($aux_page[$langPref.'aux_page_name']) != "") {
					$aux_page["aux_page_name"] = $aux_page[$langPref."aux_page_name"];
				} else {
					if($langN !='') {
						Redirect("/".$langN."/");
					} else {
						Redirect("/");
					}
				}
				
					$aux_text = checkGalleriesTags('aux',$show_aux_page,$aux_page[$langPref."aux_page_text"]);
					$aux_text = checkSpoiler($aux_text);
					
					if(strstr($aux_text, "#priceTable#")) {
						$pCounter = $smarty->fetch('pcounter.tpl.html');
						$aux_text = str_replace('#priceTable#',$pCounter,$aux_text);
					}
					$aux_text = str_replace('#priceTable#','',$aux_text);
					
                        $smarty->assign("page_body", $aux_text );
                        $smarty->assign("aux_page_name", $aux_page["aux_page_name"] );
                        $smarty->assign("show_aux_page", $aux_page["aux_page_ID"] );
                        $smarty->assign("main_content_template", "show_aux_page.tpl.html" );
                }
                else
                {
                        header("HTTP/1.0 404 Not Found");
                        header("HTTP/1.1 404 Not Found");
                        header("Status: 404 Not Found");
                        die(ERROR_404_HTML);
                }
        }
?>