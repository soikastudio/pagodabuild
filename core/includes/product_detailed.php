<?php
#####################################
# ShopCMS: Скрипт интернет-магазина
# Copyright (c) by ADGroup
# http://shopcms.ru
#####################################

if (isset($_POST["cart_x"])) //add product to cart
{
  $variants = array();
  foreach ($_POST as $key => $val) {
    if (strstr($key, "option_select_hidden_"))
      $variants[] = $val;
  }
  unset($_SESSION["variants"]);
  $_SESSION["variants"] = $variants;
  Redirect("index.php?shopping_cart=yes&add2cart=" . (int)$_GET['productID'] . "&multyaddcount=" . (int)$_POST['multyaddcount']);
}


// product detailed information view

if (isset($_GET["vote"]) && isset($productID)) //vote for a product
{
  if (!isset($_SESSION["vote_completed"][$productID]) && isset($_GET["mark"]) && strlen($_GET["mark"]) > 0) {
    $mark = (int)$_GET["mark"];

    if ($mark > 0 && $mark <= 5) {
      db_query("UPDATE " . PRODUCTS_TABLE . " SET customers_rating=(customers_rating*customer_votes+'" . $mark . "')/(customer_votes+1), customer_votes=customer_votes+1 WHERE productID=" . $productID);
    }
  }
  $_SESSION["vote_completed"][$productID] = 1;
}

//echo 





if (isset($_POST["request_information"])) //email inquiry to administrator
{
  $customer_name = $_POST["customer_name"];
  $customer_email = $_POST["customer_email"];
  $message_subject = $_POST["message_subject"] . " (" . CONF_FULL_SHOP_URL . "index.php?productID=" . $productID . ")";
  $message_text = $_POST["message_text"];

  //validate input data
  if (trim($customer_email) != "" && trim($customer_name) != "" && trim($message_subject) != "" && trim($message_text) != "" && preg_match("/^[_\.a-z0-9-]{1,20}@(([a-z0-9-]+\.)+(com|net|org|mil|edu|gov|arpa|info|biz|inc|name|[a-z]{2})|[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})$/is", $customer_email)) {
    //send a message to store administrator
    if (CONF_ENABLE_CONFIRMATION_CODE) {
      $error_p = 1;
      if (!$_POST['fConfirmationCode'] || !isset($_SESSION['captcha_keystring']) || $_SESSION['captcha_keystring'] !== $_POST['fConfirmationCode']) {
        $error_p = 7;
        $smarty->assign("error", $error_p);
      }
      unset($_SESSION['captcha_keystring']);
      if ($error_p == 1) {
        xMailTxtHTML(CONF_GENERAL_EMAIL, $message_subject, $message_text, $customer_email, $customer_name);
        Redirect("index.php?productID=" . $productID . "&sent=yes");
      }
    } else {
      xMailTxtHTML(CONF_GENERAL_EMAIL, $message_subject, $message_text, $customer_email, $customer_name);
      Redirect("index.php?productID=" . $productID . "&sent=yes");
    }
  } else if (isset($_POST["request_information"])) $smarty->assign("error", 1);
}


//show product information
if (isset($productID) && $productID > 0 && !isset($_POST["add_topic"]) && !isset($_POST["discuss"])) {
  $product = GetProduct($productID);

  //echo $productID; die();
 // var_dump($product); die();
  //echo '<pre>'; echo '</pre>';

   // получить изображение для блока справа
  $productimage = '';
   $q = db_query("select filename, enlarged from pgd_product_pictures where productID=".$productID);
         while ($row = db_fetch_row($q)) {
           $out['image'] = $row['enlarged'];
         }
  //echo "select filename from pgd_product_pictures where productID=".$productID;
  //echo '<pre>'; print_r($row); echo '</pre>'; die();
  if ($out['image']) {
    $productimage = $out['image'];
  }
  $smarty->assign("productimage", $productimage);



  if (!$product || $product["enabled"] == 0) {

    header("HTTP/1.0 404 Not Found");
    header("HTTP/1.1 404 Not Found");
    header("Status: 404 Not Found");
    die(ERROR_404_HTML);

  } else {
    if (trim($product[$langPref . 'name']) != '') {
      $product['name'] = $product[$langPref . 'name'];
      if (trim($product[$langPref . 'header']) == '') {
        $product['header'] = $product[$langPref . 'name'];
      } else {
        $product['header'] = $product[$langPref . 'header'];

      }
    } else {
      if (trim($product[$langPref . 'header']) == '') {
        $product['header'] = $product['name'];
      } else {
        $product['header'] = $product[$langPref . 'header'];

      }
    }


    $product['brief_description'] = $product[$langPref . 'brief_description'];

    $product['description'] = $product[$langPref . 'description'];
    
    //echo 'dd'; exit();
    
    //echo '<pre>'; print_r($product); echo '</pre>'; die();
    if (preg_match("/##gallery_[0-9]+##/", $product['description'], $matches )) {
      //echo '<pre>'; print_r($matches); echo '</pre>'; die();
      $tok = $matches[0];      
      $newsid = preg_replace("/[^0-9]/", '', $tok);      
      //echo $newsid; die();
      
      $product['description'] = checkGalleriesTagsId('blog',$newsid, $product['description']);
    }
    
    $newsid = 57;
    $product['description'] = checkGalleriesTags('blog',$newsid,$product['description']);


    if (!isset($_GET["vote"])) IncrementProductViewedTimes($productID);

    $dontshowcategory = 1;

    $smarty->assign("main_content_template", "product_detailed.tpl.html");

    $a = $product;
    $a["PriceWithUnit"] = show_price($a["Price"]);
    $a["list_priceWithUnit"] = show_price($a["list_price"]);

    if (((float)$a["shipping_freight"]) > 0)
      $a["shipping_freightUC"] = show_price($a["shipping_freight"]);

    if (isset($_GET["picture_id"])) {
      $picture = db_query("select filename, thumbnail, enlarged from " .
        PRODUCT_PICTURES . " where photoID=" . (int)$_GET["picture_id"]);
      $picture_row = db_fetch_row($picture);
    } else if (!is_null($a["default_picture"])) {
      $picture = db_query("select filename, thumbnail, enlarged from " .
        PRODUCT_PICTURES . " where photoID=" . (int)$a["default_picture"]);
      $picture_row = db_fetch_row($picture);
    } else {
      $picture = db_query(
        "select filename, thumbnail, enlarged, photoID from " . PRODUCT_PICTURES .
        " where productID=" . $productID);
      if ($picture_row = db_fetch_row($picture))
        $a["default_picture"] = $picture_row["photoID"];
      else
        $picture_row = null;
    }
    if ($picture_row) {
      $a["picture"] = $picture_row[0];
      $a["thumbnail"] = $picture_row[1];
      $a["big_picture"] = $picture_row[2];
    } else {
      $a["picture"] = "";
      $a["thumbnail"] = "";
      $a["big_picture"] = "";
    }

    if ($a) //product found
    {
      if (!isset($categoryID)) $categoryID = $a["categoryID"];
      
      global $lang;  
      //echo $lang ;die();      
      $catalog_title = ($categoryID == 7) ? 'Мрамор' : "Гранит";
      
      if ($lang == '/en') {
        $catalog_title = ($categoryID == 7) ? 'Marble' : "Granite";
      } 
      
      if ($lang == '/ch') {        
        $catalog_title = ($categoryID == 7) ? '大理石' : "花岗岩";
      }
      
      $a["catalog_title"] = $catalog_title;
      
      //get selected category info
      $q = db_query("select categoryID, name, description, picture, allow_products_comparison FROM " . CATEGORIES_TABLE . " WHERE categoryID=" . (int)$categoryID);
      $row = db_fetch_row($q);
      if ($row) {
        if (!file_exists("data/category/" . $row[3])) $row[3] = "";
        $smarty->assign("selected_category", $row);
        $a["allow_products_comparison"] = $row[4];
      } else {
        $smarty->assign("selected_category", NULL);
        $a["allow_products_comparison"] = NULL;
      }

      //calculate a path to the category
      $smarty->assign("product_category_path", catCalculatePathToCategory((int)$categoryID));

      //reviews number
      $q = db_query("select count(*) FROM " . DISCUSSIONS_TABLE . " WHERE productID=" . $productID);
      $k = db_fetch_row($q);
      $k = $k[0];

      $catTmp = catGetCategoryById($categoryID);

      $prCategoryName = $catTmp[$langPref . 'name'];
      $smarty->assign('prCategoryName', $prCategoryName);

      //extra parameters
      $extra = GetExtraParametrs((int)$productID);
      $extracount = count($extra);
      //related items

      foreach ($extra as $option) {
        if ($option['optionID'] == 5) {
          foreach ($option['values_to_select'] as $variant) {
            $colorLink = $lang . m_CatRedir((int)$categoryID) . '/' . $variant['link'] . '/';

            $dataT = db_query('SELECT ' . $langPref . 'title  FROM ' . DB_PRFX .
              'catcolortitle  WHERE optionID=' . (int)$option['optionID'] . ' AND variantID=' . (int)$variant['variantID'] . ' AND categoryID=' . (int)$categoryID . '  ');
            if ($rowTit = db_fetch_assoc($dataT)) {
              if (trim($rowTit[$langPref . 'title']) != '') {
                $colorTitleP = $rowTit[$langPref . 'title'];
                $temp = 1;
              } else {

                $catName = mb_strtolower($catTmp[$langPref . 'name'], 'utf-8');
                $colorTitleP = $variant['option_value'] . ' <i class="colorCatHd">' . strtolower($catName) . '</i>';
                $temp = 2;
              }

            } else {

              $catName = mb_strtolower($catTmp[$langPref . 'name'], 'utf-8');
              $colorTitleP = $variant['option_value'] . ' <i class="colorCatHd">' . strtolower($catName) . '</i>';
              $temp = 3;
            }

          }
          $Tmar = print_r($option, true);
          //	$smarty->assign('prodColor', '<textarea name="" id="" cols="30" rows="10" style="display:none">'.$Tmar.'</textarea>');
          $smarty->assign('prodColor', '<a href="' . $colorLink . '">' . $colorTitleP . '</a>');
          $smarty->assign('colorTitleP', $colorTitleP);
          $smarty->assign('colorLink', $colorLink);
          $smarty->assign('temp', $temp);
          break;
        }
      }
      
      // related products
      $related = array();
      $q = db_query("select count(*) FROM " . RELATED_PRODUCTS_TABLE . " WHERE Owner=" . $productID);
      $cnt = db_fetch_row($q);
      $smarty->assign("product_related_number", $cnt[0]);
      if ($cnt[0] > 0) {
        $q = db_query("select productID FROM " . RELATED_PRODUCTS_TABLE . " WHERE Owner=" . $productID);

        while ($row = db_fetch_row($q)) {
          $p = db_query("select productID, name, Price, categoryID, alias FROM " . PRODUCTS_TABLE . " WHERE productID=" . $row[0] . " and enabled=1");
          if ($r = db_fetch_row($p)) {
            $r["Price"] = show_price($r["Price"]);
            $r['url'] = m_Prod($r);

            /** Добавляем картинки к рекомендуемым товарам*/
            $imgs = db_query("select photoID, filename, thumbnail, enlarged from " . PRODUCT_PICTURES . " where productID=" . $row[0]);
            while ($img = db_fetch_row($imgs)) {
              $r['preview'] = $img['filename'];
              $r['thumbnail'] = $img['thumbnail'];
              $r['img'] = $img['enlarged'];
            }

            $related[] = $r;
          }
        }

      }
      
      // related projects
      $related_projects = array();
      global $langPref;
      //echo ''; die();
      $qp = db_query("select blogNews, count(blogNews) as pCount FROM " . PRODUCTS_TABLE . " WHERE productID=" . $productID);
      //$qp = db_query("select count(*) as pCount FROM pgd_project WHERE NID IN (". $cntp['blogNews']  . ")" );
      $cntp = db_fetch_row($qp);
      
      $smarty->assign("project_related_number", $cntp['pCount']);
      if ($cntp['pCount'] > 0) {
        
        $qpr = db_query("select * FROM pgd_project WHERE NID IN (". $cntp['blogNews']  . ")" );
        
        $row_count = 1;
        while ($row = db_fetch_row($qpr)) {
            $class = $row_count % 2 == 0 ? 'right' : 'left';
            $row['position'] = $class;
            $row['title'] = $row[$langPref.'title'];
            $row['textToPrePublication'] = $row[$langPref.'textToPrePublication'];
            $related_projects[] = $row;
            
            $row_count++;
          //}
        }
        
        //echo '<pre>'; print_r($related_projects); echo '</pre>'; die();

      } 
      
      // related articles
      $related_articles = array();
      global $langPref;
      //echo ''; die();
      $qp = db_query("select blogNews, count(blogNews) as pCount FROM " . PRODUCTS_TABLE . " WHERE productID=" . $productID . " AND blogNews <> ''");
      //$qp = db_query("select count(*) as pCount FROM pgd_project WHERE NID IN (". $cntp['blogNews']  . ")" );
      $cntp = db_fetch_row($qp);
      
      $smarty->assign("article_related_number", $cntp['pCount']);
      //echo $cntp['pCount']; die();
      if ($cntp['pCount'] > 0) {
        
        $qpr = db_query("select * FROM pgd_news_table WHERE NID IN (". $cntp['blogNews']  . ")" );

        //echo $langPref; die();

        $row_count = 1;
        while ($row = db_fetch_row($qpr)) {
            $class = $row_count % 2 == 0 ? 'right' : 'left';
            $row['position'] = $class;
            $row['title'] = $row[$langPref.'title'];
            $content_text = strip_tags($row[$langPref.'textToPrePublication']);
            $row['textToPrePublication'] = substr($content_text, 0, strpos($content_text, ' ', 140)).'...';

          if ($langPref == 'en_' || $langPref == 'ch_') {
            //$row['pref'] = '' . substr( $langPref, 0, strlen($langPref) - 1 ) . '/' . $row['alias'];
            $row['alias'] = '' . substr( $langPref, 0, strlen($langPref) - 1 ) . '/blog/' . $row['alias'];
          }
          else {
            $row['alias'] = 'blog/' . $row['alias'];
          }

          //sub

            $related_articles[] = $row;
            $row_count++;
          //}
        }
        
        //echo '<pre>'; print_r($related_projects); echo '</pre>'; die();

      }
      
      
      
      $smarty->assign("productslinkscat", getcontentprod($productID));
      //update several product fields
      if (!file_exists("data/small/" . $a["picture"])) $a["picture"] = 0;
      if (!file_exists("data/medium/" . $a["thumbnail"])) $a["thumbnail"] = 0;
      if (!file_exists("data/big/" . $a["big_picture"])) $a["big_picture"] = 0;
      else if ($a["big_picture"]) {
        $size = getimagesize("data/big/" . $a["big_picture"]);
        $a[16] = $size[0] + 40;
        $a[17] = $size[1] + 30;
      }
      $a[12] = show_price($a["Price"]);
      $a[13] = show_price($a["list_price"]);
      $a[14] = show_price($a["list_price"] - $a["Price"]); //you save (value)
      $a["PriceWithOutUnit"] = show_priceWithOutUnit($a["Price"]);
      if ($a["list_price"]) $a[15] =
        ceil(((($a["list_price"] - $a["Price"]) /
            $a["list_price"]) * 100)); //you save (%)


      if (isset($_GET["picture_id"])) {
        $pictures = db_query("select photoID, filename, thumbnail, enlarged from " .
          PRODUCT_PICTURES . " where photoID!=" . (int)$_GET["picture_id"] .
          " AND productID=" . $productID);
      } else if (!is_null($a["default_picture"])) {
        $pictures = db_query("select photoID, filename, thumbnail, enlarged from " .
          PRODUCT_PICTURES . " where photoID!=" . $a["default_picture"] .
          " AND productID=" . $productID);
      } else {
        $pictures = db_query("select photoID, filename, thumbnail, enlarged from " .
          PRODUCT_PICTURES . " where productID=" . $productID);
      }
      $all_product_pictures = array();
      $all_product_pictures_id = array();
      while ($picture = db_fetch_row($pictures)) {
        if ($picture["filename"] != "") {
          if (file_exists("data/small/" . $picture["filename"])) {
            if (!file_exists("data/medium/" . $picture["thumbnail"])) $picture["thumbnail"] = 0;
            if (!file_exists("data/big/" . $picture["enlarged"])) $picture["enlarged"] = 0;
            $all_product_pictures[] = $picture;
            $all_product_pictures_id[] = $picture[0];
          }
        }
      }

      //eproduct
      if (strlen($a["eproduct_filename"]) > 0 && file_exists("core/files/" . $a["eproduct_filename"])) {
        $size = filesize("core/files/" . $a["eproduct_filename"]);
        if ($size > 1000) $size = round($size / 1000);
        $a["eproduct_filesize"] = $size . " Kb";
      } else {
        $a["eproduct_filename"] = "";
      }

      //initialize product "request information" form in case it has not been already submitted
      if (!isset($_POST["request_information"])) {
        if (!isset($_SESSION["log"])) {
          $customer_name = "";
          $customer_email = "";
        } else {
          $custinfo = regGetCustomerInfo2($_SESSION["log"]);
          $customer_name = $custinfo["first_name"] . " " . $custinfo["last_name"];
          $customer_email = $custinfo["Email"];
        }

        $message_text = "";
      }

      $smarty->hassign("customer_name", $customer_name);
      $smarty->hassign("customer_email", $customer_email);
      $smarty->hassign("message_text", $message_text);

      if (isset($_GET["sent"])) $smarty->assign("sent", 1);


      if ($a['blogNews'] != '') {
        $prNews = explode(',', $a['blogNews']);

        $qPNews = db_query("select NID, add_date, title, en_title, ch_title, textToPrePublication, en_textToPrePublication, ch_textToPrePublication, alias from " . NEWS_TABLE . " WHERE NID IN (" . implode(',', $prNews) . ") order by add_date DESC ");
        $product_news = array();
        while ($r = db_fetch_row($qPNews)) {
          $r["add_date"] = dtConvertToStandartForm($r["add_date"]);
          if (trim($r[$langPref . 'title']) != '') {
            $r['title'] = $r[$langPref . 'title'];
          }
          if (trim(strip_tags($r[$langPref . 'textToPrePublication'])) != '') {
            $r['textToPrePublication'] = $r[$langPref . 'textToPrePublication'];
          }
          $product_news[] = $r;
        }
        $smarty->assign("product_news", $product_news);
      }


      $smarty->assign("all_product_pictures_id", $all_product_pictures_id);
      $smarty->assign("all_product_pictures", $all_product_pictures);
      
      
      
      $smarty->assign("product_info", $a);
      
      $smarty->assign("product_reviews_count", $k);
      $smarty->assign("product_extra", $extra);
      $smarty->assign("product_extra_count", $extracount);
      $smarty->assign("product_related", $related);
      $smarty->assign("product_related_projects", $related_projects);
      $smarty->assign("product_related_articles", $related_articles);
    } else {
      //product not found
      header("HTTP/1.0 404 Not Found");
      header("HTTP/1.1 404 Not Found");
      header("Status: 404 Not Found");
      die(ERROR_404_HTML);
    }
  }
}

?>