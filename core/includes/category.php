<?php
#####################################
# ShopCMS: Скрипт интернет-магазина
# Copyright (c) by ADGroup
# http://shopcms.ru
#####################################


        if ( isset($categoryID) && !isset($_GET["search_with_change_category_ability"]) && !isset($productID))
        {
          
            //echo 'set'; die();

                if ( isset($_GET["prdID"]) )
                        $_GET["prdID"] = (int)$_GET["prdID"];
                if ( isset($_GET["search_price_from"]) )
                        if ( trim($_GET["search_price_from"]) != "" )
                                $_GET["search_price_from"] = (int)$_GET["search_price_from"];
                if ( isset($_GET["search_price_to"]) )
                        if (  trim($_GET["search_price_to"])!="" )
                                $_GET["search_price_to"] = (int)$_GET["search_price_to"];
                if ( isset($_GET["categoryID"]) )
                        $_GET["categoryID"] = (int)$_GET["categoryID"];
                if ( isset($_GET["offset"]) )
                        $_GET["offset"] = (int)$_GET["offset"];

                if  (  !catGetCategoryById($_GET["categoryID"])  )  {
                header("HTTP/1.0 404 Not Found");
                header("HTTP/1.1 404 Not Found");
                header("Status: 404 Not Found");
                die(ERROR_404_HTML);
                }

                function _getUrlToNavigate( $categoryID )
                {
                        $url = "index.php?categoryID=".$categoryID;
                        $data = ScanGetVariableWithId( array("param") );
                        if ( isset($_GET["search_name"]) )
                                $url .= "&search_name=".$_GET["search_name"];
                        if ( isset($_GET["search_price_from"]) )
                                $url .= "&search_price_from=".$_GET["search_price_from"];
                        if ( isset($_GET["search_price_to"]) )
                                $url .= "&search_price_to=".$_GET["search_price_to"];
                        foreach( $data as $key => $val )
                        {
                                $url .= "&param_".$key;
                                $url .= "=".$val["param"];
                        }
                        if ( isset($_GET["search_in_subcategory"]) )
                                $url .= "&search_in_subcategory=1";
                        if ( isset($_GET["sort"]) )
                                $url .= "&sort=".$_GET["sort"];
                        if ( isset($_GET["direction"]) )
                                $url .= "&direction=".$_GET["direction"];
                        if ( isset($_GET["advanced_search_in_category"]) )
                                $url .= "&advanced_search_in_category=".$_GET["advanced_search_in_category"];
                        if (CONF_MOD_REWRITE && $url == "index.php?categoryID=".$categoryID)
                                $url = "category_".$categoryID;
                        return $url;
                }

                function _getUrlToSort( $categoryID )
                {
                        $url = "index.php?categoryID=$categoryID";
                        $data = ScanGetVariableWithId( array("param") );
                        if ( isset($_GET["search_name"]) )
                                $url .= "&search_name=".$_GET["search_name"];
                        if ( isset($_GET["search_price_from"]) )
                                $url .= "&search_price_from=".$_GET["search_price_from"];
                        if ( isset($_GET["search_price_to"]) )
                                $url .= "&search_price_to=".$_GET["search_price_to"];
                        foreach( $data as $key => $val )
                        {
                                $url .= "&param_".$key;
                                $url .= "=".$val["param"];
                        }
                        if ( isset($_GET["offset"]) )
                                $url .= "&offset=".$_GET["offset"];
                        if ( isset($_GET["show_all"]) )
                                $url .= "&show_all=yes";
                        if ( isset($_GET["search_in_subcategory"]) )
                                $url .= "&search_in_subcategory=1";
                        if ( isset($_GET["advanced_search_in_category"]) )
                                $url .= "&advanced_search_in_category=".$_GET["advanced_search_in_category"];
                        return $url;
                }

                function _sortSetting( &$smarty, $urlToSort )
                {
                        if(CONF_USE_RATING == 1){
                        $sort_string = STRING_PRODUCT_SORTN;
                        }else{
                        $sort_string = STRING_PRODUCT_SORT;
                        }
                        $sort_string = str_replace( "{ASC_NAME}",   "<a href='".$urlToSort."&sort=name&direction=ASC'>".STRING_ASC."</a>",        $sort_string );
                        $sort_string = str_replace( "{DESC_NAME}",  "<a href='".$urlToSort."&sort=name&direction=DESC'>".STRING_DESC."</a>",        $sort_string );
                        $sort_string = str_replace( "{ASC_PRICE}",   "<a href='".$urlToSort."&sort=Price&direction=ASC'>".STRING_ASC."</a>",        $sort_string );
                        $sort_string = str_replace( "{DESC_PRICE}",  "<a href='".$urlToSort."&sort=Price&direction=DESC'>".STRING_DESC."</a>",        $sort_string );
                        $sort_string = str_replace( "{ASC_RATING}",   "<a href='".$urlToSort."&sort=customers_rating&direction=ASC'>".STRING_ASC."</a>",        $sort_string );
                        $sort_string = str_replace( "{DESC_RATING}",  "<a href='".$urlToSort."&sort=customers_rating&direction=DESC'>".STRING_DESC."</a>",        $sort_string );
                        $smarty->assign( "string_product_sort", html_amp($sort_string));

                }

                //get selected category info
                $category = catGetCategoryById( $categoryID );
                if ( !$category )
                {
                                header("HTTP/1.0 404 Not Found");
                                header("HTTP/1.1 404 Not Found");
                                header("Status: 404 Not Found");
                                die(ERROR_404_HTML);
                }
                else
                {
					if(trim($category[$langPref."name"]) != '') {
						 $category["name"] =  $category[$langPref."name"];
					}
					$category["description"] =  $category[$langPref."description"];
				
					if(isset($_GET['param_5']) && isset($_GET['paramC_5'])) {
						$colDescription = catGetColorParam($categoryID, 5, $_GET['param_5'], $langPref.'description');
						if(trim($colDescription) != '' ) {
							$category["description"] = $colDescription;
						}
					}
                       if(!$adminislog) IncrementCategoryViewedTimes($categoryID);

                        if ( isset($_GET["prdID"]) )
                        {
                                if (  isset($_POST["cart_".$_GET["prdID"]."_x"])  )
                                {
                                        $variants=array();
                                        foreach( $_POST as $key => $val )
                                        {
                                                if ( strstr($key, "option_select_hidden") )
                                                {
                                                        $arr=explode( "_", str_replace("option_select_hidden_","",$key) );
                                                        if ( (string)$arr[1] == (string)$_GET["prdID"] )
                                                                $variants[]=$val;
                                                }
                                        }
                                        unset($_SESSION["variants"]);
                                        $_SESSION["variants"]=$variants;
                                        Redirect( "index.php?shopping_cart=yes&add2cart=".$_GET["prdID"]."&multyaddcount=".(int)$_POST['multyaddcount'] );
                                }
                        }

                        //category thumbnail
                        if (!file_exists("data/category/".$category["picture"])) $category["picture"] = "";
                        $smarty->assign("selected_category", $category );


                        if ( $category["show_subcategories_products"] == 1 )
                                $smarty->assign( "show_subcategories_products", 1 );

                        if ( $category["allow_products_search"] )
                                $smarty->assign( "allow_products_search", 1 );

                        $callBackParam               = array();
                        $products                    = array();
                        $products_marble                    = array();
                        $products_granite                    = array();
                        $categoryIDM = 7;  // marble id
                        $categoryIDG = 8;  // granite id

                        $callBackParam["categoryID"] = (int)$categoryID;
                        $callBackParam["enabled"]    = 1;


                        if (  isset($_GET["search_in_subcategory"]) )
                                if ( $_GET["search_in_subcategory"] == 1 )
                                {
                                        $callBackParam["searchInSubcategories"] = true;
                                        $callBackParam["searchInEnabledSubcategories"] = true;
                                }

                        if ( isset($_GET["sort"]) )
                                $callBackParam["sort"] = $_GET["sort"];
                        if ( isset($_GET["direction"]) )
                                $callBackParam["direction"] = $_GET["direction"];

                        // search parametrs to advanced search
                        if ( $extraParametrsTemplate != null )
                                        $callBackParam["extraParametrsTemplate"] = $extraParametrsTemplate;
                        if ( $searchParamName != null )
                                        $callBackParam["name"] = $searchParamName;
                        if ( $rangePrice != null )
                                        $callBackParam["price"] = $rangePrice;

                        if ( $category["show_subcategories_products"] ) $callBackParam["searchInSubcategories"] = true;

                        $count = 0;
						$url_alias = preg_replace('#[^a-z0-9_/-]+#i', '', $_GET['cat_alias']);
                        if (CONF_MOD_REWRITE){
                        $urlfarse = _getUrlToNavigate( $categoryID );
                        if($urlfarse == "category_".$categoryID) $urlflag = 1; else $urlflag = 0;
                        $navigatorHtml = GetNavigatorHtmlmd(
                                                $urlfarse, CONF_PRODUCTS_PER_PAGE,
                                                'prdSearchProductByTemplate', $callBackParam,
                                                $products, $offset, $count, $urlflag );
												$navigatorHtml = strtr($navigatorHtml,array("_offset_0"=>""));
												$navigatorHtml = strtr($navigatorHtml,array("_offset_"=>""));
												$navigatorHtml = strtr($navigatorHtml,array("_show_all.html"=>"all/"));
												$navigatorHtml = strtr($navigatorHtml,array(".html"=>"/"));
												$navigatorHtml = strtr($navigatorHtml,array("category_".$categoryID=>$url_alias));
												$navigatorHtml = strtr($navigatorHtml,array("//"=>"/"));
                            //var_dump($products);
                            // заполняются продукты products

                            //


                            // выбираем мрамор
                             
                             //echo $urlfarse; echo '<br />'; // = category_6
                             
                              //$callBackParam  = array();
                              //$callBackParam["categoryID"] = (int)$categoryID;
                              //$callBackParam["enabled"]    = 1;
                             
                             //echo $callBackParam; echo '<br />'; // 
                             //echo $urlflag; echo '<br />'; //  = 1
                             //echo $offset; echo '<br />'; //    0 
                             //echo $count; die(); //             18

                            $callBackParam["categoryID"] = (int)$categoryIDM;
                            $callBackParam["enabled"]    = 1;
                            $navigatorHtmlMarble = GetNavigatorHtmlmd(
                                $urlfarse, CONF_PRODUCTS_PER_PAGE,
                                'prdSearchProductByTemplate', $callBackParam,
                                $products_marble, $offset, $count, $urlflag );

                            /*
                            $navigatorHtmlMarble = strtr($navigatorHtml,array("_offset_0"=>""));
                            $navigatorHtmlMarble = strtr($navigatorHtml,array("_offset_"=>""));
                            $navigatorHtmlMarble = strtr($navigatorHtml,array("_show_all.html"=>"all/"));
                            $navigatorHtmlMarble = strtr($navigatorHtml,array(".html"=>"/"));
                            $navigatorHtmlMarble = strtr($navigatorHtml,array("category_".$categoryID=>$url_alias));
                            $navigatorHtmlMarble = strtr($navigatorHtml,array("//"=>"/"));
                            */

                            // выбираем гранит
                            $callBackParam["categoryID"] = (int)$categoryIDG;
                            $navigatorHtmlGranite = GetNavigatorHtmlmd(
                                $urlfarse, CONF_PRODUCTS_PER_PAGE,
                                'prdSearchProductByTemplate', $callBackParam,
                                $products_granite, $offset, $count, $urlflag );

                            /*
                            $navigatorHtmlGranite = strtr($navigatorHtml,array("_offset_0"=>""));
                            $navigatorHtmlGranite = strtr($navigatorHtml,array("_offset_"=>""));
                            $navigatorHtmlGranite = strtr($navigatorHtml,array("_show_all.html"=>"all/"));
                            $navigatorHtmlGranite = strtr($navigatorHtml,array(".html"=>"/"));
                            $navigatorHtmlGranite = strtr($navigatorHtml,array("category_".$categoryID=>$url_alias));
                            $navigatorHtmlGranite = strtr($navigatorHtml,array("//"=>"/"));
                            */

                            //echo '<pre>'; print_r($products_marble); echo '</pre>'; die();
                            // end мрамор



                        }else{
                        $navigatorHtml = GetNavigatorHtml(
                                                _getUrlToNavigate( $categoryID ), CONF_PRODUCTS_PER_PAGE,
                                                'prdSearchProductByTemplate', $callBackParam,
                                                $products, $offset, $count );
												$navigatorHtml = strtr($navigatorHtml,array("&offset=0"=>"","&amp;offset=0"=>""));
                        }

                        //var_dump($products);

                        //echo $categoryID; die();  6

                        $show_comparison = $category["allow_products_comparison"];
                        $cc_products = count($products);
                        for($i=0; $i<$cc_products; $i++) $products[$i]["allow_products_comparison"] = $show_comparison;


                        if (CONF_PRODUCT_SORT) _sortSetting( $smarty, _getUrlToSort($categoryID) );

                        if(CONF_SHOW_PARENCAT){
                        $smarty->assign( "catrescur", getcontentcatresc($categoryID));
                        }
                        $smarty->assign( "subcategories_to_be_shown", catGetSubCategoriesSingleLayer($categoryID) );
                        $smarty->assign( "categorylinkscat", getcontentcat($categoryID));
                        //calculate a path to the category
                        $smarty->assign( "product_category_path",
                                                catCalculatePathToCategory($categoryID) );
                        $smarty->assign( "show_comparison", $show_comparison );
                        $smarty->assign( "catalog_navigator", $navigatorHtml );



                        //$smarty->assign( "products_to_show", $products);

                        //marble
                        $smarty->assign( "products_to_show_marble", $products_marble);

                        //granite
                        
                        $smarty->assign( "products_to_show_granite", $products_granite);

												//echo '<pre>'; print_r($products); echo '</pre>';
												
                        $smarty->assign( "products_to_show_counter", count($products));
                        if(isset($_GET["advanced_search_in_category"])){
                        $smarty->assign( "products_to_showc", count($products));
                        }else{
                        if ( $category["show_subcategories_products"] )
                        $smarty->assign( "products_to_showc", $category["products_count"]);
                        else $smarty->assign( "products_to_showc", catGetCategoryProductCount( $categoryID, true));
                        }
						
                        $smarty->assign( "categoryID", $categoryID);
                        $smarty->assign( "categoryName", $category["name"]);
                        $smarty->assign( "main_content_template", "category.tpl.html");
                }
        }
        
        $menu_mobile_products_marble = getMobileMenuProducts(7);
        $menu_mobile_products_granite = getMobileMenuProducts(8);
        $smarty->assign( "menu_mobile_products_marble", $menu_mobile_products_marble);
        $smarty->assign( "menu_mobile_products_granite", $menu_mobile_products_granite);
        

 //todo исправляется ошибка из-за параметра $_GET   
    
  $products_marble = array();
  $products_granite = array();       
  $callBackParam  = array();
     
  $urlfarse = 'category_6';
  $urlflag = 1;
  $offset = 0;
  $count = 18;
  $callBackParam["categoryID"] = 8;
  $callBackParam["enabled"]    = 1;

  $navigatorHtmlGranite = GetNavigatorHtmlmd(
      $urlfarse, CONF_PRODUCTS_PER_PAGE,
      'prdSearchProductByTemplate', $callBackParam,
      $products_granite, $offset, $count, $urlflag );
                                           
    // обработать массив на наличие пустых значений
        
    //echo '<pre>'; print_r($products_granite); echo '</pre>';  die();
    
    // all products для китайской версии
    
    
    if ($langPref == 'ch_') {
      $products_count = 0;
      foreach ($products as $prod) {
         
        if (strlen( trim($products[$products_count]['ch_brief_description'])) < 7) {
          $products[$products_count]['ch_brief_description'] = '';
        }          
       
        $products[$products_count]['brief_description'] = $products[$products_count]['ch_brief_description'];

        $products_count++;
      }
      
      $smarty->assign( "products_to_show", $products);
    }
    
    
    //echo '<pre>'; print_r($products); echo '</pre>';  die();
    
    
    $granite_count = 0;
    foreach ($products_granite as $granite_prod) {
       
      if (strlen( trim($products_granite[$granite_count]['ch_brief_description'])) < 7) {
        $products_granite[$granite_count]['ch_brief_description'] = '';
      }          
      if (strlen( trim($products_granite[$granite_count]['en_brief_description'])) < 7) {
        $products_granite[$granite_count]['en_brief_description'] = '';
      }
      
      if (strlen( trim($products_granite[$granite_count]['brief_description'])) < 7) {
        $products_granite[$granite_count]['brief_description'] = '';
      }
      
      
      if ($langPref == 'ch_') {
        $products_granite[$granite_count]['brief_description'] = $products_granite[$granite_count]['ch_brief_description'];
      }
      
      if ($langPref == 'en_') {
        $products_granite[$granite_count]['brief_description'] = $products_granite[$granite_count]['en_brief_description'];
      }
              
      $granite_count++;
    }
    
    //print_r(count($products_granite ));
   
    $smarty->assign( "products_to_show_granite", $products_granite);    
    
       
    // END обработать массив
 
    // мрамор   
    $callBackParam["categoryID"] = 7;
    $navigatorHtmlMarble = GetNavigatorHtmlmd(
                            $urlfarse, CONF_PRODUCTS_PER_PAGE,
                            'prdSearchProductByTemplate', $callBackParam,
                            $products_marble, $offset, $count, $urlflag );


    $marble_count = 0;
    foreach ($products_marble as $granite_prod) {
       
      if (strlen( trim($products_marble[$marble_count]['ch_brief_description'])) < 7) {
        $products_marble[$marble_count]['ch_brief_description'] = '';
      }          
      if (strlen( trim($products_marble[$marble_count]['en_brief_description'])) < 7) {
        $products_marble[$marble_count]['en_brief_description'] = '';
      } 
      
      if (strlen( trim($products_marble[$marble_count]['brief_description'])) < 7) {
        $products_marble[$marble_count]['brief_description'] = '';
      }
      
      if ($langPref == 'ch_') {
        $products_marble[$marble_count]['brief_description'] = $products_marble[$marble_count]['ch_brief_description'];
      }
      
      if ($langPref == 'en_') {
        $products_marble[$marble_count]['brief_description'] = $products_marble[$marble_count]['en_brief_description'];
      }
              
      $marble_count++;
    }         
            
    $smarty->assign( "products_to_show_marble", $products_marble);
    
    //echo '<pre>'; print_r($products_marble); echo '<pre>'; die();
    
    /*
    if ($langPref == 'ch_') {
      echo '<pre>'; print_r($products); echo '<pre>'; die();
    }
    */
    
?>