<?php
#####################################
# ShopCMS: Скрипт интернет-магазина
# Copyright (c) by ADGroup
# http://shopcms.ru
#####################################


        if (!strcmp($sub, "chpu"))
        {
        if ( CONF_BACKEND_SAFEMODE != 1 && (!isset($_SESSION["log"]) || !in_array(100,$relaccess))) //unauthorized
        {
                          $smarty->assign("admin_sub_dpt", "error_forbidden.tpl.html");
                        } else {

                if ( isset($_POST["save"]) )
                {
                        if (CONF_BACKEND_SAFEMODE) //this action is forbidden when SAFE MODE is ON
                        {
                                Redirect(ADMIN_FILE."?dpt=modules&sub=chpu&type=".$_GET["type"]."&safemode=yes" );
                        }

                        $data = ScanPostVariableWithId( array("alias", "name") );
						$i=0;
                        foreach( $data as $key => $val )
                        {
							$i++;
							if ($i>150)
								Redirect(ADMIN_FILE."?dpt=modules&sub=chpu&type=".$_GET["type"] );
                             if ($_GET['type'] == "4")
								aliasGenerateAliasAux($key, $val['alias'], $val['name'], "upd");
							 elseif ($_GET['type'] == "3")
								aliasGenerateAliasNews($key, $val['alias'], $val['name'], "upd");
							 elseif ($_GET['type'] == "5")
								aliasGenerateAliasDiary($key, $val['alias'], $val['name'], "upd");
								aliasGenerateAliasproject($key, $val['alias'], $val['name'], "upd");
							 elseif ($_GET['type'] == "2")
								aliasGenerateAliasProd($key, $val['alias'], $val['name'], "upd");
							 elseif ($_GET['type'] == "1")
								aliasGenerateAliasCat($key, $val['alias'], $val['name'], "upd");
                        }
                        Redirect(ADMIN_FILE."?dpt=modules&sub=chpu&type=".$_GET["type"] );
                }

                //if country is not selected, select the first country from the database
                if ( !isset($_GET["type"]) )
                {
                        Redirect(ADMIN_FILE."?dpt=modules&sub=chpu&type=1" );
                }
                
				$types = array();
				$types[] = array("typeID"=>1, "name"=>"Категории");
				$types[] = array("typeID"=>2, "name"=>"Товары");
				$types[] = array("typeID"=>3, "name"=>"Новости");
				$types[] = array("typeID"=>5, "name"=>"Произв. дневник");
				$types[] = array("typeID"=>4, "name"=>"Доп. страницы");
                $smarty->assign("types", $types);

				$data = array();
                if ($_GET['type'] == "4")
					$q = db_query("SELECT aux_page_ID, alias, aux_page_name FROM ".AUX_PAGES_TABLE." WHERE alias='' LIMIT 150");
				elseif ($_GET['type'] == "3")
					$q = db_query("SELECT NID, alias, title FROM ".NEWS_TABLE." WHERE alias='' LIMIT 150");
				elseif ($_GET['type'] == "5")
					$q = db_query("SELECT NID, alias, title FROM ".DB_PRFX."diary WHERE alias='' LIMIT 150");
				elseif ($_GET['type'] == "2")
					$q = db_query("SELECT productID, alias, name FROM ".PRODUCTS_TABLE." WHERE alias='' LIMIT 150");
				else
					$q = db_query("SELECT categoryID, alias, name FROM ".CATEGORIES_TABLE." WHERE alias='' LIMIT 150");
				while ($row = db_fetch_row($q))
					$data[] = $row;
				
                $smarty->assign("aliases",$data);
                $smarty->assign("aliases_count",count($data));

                $smarty->assign("type", $_GET["type"] );

                //set sub-department template
                $smarty->assign("admin_sub_dpt", "modules_chpu.tpl.html");
        }
        }
?>