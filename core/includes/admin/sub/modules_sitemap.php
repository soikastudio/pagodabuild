<?PHP
#####################################
# ShopCMS: Скрипт интернет-магазина
# Copyright (c) by Levitaly
# ICQ - 452842661, Skype - levitalyk,
# E-mail - levitaly@yandex.ru
#####################################

	if(!function_exists('getSitemap')) {
		function getSitemap()
		{
			$f = getsm_header();

			if (isset($_GET['exp_catalog']))
				$f .= getsm_catalog(1,0);
			if (isset($_GET['exp_aux']))
				$f .= getsm_auxpages();
			if (isset($_GET['exp_news']))
				$f .= getsm_news();
			
			$f .= getsm_footer();
			$s = str_replace("www.", "", $_SERVER["SERVER_NAME"]);
			if ($_GET['url_type']=="1")
				$f = str_replace("http://".$s, "http://www.".$s, str_replace("www.".$s, $s, $f));
			else
				$f = str_replace("www.".$s, $s, $f);
			return $f;
		}
	}
		
	if(!function_exists('getsm_header')) {
		function getsm_header()
		{
			$index = "http://".$_SERVER["SERVER_NAME"]."/";
			$f = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n";
			$f .= " <urlset xmlns=\"http://www.google.com/schemas/sitemap/0.84\">\r\n" ; //xml Googl
			$f .= getsm_element($index, "1.0");
			return $f;
		}
	}
	
	if(!function_exists('getsm_element')) {
		function getsm_element($url, $priority="0.8", $changefreq="weekly")
		{
			global $cntr;
            $f = "  <url> \r\n";
			$f .= "   <loc>".getsm_clear($url)."</loc>\r\n";
            $f .= "   <lastmod>".date("Y-m-d")."</lastmod>\r\n";
            $f .= "   <changefreq>".$changefreq."</changefreq>\r\n";
			$f .= "   <priority>".$priority."</priority>\r\n";
            $f .= "  </url>\r\n";
			$cntr++;
			return $f;
		}
	}
		
	if(!function_exists('getsm_footer')) {
		function getsm_footer()
		{
			$f = "</urlset>\r\n";
			return $f;
		}
	}
		
	if(!function_exists('getsm_clear')) {
		function getsm_clear($url)
		{
			$url = str_replace("&", "&amp;",$url);
            $url = str_replace("'", "&apos;",$url);
            $url = str_replace("\"", "&quot;",$url);
            $url = str_replace(">", "&gt;",$url);
            $url = str_replace("<", "&lt;",$url);
			return $url;
		}
	}
		
	if(!function_exists('getsm_catalog')) {
        function getsm_catalog($parent,$level)
        {
                $f = "";
                $q = db_query("select categoryID, alias from ".CATEGORIES_TABLE." where parent='".(int)$parent."' order by sort_order, name") or die (db_error());
                while ($row = db_fetch_row($q))
                {
					$url = "http://".$_SERVER["SERVER_NAME"]."/".m_Cat($row[0]);
					$f .= getsm_element($url, "0.7");
                       
					//add products
                    $q1 = db_query("select productID, categoryID, alias from ".PRODUCTS_TABLE." where categoryID=".$row[0]." and enabled=1 order by sort_order") or die (db_error());
                    while ($row1 = db_fetch_row($q1))
                    {
						$url = "http://".$_SERVER["SERVER_NAME"]."/".m_Prod($row1);
						$f .= getsm_element($url, "0.8");
                    }

                    //process all subcategories
                    $f .= getsm_catalog($row[0], $level+1);
                }
                return $f;
        }
    }
		
	if(!function_exists('getsm_news')) {
		function getsm_news()
        {
                $f = "";
				$url = "http://".$_SERVER["SERVER_NAME"]."/news/";
				$f .= getsm_element($url, "0.7");
				
                $q = db_query("select alias from ".DB_PRFX."news_table order by title") or die (db_error());
                while ($row = db_fetch_row($q))
                {
					$url = "http://".$_SERVER["SERVER_NAME"]."/".m_News($row[0]);
					$f .= getsm_element($url, "0.8");
                }
                return $f;
        }
    }
		
	if(!function_exists('getsm_auxpages')) {
		function getsm_auxpages()
        {
                $f = "";
                $q = db_query("select alias from ".DB_PRFX."aux_pages order by aux_page_name") or die (db_error());
                while ($row = db_fetch_row($q))
                {
					$url = "http://".$_SERVER["SERVER_NAME"]."/".m_Aux($row[0]);
					$f .= getsm_element($url, "0.8");
                }
                return $f;
        }
    }

        if ($_GET["sub"]=="sitemap")
        {
			$url_www = str_replace("http://", "http://www.", str_replace("www.", "", "http://".$_SERVER["SERVER_NAME"]."/"));
			$url_notwww = str_replace("www.", "", "http://".$_SERVER["SERVER_NAME"]."/");
			$smarty->assign("url_www", $url_www);
			$smarty->assign("url_notwww", $url_notwww);
            $smarty->assign("admin_sub_dpt", "modules_sitemap.tpl.html");
			if ($_GET["creat"]=="yes")
			{
				$smarty->assign("creat", "yes");
				$cntr = 0;
				$sitemap_file = getSitemap();
				$file_sitemap="sitemap.xml";
				$handle = fopen($file_sitemap, "w+");
				if (fwrite($handle, $sitemap_file) === FALSE)
					$smarty->assign("message", "Не удалось произвести запись в файл  http://".$_SERVER["SERVER_NAME"]."/".$file_sitemap);
				else {
					$mes = "Запись xml данных в файл  <i>http://".$_SERVER["SERVER_NAME"]."/".$file_sitemap."</i>  завершена.<br/>Количество адресов: ".$cntr.".<br/>В карту сайта входят:<br/>";
					if (isset($_GET['exp_catalog']))
						$mes .= "&nbsp;&nbsp;- каталог товаров<br/>";
					if (isset($_GET['exp_aux']))
						$mes .= "&nbsp;&nbsp;- дополнительные страницы<br/>";
					if (isset($_GET['exp_news']))
						$mes .= "&nbsp;&nbsp;- новости<br/>";
					$smarty->assign("message",$mes);
				}
				fclose($handle);
			}
			
			if (file_exists($file_sitemap))
			{
				$size = filesize($file_sitemap);
                $filesize = round($size/1000)." Kb";
				$smarty->assign("filesize", $filesize);
			}
        }
?>