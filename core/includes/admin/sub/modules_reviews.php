<?php
#####################################
# ShopCMS: Скрипт интернет-магазина
# Copyright (c) by ADGroup
# http://shopcms.ru
#####################################
if(isset($_POST['ajaxReview'])) {
	/*
	foreach($_POST as $key => $val) {
		$_POST[$key] = iconv('utf-8', 'CP1251',urldecode($val));
	}
	*/
	
	if(isset($_POST['addRewiev'])) {
		
		db_query("INSERT INTO ".DB_PRFX."reviews SET rName='".$_POST['rName']."',  rCompany='".$_POST['rCompany']."',  rText='".$_POST['rText']."', en_rName='".$_POST['en_rName']."',  en_rCompany='".$_POST['en_rCompany']."',  en_rText='".$_POST['en_rText']."', ch_rName='".$_POST['ch_rName']."',  ch_rCompany='".$_POST['ch_rCompany']."',  ch_rText='".$_POST['ch_rText']."',  rType='".$_POST['rType']."' ");
		
		$rID = db_insert_id( DB_PRFX."reviews" );
		
		$rTypeComp = "";
		$rTypeCompV = "";
		$rTypeTrain = "";
		if($_POST['rType'] == 'comp') {
			$rTypeComp = " selected='selected' ";
			$rTypeVal = "О компании";
		} elseif ($_POST['rType'] == 'compV') {
			$rTypeCompV = " selected='selected' ";
			$rTypeVal = "О компании \"Видео\"";
		} elseif ($_POST['rType'] == 'trainV') {
			$rTypeCompV = " selected='selected' ";
			$rTypeVal = "Тренинги \"Видео\"";
		} elseif ($_POST['rType'] == 'train') {
			$rTypeTrain = " selected='selected' ";
			$rTypeVal = "Тренинги и обучение";
		}
		
		$res = '<tr class="lineybig hover line_'.$rID.'">
					<td class="rwCompany_'.$rID.'">'.$_POST['rCompany'].'</td>
					<td class="rwName_'.$rID.'">'.$_POST['rName'].'</td>
					<td class="rwType_'.$rID.'">'.$rTypeVal.'</td>
					<td><div class="editBtt" rel="'.$rID.'">Радактировать</div></td>
					<td>
						<div class="remRw" rel="'.$rID.'">Удалить</div>
					</td>
				</tr>
				<tr class="editRw_'.$rID.' editRW">
					<td colspan="5">
					<form action="" id="rForm_'.$rID.'">
						
					<table cellspacing="0" cellpadding="0" border="0" width="">
						<tr>
							<td width="50%">
								Компания: <input type="text" value="'.$_POST['rCompany'].'" name="rCompany"/>
							</td>
							<td width="50%">
								Имя автора: <input type="text" value="'.$_POST['rName'].'" name="rName"/></td>
						</tr>
						<tr>
							<td width="" colspan="2">
								Текст отзыва:<br>
								<textarea name="rText" id="" cols="30" rows="10">'.$_POST['rText'].'</textarea>
							</td>
						</tr>
						<tr>
							<td width="50%">
								Тип отзыва: 
								<select name="rType" id="">
									<option value="">Выбрать</option>
									<option value="comp" '.$rTypeComp.'>О компании</option>
									<option value="compV"  '.$rTypeCompV.'>О компании "Видео"</option>
									<option value="train"  '.$rTypeTrain.'>Тренинги и обучение</option>
								</select>
							</td>
							<td width="50%">
								<div class="saveRwBtt" rel="'.$rID.'">Сохранить</div>
								<div class="clear"></div>
							</td>
						</tr>
					</table>
					
					</form>
					
					</td>
				</tr>
				<tr class="separ_'.$rID.'"><td colspan="5" class="separ"><img src="data/admin/pixel.gif" alt="" class="sep"></td></tr>';
		echo $res;
	}
	
	if (isset($_POST['saveRewiev'])) {
		db_query("UPDATE ".DB_PRFX."reviews SET rName='".$_POST['rName']."',  rCompany='".$_POST['rCompany']."',  rText='".$_POST['rText']."', en_rName='".$_POST['en_rName']."',  en_rCompany='".$_POST['en_rCompany']."',  en_rText='".$_POST['en_rText']."', ch_rName='".$_POST['ch_rName']."',  ch_rCompany='".$_POST['ch_rCompany']."',  ch_rText='".$_POST['ch_rText']."',  rType='".$_POST['rType']."' WHERE rID=".(int)$_POST['rId']);
		
		if($_POST['rType'] == 'comp') {
			$rTypeVal = "О компании";
		} elseif ($_POST['rType'] == 'compV') {
			$rTypeVal = "О компании \"Видео\"";
		} elseif ($_POST['rType'] == 'trainV') {
			$rTypeVal = "Тренинги \"Видео\"";
		} elseif ($_POST['rType'] == 'train') {
			$rTypeVal = "Тренинги и обучение";
		}
		
		echo $_POST['rName'].":".$_POST['rCompany'].":".$rTypeVal;
		
	}
	
	if(isset($_POST['remRewiev'])) {
		db_query("DELETE FROM ".DB_PRFX."reviews WHERE rID=".(int)$_POST['rId']) or die( '0' );
		echo '1';
	}
	
	
	die();
}


if (!strcmp($sub, "reviews")) {
		
	$q = db_query("SELECT * FROM ".DB_PRFX."reviews ORDER BY rID DESC");
	
	$reviews = array();
	while($row=db_fetch_row($q)) {
		
		if($row['rType'] == 'comp') {
			$row['rTypeVal'] = "О компании";
		} elseif ($row['rType'] == 'compV') {
			$row['rTypeVal'] = "О компании \"Видео\"";
		} elseif ($_POST['rType'] == 'trainV') {
			$row['rTypeVal'] = "Тренинги \"Видео\"";
		} elseif ($row['rType'] == 'train') {
			$row['rTypeVal'] = "Тренинги и обучение";
		}
		
		$reviews[] = $row;
	}

	$smarty->assign('reviews', $reviews);
    $smarty->assign( "admin_sub_dpt", "modules_reviews.tpl.html" );
}
?>