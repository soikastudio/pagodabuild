<?php
#####################################
# ShopCMS: Скрипт интернет-магазина
# Copyright (c) by ADGroup
# http://shopcms.ru
#####################################
//diary module

if (!strcmp($sub, "diary"))
{

    if ( isset($_GET["edit"]) && isset($_POST['moveNewsTo']) && $_POST['moveCat'] != '') {
        Redirect(moveNewsTo($_GET["edit"],"diary", $_POST['moveCat']));
    } elseif (isset($_GET["edit"]) && isset($_POST['moveNewsTo']) && $_POST['moveCat'] =="") {
        $smarty->assign('noCat', 1);
    }


    if ( CONF_BACKEND_SAFEMODE != 1 && (!isset($_SESSION["log"]) || !in_array(18,$relaccess))) //unauthorized
    {
        $smarty->assign("admin_sub_dpt", "error_forbidden.tpl.html");
    } else {


        if(!function_exists('_getUrlToSubmit')) {
            function _getUrlToSubmit()
            {
                $url = ADMIN_FILE."?dpt=modules&sub=diary";
                if ( isset($_GET["offset"]) )
                    $url .= "&offset=".$_GET["offset"];
                if ( isset($_GET["show_all"]) )
                    $url .= "&show_all=".$_GET["show_all"];
                return $url;
            }
        }

        if(!function_exists('_getUrlToDelete')) {
            function _getUrlToDelete()
            {
                return _getUrlToSubmit();
            }
        }

        if (isset($_GET["save_successful"])) //show successful save confirmation message
            $smarty->assign("configuration_saved", 1);

        //current time
        $s = dtConvertToStandartForm( get_current_time() );
        $smarty->assign( "current_date", $s );

        if ( isset($_POST["diary_save"]) )
        {
            if (CONF_BACKEND_SAFEMODE) //this action is forbidden when SAFE MODE is ON
            {
                Redirect( _getUrlToSubmit()."&safemode=yes" );
            }

            $picture = "";

            $NID = diaryAddNews($_POST["add_date"], $_POST["title"], $_POST["en_title"], $_POST["ch_title"], $_POST["textToPrePublication"], $_POST["en_textToPrePublication"], $_POST["ch_textToPrePublication"],
                $_POST["textToPublication"],
                $_POST["en_textToPublication"],
                $_POST["ch_textToPublication"], $_POST["textToMail"], $_POST["old_url"], $_POST["en_old_url"], $_POST["ch_old_url"] );
            if ( isset($_POST["send"]) ) { //send news to subscribers
                //    diarySendNews( $NID );
                mailChimpSend('diary',$NID);
            }

            Redirect( _getUrlToSubmit()."&save_successful=yes" );
        }

        if ( isset($_GET["edit"]) )
        {
            $galleries = getGalleries('diary_'.$_GET["edit"]);
            $smarty->assign( "galleries", $galleries);
            $edit_diary = diaryGetNewsToEdit($_GET["edit"]);
            $edit_diary["textToPrePublication"] = html_spchars($edit_diary["textToPrePublication"]);
            $edit_diary["textToPublication"] = html_spchars($edit_diary["textToPublication"]);
            $smarty->assign( "edit_diary", $edit_diary );
            $smarty->assign( "edit_diary_id", (int)$_GET["edit"]);
            $smarty->assign( "diary_editor", 1);
            $smarty->assign( "showGallery", 1);
            $smarty->assign( "pageType", 'diary');
            $smarty->assign( "pageID", $_GET["edit"]);
        }

        if ( isset($_GET["add_diary"]) )
        {
            $smarty->assign( "diary_editor", 1);
            $smarty->assign( "showGallery", 1);
            $smarty->assign( "pageType", 'diary');
            $smarty->assign( "pageID", 0);
        }

        if ( isset($_POST["update_diary"]) )
        {
            if (CONF_BACKEND_SAFEMODE) //this action is forbidden when SAFE MODE is ON
            {
                Redirect( _getUrlToSubmit()."&safemode=yes" );
            }

            diaryUpdateNews($_POST["add_date"], $_POST["title"], $_POST["en_title"], $_POST["ch_title"], $_POST["textToPrePublication"], $_POST["en_textToPrePublication"], $_POST["ch_textToPrePublication"], $_POST["textToPublication"], $_POST["en_textToPublication"], $_POST["ch_textToPublication"], $_POST["textToMail"], $_POST["edit_diary_id"],  $_POST["old_url"], $_POST["en_old_url"], $_POST["ch_old_url"] );

            if ( isset($_POST["send"]) ) { //send news to subscribers
                //    diarySendNews( $_POST["edit_diary_id"] );
                mailChimpSend('diary',$_POST["edit_diary_id"]);
            }

            Redirect( _getUrlToSubmit()."&save_successful=yes" );
        }

        if (isset($_GET["delete"]))
        {
            if (CONF_BACKEND_SAFEMODE) //this action is forbidden when SAFE MODE is ON
            {
                Redirect( _getUrlToDelete()."&safemode=yes" );
            }
            diaryDeleteNews($_GET["delete"]);
            removeGalleryByGPage('diary', $_GET["delete"]);
            Redirect( _getUrlToDelete() );
        }

        if(isset($_POST['saveNewsSetts'])) {
            $setts = ScanPostVariableWithId(array('main','mnsrt','sort'));

            //	print_r($setts);

            /**/	foreach($setts AS $NID => $set) {
                if(isset($set['main'])) {
                    $main = 1;
                } else {
                    $main = 0;
                }
                $main_sort = (int)$set['mnsrt'];
                $sort = (int)$set['sort'];
                db_query("UPDATE ".DB_PRFX."diary SET main='".$main."',  main_sort='".$main_sort."',  sort='".$sort."' WHERE NID=".$NID);
            }

            Redirect(_getUrlToSubmit());

        }

        $callBackParam        = array();
        $diary_posts                = array();
        $navigatorHtml = GetNavigatorHtml(ADMIN_FILE."?dpt=modules&sub=diary", 20,
            'diaryGetAllNews', $callBackParam, $diary_posts, $offset, $count );
        $smarty->assign( "navigator", $navigatorHtml );
        $smarty->assign( "diary_posts", $diary_posts );

        $smarty->hassign( "urlToSubmit", _getUrlToSubmit() );
        $smarty->hassign( "urlToDelete", _getUrlToDelete() );

        //set sub-department template
        $smarty->assign( "admin_sub_dpt", "modules_diary.tpl.html" );
    }
}
?>