<?php
#####################################
# ShopCMS: Скрипт интернет-магазина
# Copyright (c) by ADGroup
# http://shopcms.ru
#####################################
        //company module

        if (!strcmp($sub, "company"))
        {
		
if ( isset($_GET["edit"]) && isset($_POST['moveNewsTo']) && $_POST['moveCat'] != '') {
	Redirect(moveNewsTo($_GET["edit"],"company", $_POST['moveCat']));
} elseif (isset($_GET["edit"]) && isset($_POST['moveNewsTo']) && $_POST['moveCat'] =="") {
		$smarty->assign('noCat', 1);
}

        if ( CONF_BACKEND_SAFEMODE != 1 && (!isset($_SESSION["log"]) || !in_array(18,$relaccess))) //unauthorized
        {
                          $smarty->assign("admin_sub_dpt", "error_forbidden.tpl.html");
                        } else {


	if(!function_exists('_getUrlToSubmit')) {
                function _getUrlToSubmit()
                {
                        $url = ADMIN_FILE."?dpt=modules&sub=company";
                        if ( isset($_GET["offset"]) )
                                $url .= "&offset=".$_GET["offset"];
                        if ( isset($_GET["show_all"]) )
                                $url .= "&show_all=".$_GET["show_all"];
                        return $url;
                }
     }

	if(!function_exists('_getUrlToDelete')) {
                function _getUrlToDelete()
                {
                        return _getUrlToSubmit();
                }
    }

                if (isset($_GET["save_successful"])) //show successful save confirmation message
                        $smarty->assign("configuration_saved", 1);

                //current time
                $s = dtConvertToStandartForm( get_current_time() );
                $smarty->assign( "current_date", $s );

                if ( isset($_POST["company_save"]) )
                {
                        if (CONF_BACKEND_SAFEMODE) //this action is forbidden when SAFE MODE is ON
                        {
                                Redirect( _getUrlToSubmit()."&safemode=yes" );
                        }

                        $picture = "";
                    $news_file = '';
                    //сохранение файла
                    $news_file = isset($_FILES['news_file']) ? $_FILES['news_file'] : '';
                    $randf = mt_rand();

                    $uploaddir = '/data/images/company/';
                    $uploaddirTh = '/data/images/company/middle/';

                    //echo '<pre>'; print_r($_FILES); echo '</pre>'; die();
                    foreach( $_FILES as $file ){
                        $news_file = $randf . $news_file['name'];

                        $imageinfo = getimagesize($file['tmp_name']);
                        $mime = $imageinfo['mime'];
                        //createphoto($_FILES['photos']['tmp_name'][$name],$uploaddir.$image_name,$mime);
                        createMiddle($file['tmp_name'],$_SERVER['DOCUMENT_ROOT'] . $uploaddirTh.$news_file,$mime);

                        if( move_uploaded_file( $file['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . $uploaddir . $news_file ) ){
                            $files[] = realpath( $uploaddir . $news_file );
                        }

                        else{
                            $error = true;
                        }
                    }

                        $NID = companyAddNews($_POST["add_date"], $_POST["title"], $_POST["en_title"], $_POST["ch_title"], $_POST["textToPrePublication"], $_POST["en_textToPrePublication"], $_POST["ch_textToPrePublication"],
                                        $_POST["textToPublication"],
                                        $_POST["en_textToPublication"],
                                        $_POST["ch_textToPublication"], $_POST["textToMail"], $_POST["old_url"], $_POST["en_old_url"], $_POST["ch_old_url"], $news_file );
										
						
						if ( isset($_POST["send"]) ) { //send news to subscribers
                            //    companySendNews( $NID );
								
							mailChimpSend('company',$NID);
						}					
								
                        Redirect( _getUrlToSubmit()."&save_successful=yes" );
                }

                if ( isset($_GET["edit"]) )
                {
				$galleries = getGalleries('company_'.$_GET["edit"]);
                $smarty->assign( "galleries", $galleries);
                $edit_company = companyGetNewsToEdit($_GET["edit"]);
                $edit_company["textToPrePublication"] = html_spchars($edit_company["textToPrePublication"]);
                $edit_company["textToPublication"] = html_spchars($edit_company["textToPublication"]);
                $smarty->assign( "edit_company", $edit_company );
                $smarty->assign( "edit_company_id", (int)$_GET["edit"]);
                $smarty->assign( "company_editor", 1);
                $smarty->assign( "showGallery", 1);
                $smarty->assign( "pageType", 'company');
                $smarty->assign( "pageID", $_GET["edit"]);
                }

                if ( isset($_GET["add_company"]) )
                {
                $smarty->assign( "company_editor", 1);
                $smarty->assign( "showGallery", 1);
                $smarty->assign( "pageType", 'company');
                $smarty->assign( "pageID", 0);
                }

                if ( isset($_POST["update_company"]) )
                {
                        if (CONF_BACKEND_SAFEMODE) //this action is forbidden when SAFE MODE is ON
                        {
                                Redirect( _getUrlToSubmit()."&safemode=yes" );
                        }

                    $news_file = '';
                    //сохранение файла
                    $news_file = isset($_FILES['news_file']) ? $_FILES['news_file'] : '';
                    $randf = mt_rand();

                    $uploaddir = '/data/images/company/';
                    $uploaddirTh = '/data/images/company/middle/';

                    //echo '<pre>'; print_r($_FILES); echo '</pre>'; die();
                    foreach( $_FILES as $file ){
                        $news_file = $randf . $news_file['name'];

                        $imageinfo = getimagesize($file['tmp_name']);
                        $mime = $imageinfo['mime'];
                        //createphoto($_FILES['photos']['tmp_name'][$name],$uploaddir.$image_name,$mime);
                        createMiddle($file['tmp_name'],$_SERVER['DOCUMENT_ROOT'] . $uploaddirTh.$news_file,$mime);

                        if( move_uploaded_file( $file['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . $uploaddir . $news_file ) ){
                            $files[] = realpath( $uploaddir . $news_file );
                        }

                        else{
                            $error = true;
                        }
                    }

                        companyUpdateNews($_POST["add_date"], $_POST["title"], $_POST["en_title"], $_POST["ch_title"], $_POST["textToPrePublication"], $_POST["en_textToPrePublication"], $_POST["ch_textToPrePublication"], $_POST["textToPublication"], $_POST["en_textToPublication"], $_POST["ch_textToPublication"], $_POST["textToMail"], $_POST["edit_company_id"],  $_POST["old_url"], $_POST["en_old_url"], $_POST["ch_old_url"], $news_file );
						
						
						if ( isset($_POST["send"]) ) { //send news to subscribers
                            //    companySendNews($_POST["edit_company_id"]);
							mailChimpSend('company',$_POST["edit_company_id"]);
						}					
						
                        Redirect( _getUrlToSubmit()."&save_successful=yes" );
                }

                if (isset($_GET["delete"]))
                {
                        if (CONF_BACKEND_SAFEMODE) //this action is forbidden when SAFE MODE is ON
                        {
                                Redirect( _getUrlToDelete()."&safemode=yes" );
                        }
                        companyDeleteNews($_GET["delete"]);
						removeGalleryByGPage('blog', $_GET["delete"]);
                        Redirect( _getUrlToDelete() );
                }

				if(isset($_POST['saveNewsSetts'])) {
					$setts = ScanPostVariableWithId(array('main','mnsrt','sort'));
					
				//	print_r($setts);
					
				/**/	foreach($setts AS $NID => $set) {
						if(isset($set['main'])) {
							$main = 1;
						} else {
							$main = 0;
						}
						$main_sort = (int)$set['mnsrt'];
						$sort = (int)$set['sort'];
						db_query("UPDATE ".DB_PRFX."company SET main='".$main."',  main_sort='".$main_sort."',  sort='".$sort."' WHERE NID=".$NID);
					}
					
					Redirect(_getUrlToSubmit());
					
				}

                $callBackParam        = array();
                $company_posts                = array();
                $navigatorHtml = GetNavigatorHtml(ADMIN_FILE."?dpt=modules&sub=company", 20,
                                                'companyGetAllNews', $callBackParam, $company_posts, $offset, $count );
                $smarty->assign( "navigator", $navigatorHtml );
                $smarty->assign( "company_posts", $company_posts );

                $smarty->hassign( "urlToSubmit", _getUrlToSubmit() );
                $smarty->hassign( "urlToDelete", _getUrlToDelete() );

                //set sub-department template
                $smarty->assign( "admin_sub_dpt", "modules_company.tpl.html" );
        }
        }
?>