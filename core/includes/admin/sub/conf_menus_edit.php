<?php
#####################################
# ShopCMS: Скрипт интернет-магазина
# Copyright (c) by ADGroup
# http://shopcms.ru
#####################################


if(isset($_POST['uploadPrice'])) {
	$uploaddir = "price/";
	$uploadfile = $uploaddir . translit(stripslashes(preg_replace('/ {2,}/', '_',  $_FILES['price']['name'])));
		
	if (move_uploaded_file($_FILES['price']['tmp_name'], $uploadfile)) {
		$newFile = 1;
		$q = db_query("select pPath FROM ".DB_PRFX."price WHERE pID=1");
		if ($r = db_fetch_row($q)) {
			$newFile = 0;
			unlink($r['pPath']);
		}
		if($newFile == 1) {
			db_query("INSERT INTO ".DB_PRFX."price (`pID`, `pPath`, `pName`, `en_pName`, `ch_pName`) VALUES (1, '".$uploadfile."', '".$_POST['pName']."', '".$_POST['en_pName']."', '".$_POST['ch_pName']."')");
		} else {
			db_query("UPDATE ".DB_PRFX."price SET  `pPath`='".$uploadfile."', `pName`='".$_POST['pName']."', `en_pName`='".$_POST['en_pName']."', `ch_pName`='".$_POST['ch_pName']."' WHERE `pID`=1");
		}
	} else {
		echo "Возможная атака с помощью файловой загрузки!\n";
		die();
	}
	Redirect($_SERVER['REQUEST_URI']);
}



function getLinkByType($type, $typePoint, $lang) {
	if($lang != '') {
		$lang = "/".$lang;
	} 
	if($type == 'free') {
		if ($typePoint[0] == '/') {
			$link = $lang.$typePoint;
		} else {
			$link = $lang.'/'.$typePoint;
		}
		
	} elseif($type == 'aux') {
		$q = db_query("select alias FROM ".AUX_PAGES_TABLE." WHERE aux_page_ID=".(int)$typePoint."");
		if ($r = db_fetch_row($q)) {
			$link = $lang."/page/".$r[0].".html";
		}
	} elseif($type == 'cats') {
		$link = m_Cat((int)$typePoint);
		$link = $lang."/".$link;
		
	}
	
	return $link;
}

  
if (isset($_POST['removePoint'])) {
	db_query("DELETE FROM ".DB_PRFX."menu_points WHERE mpID=".(int)$_POST['removePoint']);
	echo "1";
	die();
}

if (!strcmp($sub, "menus_edit")) {
   	
		if (isset($_POST['newPoint'])) {
			(isset($_POST['newOn'])) ? $active = 1: $active = 0;
			
			$anchor = $_POST['newAnchor'];
			$type = $_POST['newType'];
			if(isset($_POST['langM']) && $_POST['langM'] != "") {
				$lang = $_POST['langM'];
			} else {
				$lang = '';
			}
			$type_point = $_POST['newTypeL'];
			$link = getLinkByType($type, $type_point, $lang);
			$sort = $_POST['newSort'];

			db_query("INSERT INTO `".DB_PRFX."menu_points` (`active`, `anchor`, `type`, `type_point`, `link`, `lang`, `sort`, `mID`) VALUES ($active, '$anchor', '$type', '$type_point', '$link', '$lang', $sort, ".$_POST['newPoint'].")"); 
			Redirect('/admin.php?dpt=conf&sub=menus_edit');
		}
		
		if(isset($_POST['savePoint'])) {
			$postData = ScanPostVariableWithId( array("on","anchor","type","langM","typepoint","sort") );
			foreach ($postData as $id => $post) {
				(isset($post['on'])) ? $active = 1: $active = 0;
				$anchor = $post['anchor'];
				$type = $post['type'];
				
			if(isset($post['langM']) && $post['langM'] != "") {
				$lang = $post['langM'];
			} else {
				$lang = '';
			}
				$type_point = $post['typepoint'];
				$link = getLinkByType($type, $type_point, $lang);
				$sort = $post['sort'];
				//echo $active.' - '.$anchor.' - '.$type.' - '.$type_point.' - '.$link.' - '.$sort.'<br>';
				db_query("UPDATE `".DB_PRFX."menu_points` SET `active`=$active, `anchor`='$anchor', `type`='$type', `type_point`='$type_point', `link`='$link', `lang`='$lang', `sort`=$sort  WHERE mpID=".(int)$id); 
			}
			Redirect('/admin.php?dpt=conf&sub=menus_edit');
		}
			
		$cats = catGetCategoryCListMin();
		$smarty->assign( "cats", $cats );
		   
		$aux_pages = auxpgGetAllPageAttributes();
        $smarty->assign( "aux_pages", $aux_pages );
		
		
    //    $smarty->assign( "sys_pages", $sys_pages );
		
		
		$menu_points = array();
		$menu_pointsL = array();
		$q = db_query("select * from ".DB_PRFX."menu_points ORDER BY sort");
		while ( $row = db_fetch_row($q)) { 
			if($row['mID'] == 1) {
				$menu_points[] = $row;
			} elseif ($row['mID'] == 2) {
				$menu_pointsL[] = $row;
			}
		}
		
        $smarty->assign( "menu_points", $menu_points );
        $smarty->assign( "menu_pointsL", $menu_pointsL );
		
		$mPrice = array();
		$q = db_query("select * FROM ".DB_PRFX."price WHERE pID=1");
		if ($r = db_fetch_row($q)) {
			if(!file_exists($r['pPath'])) {
				$r['pPath'] = '';
			}
			$mPrice = $r;
		}
		
        $smarty->assign( "mPrice", $mPrice );
		
        $smarty->assign( "edit", 1 );
       
		$smarty->assign("admin_sub_dpt", "conf_menus_edit.tpl.html");
		
}
           
?>