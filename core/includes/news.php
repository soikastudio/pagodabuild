<?php
#####################################
# ShopCMS: Скрипт интернет-магазина
# Copyright (c) by ADGroup
# http://shopcms.ru
#####################################


$news_array = newsGetNewsToCustomer();

//echo '<pre>'; print_r($news_array); echo '</pre>'; die();

$news_array_mob = newsGetNewsToCustomerMob();

$news_item_to_blog = newsGetNewsItemToBlog();

$smarty->assign("news_array", $news_array);
$smarty->assign("news_item_to_blog", $news_item_to_blog);
$smarty->assign("news_array_mob", $news_array_mob);

if (isset($_POST["subscribe"])) {
  $error = subscrVerifyEmailAddress($_POST["email"]);
  if ($_POST["modesubs"] == 0) {
    if ($error == "") {
      if (_subscriberIsSubscribed($_POST["email"])) {
        subscrUnsubscribeSubscriberByEmail2($_POST["email"]);
        $smarty->assign("un_pol", 1);
      } else {
        $smarty->assign("un_pol", 2);
      }
    } else
      $smarty->assign("error_message", $error);
  } else {
    if ($error == "") {
      $smarty->assign("subscribe", 1);
      subscrAddUnRegisteredCustomerEmail($_POST["email"]);
    } else
      $smarty->assign("error_message", $error);
  }
  $smarty->assign("main_content_template", "subscribe.tpl.html");
}

if (isset($_POST["email"]))
  $smarty->hassign("email_to_subscribe", $_POST["email"]);
else
  $smarty->assign("email_to_subscribe", "Email");

if (isset($_GET["news"])) {
  function _getUrlToNavigateNews()
  {
    //   $url = "blog/";
    $url = "/blog/";
    return $url;
  }

  function newsSearchNewsByTemplate($callBackParam, &$count_row, $navigatorParams = null)
  {
//----------------->
    global $langPref;
    if ($navigatorParams != null) {
      $offset = xEscSQL($navigatorParams["offset"]);
      $CountRowOnPage = xEscSQL($navigatorParams["CountRowOnPage"]);
    } else {
      $offset = 0;
      $CountRowOnPage = 0;
    }

    $order_by_clause = " order by add_date DESC";

    $sqlQueryCount = "select count(*) from " . NEWS_TABLE . " WHERE " . $langPref . "title !='' ";
    $q = db_query($sqlQueryCount);
    $news_count = db_fetch_row($q);
    $news_count = $news_count[0];
    $limit_clause = (!$CountRowOnPage) ? "" : " LIMIT " . $offset . "," . $CountRowOnPage;
//----------------->
    $sqlQuery = "select NID, add_date, title, en_title, ch_title, textToPrePublication, en_textToPrePublication, ch_textToPrePublication, alias from " . NEWS_TABLE . " WHERE " . $langPref . "title !='' " .
      $order_by_clause . $limit_clause;

    $q = db_query($sqlQuery);
    $result = array();
    $i = 0;

    if ($offset >= 0 && $offset <= $news_count) {
      while ($row = db_fetch_row($q)) {
        if (($i < $offset + $CountRowOnPage) ||
          $navigatorParams == null
        ) {
//----------------->							
          if (trim($row[$langPref . 'title']) != "") {


            $row["title"] = $row[$langPref . 'title'];
            $row["add_date"] = dtConvertToStandartForm($row["add_date"]);
            $row["textToPrePublication"] = preg_replace("/##gallery_[0-9]+##/", '', $row[$langPref . "textToPrePublication"]);

            $result[] = $row;
          }
        }
        $i++;
      }
    }
    $count_row = $news_count;
    return $result;
  }

  $callBackParam = array();
  $pre_news_array = array();

  $count = 0;
  $navigatorHtml = GetNavigatorHtml(
    _getUrlToNavigateNews(), CONF_NEWS_COUNT_IN_NEWS_PAGE,
    'newsSearchNewsByTemplate', $callBackParam,
    $pre_news_array, $offset, $count);
  $navigatorHtml = strtr($navigatorHtml, array("&amp;offset=0" => ""));
  $navigatorHtml = strtr($navigatorHtml, array("&amp;offset=" => ""));
  $navigatorHtml = strtr($navigatorHtml, array("&amp;show_all=yes" => "all/"));
  $navigatorHtml = strtr($navigatorHtml, array("//" => "/"));

  $smarty->assign("news_navigator", $navigatorHtml);
  $smarty->assign("pre_news_array", $pre_news_array);
  $smarty->assign("pre_news_array_counter", count($pre_news_array));
  $smarty->assign("main_content_template", "show_news.tpl.html");
}

if (isset($_GET["fullnews"])) {

  $fullnews_array = newsGetFullNewsToCustomer($_GET["fullnews"]);

  if ($fullnews_array) {
    $smarty->assign("news_full_array", $fullnews_array);
    $smarty->assign("main_content_template", "show_full_news.tpl.html");
  } else {
    header("HTTP/1.0 404 Not Found");
    header("HTTP/1.1 404 Not Found");
    header("Status: 404 Not Found");
    die(ERROR_404_HTML);
  }

}
?>