<?php


  //$main_projects

	$pcPortAdd = array();
	$qP = db_query("SELECT * FROM ".DB_PRFX."pc_china");
	while($row=db_fetch_row($qP)) {
		$pcPortAdd[] =  $row;
	}
	
	$pcFinishingAdd = array();
	$qF = db_query("SELECT * FROM ".DB_PRFX."pc_finishing");
	while($row=db_fetch_row($qF)) {
		$pcFinishingAdd[] = $row;
	}
	
	
	$pcMatAdd = array();
	$qM = db_query("SELECT * FROM ".DB_PRFX."pc_materials");
	while($row=db_fetch_row($qM)) {
		$pcMatAdd[] = $row;
	}
	
	$pcCityAdd = array();
	$qR = db_query("SELECT * FROM ".DB_PRFX."pc_russia");
	while($row=db_fetch_row($qR)) {
		$pcCityAdd[] = $row;
	}
	
	
	$smarty->assign('pcPort', $pcPortAdd);
	$smarty->assign('pcFinish', $pcFinishingAdd);
	$smarty->assign('pcMat', $pcMatAdd);
	$smarty->assign('pcCity', $pcCityAdd);
 
 
$main_projects = projectMainProjects();
$smarty->assign("main_projects", $main_projects);
//echo '<pre>'; print_r($main_projects); echo '</pre>'; die();


$reviewsTrain = array();
$q = db_query("select * from ".DB_PRFX."reviews WHERE rType='train' OR  rType='trainV' ORDER BY rID DESC");
while ( $row = db_fetch_row($q)) { 
	if(trim($row[$langPref.'rText'])!= '' || trim($row[$langPref.'rName'])!= '' || trim($row[$langPref.'rCompany'])!= '') {
		$row['rText'] = str_replace("\n","<br>",$row[$langPref.'rText']);
		$row['rName'] = $row[$langPref.'rName'];
		$row['rCompany'] =  $row[$langPref.'rCompany'];
		$reviewsTrain[] = $row;
	}
}
$smarty->assign( "reviewsTrain", $reviewsTrain );
	
$reviewsComp = array();
$q = db_query("select * from ".DB_PRFX."reviews WHERE rType='comp' OR  rType='compV' ORDER BY rID DESC");
while ( $row = db_fetch_row($q)) { 
	if(trim($row[$langPref.'rText'])!= '' || trim($row[$langPref.'rName'])!= '' || trim($row[$langPref.'rCompany'])!= '') {
		$row['rText'] = str_replace("\n","<br>",$row[$langPref.'rText']);
		$row['rName'] = $row[$langPref.'rName'];
		$row['rCompany'] =  $row[$langPref.'rCompany'];
		$reviewsComp[] = $row;
	}
}
$smarty->assign( "reviewsComp", $reviewsComp );




$whrL = '';
if($langN != '') {
	$whrL = " lang='".$langN."' ";
} else {
	$whrL = " lang='' ";
}
$menu_points = array();
$menu_pointsL = array();
	$qT = db_query("select COUNT(*) from ".DB_PRFX."menu_points where ".$whrL." ORDER BY sort");
	if ($rt = db_fetch_row($qT)) {
		$cntMP =  $rt[0];
	}
	
	if($cntMP == 0) {
		$whrL = '';
	}
	
		$q = db_query("select * from ".DB_PRFX."menu_points where  ".$whrL." ORDER BY sort");
		while ( $row = db_fetch_row($q)) {

      if ($row['active'] == 1) {    
        if($row['mID'] == 1) {
          $menu_points[] = $row;
        } elseif ($row['mID'] == 2) {
          $menu_pointsL[] = $row;
        }
      }
		}
		
        $smarty->assign( "menu_points", $menu_points );
        $smarty->assign( "menu_pointsL", $menu_pointsL );
		
		$mPrice = array();
		$q = db_query("select * FROM ".DB_PRFX."price WHERE pID=1");
		if ($r = db_fetch_row($q)) {
			$r['pName'] = $r[$langPref."pName"];
			if(trim($r['pName']) != '' && file_exists($r['pPath'])) {
				$mPrice = $r;
			}
		}
		
        $smarty->assign( "mPrice", $mPrice );






        //special offers

        $result = array();

        $q = db_query("select s.productID, s.categoryID, s.name, s.Price, s.brief_description, s.product_code,
        s.default_picture, s.enabled, s.alias, b.productID, t.filename FROM ".SPECIAL_OFFERS_TABLE."
        AS b INNER JOIN ".PRODUCTS_TABLE." AS s on (b.productID=s.productID) INNER JOIN ".PRODUCT_PICTURES." AS
        t on (s.default_picture=t.photoID AND s.productID=t.productID) WHERE s.enabled=1 order by b.sort_order");

        while ($row = db_fetch_row($q))
        {
              if (strlen($row["filename"])>0 && file_exists( "data/small/".$row["filename"])){
                                        $row["default_picture"] = "small/".$row["filename"];
                                        $row["cena"] = $row[3];
                                        $row["Price"] = show_price($row[3]);
										$row["url"] = m_Prod($row);
                                        $result[] = $row;
              }

        }

        $smarty->assign("special_offers",$result);


        $cifra = 8; //количество последних товаров для выбора
        $result = array();

        $q = db_query("select s.productID, s.categoryID, s.name, s.Price, s.enabled, s.alias, t.filename FROM ".PRODUCTS_TABLE." AS s LEFT JOIN ".PRODUCT_PICTURES."
        AS t on (s.default_picture=t.photoID AND s.productID=t.productID) WHERE s.categoryID!=1 AND s.enabled=1 ORDER BY s.date_added DESC LIMIT 0,".$cifra);

        while ($row = db_fetch_row($q))
        {
              if (strlen($row["filename"])>0 && file_exists( "data/small/".$row["filename"])){
                                        $row["filename"] = "small/".$row["filename"];
                                        $row["cena"] = $row["Price"];
                                        $row["Price"] = show_price($row["Price"]);
										$row["url"] = m_Prod($row);
                                        $result[] = $row;

              }else{
                                        $row["filename"] = "empty.gif";
                                        $row["cena"] = $row["Price"];
                                        $row["Price"] = show_price($row["Price"]);
										$row["url"] = m_Prod($row);
                                        $result[] = $row;
              }
        }
        $smarty->assign("new_products", $result);


        $cifra = 8; //количество последних товаров для выбора
        $result = array();

        $q = db_query("select s.productID, s.categoryID, s.name, s.Price, s.enabled, s.alias, t.filename FROM ".PRODUCTS_TABLE." AS s LEFT JOIN ".PRODUCT_PICTURES."
        AS t on (s.default_picture=t.photoID AND s.productID=t.productID) WHERE s.categoryID!=1 AND s.enabled=1 ORDER BY s.items_sold DESC LIMIT 0,".$cifra);

        while ($row = db_fetch_row($q))
        {
              if (strlen($row["filename"])>0 && file_exists( "data/small/".$row["filename"])){
                                        $row["filename"] = "small/".$row["filename"];
                                        $row["cena"] = $row["Price"];
                                        $row["Price"] = show_price($row["Price"]);
										$row["url"] = m_Prod($row);
                                        $result[] = $row;

              }else{
                                        $row["filename"] = "empty.gif";
                                        $row["cena"] = $row["Price"];
                                        $row["Price"] = show_price($row["Price"]);
										$row["url"] = m_Prod($row);
                                        $result[] = $row;
              }
        }
        $smarty->assign("popular_products", $result);


/*
        $result = array();
        $q = db_query("select productID FROM ".PRODUCTS_TABLE." WHERE categoryID!=1 AND enabled=1");
        while ($row = db_fetch_row($q))$result[] = $row[0];
        $q = db_query("select s.productID, s.categoryID, s.name, s.Price, s.enabled, s.alias, t.filename FROM ".PRODUCTS_TABLE." AS s LEFT JOIN ".PRODUCT_PICTURES."
        AS t on (s.default_picture=t.photoID AND s.productID=t.productID) WHERE s.productID=".$result[rand(0, count($result)-1)]);
        $result = array();
        $row = db_fetch_row($q);

              if (strlen($row["filename"])>0 && file_exists( "data/small/".$row["filename"])){
                                        $row["filename"] = "small/".$row["filename"];
                                        $row["cena"] = $row["Price"];
                                        $row["Price"] = show_price($row["Price"]);
										$row["url"] = m_Prod($row);
                                        $result[] = $row;

              }else{
                                        $row["filename"] = "empty.gif";
                                        $row["cena"] = $row["Price"];
                                        $row["Price"] = show_price($row["Price"]);
										$row["url"] = m_Prod($row);
                                        $result[] = $row;
              }

        $smarty->assign("rand_product", $result[0]);
*/
	
	$q = db_query( "
	(SELECT * FROM ".NEWS_TABLE." WHERE main=1 )
	UNION
	(SELECT * FROM ".DB_PRFX."diary WHERE main=1)
	UNION
	(SELECT * FROM ".DB_PRFX."company WHERE main=1)
	ORDER BY main_sort ASC, add_date DESC
	");
    $news_home = array();
	
	$i=0;
        while( $r=db_fetch_row($q) )
        { 
			if(trim($r[$langPref.'title']) != "") {
				
			$r['title'] = $r[$langPref.'title'];
               $r["add_date"]=dtConvertToStandartForm($r["add_date"]);
			   $tmText = preg_replace("/##gallery_[0-9]+##/",'',$r[$langPref."textToPrePublication"]);
			   $r["textToPrePublication"] = $tmText;
			 
			 switch ($r['typeN']) {
				case 'news':
					$r['link'] = $langRev."blog/".$r['alias'].".html";
					break;
				case 'comp':
					$r['link'] = $langRev."company/".$r['alias'].".html";
					break;
				case 'diary':
					$r['link'] = $langRev."diary/".$r['alias'].".html";
					break;
			 }
			  
               $news_home[] = $r;
			  $i++;
			 if($i == CONF_NEWS_COUNT_IN_CUSTOMER_PART) {
				break;
			 }
			}
        }

	
        $smarty->assign( "news_home", $news_home );



?>