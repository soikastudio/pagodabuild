<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * Smarty {assign_debug_info_quicky} function plugin
 *
 *
 * Type:     function<br>
 * Name:     assign_debug_info<br>
 * Purpose:  assign debug info to the template<br>
 * @param array unused in this plugin
 * @param Smarty
 */
 
 function smarty_function_assign_debug_info_quicky($params, $smarty){
	$assigned_vars = $smarty->_tpl_vars;
	$vars = $smarty->_tpl_vars;
	ksort($vars);
	$var_info = array();
	foreach ($vars as $k => &$v){
		$var_info[$k] = array(
			'value' => $v,
			'trace' => false,
			'name' => $k
		);
	}
	$smarty->assign("_debug_info", array('var'=>$var_info));
}
?>
